<p>
<s>
Haná	Haná	k1gFnSc1	Haná
též	též	k9	též
Hanácko	Hanácko	k1gNnSc4	Hanácko
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Hanna	Hanna	k1gFnSc1	Hanna
nebo	nebo	k8xC	nebo
Hanakei	Hanakei	k1gNnSc1	Hanakei
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
etnografická	etnografický	k2eAgFnSc1d1	etnografická
oblast	oblast	k1gFnSc1	oblast
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hornomoravského	hornomoravský	k2eAgInSc2d1	hornomoravský
úvalu	úval	k1gInSc2	úval
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c4	na
území	území	k1gNnSc4	území
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
Holešov	Holešov	k1gInSc1	Holešov
a	a	k8xC	a
Litovel	Litovel	k1gFnSc1	Litovel
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
centra	centrum	k1gNnPc1	centrum
Hané	Haná	k1gFnSc2	Haná
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
především	především	k9	především
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
okolo	okolo	k7c2	okolo
řek	řeka	k1gFnPc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
Bečvy	Bečva	k1gFnSc2	Bečva
a	a	k8xC	a
Hané	Haná	k1gFnSc2	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
(	(	kIx(	(
<g/>
okresy	okres	k1gInPc1	okres
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Prostějov	Prostějov	k1gInSc1	Prostějov
a	a	k8xC	a
Přerov	Přerov	k1gInSc1	Přerov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zlínského	zlínský	k2eAgMnSc4d1	zlínský
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc7d1	známá
vysokou	vysoký	k2eAgFnSc7d1	vysoká
úrodností	úrodnost	k1gFnSc7	úrodnost
<g/>
,	,	kIx,	,
kroji	kroj	k1gInPc7	kroj
a	a	k8xC	a
lidovými	lidový	k2eAgInPc7d1	lidový
zvyky	zvyk	k1gInPc7	zvyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haná	Haná	k1gFnSc1	Haná
dala	dát	k5eAaPmAgFnS	dát
název	název	k1gInSc4	název
širší	široký	k2eAgInSc4d2	širší
skupině	skupina	k1gFnSc3	skupina
nářečí	nářečí	k1gNnSc2	nářečí
(	(	kIx(	(
<g/>
nářečí	nářečí	k1gNnSc2	nářečí
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
<g/>
,	,	kIx,	,
hanáčtina	hanáčtina	k1gFnSc1	hanáčtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
označovaných	označovaný	k2eAgFnPc2d1	označovaná
přesněji	přesně	k6eAd2	přesně
jako	jako	k9	jako
středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
<g/>
.	.	kIx.	.
</s>
<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nářečí	nářečí	k1gNnSc1	nářečí
z	z	k7c2	z
dřívější	dřívější	k2eAgFnSc2d1	dřívější
doby	doba	k1gFnSc2	doba
je	být	k5eAaImIp3nS	být
uchováno	uchován	k2eAgNnSc1d1	uchováno
i	i	k9	i
v	v	k7c6	v
tištěné	tištěný	k2eAgFnSc6d1	tištěná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Hanácké	hanácký	k2eAgFnPc1d1	Hanácká
zpěvohry	zpěvohra	k1gFnPc1	zpěvohra
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
hanácké	hanácký	k2eAgFnSc2d1	Hanácká
opery	opera	k1gFnSc2	opera
<g/>
)	)	kIx)	)
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
výbor	výbor	k1gInSc1	výbor
Copak	copak	k9	copak
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
ta	ten	k3xDgFnSc1	ten
mozeka	mozeka	k1gFnSc1	mozeka
hraje	hrát	k5eAaImIp3nS	hrát
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Ignác	Ignác	k1gMnSc1	Ignác
Plumlovský	plumlovský	k2eAgMnSc1d1	plumlovský
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Mauritius	Mauritius	k1gInSc4	Mauritius
Bulín	Bulín	k1gMnSc1	Bulín
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
připravil	připravit	k5eAaPmAgMnS	připravit
Eduard	Eduard	k1gMnSc1	Eduard
Petrů	Petrů	k1gMnSc1	Petrů
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc4	název
získala	získat	k5eAaPmAgFnS	získat
tato	tento	k3xDgFnSc1	tento
národopisná	národopisný	k2eAgFnSc1d1	národopisná
oblast	oblast	k1gFnSc1	oblast
po	po	k7c6	po
řece	řeka	k1gFnSc6	řeka
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
protéká	protékat	k5eAaImIp3nS	protékat
jižním	jižní	k2eAgInSc7d1	jižní
okrajem	okraj	k1gInSc7	okraj
regionu	region	k1gInSc2	region
a	a	k8xC	a
vlévá	vlévat	k5eAaImIp3nS	vlévat
se	se	k3xPyFc4	se
do	do	k7c2	do
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c7	mezi
Kojetínem	Kojetín	k1gInSc7	Kojetín
a	a	k8xC	a
Kroměříží	Kroměříž	k1gFnSc7	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgNnSc2	tento
slova	slovo	k1gNnSc2	slovo
označujícího	označující	k2eAgNnSc2d1	označující
nejen	nejen	k6eAd1	nejen
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zdejší	zdejší	k2eAgMnPc4d1	zdejší
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
nesprávně	správně	k6eNd1	správně
přikládán	přikládat	k5eAaImNgInS	přikládat
historicky	historicky	k6eAd1	historicky
neprokázanému	prokázaný	k2eNgMnSc3d1	neprokázaný
knížeti	kníže	k1gMnSc3	kníže
Hanovi	Hana	k1gMnSc3	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnPc1	pojmenování
řeky	řeka	k1gFnSc2	řeka
Hané	Haná	k1gFnSc2	Haná
je	být	k5eAaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doloženo	doložen	k2eAgNnSc1d1	doloženo
již	již	k6eAd1	již
v	v	k7c6	v
pramenech	pramen	k1gInPc6	pramen
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
i	i	k9	i
bádání	bádání	k1gNnSc2	bádání
topografa	topograf	k1gMnSc2	topograf
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
Schwoye	Schwoy	k1gMnSc2	Schwoy
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgNnSc2	tento
hydronyma	hydronymum	k1gNnSc2	hydronymum
nebyl	být	k5eNaImAgInS	být
dosud	dosud	k6eAd1	dosud
spolehlivě	spolehlivě	k6eAd1	spolehlivě
prokázán	prokázat	k5eAaPmNgInS	prokázat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
etymologové	etymolog	k1gMnPc1	etymolog
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
předslovanský	předslovanský	k2eAgInSc4d1	předslovanský
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
<g/>
Zřejmě	zřejmě	k6eAd1	zřejmě
první	první	k4xOgNnPc1	první
označení	označení	k1gNnPc2	označení
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
jako	jako	k8xS	jako
Hanáků	Hanák	k1gMnPc2	Hanák
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
Gramatika	gramatik	k1gMnSc2	gramatik
česká	český	k2eAgFnSc1d1	Česká
Jana	Jana	k1gFnSc1	Jana
Blahoslava	Blahoslava	k1gFnSc1	Blahoslava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1571	[number]	k4	1571
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
doklady	doklad	k1gInPc1	doklad
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
v	v	k7c6	v
urbářích	urbář	k1gInPc6	urbář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dochovaly	dochovat	k5eAaPmAgInP	dochovat
četné	četný	k2eAgInPc1d1	četný
záznamy	záznam	k1gInPc1	záznam
o	o	k7c4	o
příjmení	příjmení	k1gNnSc4	příjmení
Hanák	Hanák	k1gMnSc1	Hanák
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
Na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
označil	označit	k5eAaPmAgMnS	označit
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
Prostějovem	Prostějov	k1gInSc7	Prostějov
<g/>
,	,	kIx,	,
Vyškovem	Vyškov	k1gInSc7	Vyškov
a	a	k8xC	a
Kroměříží	Kroměříž	k1gFnSc7	Kroměříž
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
mapě	mapa	k1gFnSc6	mapa
Moravy	Morava	k1gFnSc2	Morava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
i	i	k8xC	i
Jan	Jan	k1gMnSc1	Jan
Ámos	Ámos	k1gMnSc1	Ámos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografické	geografický	k2eAgNnSc4d1	geografické
vymezení	vymezení	k1gNnSc4	vymezení
oblasti	oblast	k1gFnSc2	oblast
==	==	k?	==
</s>
</p>
<p>
<s>
Hranice	hranice	k1gFnSc1	hranice
Hané	Haná	k1gFnSc2	Haná
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
určují	určovat	k5eAaImIp3nP	určovat
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
Komenského	Komenského	k2eAgNnSc1d1	Komenského
označení	označení	k1gNnSc1	označení
regionu	region	k1gInSc2	region
Na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
obtížně	obtížně	k6eAd1	obtížně
vymezitelnou	vymezitelný	k2eAgFnSc4d1	vymezitelná
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
Vyškovem	Vyškov	k1gInSc7	Vyškov
<g/>
,	,	kIx,	,
Prostějovem	Prostějov	k1gInSc7	Prostějov
a	a	k8xC	a
Kroměříží	Kroměříž	k1gFnSc7	Kroměříž
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
osou	osa	k1gFnSc7	osa
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
samotná	samotný	k2eAgFnSc1d1	samotná
řeka	řeka	k1gFnSc1	řeka
Haná	Haná	k1gFnSc1	Haná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobové	dobový	k2eAgFnSc6d1	dobová
literatuře	literatura	k1gFnSc6	literatura
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
již	již	k6eAd1	již
oblast	oblast	k1gFnSc1	oblast
Hané	Haná	k1gFnSc2	Haná
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
mnohem	mnohem	k6eAd1	mnohem
rozsáhlejší	rozsáhlý	k2eAgFnSc4d2	rozsáhlejší
plochu	plocha	k1gFnSc4	plocha
centrální	centrální	k2eAgFnSc2d1	centrální
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Hané	Haná	k1gFnSc6	Haná
byly	být	k5eAaImAgFnP	být
připojovány	připojován	k2eAgFnPc4d1	připojována
oblasti	oblast	k1gFnPc4	oblast
okolo	okolo	k7c2	okolo
Tovačova	Tovačův	k2eAgInSc2d1	Tovačův
<g/>
,	,	kIx,	,
Kojetína	Kojetín	k1gInSc2	Kojetín
či	či	k8xC	či
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Hané	Haná	k1gFnSc2	Haná
vymezil	vymezit	k5eAaPmAgMnS	vymezit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
Jurende	Jurend	k1gInSc5	Jurend
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
za	za	k7c4	za
severní	severní	k2eAgFnSc4d1	severní
hranici	hranice	k1gFnSc4	hranice
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
považoval	považovat	k5eAaImAgMnS	považovat
města	město	k1gNnPc1	město
Litovel	Litovel	k1gFnSc1	Litovel
<g/>
,	,	kIx,	,
Uničov	Uničov	k1gInSc1	Uničov
a	a	k8xC	a
Šternberk	Šternberk	k1gInSc1	Šternberk
<g/>
,	,	kIx,	,
za	za	k7c4	za
východní	východní	k2eAgNnPc4d1	východní
města	město	k1gNnPc4	město
Velká	velký	k2eAgFnSc1d1	velká
Bystřice	Bystřice	k1gFnSc1	Bystřice
<g/>
,	,	kIx,	,
Lipník	Lipník	k1gInSc1	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
Bystřice	Bystřice	k1gFnSc1	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
a	a	k8xC	a
Holešov	Holešov	k1gInSc1	Holešov
<g/>
,	,	kIx,	,
za	za	k7c2	za
jižní	jižní	k2eAgFnSc2d1	jižní
obce	obec	k1gFnSc2	obec
Napajedla	napajedlo	k1gNnSc2	napajedlo
<g/>
,	,	kIx,	,
Střílky	Střílek	k1gMnPc4	Střílek
a	a	k8xC	a
Bučovice	Bučovice	k1gFnPc4	Bučovice
a	a	k8xC	a
za	za	k7c4	za
západní	západní	k2eAgFnSc4d1	západní
hranici	hranice	k1gFnSc4	hranice
obce	obec	k1gFnSc2	obec
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
,	,	kIx,	,
Plumlov	Plumlov	k1gInSc1	Plumlov
a	a	k8xC	a
Pozořice	Pozořice	k1gFnSc1	Pozořice
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
šířeji	šířej	k1gMnSc3	šířej
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
oblast	oblast	k1gFnSc1	oblast
Hané	Haná	k1gFnSc2	Haná
anonymní	anonymní	k2eAgFnSc1d1	anonymní
Mappa	Mappa	k1gFnSc1	Mappa
geographica	geographica	k1gFnSc1	geographica
specialis	specialis	k1gFnSc1	specialis
terre	terr	k1gInSc5	terr
Promissae	Promissae	k1gNnPc1	Promissae
vulgo	vulgo	k?	vulgo
sacrae	sacra	k1gMnSc2	sacra
Hannae	Hanna	k1gMnSc2	Hanna
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c4	za
jejíhož	jejíž	k3xOyRp3gMnSc4	jejíž
autora	autor	k1gMnSc4	autor
bývá	bývat	k5eAaImIp3nS	bývat
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgMnSc1d1	považován
kojetínský	kojetínský	k2eAgMnSc1d1	kojetínský
učitel	učitel	k1gMnSc1	učitel
Jan	Jan	k1gMnSc1	Jan
Tomáš	Tomáš	k1gMnSc1	Tomáš
Kuzník	Kuzník	k1gMnSc1	Kuzník
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mapa	mapa	k1gFnSc1	mapa
zachycující	zachycující	k2eAgNnSc4d1	zachycující
rozšíření	rozšíření	k1gNnSc4	rozšíření
hanáckého	hanácký	k2eAgNnSc2d1	Hanácké
nářečí	nářečí	k1gNnSc2	nářečí
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
územím	území	k1gNnSc7	území
centrální	centrální	k2eAgFnSc2d1	centrální
Hané	Haná	k1gFnSc2	Haná
i	i	k8xC	i
oblasti	oblast	k1gFnSc2	oblast
západní	západní	k2eAgFnSc2d1	západní
či	či	k8xC	či
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
tak	tak	k8xC	tak
bývá	bývat	k5eAaImIp3nS	bývat
jako	jako	k9	jako
Haná	Haná	k1gFnSc1	Haná
označována	označován	k2eAgFnSc1d1	označována
nížina	nížina	k1gFnSc1	nížina
mezi	mezi	k7c7	mezi
Bludovem	Bludovo	k1gNnSc7	Bludovo
a	a	k8xC	a
Napajedly	Napajedla	k1gNnPc7	Napajedla
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
region	region	k1gInSc4	region
zahrnující	zahrnující	k2eAgInSc4d1	zahrnující
rovinatou	rovinatý	k2eAgFnSc4d1	rovinatá
oblast	oblast	k1gFnSc4	oblast
mezi	mezi	k7c7	mezi
Litovlí	Litovel	k1gFnSc7	Litovel
<g/>
,	,	kIx,	,
Šternberkem	Šternberk	k1gInSc7	Šternberk
<g/>
,	,	kIx,	,
Lipníkem	Lipník	k1gInSc7	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
<g/>
,	,	kIx,	,
Holešovem	Holešov	k1gInSc7	Holešov
<g/>
,	,	kIx,	,
Napajedly	Napajedla	k1gNnPc7	Napajedla
<g/>
,	,	kIx,	,
Vyškovem	Vyškov	k1gInSc7	Vyškov
a	a	k8xC	a
Prostějovem	Prostějov	k1gInSc7	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
nížinu	nížina	k1gFnSc4	nížina
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgInPc4d1	nacházející
se	se	k3xPyFc4	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
ohraničují	ohraničovat	k5eAaImIp3nP	ohraničovat
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
Oderské	oderský	k2eAgNnSc1d1	oderské
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Hostýnské	hostýnský	k2eAgInPc4d1	hostýnský
vrchy	vrch	k1gInPc4	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslnou	pomyslný	k2eAgFnSc4d1	pomyslná
jižní	jižní	k2eAgFnSc4d1	jižní
hranici	hranice	k1gFnSc4	hranice
Hané	Haná	k1gFnSc2	Haná
tvoří	tvořit	k5eAaImIp3nP	tvořit
Litenčické	Litenčický	k2eAgInPc4d1	Litenčický
vrchy	vrch	k1gInPc4	vrch
a	a	k8xC	a
Chřiby	Chřiby	k1gInPc4	Chřiby
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
oblast	oblast	k1gFnSc4	oblast
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
kopce	kopec	k1gInPc4	kopec
Drahanské	Drahanský	k2eAgFnSc2d1	Drahanská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Osu	osa	k1gFnSc4	osa
Hané	Haná	k1gFnSc2	Haná
představuje	představovat	k5eAaImIp3nS	představovat
řeka	řeka	k1gFnSc1	řeka
Morava	Morava	k1gFnSc1	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Hanou	Haná	k1gFnSc4	Haná
typické	typický	k2eAgNnSc1d1	typické
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
"	"	kIx"	"
<g/>
expanze	expanze	k1gFnSc2	expanze
<g/>
"	"	kIx"	"
hanáckých	hanácký	k2eAgInPc2d1	hanácký
zvyků	zvyk	k1gInPc2	zvyk
i	i	k8xC	i
nářečí	nářečí	k1gNnSc2	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
hraniční	hraniční	k2eAgFnPc4d1	hraniční
oblasti	oblast	k1gFnPc4	oblast
jako	jako	k8xS	jako
například	například	k6eAd1	například
Malá	malý	k2eAgFnSc1d1	malá
Haná	Haná	k1gFnSc1	Haná
<g/>
,	,	kIx,	,
Záhoří	Záhoří	k1gNnSc1	Záhoří
či	či	k8xC	či
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
Slovácko	Slovácko	k1gNnSc1	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jinak	jinak	k6eAd1	jinak
územně	územně	k6eAd1	územně
nesouvisející	související	k2eNgFnSc1d1	nesouvisející
oblast	oblast	k1gFnSc1	oblast
Osoblažska	Osoblažsk	k1gInSc2	Osoblažsk
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
rovinatost	rovinatost	k1gFnSc1	rovinatost
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Slezskou	slezský	k2eAgFnSc7d1	Slezská
Hanou	Hana	k1gFnSc7	Hana
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřesněji	přesně	k6eAd3	přesně
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vymezit	vymezit	k5eAaPmF	vymezit
východní	východní	k2eAgFnPc4d1	východní
hranice	hranice	k1gFnPc4	hranice
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tento	tento	k3xDgInSc1	tento
nížinatý	nížinatý	k2eAgInSc1d1	nížinatý
region	region	k1gInSc1	region
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
hornatou	hornatý	k2eAgFnSc7d1	hornatá
oblastí	oblast	k1gFnSc7	oblast
Valašska	Valašsko	k1gNnSc2	Valašsko
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
problém	problém	k1gInSc1	problém
však	však	k9	však
představuje	představovat	k5eAaImIp3nS	představovat
rozlišení	rozlišení	k1gNnSc1	rozlišení
mezi	mezi	k7c7	mezi
Hanou	Hana	k1gFnSc7	Hana
a	a	k8xC	a
Slováckem	Slovácko	k1gNnSc7	Slovácko
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
mezi	mezi	k7c7	mezi
Hanou	Hana	k1gFnSc7	Hana
a	a	k8xC	a
oblastmi	oblast	k1gFnPc7	oblast
Horácka	Horácko	k1gNnSc2	Horácko
a	a	k8xC	a
Podhorácka	Podhorácko	k1gNnSc2	Podhorácko
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Haná	Haná	k1gFnSc1	Haná
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
etnografických	etnografický	k2eAgInPc2d1	etnografický
regionů	region	k1gInPc2	region
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Hané	Haná	k1gFnSc2	Haná
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
přináleží	přináležet	k5eAaImIp3nS	přináležet
Zlínskému	zlínský	k2eAgInSc3d1	zlínský
kraji	kraj	k1gInSc3	kraj
a	a	k8xC	a
Vyškovsko	Vyškovsko	k1gNnSc4	Vyškovsko
tvořící	tvořící	k2eAgNnSc4d1	tvořící
jižní	jižní	k2eAgInSc4d1	jižní
okraj	okraj	k1gInSc4	okraj
regionu	region	k1gInSc2	region
pak	pak	k6eAd1	pak
k	k	k7c3	k
Jihomoravskému	jihomoravský	k2eAgInSc3d1	jihomoravský
kraji	kraj	k1gInSc3	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Hané	Haná	k1gFnSc2	Haná
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Hornomoravském	hornomoravský	k2eAgInSc6d1	hornomoravský
úvalu	úval	k1gInSc6	úval
tvoří	tvořit	k5eAaImIp3nS	tvořit
mírně	mírně	k6eAd1	mírně
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
nížina	nížina	k1gFnSc1	nížina
kolem	kolem	k7c2	kolem
horního	horní	k2eAgInSc2d1	horní
toku	tok	k1gInSc2	tok
řeky	řeka	k1gFnSc2	řeka
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
osu	osa	k1gFnSc4	osa
celého	celý	k2eAgInSc2d1	celý
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Moravy	Morava	k1gFnSc2	Morava
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgFnPc7d3	veliký
hanáckými	hanácký	k2eAgFnPc7d1	Hanácká
řekami	řeka	k1gFnPc7	řeka
její	její	k3xOp3gNnSc1	její
přítoky	přítok	k1gInPc1	přítok
<g/>
,	,	kIx,	,
Haná	Haná	k1gFnSc1	Haná
a	a	k8xC	a
Bečva	Bečva	k1gFnSc1	Bečva
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
bod	bod	k1gInSc4	bod
regionu	region	k1gInSc2	region
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
vrch	vrch	k1gInSc1	vrch
Velký	velký	k2eAgInSc1d1	velký
Kosíř	kosíř	k1gInSc1	kosíř
(	(	kIx(	(
<g/>
442	[number]	k4	442
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
označovaný	označovaný	k2eAgInSc1d1	označovaný
jako	jako	k8xS	jako
hanácký	hanácký	k2eAgMnSc1d1	hanácký
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Hané	Haná	k1gFnSc2	Haná
zabírají	zabírat	k5eAaImIp3nP	zabírat
rozlehlá	rozlehlý	k2eAgNnPc4d1	rozlehlé
pole	pole	k1gNnPc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
roztroušené	roztroušený	k2eAgInPc1d1	roztroušený
lesní	lesní	k2eAgInPc1d1	lesní
porosty	porost	k1gInPc1	porost
jsou	být	k5eAaImIp3nP	být
nevelké	velký	k2eNgInPc1d1	nevelký
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
je	on	k3xPp3gInPc4	on
především	především	k6eAd1	především
listnaté	listnatý	k2eAgInPc4d1	listnatý
stromy	strom	k1gInPc4	strom
jako	jako	k8xC	jako
habry	habr	k1gInPc4	habr
<g/>
,	,	kIx,	,
duby	dub	k1gInPc4	dub
<g/>
,	,	kIx,	,
lípy	lípa	k1gFnPc4	lípa
a	a	k8xC	a
olše	olše	k1gFnPc4	olše
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
představují	představovat	k5eAaImIp3nP	představovat
dochované	dochovaný	k2eAgInPc1d1	dochovaný
lužní	lužní	k2eAgInPc1d1	lužní
lesy	les	k1gInPc1	les
okolo	okolo	k7c2	okolo
toku	tok	k1gInSc2	tok
Moravy	Morava	k1gFnSc2	Morava
(	(	kIx(	(
<g/>
CHKO	CHKO	kA	CHKO
Litovelské	litovelský	k2eAgNnSc1d1	Litovelské
Pomoraví	Pomoraví	k1gNnSc1	Pomoraví
<g/>
)	)	kIx)	)
a	a	k8xC	a
lesy	les	k1gInPc1	les
rozkládající	rozkládající	k2eAgInPc1d1	rozkládající
se	se	k3xPyFc4	se
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
okolními	okolní	k2eAgFnPc7d1	okolní
vrchovinami	vrchovina	k1gFnPc7	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
vytvořených	vytvořený	k2eAgFnPc2d1	vytvořená
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Rybníky	rybník	k1gInPc1	rybník
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
zakládány	zakládat	k5eAaImNgFnP	zakládat
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
(	(	kIx(	(
<g/>
Tovačov	Tovačov	k1gInSc1	Tovačov
<g/>
,	,	kIx,	,
Záhlinice	Záhlinice	k1gFnSc1	Záhlinice
či	či	k8xC	či
Chropyně	Chropyně	k1gFnSc1	Chropyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgMnPc1d1	zdejší
jezera	jezero	k1gNnSc2	jezero
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těžby	těžba	k1gFnSc2	těžba
štěrkopísku	štěrkopísek	k1gInSc2	štěrkopísek
(	(	kIx(	(
<g/>
Tovačov	Tovačov	k1gInSc1	Tovačov
<g/>
,	,	kIx,	,
Náklo	Náklo	k1gNnSc1	Náklo
či	či	k8xC	či
Chomoutov	Chomoutov	k1gInSc1	Chomoutov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgFnPc4d3	nejstarší
české	český	k2eAgFnPc4d1	Česká
přehradní	přehradní	k2eAgFnPc4d1	přehradní
nádrže	nádrž	k1gFnPc4	nádrž
patří	patřit	k5eAaImIp3nS	patřit
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Plumlov	Plumlov	k1gInSc1	Plumlov
<g/>
.	.	kIx.	.
<g/>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
úrodné	úrodný	k2eAgFnSc3d1	úrodná
půdě	půda	k1gFnSc3	půda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
odedávna	odedávna	k6eAd1	odedávna
obdělávána	obděláván	k2eAgFnSc1d1	obdělávána
<g/>
,	,	kIx,	,
proslula	proslout	k5eAaPmAgFnS	proslout
Haná	Haná	k1gFnSc1	Haná
jako	jako	k8xC	jako
bohatý	bohatý	k2eAgInSc1d1	bohatý
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
region	region	k1gInSc1	region
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zdejší	zdejší	k2eAgFnPc4d1	zdejší
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
plodiny	plodina	k1gFnPc4	plodina
patří	patřit	k5eAaImIp3nS	patřit
pšenice	pšenice	k1gFnSc1	pšenice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
hanácky	hanácky	k6eAd1	hanácky
žito	žít	k5eAaImNgNnS	žít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
(	(	kIx(	(
<g/>
hanácky	hanácky	k6eAd1	hanácky
rež	rež	k1gFnSc1	rež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
řepa	řepa	k1gFnSc1	řepa
<g/>
,	,	kIx,	,
řepka	řepka	k1gFnSc1	řepka
olejná	olejný	k2eAgFnSc1d1	olejná
či	či	k8xC	či
chmel	chmel	k1gInSc1	chmel
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgFnPc4d1	silná
pozice	pozice	k1gFnPc4	pozice
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dodnes	dodnes	k6eAd1	dodnes
zastává	zastávat	k5eAaImIp3nS	zastávat
zemědělství	zemědělství	k1gNnSc4	zemědělství
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
též	též	k6eAd1	též
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Hanáckou	hanácký	k2eAgFnSc7d1	Hanácká
specialitou	specialita	k1gFnSc7	specialita
jsou	být	k5eAaImIp3nP	být
olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
v	v	k7c6	v
Lošticích	Lošticí	k2eAgFnPc6d1	Lošticí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
jsou	být	k5eAaImIp3nP	být
koncentrována	koncentrovat	k5eAaBmNgNnP	koncentrovat
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
či	či	k8xC	či
Uničově	Uničův	k2eAgFnSc3d1	Uničova
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
nebo	nebo	k8xC	nebo
Chropyni	Chropyně	k1gFnSc6	Chropyně
a	a	k8xC	a
oděvní	oděvní	k2eAgInSc1d1	oděvní
průmysl	průmysl	k1gInSc1	průmysl
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
v	v	k7c6	v
regionu	region	k1gInSc6	region
začíná	začínat	k5eAaImIp3nS	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
i	i	k9	i
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
těžící	těžící	k2eAgInSc1d1	těžící
z	z	k7c2	z
příhodných	příhodný	k2eAgFnPc2d1	příhodná
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
a	a	k8xC	a
potenciálu	potenciál	k1gInSc3	potenciál
zdejších	zdejší	k2eAgFnPc2d1	zdejší
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýraznější	výrazný	k2eAgInPc4d3	nejvýraznější
symboly	symbol	k1gInPc4	symbol
regionu	region	k1gInSc2	region
patří	patřit	k5eAaImIp3nP	patřit
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgInPc1d1	zdobený
kroje	kroj	k1gInPc1	kroj
<g/>
,	,	kIx,	,
venkovská	venkovský	k2eAgFnSc1d1	venkovská
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nářečí	nářečí	k1gNnSc1	nářečí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
obývající	obývající	k2eAgFnSc4d1	obývající
nížinatou	nížinatý	k2eAgFnSc4d1	nížinatá
oblast	oblast	k1gFnSc4	oblast
centrální	centrální	k2eAgFnSc2d1	centrální
Moravy	Morava	k1gFnSc2	Morava
představují	představovat	k5eAaImIp3nP	představovat
nejstarší	starý	k2eAgFnSc4d3	nejstarší
zformovanou	zformovaný	k2eAgFnSc4d1	zformovaná
etnografickou	etnografický	k2eAgFnSc4d1	etnografická
skupinu	skupina	k1gFnSc4	skupina
moravského	moravský	k2eAgNnSc2d1	Moravské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
etnografické	etnografický	k2eAgNnSc1d1	etnografické
povědomí	povědomí	k1gNnSc1	povědomí
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
již	již	k6eAd1	již
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obyvatelích	obyvatel	k1gMnPc6	obyvatel
Hané	Haná	k1gFnSc2	Haná
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
úrodnosti	úrodnost	k1gFnSc3	úrodnost
jejich	jejich	k3xOp3gInPc4	jejich
kraje	kraj	k1gInPc4	kraj
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisejícímu	související	k2eAgInSc3d1	související
snadnějšímu	snadný	k2eAgInSc3d2	snadnější
životnímu	životní	k2eAgInSc3d1	životní
stylu	styl	k1gInSc3	styl
vytvářen	vytvářen	k2eAgMnSc1d1	vytvářen
často	často	k6eAd1	často
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
stereotypní	stereotypní	k2eAgInSc4d1	stereotypní
obraz	obraz	k1gInSc4	obraz
jejich	jejich	k3xOp3gFnSc2	jejich
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
kaplan	kaplan	k1gMnSc1	kaplan
u	u	k7c2	u
sv.	sv.	kA	sv.
Mořice	Mořic	k1gMnSc2	Mořic
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
Filip	Filip	k1gMnSc1	Filip
Friebeck	Friebeck	k1gMnSc1	Friebeck
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
popisoval	popisovat	k5eAaImAgMnS	popisovat
Hanáky	Hanák	k1gMnPc4	Hanák
jako	jako	k8xC	jako
vážné	vážný	k1gMnPc4	vážný
<g/>
,	,	kIx,	,
pohodlné	pohodlný	k2eAgMnPc4d1	pohodlný
a	a	k8xC	a
zbožné	zbožný	k2eAgMnPc4d1	zbožný
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
milují	milovat	k5eAaImIp3nP	milovat
svůj	svůj	k3xOyFgInSc4	svůj
domov	domov	k1gInSc4	domov
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
neradi	nerad	k2eAgMnPc1d1	nerad
opouštějí	opouštět	k5eAaImIp3nP	opouštět
<g/>
.	.	kIx.	.
</s>
<s>
Hanácké	hanácký	k2eAgFnPc1d1	Hanácká
operety	opereta	k1gFnPc1	opereta
a	a	k8xC	a
opery	opera	k1gFnPc1	opera
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
typické	typický	k2eAgFnPc1d1	typická
hanácké	hanácký	k2eAgFnPc1d1	Hanácká
vlastnosti	vlastnost	k1gFnPc1	vlastnost
vyzdvihují	vyzdvihovat	k5eAaImIp3nP	vyzdvihovat
hlavně	hlavně	k9	hlavně
pohodlnost	pohodlnost	k1gFnSc4	pohodlnost
<g/>
,	,	kIx,	,
nerozhodnost	nerozhodnost	k1gFnSc4	nerozhodnost
a	a	k8xC	a
hrdost	hrdost	k1gFnSc4	hrdost
<g/>
.	.	kIx.	.
</s>
<s>
Dobová	dobový	k2eAgFnSc1d1	dobová
literatura	literatura	k1gFnSc1	literatura
dále	daleko	k6eAd2	daleko
připomíná	připomínat	k5eAaImIp3nS	připomínat
jejich	jejich	k3xOp3gFnSc1	jejich
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
rozvážnost	rozvážnost	k1gFnSc1	rozvážnost
a	a	k8xC	a
střízlivost	střízlivost	k1gFnSc1	střízlivost
<g/>
.	.	kIx.	.
</s>
<s>
Hrdost	hrdost	k1gFnSc1	hrdost
a	a	k8xC	a
sebevědomí	sebevědomí	k1gNnSc1	sebevědomí
bohatých	bohatý	k2eAgMnPc2d1	bohatý
Hanáků	Hanák	k1gMnPc2	Hanák
pak	pak	k6eAd1	pak
v	v	k7c6	v
hodnocení	hodnocení	k1gNnSc6	hodnocení
zvenčí	zvenčí	k6eAd1	zvenčí
často	často	k6eAd1	často
hraničilo	hraničit	k5eAaImAgNnS	hraničit
až	až	k9	až
s	s	k7c7	s
pýchou	pýcha	k1gFnSc7	pýcha
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
Hanáci	Hanák	k1gMnPc1	Hanák
hodnoceni	hodnotit	k5eAaImNgMnP	hodnotit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
lid	lid	k1gInSc1	lid
rozšafný	rozšafný	k2eAgInSc1d1	rozšafný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nerad	nerad	k2eAgMnSc1d1	nerad
se	se	k3xPyFc4	se
v	v	k7c6	v
úsudku	úsudek	k1gInSc6	úsudek
unáhlí	unáhlit	k5eAaPmIp3nS	unáhlit
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
vykládá	vykládat	k5eAaImIp3nS	vykládat
za	za	k7c4	za
váhavost	váhavost	k1gFnSc4	váhavost
i	i	k9	i
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
naopak	naopak	k6eAd1	naopak
koná	konat	k5eAaImIp3nS	konat
pravidelně	pravidelně	k6eAd1	pravidelně
velmi	velmi	k6eAd1	velmi
důkladně	důkladně	k6eAd1	důkladně
a	a	k8xC	a
s	s	k7c7	s
houževnatou	houževnatý	k2eAgFnSc7d1	houževnatá
vytrvalostí	vytrvalost	k1gFnSc7	vytrvalost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Mezi	mezi	k7c4	mezi
vlastnosti	vlastnost	k1gFnPc4	vlastnost
zdejších	zdejší	k2eAgMnPc2d1	zdejší
sedláků	sedlák	k1gMnPc2	sedlák
patřil	patřit	k5eAaImAgInS	patřit
také	také	k9	také
konzervatismus	konzervatismus	k1gInSc4	konzervatismus
a	a	k8xC	a
rezervovaný	rezervovaný	k2eAgInSc4d1	rezervovaný
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
novinkám	novinka	k1gFnPc3	novinka
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
částečně	částečně	k6eAd1	částečně
naboural	nabourat	k5eAaPmAgInS	nabourat
revoluční	revoluční	k2eAgInSc1d1	revoluční
rok	rok	k1gInSc1	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nastartoval	nastartovat	k5eAaPmAgInS	nastartovat
společenskou	společenský	k2eAgFnSc4d1	společenská
a	a	k8xC	a
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
proměnu	proměna	k1gFnSc4	proměna
zdejší	zdejší	k2eAgFnSc2d1	zdejší
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
začali	začít	k5eAaPmAgMnP	začít
přijímat	přijímat	k5eAaImF	přijímat
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
technologické	technologický	k2eAgFnPc4d1	technologická
novinky	novinka	k1gFnPc4	novinka
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
"	"	kIx"	"
<g/>
popanštění	popanštěný	k2eAgMnPc1d1	popanštěný
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
projevovalo	projevovat	k5eAaImAgNnS	projevovat
i	i	k9	i
v	v	k7c6	v
odkládáním	odkládání	k1gNnSc7	odkládání
krojů	kroj	k1gInPc2	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Povaha	povaha	k1gFnSc1	povaha
Hanáků	Hanák	k1gMnPc2	Hanák
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
předmětem	předmět	k1gInSc7	předmět
mnoha	mnoho	k4c2	mnoho
vyprávění	vyprávění	k1gNnPc2	vyprávění
a	a	k8xC	a
anekdot	anekdota	k1gFnPc2	anekdota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dělení	dělení	k1gNnSc1	dělení
Hanáků	Hanák	k1gMnPc2	Hanák
===	===	k?	===
</s>
</p>
<p>
<s>
Haná	Haná	k1gFnSc1	Haná
bývá	bývat	k5eAaImIp3nS	bývat
vnitřně	vnitřně	k6eAd1	vnitřně
dělena	dělen	k2eAgFnSc1d1	dělena
buď	buď	k8xC	buď
podle	podle	k7c2	podle
rozdílného	rozdílný	k2eAgNnSc2d1	rozdílné
nářečí	nářečí	k1gNnSc2	nářečí
či	či	k8xC	či
podle	podle	k7c2	podle
odlišností	odlišnost	k1gFnPc2	odlišnost
zdejších	zdejší	k2eAgInPc2d1	zdejší
lidových	lidový	k2eAgInPc2d1	lidový
krojů	kroj	k1gInPc2	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozlišovali	rozlišovat	k5eAaImAgMnP	rozlišovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hanáckého	hanácký	k2eAgNnSc2d1	Hanácké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
několik	několik	k4yIc1	několik
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
samotným	samotný	k2eAgMnPc3d1	samotný
Hanákům	Hanák	k1gMnPc3	Hanák
<g/>
,	,	kIx,	,
obývajícím	obývající	k2eAgMnPc3d1	obývající
oblasti	oblast	k1gFnSc6	oblast
centrální	centrální	k2eAgFnSc2d1	centrální
Hané	Haná	k1gFnSc2	Haná
mezi	mezi	k7c7	mezi
Vyškovem	Vyškov	k1gInSc7	Vyškov
<g/>
,	,	kIx,	,
Prostějovem	Prostějov	k1gInSc7	Prostějov
<g/>
,	,	kIx,	,
Olomoucí	Olomouc	k1gFnSc7	Olomouc
<g/>
,	,	kIx,	,
Přerovem	Přerov	k1gInSc7	Přerov
a	a	k8xC	a
Kojetínem	Kojetín	k1gInSc7	Kojetín
<g/>
,	,	kIx,	,
započítávali	započítávat	k5eAaImAgMnP	započítávat
i	i	k8xC	i
tzv.	tzv.	kA	tzv.
Blatňáky	Blatňák	k1gInPc1	Blatňák
(	(	kIx(	(
<g/>
též	též	k9	též
Blaťáky	Blaťák	k1gMnPc4	Blaťák
či	či	k8xC	či
Blaťany	Blaťan	k1gMnPc4	Blaťan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
obavatele	obavatel	k1gMnSc4	obavatel
žijící	žijící	k2eAgMnSc1d1	žijící
okolo	okolo	k7c2	okolo
říčky	říčka	k1gFnSc2	říčka
Blaty	Blata	k1gNnPc7	Blata
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
nářečí	nářečí	k1gNnSc2	nářečí
pak	pak	k6eAd1	pak
dále	daleko	k6eAd2	daleko
vyčleňovali	vyčleňovat	k5eAaImAgMnP	vyčleňovat
tzv.	tzv.	kA	tzv.
Čuháky	Čuhák	k1gMnPc4	Čuhák
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgInSc4d1	obývající
prostor	prostor	k1gInSc4	prostor
mezi	mezi	k7c7	mezi
Prostějovem	Prostějov	k1gInSc7	Prostějov
<g/>
,	,	kIx,	,
Olomoucí	Olomouc	k1gFnSc7	Olomouc
a	a	k8xC	a
Tovačovem	Tovačovo	k1gNnSc7	Tovačovo
<g/>
,	,	kIx,	,
Zábečáky	Zábečák	k1gMnPc7	Zábečák
(	(	kIx(	(
<g/>
též	též	k9	též
Zábečváky	Zábečvák	k1gInPc4	Zábečvák
či	či	k8xC	či
Bečváky	Bečvák	k1gInPc4	Bečvák
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hanácké	hanácký	k2eAgNnSc1d1	Hanácké
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
žijící	žijící	k2eAgFnSc1d1	žijící
okolo	okolo	k7c2	okolo
Přerova	Přerov	k1gInSc2	Přerov
a	a	k8xC	a
Holešova	Holešov	k1gInSc2	Holešov
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnPc1	řeka
Bečvy	Bečva	k1gFnPc1	Bečva
<g/>
,	,	kIx,	,
Zámoravjáky	Zámoravják	k1gInPc1	Zámoravják
(	(	kIx(	(
<g/>
též	též	k9	též
Zámoravčíky	Zámoravčík	k1gMnPc4	Zámoravčík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc1d1	obývající
oblast	oblast	k1gFnSc1	oblast
Hulínska	Hulínsko	k1gNnSc2	Hulínsko
a	a	k8xC	a
Kroměřížska	Kroměřížsek	k1gMnSc4	Kroměřížsek
a	a	k8xC	a
Podhoráky	podhorák	k1gMnPc4	podhorák
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
tzv.	tzv.	kA	tzv.
Záhoří	Záhoří	k1gNnPc4	Záhoří
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Lipníku	Lipník	k1gInSc2	Lipník
nad	nad	k7c7	nad
Bečvou	Bečva	k1gFnSc7	Bečva
a	a	k8xC	a
Bystřice	Bystřice	k1gFnSc1	Bystřice
pod	pod	k7c7	pod
Hostýnem	Hostýn	k1gInSc7	Hostýn
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
první	první	k4xOgFnSc1	první
samostatná	samostatný	k2eAgFnSc1d1	samostatná
mapa	mapa	k1gFnSc1	mapa
Hané	Haná	k1gFnSc2	Haná
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
vedle	vedle	k7c2	vedle
samotných	samotný	k2eAgMnPc2d1	samotný
"	"	kIx"	"
<g/>
pravých	pravý	k2eAgMnPc2d1	pravý
<g/>
"	"	kIx"	"
Hanáků	Hanák	k1gMnPc2	Hanák
žijících	žijící	k2eAgMnPc2d1	žijící
okolo	okolo	k7c2	okolo
toku	tok	k1gInSc2	tok
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
též	též	k9	též
další	další	k2eAgFnPc1d1	další
skupiny	skupina	k1gFnPc1	skupina
hanáckého	hanácký	k2eAgNnSc2d1	Hanácké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
podle	podle	k7c2	podle
typických	typický	k2eAgNnPc2d1	typické
povolání	povolání	k1gNnPc2	povolání
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
tzv.	tzv.	kA	tzv.
Slaměnkáře	Slaměnkář	k1gMnSc4	Slaměnkář
<g/>
,	,	kIx,	,
Kopáče	kopáč	k1gMnSc4	kopáč
<g/>
,	,	kIx,	,
Metlaře	Metlař	k1gMnSc4	Metlař
<g/>
,	,	kIx,	,
Zábečváky	Zábečvák	k1gMnPc4	Zábečvák
<g/>
,	,	kIx,	,
Tragačníky	Tragačník	k1gMnPc4	Tragačník
<g/>
,	,	kIx,	,
Lopatáře	lopatář	k1gMnPc4	lopatář
<g/>
,	,	kIx,	,
Kolomazníky	Kolomazník	k1gMnPc4	Kolomazník
a	a	k8xC	a
Hrnčíře	Hrnčíř	k1gMnPc4	Hrnčíř
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
výrazných	výrazný	k2eAgFnPc2d1	výrazná
odlišností	odlišnost	k1gFnPc2	odlišnost
v	v	k7c6	v
tradičních	tradiční	k2eAgInPc6d1	tradiční
krojích	kroj	k1gInPc6	kroj
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
dle	dle	k7c2	dle
rozdílné	rozdílný	k2eAgFnSc2d1	rozdílná
barevy	bareva	k1gFnSc2	bareva
mužských	mužský	k2eAgFnPc2d1	mužská
nohavic	nohavice	k1gFnPc2	nohavice
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
Hanáci	Hanák	k1gMnPc1	Hanák
děleni	dělit	k5eAaImNgMnP	dělit
též	též	k9	též
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
žluté	žlutý	k2eAgInPc1d1	žlutý
(	(	kIx(	(
<g/>
Žluťáky	žluťák	k1gInPc1	žluťák
<g/>
)	)	kIx)	)
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
vedle	vedle	k6eAd1	vedle
niž	jenž	k3xRgFnSc4	jenž
bývají	bývat	k5eAaImIp3nP	bývat
rozlišováni	rozlišován	k2eAgMnPc1d1	rozlišován
ještě	ještě	k6eAd1	ještě
tzv.	tzv.	kA	tzv.
Baňáci	Baňák	k1gMnPc1	Baňák
s	s	k7c7	s
baňatým	baňatý	k2eAgInSc7d1	baňatý
tvarem	tvar	k1gInSc7	tvar
nohavic	nohavice	k1gFnPc2	nohavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nářečí	nářečí	k1gNnSc2	nářečí
===	===	k?	===
</s>
</p>
<p>
<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nářečí	nářečí	k1gNnSc1	nářečí
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
moravské	moravský	k2eAgInPc4d1	moravský
dialekty	dialekt	k1gInPc4	dialekt
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
zahrnováno	zahrnován	k2eAgNnSc1d1	zahrnováno
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
středomoravské	středomoravský	k2eAgFnSc2d1	Středomoravská
nářeční	nářeční	k2eAgFnSc2d1	nářeční
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Středomoravská	středomoravský	k2eAgFnSc1d1	Středomoravská
nářeční	nářeční	k2eAgFnSc1d1	nářeční
skupina	skupina	k1gFnSc1	skupina
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
centrální	centrální	k2eAgFnSc2d1	centrální
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Moravy	Morava	k1gFnSc2	Morava
včetně	včetně	k7c2	včetně
Brněnska	Brněnsko	k1gNnSc2	Brněnsko
a	a	k8xC	a
Třebíčska	Třebíčsko	k1gNnSc2	Třebíčsko
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
dělena	dělen	k2eAgFnSc1d1	dělena
do	do	k7c2	do
několika	několik	k4yIc2	několik
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
lišících	lišící	k2eAgFnPc2d1	lišící
nářečních	nářeční	k2eAgFnPc2d1	nářeční
podskupin	podskupina	k1gFnPc2	podskupina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
centrální	centrální	k2eAgFnSc4d1	centrální
hanáčtinu	hanáčtina	k1gFnSc4	hanáčtina
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
čuhácké	čuhácký	k2eAgFnSc2d1	čuhácký
podskupiny	podskupina	k1gFnSc2	podskupina
typické	typický	k2eAgNnSc1d1	typické
zaměňování	zaměňování	k1gNnSc1	zaměňování
hlásek	hláska	k1gFnPc2	hláska
ý	ý	k?	ý
a	a	k8xC	a
ú	ú	k0	ú
respektive	respektive	k9	respektive
ej	ej	k0	ej
a	a	k8xC	a
ou	ou	k0	ou
hláskami	hláska	k1gFnPc7	hláska
é	é	k0	é
a	a	k8xC	a
ó	ó	k0	ó
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
stréc	stréc	k6eAd1	stréc
brósí	brósí	k1gNnSc1	brósí
nožék	nožéka	k1gFnPc2	nožéka
místo	místo	k1gNnSc1	místo
strýc	strýc	k1gMnSc1	strýc
brousí	brousit	k5eAaImIp3nS	brousit
nožík	nožík	k1gInSc4	nožík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlásky	hláska	k1gFnPc1	hláska
y	y	k?	y
a	a	k8xC	a
e	e	k0	e
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
dále	daleko	k6eAd2	daleko
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
širokými	široký	k2eAgFnPc7d1	široká
hláskami	hláska	k1gFnPc7	hláska
e	e	k0	e
<g/>
̬	̬	k?	̬
a	a	k8xC	a
o	o	k7c6	o
<g/>
̬	̬	k?	̬
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
výrazy	výraz	k1gInPc1	výraz
re	re	k9	re
<g/>
̬	̬	k?	̬
<g/>
ba	ba	k9	ba
a	a	k8xC	a
do	do	k7c2	do
<g/>
̬	̬	k?	̬
<g/>
b	b	k?	b
namísto	namísto	k7c2	namísto
spisovných	spisovný	k2eAgNnPc2d1	spisovné
slov	slovo	k1gNnPc2	slovo
ryba	ryba	k1gFnSc1	ryba
a	a	k8xC	a
dub	dub	k1gInSc1	dub
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
zvyky	zvyk	k1gInPc1	zvyk
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Kroje	kroj	k1gInPc1	kroj
===	===	k?	===
</s>
</p>
<p>
<s>
Hanácký	hanácký	k2eAgInSc1d1	hanácký
kroj	kroj	k1gInSc1	kroj
představuje	představovat	k5eAaImIp3nS	představovat
nejvýraznější	výrazný	k2eAgInSc1d3	nejvýraznější
projev	projev	k1gInSc1	projev
tradiční	tradiční	k2eAgFnSc2d1	tradiční
lidové	lidový	k2eAgFnSc2d1	lidová
kultury	kultura	k1gFnSc2	kultura
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
rozloze	rozloha	k1gFnSc3	rozloha
této	tento	k3xDgFnSc2	tento
národopisné	národopisný	k2eAgFnSc2d1	národopisná
oblasti	oblast	k1gFnSc2	oblast
však	však	k9	však
není	být	k5eNaImIp3nS	být
jednotný	jednotný	k2eAgInSc1d1	jednotný
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
Hané	Haná	k1gFnSc2	Haná
tak	tak	k9	tak
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
vícero	vícero	k1gNnSc4	vícero
jeho	jeho	k3xOp3gFnPc2	jeho
variant	varianta	k1gFnPc2	varianta
lišících	lišící	k2eAgFnPc2d1	lišící
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
především	především	k6eAd1	především
střihem	střih	k1gInSc7	střih
a	a	k8xC	a
barvou	barva	k1gFnSc7	barva
mužských	mužský	k2eAgFnPc2d1	mužská
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
,	,	kIx,	,
úpravou	úprava	k1gFnSc7	úprava
doplňků	doplněk	k1gInPc2	doplněk
na	na	k7c6	na
ženských	ženský	k2eAgInPc6d1	ženský
krojích	kroj	k1gInPc6	kroj
či	či	k8xC	či
úvazy	úvaz	k1gInPc4	úvaz
šátků	šátek	k1gInPc2	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
a	a	k8xC	a
proměny	proměna	k1gFnPc1	proměna
hanáckého	hanácký	k2eAgInSc2d1	hanácký
kroje	kroj	k1gInSc2	kroj
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
rekonstruovat	rekonstruovat	k5eAaBmF	rekonstruovat
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
literárních	literární	k2eAgInPc2d1	literární
a	a	k8xC	a
obrazových	obrazový	k2eAgInPc2d1	obrazový
dokladů	doklad	k1gInPc2	doklad
dokumentujících	dokumentující	k2eAgInPc2d1	dokumentující
jeho	jeho	k3xOp3gMnSc3	jeho
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Kroj	kroj	k1gInSc1	kroj
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
součástí	součást	k1gFnPc2	součást
každodenního	každodenní	k2eAgInSc2d1	každodenní
oděvu	oděv	k1gInSc2	oděv
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
roboty	robota	k1gFnSc2	robota
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
oblastech	oblast	k1gFnPc6	oblast
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
postupnému	postupný	k2eAgNnSc3d1	postupné
odložení	odložení	k1gNnSc3	odložení
kvůli	kvůli	k7c3	kvůli
hospodářským	hospodářský	k2eAgFnPc3d1	hospodářská
a	a	k8xC	a
společenským	společenský	k2eAgFnPc3d1	společenská
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
samotné	samotný	k2eAgFnSc2d1	samotná
Hané	Haná	k1gFnSc2	Haná
se	se	k3xPyFc4	se
tak	tak	k9	tak
nejprve	nejprve	k6eAd1	nejprve
stalo	stát	k5eAaPmAgNnS	stát
na	na	k7c6	na
Olomoucku	Olomoucko	k1gNnSc6	Olomoucko
a	a	k8xC	a
Prostějovsku	Prostějovsko	k1gNnSc6	Prostějovsko
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
uchovávání	uchovávání	k1gNnSc3	uchovávání
a	a	k8xC	a
nošení	nošení	k1gNnSc3	nošení
krojů	kroj	k1gInPc2	kroj
se	se	k3xPyFc4	se
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
začali	začít	k5eAaPmAgMnP	začít
vracet	vracet	k5eAaImF	vracet
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
nastupujícím	nastupující	k2eAgNnSc7d1	nastupující
folklórním	folklórní	k2eAgNnSc7d1	folklórní
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
mužského	mužský	k2eAgInSc2d1	mužský
kroje	kroj	k1gInSc2	kroj
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
konopná	konopný	k2eAgFnSc1d1	konopná
nebo	nebo	k8xC	nebo
bavlněná	bavlněný	k2eAgFnSc1d1	bavlněná
košile	košile	k1gFnSc1	košile
tenčice	tenčice	k1gFnSc2	tenčice
zdobená	zdobený	k2eAgFnSc1d1	zdobená
hnědočernou	hnědočerný	k2eAgFnSc7d1	hnědočerná
výšivkou	výšivka	k1gFnSc7	výšivka
límečku	límeček	k1gInSc2	límeček
u	u	k7c2	u
krku	krk	k1gInSc2	krk
a	a	k8xC	a
krémovou	krémový	k2eAgFnSc7d1	krémová
až	až	k8xS	až
žlutou	žlutý	k2eAgFnSc7d1	žlutá
výšivkou	výšivka	k1gFnSc7	výšivka
na	na	k7c6	na
rameni	rameno	k1gNnSc6	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Košile	košile	k1gFnSc1	košile
bývala	bývat	k5eAaImAgFnS	bývat
doplněna	doplnit	k5eAaPmNgFnS	doplnit
červenou	červený	k2eAgFnSc7d1	červená
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc7d1	černá
stužkou	stužka	k1gFnSc7	stužka
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
též	též	k9	též
černým	černý	k2eAgInSc7d1	černý
šátkem	šátek	k1gInSc7	šátek
ovázaným	ovázaný	k2eAgInSc7d1	ovázaný
okolo	okolo	k7c2	okolo
krku	krk	k1gInSc2	krk
<g/>
.	.	kIx.	.
</s>
<s>
Kalhoty	kalhoty	k1gFnPc1	kalhoty
gatě	gatě	k1gFnPc1	gatě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
původně	původně	k6eAd1	původně
sahaly	sahat	k5eAaImAgFnP	sahat
pouze	pouze	k6eAd1	pouze
po	po	k7c4	po
kolena	koleno	k1gNnPc4	koleno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvazovaly	uvazovat	k5eAaImAgFnP	uvazovat
zdobenými	zdobený	k2eAgInPc7d1	zdobený
řemínky	řemínek	k1gInPc7	řemínek
<g/>
,	,	kIx,	,
bývaly	bývat	k5eAaImAgFnP	bývat
zhotovovány	zhotovovat	k5eAaImNgFnP	zhotovovat
z	z	k7c2	z
konopného	konopný	k2eAgNnSc2d1	konopné
plátna	plátno	k1gNnSc2	plátno
nebo	nebo	k8xC	nebo
z	z	k7c2	z
ovčích	ovčí	k2eAgFnPc2d1	ovčí
či	či	k8xC	či
kozích	kozí	k2eAgFnPc2d1	kozí
kůží	kůže	k1gFnPc2	kůže
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
též	též	k9	též
z	z	k7c2	z
jelenice	jelenice	k1gFnSc2	jelenice
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
žluté	žlutý	k2eAgFnSc6d1	žlutá
barvě	barva	k1gFnSc6	barva
těchto	tento	k3xDgFnPc2	tento
kalhot	kalhoty	k1gFnPc2	kalhoty
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
hanáckého	hanácký	k2eAgInSc2d1	hanácký
kroje	kroj	k1gInSc2	kroj
staly	stát	k5eAaPmAgFnP	stát
gatě	gatě	k1gFnPc1	gatě
cihlově	cihlově	k6eAd1	cihlově
červené	červený	k2eAgFnPc1d1	červená
či	či	k8xC	či
hnědočervené	hnědočervený	k2eAgFnPc1d1	hnědočervená
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
kalhot	kalhoty	k1gFnPc2	kalhoty
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
částech	část	k1gFnPc6	část
regionu	region	k1gInSc2	region
zachovala	zachovat	k5eAaPmAgFnS	zachovat
a	a	k8xC	a
například	například	k6eAd1	například
historik	historik	k1gMnSc1	historik
Beda	Beda	k1gMnSc1	Beda
Dudík	Dudík	k1gMnSc1	Dudík
dělil	dělit	k5eAaImAgMnS	dělit
Hanáky	Hanák	k1gMnPc4	Hanák
dle	dle	k7c2	dle
barvy	barva	k1gFnSc2	barva
jejich	jejich	k3xOp3gFnPc2	jejich
kalhot	kalhoty	k1gFnPc2	kalhoty
na	na	k7c6	na
žluté	žlutý	k2eAgFnSc6d1	žlutá
a	a	k8xC	a
červené	červený	k2eAgFnSc6d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
košily	košila	k1gFnPc4	košila
bývala	bývat	k5eAaImAgFnS	bývat
nošena	nošen	k2eAgFnSc1d1	nošena
vesta	vesta	k1gFnSc1	vesta
zdobená	zdobený	k2eAgFnSc1d1	zdobená
výšivkou	výšivka	k1gFnSc7	výšivka
s	s	k7c7	s
florálními	florální	k2eAgInPc7d1	florální
a	a	k8xC	a
geometrickými	geometrický	k2eAgInPc7d1	geometrický
motivy	motiv	k1gInPc7	motiv
označováná	označováný	k2eAgFnSc1d1	označováná
jako	jako	k9	jako
frydka	frydka	k1gFnSc1	frydka
<g/>
,	,	kIx,	,
lajdík	lajdík	k1gInSc1	lajdík
nebo	nebo	k8xC	nebo
kordula	kordula	k1gFnSc1	kordula
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Hané	Haná	k1gFnSc2	Haná
se	se	k3xPyFc4	se
zeleně	zeleně	k6eAd1	zeleně
lemovaná	lemovaný	k2eAgFnSc1d1	lemovaná
vesta	vesta	k1gFnSc1	vesta
šila	šít	k5eAaImAgFnS	šít
z	z	k7c2	z
černého	černý	k2eAgNnSc2d1	černé
sukna	sukno	k1gNnSc2	sukno
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
z	z	k7c2	z
modrého	modré	k1gNnSc2	modré
lemovaného	lemovaný	k2eAgNnSc2d1	lemované
červenou	červená	k1gFnSc4	červená
nebo	nebo	k8xC	nebo
bílou	bílý	k2eAgFnSc4d1	bílá
a	a	k8xC	a
především	především	k6eAd1	především
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
vesta	vesta	k1gFnSc1	vesta
ušitá	ušitý	k2eAgFnSc1d1	ušitá
ze	z	k7c2	z
sukna	sukno	k1gNnSc2	sukno
zelené	zelený	k2eAgInPc4d1	zelený
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
hnědé	hnědý	k2eAgInPc4d1	hnědý
bravy	brav	k1gInPc4	brav
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
mužského	mužský	k2eAgInSc2d1	mužský
oděvu	oděv	k1gInSc2	oděv
byl	být	k5eAaImAgInS	být
i	i	k9	i
kabátek	kabátek	k1gInSc4	kabátek
marinka	marinka	k1gFnSc1	marinka
nebo	nebo	k8xC	nebo
po	po	k7c4	po
kolena	koleno	k1gNnPc4	koleno
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
černý	černý	k2eAgInSc4d1	černý
či	či	k8xC	či
bílý	bílý	k2eAgInSc4d1	bílý
kabát	kabát	k1gInSc4	kabát
zvaný	zvaný	k2eAgMnSc1d1	zvaný
ketla	ketla	k1gMnSc1	ketla
či	či	k8xC	či
plátynka	plátynka	k1gFnSc1	plátynka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nosili	nosit	k5eAaImAgMnP	nosit
muži	muž	k1gMnPc1	muž
bílý	bílý	k1gMnSc1	bílý
či	či	k8xC	či
žlutý	žlutý	k2eAgInSc1d1	žlutý
kožich	kožich	k1gInSc1	kožich
z	z	k7c2	z
ovčí	ovčí	k2eAgFnSc2d1	ovčí
nebo	nebo	k8xC	nebo
beraní	beraní	k2eAgFnSc2d1	beraní
kožešiny	kožešina	k1gFnSc2	kožešina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
ocáskový	ocáskový	k2eAgInSc4d1	ocáskový
kožich	kožich	k1gInSc4	kožich
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
část	část	k1gFnSc4	část
oblečení	oblečení	k1gNnSc2	oblečení
však	však	k9	však
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nahradily	nahradit	k5eAaPmAgInP	nahradit
kožichy	kožich	k1gInPc1	kožich
jiné	jiný	k2eAgInPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
hanáckého	hanácký	k2eAgInSc2d1	hanácký
mužského	mužský	k2eAgInSc2d1	mužský
kroje	kroj	k1gInSc2	kroj
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
i	i	k9	i
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
řasený	řasený	k2eAgInSc1d1	řasený
plášť	plášť	k1gInSc1	plášť
z	z	k7c2	z
tmavomodrého	tmavomodrý	k2eAgNnSc2d1	tmavomodré
až	až	k8xS	až
černého	černý	k2eAgNnSc2d1	černé
sukna	sukno	k1gNnSc2	sukno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
Hané	Haná	k1gFnSc2	Haná
byly	být	k5eAaImAgFnP	být
typické	typický	k2eAgFnPc1d1	typická
vysoké	vysoký	k2eAgFnPc1d1	vysoká
černé	černý	k2eAgFnPc1d1	černá
kožené	kožený	k2eAgFnPc1d1	kožená
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
mezi	mezi	k7c7	mezi
botou	bota	k1gFnSc7	bota
a	a	k8xC	a
kalhotami	kalhoty	k1gFnPc7	kalhoty
měli	mít	k5eAaImAgMnP	mít
muži	muž	k1gMnPc1	muž
navlečené	navlečený	k2eAgFnSc2d1	navlečená
plátěné	plátěný	k2eAgFnSc2d1	plátěná
punčochy	punčocha	k1gFnSc2	punčocha
tzv.	tzv.	kA	tzv.
vyléčky	vyléčky	k6eAd1	vyléčky
či	či	k8xC	či
podléčky	podléčka	k1gFnPc1	podléčka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
uvazovaly	uvazovat	k5eAaImAgFnP	uvazovat
nad	nad	k7c7	nad
koleny	koleno	k1gNnPc7	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
muži	muž	k1gMnPc1	muž
nosívali	nosívat	k5eAaImAgMnP	nosívat
černý	černý	k2eAgInSc4d1	černý
klobouk	klobouk	k1gInSc4	klobouk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
výzdoba	výzdoba	k1gFnSc1	výzdoba
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgFnP	měnit
dle	dle	k7c2	dle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
konkrétního	konkrétní	k2eAgMnSc2d1	konkrétní
nositele	nositel	k1gMnSc2	nositel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
pak	pak	k6eAd1	pak
muži	muž	k1gMnPc1	muž
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nosili	nosit	k5eAaImAgMnP	nosit
na	na	k7c6	na
hlavách	hlava	k1gFnPc6	hlava
kožešinové	kožešinový	k2eAgFnSc2d1	kožešinová
čepice	čepice	k1gFnSc2	čepice
aksamitky	aksamitka	k1gFnSc2	aksamitka
či	či	k8xC	či
sobolovice	sobolovice	k1gFnSc2	sobolovice
<g/>
.	.	kIx.	.
<g/>
Spodní	spodní	k2eAgFnSc7d1	spodní
částí	část	k1gFnSc7	část
ženského	ženský	k2eAgInSc2d1	ženský
kroje	kroj	k1gInSc2	kroj
tvořila	tvořit	k5eAaImAgFnS	tvořit
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
suknice	suknice	k1gFnSc1	suknice
z	z	k7c2	z
konopného	konopný	k2eAgNnSc2d1	konopné
plátna	plátno	k1gNnSc2	plátno
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
trháčena	trháčen	k2eAgFnSc1d1	trháčen
<g/>
,	,	kIx,	,
oplíčena	oplíčen	k2eAgFnSc1d1	oplíčen
<g/>
,	,	kIx,	,
spodnica	spodnic	k2eAgFnSc1d1	spodnic
<g/>
,	,	kIx,	,
podolek	podolek	k1gInSc1	podolek
<g/>
,	,	kIx,	,
ščórák	ščórák	k1gInSc1	ščórák
<g/>
,	,	kIx,	,
opasnice	opasnice	k1gFnSc1	opasnice
či	či	k8xC	či
rubanda	rubanda	k1gFnSc1	rubanda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
do	do	k7c2	do
půli	půle	k1gFnSc6	půle
zad	záda	k1gNnPc2	záda
oblékalo	oblékat	k5eAaImAgNnS	oblékat
vyšívané	vyšívaný	k2eAgNnSc1d1	vyšívané
oplečí	oplečí	k1gNnSc1	oplečí
známé	známý	k2eAgNnSc1d1	známé
též	též	k9	též
jako	jako	k8xS	jako
oplíčko	oplíčko	k1gNnSc4	oplíčko
a	a	k8xC	a
lněné	lněný	k2eAgInPc4d1	lněný
rukávce	rukávec	k1gInPc4	rukávec
zdobené	zdobený	k2eAgInPc4d1	zdobený
vloženými	vložený	k2eAgInPc7d1	vložený
díly	díl	k1gInPc7	díl
na	na	k7c6	na
ramenou	rameno	k1gNnPc6	rameno
(	(	kIx(	(
<g/>
náramky	náramek	k1gInPc4	náramek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyšívanými	vyšívaný	k2eAgInPc7d1	vyšívaný
pruhy	pruh	k1gInPc7	pruh
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rukávů	rukáv	k1gInPc2	rukáv
(	(	kIx(	(
<g/>
límečky	límeček	k1gInPc1	límeček
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kanýrky	kanýrky	k?	kanýrky
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rukávů	rukáv	k1gInPc2	rukáv
(	(	kIx(	(
<g/>
tacle	tacle	k1gFnSc2	tacle
<g/>
)	)	kIx)	)
a	a	k8xC	a
výrazným	výrazný	k2eAgInSc7d1	výrazný
límcem	límec	k1gInSc7	límec
okolo	okolo	k7c2	okolo
krku	krk	k1gInSc2	krk
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
krézl	kréznout	k5eAaPmAgMnS	kréznout
<g/>
,	,	kIx,	,
obojek	obojek	k1gInSc1	obojek
nebo	nebo	k8xC	nebo
placák	placák	k1gInSc1	placák
<g/>
.	.	kIx.	.
</s>
<s>
Límec	límec	k1gInSc1	límec
i	i	k8xC	i
límečky	límeček	k1gInPc1	límeček
z	z	k7c2	z
rukávů	rukáv	k1gInPc2	rukáv
byly	být	k5eAaImAgFnP	být
odnímatelné	odnímatelný	k2eAgFnPc1d1	odnímatelná
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
nošeny	nosit	k5eAaImNgInP	nosit
jen	jen	k9	jen
při	při	k7c6	při
slavnostních	slavnostní	k2eAgFnPc6d1	slavnostní
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Široké	Široké	k2eAgNnSc1d1	Široké
okruží	okruží	k1gNnSc1	okruží
bývalo	bývat	k5eAaImAgNnS	bývat
škrobeno	škrobit	k5eAaImNgNnS	škrobit
a	a	k8xC	a
lemováno	lemovat	k5eAaImNgNnS	lemovat
vambereckou	vamberecký	k2eAgFnSc7d1	vamberecká
krajkou	krajka	k1gFnSc7	krajka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
ženského	ženský	k2eAgInSc2d1	ženský
kroje	kroj	k1gInSc2	kroj
byl	být	k5eAaImAgInS	být
také	také	k9	také
živůtek	živůtek	k1gInSc1	živůtek
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
říkalo	říkat	k5eAaImAgNnS	říkat
kordulka	kordulka	k1gFnSc1	kordulka
<g/>
,	,	kIx,	,
frydka	frydka	k1gFnSc1	frydka
<g/>
,	,	kIx,	,
lajdík	lajdík	k1gInSc1	lajdík
nebo	nebo	k8xC	nebo
bruslek	bruslek	k1gInSc1	bruslek
<g/>
.	.	kIx.	.
</s>
<s>
Všední	všední	k2eAgInPc1d1	všední
živůtky	živůtek	k1gInPc1	živůtek
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
šily	šít	k5eAaImAgFnP	šít
z	z	k7c2	z
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
barchetu	barchet	k1gInSc2	barchet
či	či	k8xC	či
kanafasu	kanafas	k1gInSc2	kanafas
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
sváteční	svátečnět	k5eAaImIp3nS	svátečnět
<g/>
,	,	kIx,	,
bohatě	bohatě	k6eAd1	bohatě
zdobené	zdobený	k2eAgFnPc1d1	zdobená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zhotovovaly	zhotovovat	k5eAaImAgFnP	zhotovovat
z	z	k7c2	z
atlasu	atlas	k1gInSc2	atlas
<g/>
,	,	kIx,	,
damašku	damašek	k1gInSc2	damašek
či	či	k8xC	či
brokátu	brokát	k1gInSc2	brokát
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
ze	z	k7c2	z
sametu	samet	k1gInSc2	samet
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
tvořila	tvořit	k5eAaImAgFnS	tvořit
základní	základní	k2eAgFnSc4d1	základní
barvu	barva	k1gFnSc4	barva
živůtku	živůtek	k1gInSc2	živůtek
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
či	či	k8xC	či
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
kroje	kroj	k1gInSc2	kroj
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
vpředu	vpředu	k6eAd1	vpředu
nesešitá	sešitý	k2eNgFnSc1d1	nesešitá
sukně	sukně	k1gFnSc1	sukně
z	z	k7c2	z
bílého	bílý	k2eAgNnSc2d1	bílé
plátna	plátno	k1gNnSc2	plátno
zvaná	zvaný	k2eAgFnSc1d1	zvaná
fěrtoch	fěrtoch	k1gInSc1	fěrtoch
či	či	k8xC	či
obarvená	obarvený	k2eAgFnSc1d1	obarvená
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
šorec	šorec	k1gInSc4	šorec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
století	století	k1gNnSc2	století
pak	pak	k6eAd1	pak
ženy	žena	k1gFnPc1	žena
nosily	nosit	k5eAaImAgFnP	nosit
sukně	sukně	k1gFnPc4	sukně
sešité	sešitý	k2eAgFnPc4d1	sešitá
<g/>
.	.	kIx.	.
</s>
<s>
Různobarevná	různobarevný	k2eAgFnSc1d1	různobarevná
zástěra	zástěra	k1gFnSc1	zástěra
zdobená	zdobený	k2eAgFnSc1d1	zdobená
výšivkami	výšivka	k1gFnPc7	výšivka
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
potištěná	potištěný	k2eAgFnSc1d1	potištěná
barevnými	barevný	k2eAgFnPc7d1	barevná
vzory	vzor	k1gInPc4	vzor
nošená	nošený	k2eAgFnSc1d1	nošená
přes	přes	k7c4	přes
spodní	spodní	k2eAgFnPc4d1	spodní
sukně	sukně	k1gFnPc4	sukně
bývala	bývat	k5eAaImAgFnS	bývat
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
fěrtušek	fěrtušek	k1gInSc1	fěrtušek
<g/>
,	,	kIx,	,
pasenec	pasenec	k1gMnSc1	pasenec
<g/>
,	,	kIx,	,
předníček	předníček	k1gMnSc1	předníček
nebo	nebo	k8xC	nebo
kasáč	kasáč	k1gMnSc1	kasáč
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ženskému	ženský	k2eAgInSc3d1	ženský
kroji	kroj	k1gInSc3	kroj
pak	pak	k6eAd1	pak
patřil	patřit	k5eAaImAgInS	patřit
i	i	k9	i
krátký	krátký	k2eAgInSc1d1	krátký
kabátek	kabátek	k1gInSc1	kabátek
s	s	k7c7	s
úzkými	úzký	k2eAgInPc7d1	úzký
rukávy	rukáv	k1gInPc7	rukáv
špenzr	špenzr	k?	špenzr
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
kožich	kožich	k1gInSc4	kožich
zvaný	zvaný	k2eAgInSc4d1	zvaný
kožoch	kožoch	k1gInSc4	kožoch
či	či	k8xC	či
kožóšek	kožóšek	k1gInSc4	kožóšek
<g/>
.	.	kIx.	.
</s>
<s>
Svobodné	svobodný	k2eAgFnPc1d1	svobodná
Hanačky	Hanačka	k1gFnPc1	Hanačka
si	se	k3xPyFc3	se
vlasy	vlas	k1gInPc1	vlas
česaly	česat	k5eAaImAgInP	česat
na	na	k7c4	na
pěšinku	pěšinka	k1gFnSc4	pěšinka
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
červenou	červený	k2eAgFnSc7d1	červená
stuhou	stuha	k1gFnSc7	stuha
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
zaplétaly	zaplétat	k5eAaImAgInP	zaplétat
do	do	k7c2	do
copu	cop	k1gInSc2	cop
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
lelík	lelík	k?	lelík
<g/>
.	.	kIx.	.
</s>
<s>
Vdané	vdaný	k2eAgFnPc1d1	vdaná
ženy	žena	k1gFnPc1	žena
si	se	k3xPyFc3	se
vlasy	vlas	k1gInPc1	vlas
česaly	česat	k5eAaImAgInP	česat
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
copů	cop	k1gInPc2	cop
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
omotávaly	omotávat	k5eAaImAgFnP	omotávat
okolo	okolo	k7c2	okolo
hlavy	hlava	k1gFnSc2	hlava
do	do	k7c2	do
věnce	věnec	k1gInSc2	věnec
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickou	charakteristický	k2eAgFnSc4d1	charakteristická
součást	součást	k1gFnSc4	součást
ženského	ženský	k2eAgInSc2d1	ženský
kroje	kroj	k1gInSc2	kroj
představují	představovat	k5eAaImIp3nP	představovat
červené	červený	k2eAgNnSc1d1	červené
lipské	lipský	k2eAgInPc1d1	lipský
šátky	šátek	k1gInPc1	šátek
rozšířené	rozšířený	k2eAgInPc1d1	rozšířený
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
Hané	Haná	k1gFnSc2	Haná
uvazovaly	uvazovat	k5eAaImAgInP	uvazovat
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
nosívaly	nosívat	k5eAaImAgFnP	nosívat
ženy	žena	k1gFnPc1	žena
kožené	kožený	k2eAgFnPc1d1	kožená
nebo	nebo	k8xC	nebo
soukenné	soukenný	k2eAgInPc1d1	soukenný
střevíce	střevíc	k1gInPc1	střevíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
doplňky	doplněk	k1gInPc7	doplněk
ženského	ženský	k2eAgInSc2d1	ženský
oděvu	oděv	k1gInSc2	oděv
patřil	patřit	k5eAaImAgInS	patřit
plátěný	plátěný	k2eAgInSc1d1	plátěný
přehoz	přehoz	k1gInSc1	přehoz
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
žena	žena	k1gFnSc1	žena
nosívala	nosívat	k5eAaImAgFnS	nosívat
přes	přes	k7c4	přes
oděv	oděv	k1gInSc4	oděv
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
opouštěla	opouštět	k5eAaImAgFnS	opouštět
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
a	a	k8xC	a
šátek	šátek	k1gInSc1	šátek
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xC	jako
kabelka	kabelka	k1gFnSc1	kabelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
===	===	k?	===
</s>
</p>
<p>
<s>
Prvky	prvek	k1gInPc1	prvek
původní	původní	k2eAgFnSc2d1	původní
hanácké	hanácký	k2eAgFnSc2d1	Hanácká
lidové	lidový	k2eAgFnSc2d1	lidová
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
sólových	sólový	k2eAgFnPc2d1	sólová
či	či	k8xC	či
ansámblových	ansámblový	k2eAgFnPc2d1	ansámblová
instrumentálních	instrumentální	k2eAgFnPc2d1	instrumentální
skladeb	skladba	k1gFnPc2	skladba
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
označovaných	označovaný	k2eAgInPc2d1	označovaný
jako	jako	k8xC	jako
hanatica	hanatica	k6eAd1	hanatica
<g/>
.	.	kIx.	.
</s>
<s>
Dochovanými	dochovaný	k2eAgFnPc7d1	dochovaná
pololidovými	pololidový	k2eAgFnPc7d1	pololidová
hudebními	hudební	k2eAgFnPc7d1	hudební
skladbami	skladba	k1gFnPc7	skladba
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
Hanácké	hanácký	k2eAgFnSc2d1	Hanácká
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dokládají	dokládat	k5eAaImIp3nP	dokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
původní	původní	k2eAgFnSc1d1	původní
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
muzika	muzika	k1gFnSc1	muzika
byla	být	k5eAaImAgFnS	být
tradičně	tradičně	k6eAd1	tradičně
hrána	hrát	k5eAaImNgFnS	hrát
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
smyčcových	smyčcový	k2eAgInPc2d1	smyčcový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníci	hudebník	k1gMnPc1	hudebník
hrávali	hrávat	k5eAaImAgMnP	hrávat
na	na	k7c4	na
housle	housle	k1gFnPc4	housle
<g/>
,	,	kIx,	,
basu	basa	k1gFnSc4	basa
případně	případně	k6eAd1	případně
cimbál	cimbál	k1gInSc1	cimbál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
později	pozdě	k6eAd2	pozdě
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
také	také	k9	také
klarinety	klarinet	k1gInPc1	klarinet
a	a	k8xC	a
flétny	flétna	k1gFnPc1	flétna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
nástup	nástup	k1gInSc1	nástup
dechových	dechový	k2eAgInPc2d1	dechový
nástrojů	nástroj	k1gInPc2	nástroj
a	a	k8xC	a
zakládání	zakládání	k1gNnSc4	zakládání
dechovek	dechovka	k1gFnPc2	dechovka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
prosadily	prosadit	k5eAaPmAgFnP	prosadit
i	i	k9	i
na	na	k7c6	na
hanáckém	hanácký	k2eAgInSc6d1	hanácký
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgInPc1d1	hudební
soubory	soubor	k1gInPc1	soubor
hrající	hrající	k2eAgInPc1d1	hrající
na	na	k7c4	na
tradiční	tradiční	k2eAgInPc4d1	tradiční
nástroje	nástroj	k1gInPc4	nástroj
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Hané	Haná	k1gFnSc2	Haná
obnovovány	obnovovat	k5eAaImNgInP	obnovovat
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
Tradiční	tradiční	k2eAgInSc1d1	tradiční
hanácký	hanácký	k2eAgInSc1d1	hanácký
tanec	tanec	k1gInSc1	tanec
představuje	představovat	k5eAaImIp3nS	představovat
třídobý	třídobý	k2eAgInSc1d1	třídobý
a	a	k8xC	a
výjimečně	výjimečně	k6eAd1	výjimečně
i	i	k9	i
dvoudobý	dvoudobý	k2eAgInSc1d1	dvoudobý
tanec	tanec	k1gInSc1	tanec
tančený	tančený	k2eAgInSc1d1	tančený
obvykle	obvykle	k6eAd1	obvykle
chlapcem	chlapec	k1gMnSc7	chlapec
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
dívkami	dívka	k1gFnPc7	dívka
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
cófavá	cófavý	k2eAgFnSc1d1	cófavý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
někdy	někdy	k6eAd1	někdy
označován	označovat	k5eAaImNgInS	označovat
též	též	k9	též
jako	jako	k8xC	jako
šópavá	šópavý	k2eAgFnSc1d1	šópavý
<g/>
,	,	kIx,	,
zpáteční	zpáteční	k2eAgFnSc1d1	zpáteční
či	či	k8xC	či
hanácká	hanácký	k2eAgFnSc1d1	Hanácká
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kráčivý	kráčivý	k2eAgInSc4d1	kráčivý
druh	druh	k1gInSc4	druh
tance	tanec	k1gInSc2	tanec
volného	volný	k2eAgNnSc2d1	volné
tempa	tempo	k1gNnSc2	tempo
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
podle	podle	k7c2	podle
základního	základní	k2eAgInSc2d1	základní
kroku	krok	k1gInSc2	krok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každé	každý	k3xTgNnSc4	každý
vykročení	vykročení	k1gNnSc4	vykročení
vpřed	vpřed	k6eAd1	vpřed
následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
kratších	krátký	k2eAgInPc2d2	kratší
kroků	krok	k1gInPc2	krok
zpátky	zpátky	k6eAd1	zpátky
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
tanci	tanec	k1gInSc6	tanec
připomínajícího	připomínající	k2eAgInSc2d1	připomínající
polonézu	polonéz	k1gInSc2	polonéz
jsou	být	k5eAaImIp3nP	být
doloženy	doložit	k5eAaPmNgInP	doložit
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Cófavá	Cófavý	k2eAgFnSc1d1	Cófavý
byla	být	k5eAaImAgFnS	být
obvykle	obvykle	k6eAd1	obvykle
tančena	tančit	k5eAaImNgFnS	tančit
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
masopustních	masopustní	k2eAgFnPc2d1	masopustní
zábav	zábava	k1gFnPc2	zábava
<g/>
,	,	kIx,	,
při	při	k7c6	při
hodech	hod	k1gInPc6	hod
<g/>
,	,	kIx,	,
svatbách	svatba	k1gFnPc6	svatba
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
náboženskými	náboženský	k2eAgInPc7d1	náboženský
texty	text	k1gInPc7	text
bývala	bývat	k5eAaImAgFnS	bývat
tancována	tancovat	k5eAaImNgFnS	tancovat
i	i	k9	i
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tradiční	tradiční	k2eAgNnSc4d1	tradiční
bydlení	bydlení	k1gNnSc4	bydlení
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
oblast	oblast	k1gFnSc4	oblast
Hané	Haná	k1gFnSc2	Haná
představuje	představovat	k5eAaImIp3nS	představovat
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
doklad	doklad	k1gInSc1	doklad
zdejší	zdejší	k2eAgFnSc2d1	zdejší
kultury	kultura	k1gFnSc2	kultura
selský	selský	k2eAgInSc1d1	selský
dům	dům	k1gInSc1	dům
–	–	k?	–
grunt	grunt	k1gInSc1	grunt
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c6	o
přízemní	přízemní	k2eAgFnSc6d1	přízemní
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
zvýšené	zvýšený	k2eAgInPc1d1	zvýšený
<g/>
,	,	kIx,	,
roubené	roubený	k2eAgInPc1d1	roubený
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
později	pozdě	k6eAd2	pozdě
nahrazovaly	nahrazovat	k5eAaImAgFnP	nahrazovat
hliněné	hliněný	k2eAgFnPc1d1	hliněná
stavby	stavba	k1gFnPc1	stavba
s	s	k7c7	s
valbovou	valbový	k2eAgFnSc7d1	valbová
střechou	střecha	k1gFnSc7	střecha
budované	budovaný	k2eAgInPc4d1	budovaný
především	především	k9	především
z	z	k7c2	z
nepálených	pálený	k2eNgFnPc2d1	nepálená
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
vesnická	vesnický	k2eAgNnPc1d1	vesnické
obydlí	obydlí	k1gNnPc1	obydlí
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
postupně	postupně	k6eAd1	postupně
přestavována	přestavovat	k5eAaImNgFnS	přestavovat
či	či	k8xC	či
budována	budovat	k5eAaImNgFnS	budovat
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
pálených	pálená	k1gFnPc2	pálená
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
nově	nově	k6eAd1	nově
budované	budovaný	k2eAgFnPc1d1	budovaná
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
typem	typ	k1gInSc7	typ
špýcharových	špýcharový	k2eAgInPc2d1	špýcharový
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zvýšené	zvýšený	k2eAgInPc1d1	zvýšený
a	a	k8xC	a
neomítnuté	omítnutý	k2eNgInPc1d1	neomítnutý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
valbové	valbový	k2eAgFnPc1d1	valbová
střechy	střecha	k1gFnPc1	střecha
kryté	krytý	k2eAgInPc4d1	krytý
došky	došek	k1gInPc4	došek
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
nahrazovaly	nahrazovat	k5eAaImAgInP	nahrazovat
střechy	střecha	k1gFnPc4	střecha
sedlové	sedlový	k2eAgNnSc4d1	sedlové
kryté	krytý	k2eAgFnSc6d1	krytá
břidlicí	břidlice	k1gFnSc7	břidlice
či	či	k8xC	či
pálenými	pálený	k2eAgFnPc7d1	pálená
střešními	střešní	k2eAgFnPc7d1	střešní
taškami	taška	k1gFnPc7	taška
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgNnSc7d1	typické
specifikem	specifikon	k1gNnSc7	specifikon
hanáckých	hanácký	k2eAgInPc2d1	hanácký
domů	dům	k1gInPc2	dům
byly	být	k5eAaImAgFnP	být
až	až	k9	až
do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tzv.	tzv.	kA	tzv.
žudry	žudr	k1gInPc1	žudr
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mohutný	mohutný	k2eAgInSc4d1	mohutný
rizalit	rizalit	k1gInSc4	rizalit
vystavěný	vystavěný	k2eAgInSc4d1	vystavěný
před	před	k7c4	před
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
patře	patro	k1gNnSc6	patro
byla	být	k5eAaImAgFnS	být
umístěna	umístěn	k2eAgFnSc1d1	umístěna
sýpka	sýpka	k1gFnSc1	sýpka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
žudrů	žudr	k1gInPc2	žudr
však	však	k9	však
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
postupně	postupně	k6eAd1	postupně
zanikala	zanikat	k5eAaImAgFnS	zanikat
a	a	k8xC	a
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
tak	tak	k9	tak
tento	tento	k3xDgInSc1	tento
stavební	stavební	k2eAgInSc1d1	stavební
prvek	prvek	k1gInSc1	prvek
na	na	k7c6	na
hanáckých	hanácký	k2eAgInPc6d1	hanácký
domech	dům	k1gInPc6	dům
zachoval	zachovat	k5eAaPmAgInS	zachovat
pouze	pouze	k6eAd1	pouze
ojediněle	ojediněle	k6eAd1	ojediněle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vesnická	vesnický	k2eAgNnPc1d1	vesnické
stavení	stavení	k1gNnPc1	stavení
byla	být	k5eAaImAgNnP	být
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
orientována	orientovat	k5eAaBmNgFnS	orientovat
okapově	okapově	k6eAd1	okapově
<g/>
.	.	kIx.	.
</s>
<s>
Štítově	štítově	k6eAd1	štítově
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
zástavba	zástavba	k1gFnSc1	zástavba
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
především	především	k9	především
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Hané	Haná	k1gFnSc2	Haná
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Zábřehu	Zábřeh	k1gInSc2	Zábřeh
<g/>
,	,	kIx,	,
Mohelnice	Mohelnice	k1gFnSc2	Mohelnice
<g/>
,	,	kIx,	,
Loštic	Loštice	k1gFnPc2	Loštice
a	a	k8xC	a
Uničova	Uničův	k2eAgFnSc1d1	Uničova
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
usedlosti	usedlost	k1gFnPc1	usedlost
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každé	každý	k3xTgFnSc2	každý
vesnice	vesnice	k1gFnSc2	vesnice
stavěny	stavěn	k2eAgFnPc1d1	stavěna
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Průčelí	průčelí	k1gNnPc1	průčelí
domů	dům	k1gInPc2	dům
byla	být	k5eAaImAgNnP	být
většinou	většinou	k6eAd1	většinou
rozdělena	rozdělen	k2eAgNnPc1d1	rozděleno
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žudr	žudr	k1gInSc1	žudr
vystavěný	vystavěný	k2eAgInSc1d1	vystavěný
uprostřed	uprostřed	k7c2	uprostřed
průčelí	průčelí	k1gNnSc2	průčelí
dělil	dělit	k5eAaImAgInS	dělit
stavení	stavení	k1gNnSc3	stavení
na	na	k7c4	na
část	část	k1gFnSc4	část
s	s	k7c7	s
obytnými	obytný	k2eAgFnPc7d1	obytná
místnostmi	místnost	k1gFnPc7	místnost
a	a	k8xC	a
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
komory	komora	k1gFnPc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předzahrádce	předzahrádka	k1gFnSc6	předzahrádka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
nacházela	nacházet	k5eAaImAgFnS	nacházet
před	před	k7c7	před
obytnou	obytný	k2eAgFnSc7d1	obytná
částí	část	k1gFnSc7	část
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vysazovány	vysazován	k2eAgFnPc4d1	vysazována
vonné	vonný	k2eAgFnPc4d1	vonná
a	a	k8xC	a
okrasné	okrasný	k2eAgFnPc4d1	okrasná
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
komorami	komora	k1gFnPc7	komora
bývaly	bývat	k5eAaImAgFnP	bývat
zasazovány	zasazovat	k5eAaImNgFnP	zasazovat
lípy	lípa	k1gFnPc1	lípa
nebo	nebo	k8xC	nebo
ovocné	ovocný	k2eAgInPc1d1	ovocný
stromy	strom	k1gInPc1	strom
jako	jako	k8xS	jako
například	například	k6eAd1	například
hrušně	hrušeň	k1gFnPc4	hrušeň
či	či	k8xC	či
ořešáky	ořešák	k1gInPc4	ořešák
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
býval	bývat	k5eAaImAgInS	bývat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vybavován	vybavován	k2eAgInSc1d1	vybavován
arkádovým	arkádový	k2eAgNnSc7d1	arkádové
zápražím	zápraží	k1gNnSc7	zápraží
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
vjíždělo	vjíždět	k5eAaImAgNnS	vjíždět
průjezdem	průjezd	k1gInSc7	průjezd
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c7	za
obytným	obytný	k2eAgNnSc7d1	obytné
stavením	stavení	k1gNnSc7	stavení
se	se	k3xPyFc4	se
rozprostíral	rozprostírat	k5eAaImAgMnS	rozprostírat
dvůr	dvůr	k1gInSc4	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
od	od	k7c2	od
sousedících	sousedící	k2eAgInPc2d1	sousedící
pozemků	pozemek	k1gInPc2	pozemek
oddělovaly	oddělovat	k5eAaImAgInP	oddělovat
chlévy	chlév	k1gInPc1	chlév
<g/>
,	,	kIx,	,
sloužící	sloužící	k1gFnSc4	sloužící
jako	jako	k8xC	jako
přístřeší	přístřeší	k1gNnSc4	přístřeší
pro	pro	k7c4	pro
chovaný	chovaný	k2eAgInSc4d1	chovaný
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
a	a	k8xC	a
kůlny	kůlna	k1gFnPc1	kůlna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývaly	bývat	k5eAaImAgInP	bývat
odstaveny	odstaven	k2eAgInPc1d1	odstaven
vozy	vůz	k1gInPc1	vůz
a	a	k8xC	a
uskladněno	uskladněn	k2eAgNnSc1d1	uskladněno
hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
nářadí	nářadí	k1gNnSc1	nářadí
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zadní	zadní	k2eAgFnSc2d1	zadní
strany	strana	k1gFnSc2	strana
pak	pak	k6eAd1	pak
většinou	většinou	k6eAd1	většinou
každý	každý	k3xTgInSc4	každý
dvůr	dvůr	k1gInSc4	dvůr
uzavírala	uzavírat	k5eAaPmAgFnS	uzavírat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
zídka	zídka	k1gFnSc1	zídka
s	s	k7c7	s
vraty	vrat	k1gInPc7	vrat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
prostor	prostor	k1gInSc4	prostor
dvora	dvůr	k1gInSc2	dvůr
oddělovala	oddělovat	k5eAaImAgFnS	oddělovat
od	od	k7c2	od
přiléhající	přiléhající	k2eAgFnSc2d1	přiléhající
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
často	často	k6eAd1	často
sloužily	sloužit	k5eAaImAgFnP	sloužit
jako	jako	k9	jako
sady	sada	k1gFnPc1	sada
<g/>
,	,	kIx,	,
bývaly	bývat	k5eAaImAgInP	bývat
vysazovány	vysazován	k2eAgInPc1d1	vysazován
ovocné	ovocný	k2eAgInPc1d1	ovocný
stromy	strom	k1gInPc1	strom
jako	jako	k8xS	jako
například	například	k6eAd1	například
jabloně	jabloň	k1gFnPc4	jabloň
<g/>
,	,	kIx,	,
hrušně	hrušeň	k1gFnPc4	hrušeň
<g/>
,	,	kIx,	,
morušovníky	morušovník	k1gInPc4	morušovník
nebo	nebo	k8xC	nebo
slivoně	slivoň	k1gFnPc4	slivoň
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
zahrady	zahrada	k1gFnSc2	zahrada
pak	pak	k6eAd1	pak
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
usedlostí	usedlost	k1gFnPc2	usedlost
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
stodola	stodola	k1gFnSc1	stodola
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaná	označovaný	k2eAgFnSc1d1	označovaná
jako	jako	k8xC	jako
humno	humno	k1gNnSc1	humno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prostor	prostor	k1gInSc4	prostor
za	za	k7c7	za
stodolou	stodola	k1gFnSc7	stodola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
oblast	oblast	k1gFnSc1	oblast
většiny	většina	k1gFnSc2	většina
vesnic	vesnice	k1gFnPc2	vesnice
přecházela	přecházet	k5eAaImAgFnS	přecházet
ve	v	k7c4	v
volnou	volný	k2eAgFnSc4d1	volná
krajinu	krajina	k1gFnSc4	krajina
s	s	k7c7	s
polními	polní	k2eAgFnPc7d1	polní
cestami	cesta	k1gFnPc7	cesta
či	či	k8xC	či
polnostmi	polnost	k1gFnPc7	polnost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
humna	humno	k1gNnSc2	humno
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
za	za	k7c7	za
humny	humna	k1gNnPc7	humna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
kuchyni	kuchyně	k1gFnSc6	kuchyně
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
zaujímaly	zaujímat	k5eAaImAgInP	zaujímat
pokrmy	pokrm	k1gInPc1	pokrm
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
z	z	k7c2	z
obilnin	obilnina	k1gFnPc2	obilnina
<g/>
,	,	kIx,	,
především	především	k9	především
kaše	kaše	k1gFnSc2	kaše
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
připravovaly	připravovat	k5eAaImAgFnP	připravovat
nejen	nejen	k6eAd1	nejen
moučné	moučný	k2eAgFnPc1d1	moučná
nebo	nebo	k8xC	nebo
krupicové	krupicový	k2eAgFnPc1d1	krupicová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
fazolové	fazolový	k2eAgInPc4d1	fazolový
či	či	k8xC	či
hrachové	hrachový	k2eAgInPc4d1	hrachový
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
kaší	kaše	k1gFnPc2	kaše
pak	pak	k6eAd1	pak
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složky	složka	k1gFnPc1	složka
potravy	potrava	k1gFnSc2	potrava
představovaly	představovat	k5eAaImAgFnP	představovat
polévky	polévka	k1gFnPc1	polévka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vařily	vařit	k5eAaImAgFnP	vařit
z	z	k7c2	z
česneku	česnek	k1gInSc2	česnek
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc2	zelí
<g/>
,	,	kIx,	,
bůčku	bůček	k1gInSc2	bůček
nebo	nebo	k8xC	nebo
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Polévka	polévka	k1gFnSc1	polévka
připravovaná	připravovaný	k2eAgFnSc1d1	připravovaná
z	z	k7c2	z
tvarůžků	tvarůžek	k1gInPc2	tvarůžek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
syrnica	syrnica	k6eAd1	syrnica
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
konzumovalo	konzumovat	k5eAaBmAgNnS	konzumovat
především	především	k9	především
po	po	k7c6	po
zabijačkách	zabijačka	k1gFnPc6	zabijačka
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
šperke	šperke	k1gFnPc2	šperke
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
nich	on	k3xPp3gInPc2	on
porážena	porážen	k2eAgNnPc1d1	poráženo
prasata	prase	k1gNnPc1	prase
<g/>
.	.	kIx.	.
</s>
<s>
Získané	získaný	k2eAgNnSc1d1	získané
maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
kvůli	kvůli	k7c3	kvůli
konzervaci	konzervace	k1gFnSc3	konzervace
udilo	udit	k5eAaImAgNnS	udit
<g/>
.	.	kIx.	.
</s>
<s>
Sváteční	sváteční	k2eAgInPc1d1	sváteční
pokrmy	pokrm	k1gInPc1	pokrm
se	se	k3xPyFc4	se
podávaly	podávat	k5eAaImAgInP	podávat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
omáčkami	omáčka	k1gFnPc7	omáčka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
rozdělovány	rozdělovat	k5eAaImNgFnP	rozdělovat
na	na	k7c4	na
černé	černý	k2eAgFnPc4d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
omáčka	omáčka	k1gFnSc1	omáčka
se	se	k3xPyFc4	se
vařila	vařit	k5eAaImAgFnS	vařit
z	z	k7c2	z
černého	černý	k2eAgNnSc2d1	černé
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
perníku	perník	k1gInSc2	perník
<g/>
,	,	kIx,	,
bezinkových	bezinkový	k2eAgNnPc2d1	bezinkové
povidel	povidla	k1gNnPc2	povidla
a	a	k8xC	a
sušených	sušený	k2eAgFnPc2d1	sušená
švestek	švestka	k1gFnPc2	švestka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
přidávaly	přidávat	k5eAaImAgFnP	přidávat
rozinky	rozinka	k1gFnPc4	rozinka
a	a	k8xC	a
ořechy	ořech	k1gInPc4	ořech
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
bílé	bílý	k2eAgFnSc2d1	bílá
omáčky	omáčka	k1gFnSc2	omáčka
tvořila	tvořit	k5eAaImAgFnS	tvořit
smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
doplněná	doplněný	k2eAgNnPc4d1	doplněné
křenem	křen	k1gInSc7	křen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
kuchyni	kuchyně	k1gFnSc3	kuchyně
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
koláče	koláč	k1gInPc1	koláč
a	a	k8xC	a
buchty	buchta	k1gFnPc1	buchta
z	z	k7c2	z
kynutého	kynutý	k2eAgNnSc2d1	kynuté
těsta	těsto	k1gNnSc2	těsto
plněné	plněný	k2eAgFnSc2d1	plněná
mákem	mák	k1gInSc7	mák
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
tradiční	tradiční	k2eAgInPc1d1	tradiční
sladké	sladký	k2eAgInPc1d1	sladký
pokrmy	pokrm	k1gInPc1	pokrm
představují	představovat	k5eAaImIp3nP	představovat
například	například	k6eAd1	například
bramborové	bramborový	k2eAgInPc1d1	bramborový
placky	placek	k1gInPc1	placek
s	s	k7c7	s
mákem	mák	k1gInSc7	mák
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
nebo	nebo	k8xC	nebo
povidly	povidla	k1gNnPc7	povidla
známé	známá	k1gFnSc2	známá
jako	jako	k8xS	jako
hertepláky	herteplák	k1gInPc4	herteplák
<g/>
,	,	kIx,	,
bramborové	bramborový	k2eAgFnPc4d1	bramborová
placičky	placička	k1gFnPc4	placička
s	s	k7c7	s
tvarohem	tvaroh	k1gInSc7	tvaroh
a	a	k8xC	a
mákem	mák	k1gInSc7	mák
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
pukáče	pukáč	k1gInPc4	pukáč
nebo	nebo	k8xC	nebo
tróč	tróč	k1gInSc1	tróč
–	–	k?	–
kynutý	kynutý	k2eAgInSc1d1	kynutý
ovocný	ovocný	k2eAgInSc1d1	ovocný
koláč	koláč	k1gInSc1	koláč
s	s	k7c7	s
drobenkou	drobenka	k1gFnSc7	drobenka
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejznámějším	známý	k2eAgInSc7d3	nejznámější
pokrmem	pokrm	k1gInSc7	pokrm
spojeným	spojený	k2eAgFnPc3d1	spojená
s	s	k7c7	s
Hanou	Hana	k1gFnSc7	Hana
jsou	být	k5eAaImIp3nP	být
Olomoucké	olomoucký	k2eAgInPc1d1	olomoucký
tvarůžky	tvarůžek	k1gInPc1	tvarůžek
známé	známá	k1gFnSc2	známá
též	též	k9	též
jako	jako	k9	jako
syrečky	syreček	k1gInPc1	syreček
nebo	nebo	k8xC	nebo
tvargle	tvargle	k1gFnPc1	tvargle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvyky	zvyk	k1gInPc4	zvyk
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Hanácké	hanácký	k2eAgNnSc4d1	Hanácké
právo	právo	k1gNnSc4	právo
====	====	k?	====
</s>
</p>
<p>
<s>
Ostatkové	ostatkový	k2eAgNnSc1d1	ostatkové
či	či	k8xC	či
též	též	k9	též
hanácké	hanácký	k2eAgNnSc1d1	Hanácké
právo	právo	k1gNnSc1	právo
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
především	především	k9	především
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
Moravě	Morava	k1gFnSc6	Morava
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
tradiční	tradiční	k2eAgInPc4d1	tradiční
zvyky	zvyk	k1gInPc4	zvyk
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
oslavami	oslava	k1gFnPc7	oslava
masopustu	masopust	k1gInSc2	masopust
<g/>
.	.	kIx.	.
</s>
<s>
Zástupcům	zástupce	k1gMnPc3	zástupce
vesnické	vesnický	k2eAgFnSc2d1	vesnická
chasy	chasa	k1gFnSc2	chasa
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
obvykle	obvykle	k6eAd1	obvykle
označování	označování	k1gNnSc4	označování
jako	jako	k8xS	jako
stárkové	stárková	k1gFnPc4	stárková
<g/>
,	,	kIx,	,
bývala	bývat	k5eAaImAgFnS	bývat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
oslav	oslava	k1gFnPc2	oslava
zapůjčena	zapůjčit	k5eAaPmNgNnP	zapůjčit
práva	právo	k1gNnSc2	právo
rychtáře	rychtář	k1gMnSc2	rychtář
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
obecního	obecní	k2eAgInSc2d1	obecní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
takto	takto	k6eAd1	takto
zapůjčeného	zapůjčený	k2eAgNnSc2d1	zapůjčené
práva	právo	k1gNnSc2	právo
byl	být	k5eAaImAgInS	být
ozdobený	ozdobený	k2eAgInSc1d1	ozdobený
meč	meč	k1gInSc1	meč
<g/>
,	,	kIx,	,
šavle	šavle	k1gFnSc1	šavle
či	či	k8xC	či
hůl	hůl	k1gFnSc1	hůl
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ostatkové	ostatkový	k2eAgFnSc2d1	ostatková
zábavy	zábava	k1gFnSc2	zábava
zasedali	zasedat	k5eAaImAgMnP	zasedat
mladí	mladý	k2eAgMnPc1d1	mladý
chasníci	chasník	k1gMnPc1	chasník
v	v	k7c6	v
hospodě	hospodě	k?	hospodě
ke	k	k7c3	k
stolu	stol	k1gInSc3	stol
<g/>
,	,	kIx,	,
nad	nad	k7c4	nad
něhož	jenž	k3xRgNnSc4	jenž
byla	být	k5eAaImAgFnS	být
obřadní	obřadní	k2eAgFnSc1d1	obřadní
šavle	šavle	k1gFnSc1	šavle
zaseknuta	zaseknout	k5eAaPmNgFnS	zaseknout
do	do	k7c2	do
stropu	strop	k1gInSc2	strop
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
právem	právo	k1gNnSc7	právo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
konala	konat	k5eAaImAgNnP	konat
parodická	parodický	k2eAgNnPc1d1	parodické
napodobování	napodobování	k1gNnPc1	napodobování
soudních	soudní	k2eAgFnPc2d1	soudní
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
byli	být	k5eAaImAgMnP	být
před	před	k7c4	před
chasu	chasa	k1gFnSc4	chasa
předvoláváni	předvoláván	k2eAgMnPc1d1	předvoláván
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
ze	z	k7c2	z
vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Usvědčení	usvědčený	k2eAgMnPc1d1	usvědčený
provinilci	provinilec	k1gMnPc1	provinilec
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
buď	buď	k8xC	buď
uhradit	uhradit	k5eAaPmF	uhradit
chase	chasa	k1gFnSc3	chasa
peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pokutu	pokuta	k1gFnSc4	pokuta
nebo	nebo	k8xC	nebo
byli	být	k5eAaImAgMnP	být
obřadně	obřadně	k6eAd1	obřadně
zbiti	zbít	k5eAaPmNgMnP	zbít
vařečkou	vařečka	k1gFnSc7	vařečka
známou	známý	k2eAgFnSc7d1	známá
jako	jako	k8xS	jako
klapačka	klapačka	k1gFnSc1	klapačka
<g/>
,	,	kIx,	,
lopatka	lopatka	k1gFnSc1	lopatka
<g/>
,	,	kIx,	,
pleskačka	pleskačka	k1gFnSc1	pleskačka
či	či	k8xC	či
ferula	ferula	k1gFnSc1	ferula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Hané	Haná	k1gFnSc2	Haná
se	se	k3xPyFc4	se
během	běh	k1gInSc7	běh
masopustu	masopust	k1gInSc2	masopust
konaly	konat	k5eAaImAgFnP	konat
veselé	veselý	k2eAgFnPc1d1	veselá
obchůzky	obchůzka	k1gFnPc1	obchůzka
vsí	ves	k1gFnPc2	ves
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
vodění	vodění	k1gNnSc1	vodění
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
průvod	průvod	k1gInSc1	průvod
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
žertovných	žertovný	k2eAgInPc6d1	žertovný
procesech	proces	k1gInPc6	proces
rovněž	rovněž	k6eAd1	rovněž
vypláceni	vyplácet	k5eAaImNgMnP	vyplácet
vařechou	vařecha	k1gFnSc7	vařecha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byli	být	k5eAaImAgMnP	být
chasou	chasa	k1gFnSc7	chasa
přidržováni	přidržovat	k5eAaImNgMnP	přidržovat
na	na	k7c6	na
připravené	připravený	k2eAgFnSc6d1	připravená
lavici	lavice	k1gFnSc6	lavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
masopustu	masopust	k1gInSc2	masopust
pak	pak	k6eAd1	pak
chasa	chasa	k1gFnSc1	chasa
vracela	vracet	k5eAaImAgFnS	vracet
právo	právo	k1gNnSc4	právo
zpátky	zpátky	k6eAd1	zpátky
rychtáři	rychtář	k1gMnSc3	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
slavnosti	slavnost	k1gFnPc1	slavnost
se	se	k3xPyFc4	se
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
udržují	udržovat	k5eAaImIp3nP	udržovat
i	i	k9	i
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
svatba	svatba	k1gFnSc1	svatba
====	====	k?	====
</s>
</p>
<p>
<s>
Svatba	svatba	k1gFnSc1	svatba
představuje	představovat	k5eAaImIp3nS	představovat
tradiční	tradiční	k2eAgInSc4d1	tradiční
přechodový	přechodový	k2eAgInSc4d1	přechodový
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
pojí	pojíst	k5eAaPmIp3nS	pojíst
výrazné	výrazný	k2eAgInPc4d1	výrazný
projevy	projev	k1gInPc4	projev
hanáckého	hanácký	k2eAgInSc2d1	hanácký
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
silně	silně	k6eAd1	silně
religiózním	religiózní	k2eAgInSc6d1	religiózní
venkově	venkov	k1gInSc6	venkov
se	se	k3xPyFc4	se
svatby	svatba	k1gFnPc1	svatba
většinou	většinou	k6eAd1	většinou
konaly	konat	k5eAaImAgFnP	konat
o	o	k7c6	o
masopustu	masopust	k1gInSc6	masopust
či	či	k8xC	či
po	po	k7c6	po
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tradičním	tradiční	k2eAgNnSc7d1	tradiční
dnem	dno	k1gNnSc7	dno
určeným	určený	k2eAgNnSc7d1	určené
pro	pro	k7c4	pro
svatby	svatba	k1gFnSc2	svatba
bylo	být	k5eAaImAgNnS	být
úterý	úterý	k1gNnSc1	úterý
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
ve	v	k7c4	v
svatební	svatební	k2eAgInSc4d1	svatební
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
ženich	ženich	k1gMnSc1	ženich
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
průvodem	průvod	k1gInSc7	průvod
vypravil	vypravit	k5eAaPmAgInS	vypravit
k	k	k7c3	k
domu	dům	k1gInSc3	dům
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
snoubenka	snoubenka	k1gFnSc1	snoubenka
přespolní	přespolní	k2eAgFnSc1d1	přespolní
<g/>
,	,	kIx,	,
jela	jet	k5eAaImAgFnS	jet
spřežení	spřežení	k1gNnSc4	spřežení
do	do	k7c2	do
její	její	k3xOp3gFnSc2	její
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oblékání	oblékání	k1gNnSc6	oblékání
do	do	k7c2	do
svatebních	svatební	k2eAgInPc2d1	svatební
šatů	šat	k1gInPc2	šat
stávala	stávat	k5eAaImAgFnS	stávat
nevěsta	nevěsta	k1gFnSc1	nevěsta
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
v	v	k7c6	v
díži	díž	k1gFnSc6	díž
na	na	k7c4	na
kynutí	kynutí	k1gNnSc4	kynutí
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jí	on	k3xPp3gFnSc3	on
mělo	mít	k5eAaImAgNnS	mít
zaručit	zaručit	k5eAaPmF	zaručit
plodnost	plodnost	k1gFnSc4	plodnost
a	a	k8xC	a
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
řeči	řeč	k1gFnSc6	řeč
družby	družba	k1gFnSc2	družba
a	a	k8xC	a
požehnání	požehnání	k1gNnSc4	požehnání
rodičů	rodič	k1gMnPc2	rodič
byla	být	k5eAaImAgFnS	být
vystrojená	vystrojený	k2eAgFnSc1d1	vystrojená
nevěsta	nevěsta	k1gFnSc1	nevěsta
vydána	vydán	k2eAgFnSc1d1	vydána
ženichovi	ženich	k1gMnSc3	ženich
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
průvod	průvod	k1gInSc1	průvod
vypravil	vypravit	k5eAaPmAgInS	vypravit
na	na	k7c4	na
obřad	obřad	k1gInSc4	obřad
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
kostela	kostel	k1gInSc2	kostel
většinou	většinou	k6eAd1	většinou
následoval	následovat	k5eAaImAgInS	následovat
rituál	rituál	k1gInSc1	rituál
zalikování	zalikování	k1gNnPc2	zalikování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
novomanželům	novomanžel	k1gMnPc3	novomanžel
mířícím	mířící	k2eAgFnPc3d1	mířící
na	na	k7c4	na
hostinu	hostina	k1gFnSc4	hostina
přehrazena	přehrazen	k2eAgFnSc1d1	přehrazena
cesta	cesta	k1gFnSc1	cesta
a	a	k8xC	a
ženich	ženich	k1gMnSc1	ženich
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
svou	svůj	k3xOyFgFnSc4	svůj
nevěstu	nevěsta	k1gFnSc4	nevěsta
vyplatit	vyplatit	k5eAaPmF	vyplatit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
nebo	nebo	k8xC	nebo
průběhu	průběh	k1gInSc3	průběh
svatební	svatební	k2eAgFnSc2d1	svatební
hostiny	hostina	k1gFnSc2	hostina
patřil	patřit	k5eAaImAgInS	patřit
zvyk	zvyk	k1gInSc1	zvyk
házení	házení	k1gNnSc2	házení
do	do	k7c2	do
žita	žito	k1gNnSc2	žito
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
byli	být	k5eAaImAgMnP	být
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
svatebčané	svatebčan	k1gMnPc1	svatebčan
postupně	postupně	k6eAd1	postupně
vyzýváni	vyzýván	k2eAgMnPc1d1	vyzýván
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
do	do	k7c2	do
talíře	talíř	k1gInSc2	talíř
s	s	k7c7	s
žitem	žito	k1gNnSc7	žito
či	či	k8xC	či
pšenicí	pšenice	k1gFnSc7	pšenice
položeného	položený	k2eAgInSc2d1	položený
před	před	k7c4	před
nevěstu	nevěsta	k1gFnSc4	nevěsta
házeli	házet	k5eAaImAgMnP	házet
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
následovalo	následovat	k5eAaImAgNnS	následovat
přinesení	přinesení	k1gNnSc1	přinesení
mohutného	mohutný	k2eAgInSc2d1	mohutný
svatebního	svatební	k2eAgInSc2d1	svatební
koláče	koláč	k1gInSc2	koláč
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
pokrmy	pokrm	k1gInPc4	pokrm
servírované	servírovaný	k2eAgInPc4d1	servírovaný
při	při	k7c6	při
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
patřila	patřit	k5eAaImAgFnS	patřit
lukšová	lukšový	k2eAgFnSc1d1	Lukšová
nebo	nebo	k8xC	nebo
dršťková	dršťkový	k2eAgFnSc1d1	dršťková
polévka	polévka	k1gFnSc1	polévka
<g/>
,	,	kIx,	,
hovězí	hovězí	k2eAgNnSc1d1	hovězí
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
křenovou	křenový	k2eAgFnSc7d1	Křenová
omáčkou	omáčka	k1gFnSc7	omáčka
<g/>
,	,	kIx,	,
vepřová	vepřový	k2eAgFnSc1d1	vepřová
pečeně	pečeně	k1gFnSc1	pečeně
se	s	k7c7	s
zelím	zelí	k1gNnSc7	zelí
či	či	k8xC	či
vdolky	vdolek	k1gInPc7	vdolek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepřehlédnutelný	přehlédnutelný	k2eNgInSc1d1	nepřehlédnutelný
prvek	prvek	k1gInSc1	prvek
ženského	ženský	k2eAgInSc2d1	ženský
svatebního	svatební	k2eAgInSc2d1	svatební
kroje	kroj	k1gInSc2	kroj
představovala	představovat	k5eAaImAgFnS	představovat
honosná	honosný	k2eAgFnSc1d1	honosná
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
označení	označení	k1gNnSc1	označení
pantlék	pantlék	k1gInSc1	pantlék
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nevěstina	nevěstin	k2eAgFnSc1d1	nevěstina
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
obručí	obruč	k1gFnSc7	obruč
a	a	k8xC	a
lepenkovou	lepenkový	k2eAgFnSc7d1	lepenková
kostrou	kostra	k1gFnSc7	kostra
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
zdobily	zdobit	k5eAaImAgInP	zdobit
korálky	korálek	k1gInPc4	korálek
<g/>
,	,	kIx,	,
flitry	flitr	k1gInPc4	flitr
a	a	k8xC	a
kytice	kytice	k1gFnPc4	kytice
většinou	většina	k1gFnSc7	většina
zřejmě	zřejmě	k6eAd1	zřejmě
umělých	umělý	k2eAgFnPc2d1	umělá
růží	růž	k1gFnPc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
koruny	koruna	k1gFnSc2	koruna
býval	bývat	k5eAaImAgInS	bývat
umístěn	umístit	k5eAaPmNgInS	umístit
samotný	samotný	k2eAgInSc1d1	samotný
pantlék	pantlék	k1gInSc1	pantlék
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
věnec	věnec	k1gInSc4	věnec
červených	červený	k2eAgFnPc2d1	červená
mašlí	mašle	k1gFnPc2	mašle
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
dozadu	dozadu	k6eAd1	dozadu
visely	viset	k5eAaImAgFnP	viset
červené	červený	k2eAgFnPc1d1	červená
stuhy	stuha	k1gFnPc1	stuha
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k1gNnSc7	další
ze	z	k7c2	z
svatebních	svatební	k2eAgInPc2d1	svatební
doplňků	doplněk	k1gInPc2	doplněk
nevěstina	nevěstin	k2eAgInSc2d1	nevěstin
kroje	kroj	k1gInSc2	kroj
byla	být	k5eAaImAgFnS	být
vyšívaná	vyšívaný	k2eAgFnSc1d1	vyšívaná
přehozová	přehozový	k2eAgFnSc1d1	přehozový
plachta	plachta	k1gFnSc1	plachta
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
úvodnice	úvodnice	k1gFnSc1	úvodnice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
dívka	dívka	k1gFnSc1	dívka
během	během	k7c2	během
svatebního	svatební	k2eAgInSc2d1	svatební
obřadu	obřad	k1gInSc2	obřad
přehazovala	přehazovat	k5eAaImAgFnS	přehazovat
přes	přes	k7c4	přes
lokty	loket	k1gInPc4	loket
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
narození	narození	k1gNnSc6	narození
dítěte	dítě	k1gNnSc2	dítě
pak	pak	k6eAd1	pak
v	v	k7c6	v
úvodnici	úvodnice	k1gFnSc6	úvodnice
brala	brát	k5eAaImAgFnS	brát
matka	matka	k1gFnSc1	matka
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
své	svůj	k3xOyFgNnSc4	svůj
novorozeně	novorozeně	k1gNnSc4	novorozeně
<g/>
.	.	kIx.	.
</s>
<s>
Ženich	ženich	k1gMnSc1	ženich
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
klobouk	klobouk	k1gInSc1	klobouk
bohatě	bohatě	k6eAd1	bohatě
zdobený	zdobený	k2eAgInSc1d1	zdobený
pentlemi	pentle	k1gFnPc7	pentle
<g/>
,	,	kIx,	,
umělými	umělý	k2eAgFnPc7d1	umělá
květinami	květina	k1gFnPc7	květina
a	a	k8xC	a
péry	péro	k1gNnPc7	péro
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
kroj	kroj	k1gInSc1	kroj
pak	pak	k6eAd1	pak
při	při	k7c6	při
svatbě	svatba	k1gFnSc6	svatba
doplňoval	doplňovat	k5eAaImAgInS	doplňovat
červený	červený	k2eAgInSc1d1	červený
lipský	lipský	k2eAgInSc1d1	lipský
šátek	šátek	k1gInSc1	šátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
přivazován	přivazován	k2eAgInSc1d1	přivazován
od	od	k7c2	od
pravého	pravý	k2eAgNnSc2d1	pravé
ramene	rameno	k1gNnSc2	rameno
k	k	k7c3	k
levé	levý	k2eAgFnSc3d1	levá
kyčli	kyčel	k1gFnSc3	kyčel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
svatebních	svatební	k2eAgInPc2d1	svatební
zvyků	zvyk	k1gInPc2	zvyk
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
předávání	předávání	k1gNnSc1	předávání
svatebních	svatební	k2eAgInPc2d1	svatební
darů	dar	k1gInPc2	dar
<g/>
.	.	kIx.	.
</s>
<s>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
často	často	k6eAd1	často
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
obřadem	obřad	k1gInSc7	obřad
posílala	posílat	k5eAaImAgFnS	posílat
svému	svůj	k3xOyFgMnSc3	svůj
nastávajícímu	nastávající	k1gMnSc3	nastávající
vyšívanou	vyšívaný	k2eAgFnSc4d1	vyšívaná
tenčici	tenčice	k1gFnSc4	tenčice
nebo	nebo	k8xC	nebo
šátek	šátek	k1gInSc4	šátek
<g/>
.	.	kIx.	.
</s>
<s>
Ženich	ženich	k1gMnSc1	ženich
pak	pak	k6eAd1	pak
obdarovával	obdarovávat	k5eAaImAgMnS	obdarovávat
svou	svůj	k3xOyFgFnSc4	svůj
nevěstu	nevěsta	k1gFnSc4	nevěsta
stříbrnými	stříbrný	k2eAgFnPc7d1	stříbrná
mincemi	mince	k1gFnPc7	mince
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jí	on	k3xPp3gFnSc7	on
kladl	klást	k5eAaImAgInS	klást
do	do	k7c2	do
klína	klín	k1gInSc2	klín
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
střevíci	střevíc	k1gInPc7	střevíc
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgInPc3	jenž
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
poslušné	poslušný	k2eAgNnSc1d1	poslušné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svatební	svatební	k2eAgFnSc6d1	svatební
hostině	hostina	k1gFnSc6	hostina
následovalo	následovat	k5eAaImAgNnS	následovat
čepení	čepení	k1gNnSc1	čepení
nevěsty	nevěsta	k1gFnSc2	nevěsta
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
rituálu	rituál	k1gInSc2	rituál
odstranily	odstranit	k5eAaPmAgFnP	odstranit
provdané	provdaný	k2eAgFnPc1d1	provdaná
ženy	žena	k1gFnPc1	žena
nevěstě	nevěsta	k1gFnSc3	nevěsta
její	její	k3xOp3gFnSc4	její
zdobnou	zdobný	k2eAgFnSc4d1	zdobná
pokrývku	pokrývka	k1gFnSc4	pokrývka
hlavy	hlava	k1gFnSc2	hlava
a	a	k8xC	a
zapletly	zaplést	k5eAaPmAgInP	zaplést
její	její	k3xOp3gInPc1	její
vlasy	vlas	k1gInPc1	vlas
okolo	okolo	k7c2	okolo
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dívka	dívka	k1gFnSc1	dívka
obdarovaná	obdarovaný	k2eAgFnSc1d1	obdarovaná
čepcem	čepec	k1gInSc7	čepec
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
mezi	mezi	k7c4	mezi
dospělé	dospělý	k2eAgFnPc4d1	dospělá
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ženich	ženich	k1gMnSc1	ženich
směl	smět	k5eAaImAgMnS	smět
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
poprvé	poprvé	k6eAd1	poprvé
obléknout	obléknout	k5eAaPmF	obléknout
plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
nevěsta	nevěsta	k1gFnSc1	nevěsta
z	z	k7c2	z
jeho	on	k3xPp3gInSc2	on
klobouku	klobouk	k1gInSc2	klobouk
"	"	kIx"	"
<g/>
otrhala	otrhat	k5eAaPmAgFnS	otrhat
<g/>
"	"	kIx"	"
pentle	pentle	k1gFnPc4	pentle
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
ozdoby	ozdoba	k1gFnPc4	ozdoba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
zvyky	zvyk	k1gInPc4	zvyk
spojené	spojený	k2eAgInPc4d1	spojený
se	s	k7c7	s
svatbou	svatba	k1gFnSc7	svatba
patřilo	patřit	k5eAaImAgNnS	patřit
také	také	k9	také
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
stínání	stínání	k1gNnSc1	stínání
kohouta	kohout	k1gMnSc2	kohout
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
hostině	hostina	k1gFnSc6	hostina
souzen	soudit	k5eAaImNgMnS	soudit
a	a	k8xC	a
zabit	zabít	k5eAaPmNgMnS	zabít
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
údajné	údajný	k2eAgInPc4d1	údajný
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Honění	honění	k1gNnSc1	honění
krále	král	k1gMnSc2	král
====	====	k?	====
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc4	svátek
letnice	letnice	k1gFnPc1	letnice
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
Hané	Haná	k1gFnSc2	Haná
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
spojen	spojit	k5eAaPmNgMnS	spojit
se	s	k7c7	s
zvykem	zvyk	k1gInSc7	zvyk
honění	honění	k1gNnSc4	honění
krála	krála	k?	krála
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
býval	bývat	k5eAaImAgInS	bývat
označovaný	označovaný	k2eAgInSc1d1	označovaný
též	též	k9	též
jako	jako	k9	jako
zvyk	zvyk	k1gInSc1	zvyk
jet	jet	k5eAaImF	jet
na	na	k7c4	na
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
jezdit	jezdit	k5eAaImF	jezdit
po	po	k7c4	po
králoch	králoch	k1gInSc4	králoch
<g/>
,	,	kIx,	,
hledat	hledat	k5eAaImF	hledat
krála	krála	k?	krála
či	či	k8xC	či
jízda	jízda	k1gFnSc1	jízda
s	s	k7c7	s
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgInSc2	tento
obřadu	obřad	k1gInSc2	obřad
se	se	k3xPyFc4	se
mladíci	mladík	k1gMnPc1	mladík
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
seskupili	seskupit	k5eAaPmAgMnP	seskupit
do	do	k7c2	do
družiny	družina	k1gFnSc2	družina
doprovázející	doprovázející	k2eAgFnSc1d1	doprovázející
vybraného	vybraný	k2eAgMnSc4d1	vybraný
mladého	mladý	k2eAgMnSc4d1	mladý
chlapce	chlapec	k1gMnSc4	chlapec
–	–	k?	–
krále	král	k1gMnSc4	král
oblečeného	oblečený	k2eAgMnSc4d1	oblečený
v	v	k7c6	v
ženských	ženský	k2eAgInPc6d1	ženský
šatech	šat	k1gInPc6	šat
a	a	k8xC	a
ozdobeného	ozdobený	k2eAgInSc2d1	ozdobený
květy	květ	k1gInPc7	květ
<g/>
.	.	kIx.	.
</s>
<s>
Družina	družin	k2eAgFnSc1d1	družina
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
šavlemi	šavle	k1gFnPc7	šavle
pak	pak	k6eAd1	pak
projížděla	projíždět	k5eAaImAgFnS	projíždět
vsí	ves	k1gFnSc7	ves
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
i	i	k8xC	i
okolními	okolní	k2eAgFnPc7d1	okolní
obcemi	obec	k1gFnPc7	obec
a	a	k8xC	a
žádala	žádat	k5eAaImAgFnS	žádat
po	po	k7c6	po
přihlížejících	přihlížející	k2eAgFnPc6d1	přihlížející
výslužku	výslužka	k1gFnSc4	výslužka
<g/>
.	.	kIx.	.
</s>
<s>
Jezdci	jezdec	k1gMnPc1	jezdec
navíc	navíc	k6eAd1	navíc
eventuálně	eventuálně	k6eAd1	eventuálně
bránili	bránit	k5eAaImAgMnP	bránit
svého	svůj	k3xOyFgMnSc4	svůj
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
nezmocnili	zmocnit	k5eNaPmAgMnP	zmocnit
mladíci	mladík	k1gMnPc1	mladík
ze	z	k7c2	z
sousedních	sousední	k2eAgFnPc2d1	sousední
vsí	ves	k1gFnPc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Rivalita	rivalita	k1gFnSc1	rivalita
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
obcemi	obec	k1gFnPc7	obec
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
mnohdy	mnohdy	k6eAd1	mnohdy
přivodila	přivodit	k5eAaBmAgFnS	přivodit
násilné	násilný	k2eAgInPc4d1	násilný
střety	střet	k1gInPc4	střet
mezi	mezi	k7c7	mezi
družinami	družina	k1gFnPc7	družina
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1819	[number]	k4	1819
se	se	k3xPyFc4	se
u	u	k7c2	u
Lověšic	Lověšice	k1gFnPc2	Lověšice
krvavě	krvavě	k6eAd1	krvavě
střetly	střetnout	k5eAaPmAgFnP	střetnout
skupiny	skupina	k1gFnPc1	skupina
přibližně	přibližně	k6eAd1	přibližně
80	[number]	k4	80
jezdců	jezdec	k1gMnPc2	jezdec
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
Moštěnice	Moštěnice	k1gFnSc2	Moštěnice
<g/>
,	,	kIx,	,
Bochoře	Bochoř	k1gFnSc2	Bochoř
<g/>
,	,	kIx,	,
Lověšic	Lověšice	k1gFnPc2	Lověšice
a	a	k8xC	a
Šířavy	šířava	k1gFnSc2	šířava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
incidentu	incident	k1gInSc6	incident
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xC	jako
bitva	bitva	k1gFnSc1	bitva
čtyř	čtyři	k4xCgMnPc2	čtyři
králů	král	k1gMnPc2	král
bylo	být	k5eAaImAgNnS	být
osm	osm	k4xCc1	osm
mladíků	mladík	k1gMnPc2	mladík
usmrceno	usmrtit	k5eAaPmNgNnS	usmrtit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
boji	boj	k1gInSc6	boj
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
pět	pět	k4xCc1	pět
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
na	na	k7c4	na
vážná	vážný	k2eAgNnPc4d1	vážné
zranění	zranění	k1gNnPc4	zranění
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Zvyk	zvyk	k1gInSc1	zvyk
honění	honění	k1gNnSc2	honění
krále	král	k1gMnSc2	král
patrně	patrně	k6eAd1	patrně
vycházel	vycházet	k5eAaImAgMnS	vycházet
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
jezdeckých	jezdecký	k2eAgInPc2d1	jezdecký
závodů	závod	k1gInPc2	závod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jako	jako	k9	jako
král	král	k1gMnSc1	král
býval	bývat	k5eAaImAgMnS	bývat
označován	označován	k2eAgMnSc1d1	označován
vítěz	vítěz	k1gMnSc1	vítěz
takovéto	takovýto	k3xDgFnSc2	takovýto
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
povětšinou	povětšinou	k6eAd1	povětšinou
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Snahy	snaha	k1gFnPc1	snaha
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
obnovení	obnovení	k1gNnSc6	obnovení
zesílily	zesílit	k5eAaPmAgInP	zesílit
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
pokusy	pokus	k1gInPc4	pokus
o	o	k7c6	o
oživení	oživení	k1gNnSc6	oživení
přicházely	přicházet	k5eAaImAgFnP	přicházet
i	i	k9	i
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podobným	podobný	k2eAgInSc7d1	podobný
zvykem	zvyk	k1gInSc7	zvyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
povětšinou	povětšinou	k6eAd1	povětšinou
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
Jízda	jízda	k1gFnSc1	jízda
králů	král	k1gMnPc2	král
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
setkáme	setkat	k5eAaPmIp1nP	setkat
na	na	k7c6	na
Slovácku	Slovácko	k1gNnSc6	Slovácko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
se	se	k3xPyFc4	se
o	o	k7c4	o
obnovu	obnova	k1gFnSc4	obnova
tohoto	tento	k3xDgInSc2	tento
zvyku	zvyk	k1gInSc2	zvyk
snaží	snažit	k5eAaImIp3nS	snažit
například	například	k6eAd1	například
v	v	k7c6	v
Kojetíně	Kojetín	k1gInSc6	Kojetín
či	či	k8xC	či
Chropyni	Chropyně	k1gFnSc6	Chropyně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bývají	bývat	k5eAaImIp3nP	bývat
během	během	k7c2	během
Hanáckých	hanácký	k2eAgFnPc2d1	Hanácká
slavností	slavnost	k1gFnPc2	slavnost
konány	konat	k5eAaImNgFnP	konat
Jízdy	jízda	k1gFnPc1	jízda
krále	král	k1gMnSc2	král
Ječmínka	Ječmínek	k1gMnSc2	Ječmínek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
a	a	k8xC	a
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Města	město	k1gNnSc2	město
===	===	k?	===
</s>
</p>
<p>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
–	–	k?	–
metropole	metropol	k1gFnSc2	metropol
Hané	Haná	k1gFnSc2	Haná
a	a	k8xC	a
první	první	k4xOgNnSc4	první
zemské	zemský	k2eAgNnSc4d1	zemské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
hanácký	hanácký	k2eAgInSc1d1	hanácký
Oxford	Oxford	k1gInSc1	Oxford
</s>
</p>
<p>
<s>
Přerov	Přerov	k1gInSc1	Přerov
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
hanácký	hanácký	k2eAgInSc1d1	hanácký
Detroit	Detroit	k1gInSc1	Detroit
</s>
</p>
<p>
<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
hanácký	hanácký	k2eAgInSc1d1	hanácký
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
</s>
</p>
<p>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
hanácké	hanácký	k2eAgFnPc1d1	Hanácká
Athény	Athéna	k1gFnPc1	Athéna
</s>
</p>
<p>
<s>
Vyškov	Vyškov	k1gInSc1	Vyškov
–	–	k?	–
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
moravské	moravský	k2eAgFnSc2d1	Moravská
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
také	také	k9	také
hanácké	hanácký	k2eAgFnPc4d1	Hanácká
Versailles	Versailles	k1gFnPc4	Versailles
</s>
</p>
<p>
<s>
Tovačov	Tovačov	k1gInSc1	Tovačov
</s>
</p>
<p>
<s>
Litovel	Litovel	k1gFnSc1	Litovel
–	–	k?	–
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
hanácké	hanácký	k2eAgFnPc1d1	Hanácká
Benátky	Benátky	k1gFnPc1	Benátky
</s>
</p>
<p>
<s>
Uničov	Uničov	k1gInSc1	Uničov
</s>
</p>
<p>
<s>
Šternberk	Šternberk	k1gInSc1	Šternberk
</s>
</p>
<p>
<s>
Kojetín	Kojetín	k1gInSc1	Kojetín
</s>
</p>
<p>
<s>
Hulín	Hulín	k1gInSc1	Hulín
</s>
</p>
<p>
<s>
Holešov	Holešov	k1gInSc1	Holešov
</s>
</p>
<p>
<s>
Náměšť	Náměšť	k1gFnSc1	Náměšť
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
</s>
</p>
<p>
<s>
Chropyně	Chropyně	k1gFnSc1	Chropyně
</s>
</p>
<p>
<s>
Kostelec	Kostelec	k1gInSc1	Kostelec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
Bystřice	Bystřice	k1gFnSc1	Bystřice
</s>
</p>
<p>
<s>
Ivanovice	Ivanovice	k1gFnPc1	Ivanovice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
</s>
</p>
<p>
<s>
Němčice	Němčice	k1gFnSc1	Němčice
nad	nad	k7c7	nad
Hanou	Hana	k1gFnSc7	Hana
</s>
</p>
<p>
<s>
===	===	k?	===
Památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
<s>
Sloup	sloup	k1gInSc1	sloup
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
</s>
</p>
<p>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
zámek	zámek	k1gInSc1	zámek
<g/>
,	,	kIx,	,
Květná	květný	k2eAgFnSc1d1	Květná
a	a	k8xC	a
Podzámecká	podzámecký	k2eAgFnSc1d1	Podzámecká
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Kopeček	Kopeček	k1gMnSc1	Kopeček
u	u	k7c2	u
Olomouce	Olomouc	k1gFnSc2	Olomouc
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
Bouzov	Bouzov	k1gInSc1	Bouzov
</s>
</p>
<p>
<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
Příkazy	příkaz	k1gInPc4	příkaz
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
Chropyně	Chropyně	k1gFnSc2	Chropyně
</s>
</p>
<p>
<s>
Litovelské	litovelský	k2eAgNnSc1d1	Litovelské
Pomoraví	Pomoraví	k1gNnSc1	Pomoraví
</s>
</p>
<p>
<s>
Javoříčské	Javoříčský	k2eAgFnPc1d1	Javoříčská
jeskyně	jeskyně	k1gFnPc1	jeskyně
</s>
</p>
<p>
<s>
Mladečské	Mladečský	k2eAgFnPc1d1	Mladečský
jeskyně	jeskyně	k1gFnPc1	jeskyně
</s>
</p>
<p>
<s>
Zbrašovské	Zbrašovská	k1gFnPc1	Zbrašovská
aragonitové	aragonitový	k2eAgFnSc2d1	aragonitová
jeskyně	jeskyně	k1gFnSc2	jeskyně
</s>
</p>
<p>
<s>
Tovačovský	Tovačovský	k2eAgInSc1d1	Tovačovský
zámek	zámek	k1gInSc1	zámek
</s>
</p>
<p>
<s>
Zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Náměšti	Náměšť	k1gFnSc6	Náměšť
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
</s>
</p>
<p>
<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Cholině	Cholina	k1gFnSc6	Cholina
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEČÁK	BEČÁK	kA	BEČÁK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
;	;	kIx,	;
POSPĚCH	pospěch	k1gInSc1	pospěch
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
ŠALÉ	ŠALÉ	kA	ŠALÉ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgNnSc1d1	lidové
umění	umění	k1gNnSc1	umění
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
:	:	kIx,	:
lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
hmotná	hmotný	k2eAgFnSc1d1	hmotná
<g/>
.	.	kIx.	.
</s>
<s>
Boskovice	Boskovice	k1gInPc1	Boskovice
<g/>
:	:	kIx,	:
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
460	[number]	k4	460
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85834	[number]	k4	85834
<g/>
-	-	kIx~	-
<g/>
43	[number]	k4	43
<g/>
-	-	kIx~	-
<g/>
x.	x.	k?	x.
</s>
</p>
<p>
<s>
BENEŠ	Beneš	k1gMnSc1	Beneš
<g/>
,	,	kIx,	,
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
<g/>
.	.	kIx.	.
</s>
<s>
Úvaha	úvaha	k1gFnSc1	úvaha
o	o	k7c6	o
Hanácích	Hanák	k1gMnPc6	Hanák
a	a	k8xC	a
Hané	Haná	k1gFnSc6	Haná
včera	včera	k6eAd1	včera
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HÝBL	hýbnout	k5eAaPmAgMnS	hýbnout
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
oděv	oděv	k1gInSc1	oděv
a	a	k8xC	a
tanec	tanec	k1gInSc1	tanec
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
konference	konference	k1gFnSc2	konference
o	o	k7c6	o
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
199	[number]	k4	199
<g/>
–	–	k?	–
<g/>
209	[number]	k4	209
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BROUČEK	Brouček	k1gMnSc1	Brouček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
JEŘÁBEK	Jeřábek	k1gMnSc1	Jeřábek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
:	:	kIx,	:
národopisná	národopisný	k2eAgFnSc1d1	národopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Věcná	věcný	k2eAgFnSc1d1	věcná
část	část	k1gFnSc1	část
A-	A-	k1gMnPc2	A-
<g/>
N.	N.	kA	N.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
636	[number]	k4	636
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1712	[number]	k4	1712
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BROUČEK	Brouček	k1gMnSc1	Brouček
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
JEŘÁBEK	Jeřábek	k1gMnSc1	Jeřábek
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
:	:	kIx,	:
národopisná	národopisný	k2eAgFnSc1d1	národopisná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Věcná	věcný	k2eAgFnSc1d1	věcná
část	část	k1gFnSc1	část
O-	O-	k1gMnPc2	O-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
664	[number]	k4	664
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1713	[number]	k4	1713
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČÍŽKOVÁ	Čížková	k1gFnSc1	Čížková
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Hanácký	hanácký	k2eAgInSc1d1	hanácký
kroj	kroj	k1gInSc1	kroj
<g/>
.	.	kIx.	.
</s>
<s>
Prostějov	Prostějov	k1gInSc1	Prostějov
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
105	[number]	k4	105
s.	s.	k?	s.
</s>
</p>
<p>
<s>
DVOŘÁČEK	Dvořáček	k1gMnSc1	Dvořáček
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
RŮŽIČKA	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Haná	Haná	k1gFnSc1	Haná
<g/>
:	:	kIx,	:
Olomoucko	Olomoucko	k1gNnSc1	Olomoucko
<g/>
,	,	kIx,	,
Prostějovsko	Prostějovsko	k1gNnSc1	Prostějovsko
<g/>
,	,	kIx,	,
Přerovsko	Přerovsko	k1gNnSc1	Přerovsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
112	[number]	k4	112
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
516	[number]	k4	516
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JORDÁN	Jordán	k1gMnSc1	Jordán
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Lidové	lidový	k2eAgInPc1d1	lidový
obyčeje	obyčej	k1gInPc1	obyčej
na	na	k7c4	na
Hané	Haná	k1gFnPc4	Haná
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
slovní	slovní	k2eAgInSc4d1	slovní
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInSc4d1	hudební
a	a	k8xC	a
taneční	taneční	k2eAgInSc4d1	taneční
projevy	projev	k1gInPc4	projev
:	:	kIx,	:
VII	VII	kA	VII
<g/>
.	.	kIx.	.
konference	konference	k1gFnSc1	konference
o	o	k7c6	o
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
24	[number]	k4	24
<g/>
.	.	kIx.	.
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1998	[number]	k4	1998
:	:	kIx,	:
sborník	sborník	k1gInSc4	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
<g/>
.	.	kIx.	.
</s>
<s>
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
Vyškovska	Vyškovsko	k1gNnSc2	Vyškovsko
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
271	[number]	k4	271
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FROLEC	FROLEC	kA	FROLEC
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
<g/>
,	,	kIx,	,
jací	jaký	k3yIgMnPc1	jaký
byli	být	k5eAaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
Muzea	muzeum	k1gNnSc2	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FROLEC	FROLEC	kA	FROLEC
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Zaslíbená	zaslíbený	k2eAgFnSc1d1	zaslíbená
země	země	k1gFnSc1	země
<g/>
"	"	kIx"	"
Haná	Haná	k1gFnSc1	Haná
a	a	k8xC	a
Hanáci	Hanák	k1gMnPc1	Hanák
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
K	k	k7c3	k
170	[number]	k4	170
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
narození	narození	k1gNnSc2	narození
Františka	František	k1gMnSc2	František
Skopalíka	Skopalík	k1gMnSc2	Skopalík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastivědný	vlastivědný	k2eAgMnSc1d1	vlastivědný
věstník	věstník	k1gMnSc1	věstník
moravský	moravský	k2eAgMnSc1d1	moravský
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
44	[number]	k4	44
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
169	[number]	k4	169
<g/>
-	-	kIx~	-
<g/>
186	[number]	k4	186
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HAINS	HAINS	kA	HAINS
<g/>
,	,	kIx,	,
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zpěvnosti	zpěvnost	k1gFnSc6	zpěvnost
a	a	k8xC	a
muzikalitě	muzikalita	k1gFnSc6	muzikalita
Hanáků	Hanák	k1gMnPc2	Hanák
<g/>
.	.	kIx.	.
</s>
<s>
Zpravodaj	zpravodaj	k1gMnSc1	zpravodaj
Muzea	muzeum	k1gNnSc2	muzeum
Prostějovska	Prostějovsko	k1gNnSc2	Prostějovsko
v	v	k7c6	v
Prostějově	Prostějov	k1gInSc6	Prostějov
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
48	[number]	k4	48
<g/>
–	–	k?	–
<g/>
51	[number]	k4	51
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
našeho	náš	k3xOp1gInSc2	náš
domova	domov	k1gInSc2	domov
-	-	kIx~	-
Haná	Haná	k1gFnSc1	Haná
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
tematických	tematický	k2eAgFnPc2d1	tematická
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
územní	územní	k2eAgNnSc4d1	územní
odborné	odborný	k2eAgNnSc4d1	odborné
pracoviště	pracoviště	k1gNnSc4	pracoviště
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
104	[number]	k4	104
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86570	[number]	k4	86570
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KVIETKOVÁ	KVIETKOVÁ	kA	KVIETKOVÁ
<g/>
,	,	kIx,	,
Jana	Jana	k1gFnSc1	Jana
<g/>
.	.	kIx.	.
</s>
<s>
Folklór	folklór	k1gInSc4	folklór
regionu	region	k1gInSc2	region
Haná	Haná	k1gFnSc1	Haná
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
nabídky	nabídka	k1gFnSc2	nabídka
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
148	[number]	k4	148
s.	s.	k?	s.
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Halina	Halina	k1gFnSc1	Halina
Kotíková	Kotíková	k1gFnSc1	Kotíková
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lidový	lidový	k2eAgInSc4d1	lidový
rok	rok	k1gInSc4	rok
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
:	:	kIx,	:
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87535	[number]	k4	87535
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUDVÍKOVÁ	Ludvíková	k1gFnSc1	Ludvíková
<g/>
,	,	kIx,	,
Miroslava	Miroslava	k1gFnSc1	Miroslava
<g/>
.	.	kIx.	.
</s>
<s>
Lidový	lidový	k2eAgInSc1d1	lidový
kroj	kroj	k1gInSc1	kroj
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Přerov	Přerov	k1gInSc1	Přerov
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
J.A.	J.A.	k1gMnSc2	J.A.
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
119	[number]	k4	119
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
238	[number]	k4	238
<g/>
-	-	kIx~	-
<g/>
8573	[number]	k4	8573
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAKLÁDAL	nakládat	k5eAaImAgMnS	nakládat
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Hanácké	hanácký	k2eAgFnPc1d1	Hanácká
tradice	tradice	k1gFnPc1	tradice
-	-	kIx~	-
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
folklor	folklor	k1gInSc1	folklor
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
multimediální	multimediální	k2eAgFnSc2d1	multimediální
prezentace	prezentace	k1gFnSc2	prezentace
lidových	lidový	k2eAgFnPc2d1	lidová
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
folkloru	folklor	k1gInSc2	folklor
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
:	:	kIx,	:
multimediální	multimediální	k2eAgFnSc2d1	multimediální
prezentace	prezentace	k1gFnSc2	prezentace
Olomouckého	olomoucký	k2eAgInSc2d1	olomoucký
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAVRÁTIL	Navrátil	k1gMnSc1	Navrátil
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
jací	jaký	k3yQgMnPc1	jaký
jsou	být	k5eAaImIp3nP	být
a	a	k8xC	a
jací	jaký	k3yRgMnPc1	jaký
byli	být	k5eAaImAgMnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
93	[number]	k4	93
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MELHUBOVÁ	MELHUBOVÁ	kA	MELHUBOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
.	.	kIx.	.
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1	lidová
píseň	píseň	k1gFnSc1	píseň
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc1	zvyk
a	a	k8xC	a
tradice	tradice	k1gFnPc1	tradice
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
využití	využití	k1gNnSc1	využití
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
stupni	stupeň	k1gInSc6	stupeň
ZŠ	ZŠ	kA	ZŠ
<g/>
..	..	k?	..
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
103	[number]	k4	103
s.	s.	k?	s.
Diplomová	diplomový	k2eAgFnSc1d1	Diplomová
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Judita	Judita	k1gFnSc1	Judita
Kučerová	Kučerová	k1gFnSc1	Kučerová
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
POSPĚCH	pospěch	k1gInSc1	pospěch
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Rolník	rolník	k1gMnSc1	rolník
-	-	kIx~	-
Haná	Haná	k1gFnSc1	Haná
-	-	kIx~	-
Příklad	příklad	k1gInSc1	příklad
Josefa	Josef	k1gMnSc2	Josef
Vysloužila	Vysloužil	k1gMnSc2	Vysloužil
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
HANUŠ	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
MALÍŘ	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
FASORA	FASORA	kA	FASORA
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
147	[number]	k4	147
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
519	[number]	k4	519
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Proměny	proměna	k1gFnPc1	proměna
hanácké	hanácký	k2eAgFnSc2d1	Hanácká
vesnice	vesnice	k1gFnSc2	vesnice
:	:	kIx,	:
lidová	lidový	k2eAgFnSc1d1	lidová
kultura	kultura	k1gFnSc1	kultura
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
:	:	kIx,	:
sborník	sborník	k1gInSc1	sborník
příspěvků	příspěvek	k1gInPc2	příspěvek
z	z	k7c2	z
IX	IX	kA	IX
<g/>
.	.	kIx.	.
odborné	odborný	k2eAgFnSc2d1	odborná
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Muzeum	muzeum	k1gNnSc1	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
141	[number]	k4	141
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85945	[number]	k4	85945
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEKANINOVÁ	Sekaninová	k1gFnSc1	Sekaninová
<g/>
,	,	kIx,	,
Tereza	Tereza	k1gFnSc1	Tereza
<g/>
.	.	kIx.	.
</s>
<s>
Kulturně-geografické	Kulturněeografický	k2eAgNnSc1d1	Kulturně-geografický
specifika	specifikon	k1gNnPc1	specifikon
Hané	Haná	k1gFnSc2	Haná
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Morava	Morava	k1gFnSc1	Morava
-	-	kIx~	-
Haná	Haná	k1gFnSc1	Haná
:	:	kIx,	:
srdce	srdce	k1gNnSc1	srdce
a	a	k8xC	a
brány	brána	k1gFnPc4	brána
dokořán	dokořán	k6eAd1	dokořán
:	:	kIx,	:
turistický	turistický	k2eAgMnSc1d1	turistický
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
Odbor	odbor	k1gInSc1	odbor
strategického	strategický	k2eAgInSc2d1	strategický
rozvoje	rozvoj	k1gInSc2	rozvoj
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
Oddělení	oddělení	k1gNnSc4	oddělení
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
43	[number]	k4	43
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
239	[number]	k4	239
<g/>
-	-	kIx~	-
<g/>
4727	[number]	k4	4727
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠIMŠA	ŠIMŠA	kA	ŠIMŠA
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Ostatkové	ostatkový	k2eAgNnSc1d1	ostatkové
právo	právo	k1gNnSc1	právo
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
masopustních	masopustní	k2eAgInPc2d1	masopustní
zvyků	zvyk	k1gInPc2	zvyk
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
obřadům	obřad	k1gInPc3	obřad
výročního	výroční	k2eAgInSc2d1	výroční
obyčejového	obyčejový	k2eAgInSc2d1	obyčejový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Národopisná	národopisný	k2eAgFnSc1d1	národopisná
revue	revue	k1gFnSc1	revue
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ŠKUBAL	škubat	k5eAaImAgMnS	škubat
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
Hané	Haná	k1gFnSc2	Haná
<g/>
:	:	kIx,	:
vymezení	vymezení	k1gNnSc2	vymezení
etnografického	etnografický	k2eAgInSc2d1	etnografický
regionu	region	k1gInSc2	region
:	:	kIx,	:
(	(	kIx(	(
<g/>
metodické	metodický	k2eAgInPc1d1	metodický
pokyny	pokyn	k1gInPc1	pokyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
11	[number]	k4	11
s.	s.	k?	s.
</s>
</p>
<p>
<s>
VÁLKA	Válka	k1gMnSc1	Válka
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
Hané	Haná	k1gFnSc6	Haná
a	a	k8xC	a
Hanácích	Hanák	k1gMnPc6	Hanák
<g/>
.	.	kIx.	.
</s>
<s>
Národopisná	národopisný	k2eAgFnSc1d1	národopisná
revue	revue	k1gFnSc1	revue
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
110	[number]	k4	110
<g/>
–	–	k?	–
<g/>
113	[number]	k4	113
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
VEČERKOVÁ	Večerková	k1gFnSc1	Večerková
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Obyčeje	obyčej	k1gInPc1	obyčej
a	a	k8xC	a
slavnosti	slavnost	k1gFnPc1	slavnost
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
512	[number]	k4	512
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
627	[number]	k4	627
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
62	[number]	k4	62
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ZAHRADNÍK	Zahradník	k1gMnSc1	Zahradník
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Marie	Maria	k1gFnSc2	Maria
Gardavské	Gardavský	k2eAgFnPc4d1	Gardavská
kdysi	kdysi	k6eAd1	kdysi
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Kojetín	Kojetín	k1gInSc1	Kojetín
<g/>
:	:	kIx,	:
MěKS	MěKS	k1gFnSc1	MěKS
Kojetín	Kojetín	k1gInSc1	Kojetín
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
42	[number]	k4	42
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
4882	[number]	k4	4882
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZAHRADNÍK	Zahradník	k1gMnSc1	Zahradník
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
.	.	kIx.	.
</s>
<s>
Hanácká	hanácký	k2eAgFnSc1d1	Hanácká
svatba	svatba	k1gFnSc1	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
přechodového	přechodový	k2eAgInSc2d1	přechodový
obřadu	obřad	k1gInSc2	obřad
k	k	k7c3	k
scénickému	scénický	k2eAgInSc3d1	scénický
folkloru	folklor	k1gInSc3	folklor
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
55	[number]	k4	55
s.	s.	k?	s.
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
Miroslav	Miroslav	k1gMnSc1	Miroslav
Válka	Válka	k1gMnSc1	Válka
<g/>
.	.	kIx.	.
s.	s.	k?	s.
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
kraj	kraj	k1gInSc1	kraj
</s>
</p>
<p>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
</s>
</p>
<p>
<s>
Zlínský	zlínský	k2eAgInSc1d1	zlínský
kraj	kraj	k1gInSc1	kraj
</s>
</p>
<p>
<s>
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
Haná	Haná	k1gFnSc1	Haná
</s>
</p>
<p>
<s>
Slezská	slezský	k2eAgFnSc1d1	Slezská
Haná	Haná	k1gFnSc1	Haná
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Haná	Haná	k1gFnSc1	Haná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Haná	Haná	k1gFnSc1	Haná
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Haná	Haná	k1gFnSc1	Haná
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
Morava	Morava	k1gFnSc1	Morava
</s>
</p>
<p>
<s>
Folklórní	folklórní	k2eAgInSc1d1	folklórní
soubor	soubor	k1gInSc1	soubor
Haná	Haná	k1gFnSc1	Haná
</s>
</p>
<p>
<s>
Folklorní	folklorní	k2eAgInSc1d1	folklorní
soubor	soubor	k1gInSc1	soubor
Hanačka	Hanačka	k1gFnSc1	Hanačka
</s>
</p>
<p>
<s>
region	region	k1gInSc1	region
Haná	Haná	k1gFnSc1	Haná
<g/>
,	,	kIx,	,
hanácký	hanácký	k2eAgInSc1d1	hanácký
kroj	kroj	k1gInSc1	kroj
<g/>
:	:	kIx,	:
ženský	ženský	k2eAgInSc4d1	ženský
a	a	k8xC	a
mužský	mužský	k2eAgInSc4d1	mužský
</s>
</p>
<p>
<s>
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
nářečí	nářečí	k1gNnSc1	nářečí
na	na	k7c4	na
www.olomouc.com/hanactina	www.olomouc.com/hanactin	k1gMnSc4	www.olomouc.com/hanactin
</s>
</p>
<p>
<s>
Hanactina	Hanactina	k1gFnSc1	Hanactina
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
hanácko-český	hanácko-český	k2eAgInSc1d1	hanácko-český
a	a	k8xC	a
česko-hanácký	českoanácký	k2eAgInSc1d1	česko-hanácký
slovník	slovník	k1gInSc1	slovník
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Hanáci	Hanák	k1gMnPc1	Hanák
Praha	Praha	k1gFnSc1	Praha
-	-	kIx~	-
spolek	spolek	k1gInSc1	spolek
rodáku	rodák	k1gMnSc6	rodák
a	a	k8xC	a
ledi	led	k1gInSc3	led
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mají	mít	k5eAaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
Hanó	Hanó	k1gMnPc1	Hanó
</s>
</p>
