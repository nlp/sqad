<s>
Kromě	kromě	k7c2	kromě
jiných	jiný	k2eAgFnPc2d1	jiná
cen	cena	k1gFnPc2	cena
získalo	získat	k5eAaPmAgNnS	získat
studio	studio	k1gNnSc4	studio
dvacet	dvacet	k4xCc1	dvacet
čtyři	čtyři	k4xCgInPc4	čtyři
Oscarů	Oscar	k1gInPc2	Oscar
<g/>
,	,	kIx,	,
šest	šest	k4xCc4	šest
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
výdělek	výdělek	k1gInSc1	výdělek
činí	činit	k5eAaImIp3nS	činit
celosvětově	celosvětově	k6eAd1	celosvětově
5.5	[number]	k4	5.5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
