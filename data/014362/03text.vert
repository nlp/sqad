<s>
Andalusie	Andalusie	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Andalucia	Andalucium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
území	území	k1gNnSc6
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
písni	píseň	k1gFnSc6
Johna	John	k1gMnSc2
Calea	Caleus	k1gMnSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Andalucia	Andalucium	k1gNnSc2
(	(	kIx(
<g/>
píseň	píseň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Comunidad	Comunidad	k6eAd1
Autónomade	Autónomad	k1gInSc5
Andalucía	Andalucíus	k1gMnSc4
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Hymna	hymna	k1gFnSc1
<g/>
:	:	kIx,
La	la	k1gNnSc1
bandera	bander	k1gMnSc2
blanca	blancus	k1gMnSc2
y	y	k?
verde	verde	k6eAd1
Geografie	geografie	k1gFnSc2
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
4	#num#	k4
<g/>
°	°	k?
<g/>
9	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
87	#num#	k4
268	#num#	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
8	#num#	k4
476	#num#	k4
718	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
97,1	97,1	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Podřízené	podřízený	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
8	#num#	k4
provincie	provincie	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1982	#num#	k4
(	(	kIx(
<g/>
získání	získání	k1gNnSc4
autonomie	autonomie	k1gFnSc2
<g/>
)	)	kIx)
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Susana	Susana	k1gFnSc1
Díaz	Díaz	k1gMnSc1
Pacheco	Pacheco	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
Parlamentní	parlamentní	k2eAgFnSc1d1
zastoupení	zastoupení	k1gNnSc4
Kongres	kongres	k1gInSc1
</s>
<s>
61	#num#	k4
křesel	křeslo	k1gNnPc2
Senát	senát	k1gInSc1
</s>
<s>
41	#num#	k4
křesel	křeslo	k1gNnPc2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-2	3166-2	k4
</s>
<s>
ES-AN	ES-AN	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.juntadeandalucia.es	www.juntadeandalucia.es	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Andalucía	Andalucíus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Autonomní	autonomní	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
Andalusie	Andalusie	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejlidnatější	lidnatý	k2eAgInSc1d3
a	a	k8xC
druhé	druhý	k4xOgNnSc4
největší	veliký	k2eAgNnSc4d3
autonomní	autonomní	k2eAgNnSc4d1
společenství	společenství	k1gNnSc4
a	a	k8xC
historické	historický	k2eAgNnSc4d1
území	území	k1gNnSc4
na	na	k7c6
jihu	jih	k1gInSc6
Španělska	Španělsko	k1gNnSc2
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
a	a	k8xC
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metropolí	metropol	k1gFnPc2
regionu	region	k1gInSc2
je	být	k5eAaImIp3nS
Sevilla	Sevilla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
Andalusie	Andalusie	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
arabského	arabský	k2eAgInSc2d1
„	„	k?
<g/>
Al	ala	k1gFnPc2
Andalus	Andalus	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
bylo	být	k5eAaImAgNnS
označení	označení	k1gNnSc1
části	část	k1gFnSc2
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
pod	pod	k7c7
arabskou	arabský	k2eAgFnSc7d1
nadvládou	nadvláda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
byl	být	k5eAaImAgInS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
Vandalů	Vandal	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
oblast	oblast	k1gFnSc4
osídlili	osídlit	k5eAaPmAgMnP
na	na	k7c6
přelomu	přelom	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nalezišť	naleziště	k1gNnPc2
v	v	k7c6
této	tento	k3xDgFnSc6
provincii	provincie	k1gFnSc6
byl	být	k5eAaImAgInS
pojmenován	pojmenovat	k5eAaPmNgInS
minerál	minerál	k1gInSc1
andalusit	andalusit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Al-Andalus	Al-Andalus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Mešita	mešita	k1gFnSc1
v	v	k7c6
Córdobě	Córdoba	k1gFnSc6
<g/>
,	,	kIx,
dochovaný	dochovaný	k2eAgInSc4d1
skvost	skvost	k1gInSc4
arabské	arabský	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
</s>
<s>
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
byla	být	k5eAaImAgFnS
už	už	k6eAd1
od	od	k7c2
nejstarších	starý	k2eAgFnPc2d3
dob	doba	k1gFnPc2
nejúrodnější	úrodný	k2eAgFnSc7d3
částí	část	k1gFnSc7
Pyrenejského	pyrenejský	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
s	s	k7c7
vyspělým	vyspělý	k2eAgNnSc7d1
zemědělstvím	zemědělství	k1gNnSc7
a	a	k8xC
městskými	městský	k2eAgNnPc7d1
středisky	středisko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starověku	starověk	k1gInSc6
byla	být	k5eAaImAgFnS
oblast	oblast	k1gFnSc1
osídlena	osídlit	k5eAaPmNgFnS
iberskými	iberský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
se	se	k3xPyFc4
nacházely	nacházet	k5eAaImAgFnP
obchodní	obchodní	k2eAgFnPc4d1
kolonie	kolonie	k1gFnPc4
Féničanů	Féničan	k1gMnPc2
–	–	k?
například	například	k6eAd1
Hyspalis	Hyspalis	k1gFnPc2
(	(	kIx(
<g/>
dnešní	dnešní	k2eAgFnSc1d1
Sevilla	Sevilla	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
Gades	Gades	k1gInSc1
(	(	kIx(
<g/>
Cádiz	Cádiz	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
v	v	k7c6
Andalusii	Andalusie	k1gFnSc6
ve	v	k7c6
zdejších	zdejší	k2eAgInPc6d1
bohatých	bohatý	k2eAgInPc6d1
dolech	dol	k1gInPc6
těžili	těžit	k5eAaImAgMnP
zlato	zlato	k1gNnSc4
a	a	k8xC
stříbro	stříbro	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
206	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
oblast	oblast	k1gFnSc4
ovládla	ovládnout	k5eAaPmAgFnS
Římská	římský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
197	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
.	.	kIx.
se	se	k3xPyFc4
v	v	k7c6
jejím	její	k3xOp3gInSc6
rámci	rámec	k1gInSc6
stala	stát	k5eAaPmAgFnS
oblast	oblast	k1gFnSc1
součástí	součást	k1gFnPc2
provincie	provincie	k1gFnSc2
Hispania	Hispanium	k1gNnSc2
Ulterior	Ulteriora	k1gFnPc2
<g/>
,	,	kIx,
po	po	k7c6
reorganizaci	reorganizace	k1gFnSc6
pak	pak	k6eAd1
roku	rok	k1gInSc2
14	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
jejím	její	k3xOp3gInSc6
rámci	rámec	k1gInSc6
provincie	provincie	k1gFnSc2
Hispania	Hispanium	k1gNnSc2
Baetica	Baeticum	k1gNnSc2
<g/>
,	,	kIx,
zahrnující	zahrnující	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
moderní	moderní	k2eAgFnSc2d1
Andalusie	Andalusie	k1gFnSc2
<g/>
,	,	kIx,
jih	jih	k1gInSc4
Extremadury	Extremadura	k1gFnSc2
<g/>
,	,	kIx,
jihozápad	jihozápad	k1gInSc4
Kastilie-La	Kastilie-L	k2eAgFnSc1d1
Mancha	Mancha	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
malou	malý	k2eAgFnSc4d1
část	část	k1gFnSc4
Portugalska	Portugalsko	k1gNnSc2
východně	východně	k6eAd1
od	od	k7c2
řeky	řeka	k1gFnSc2
Guadiana	Guadian	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
přelomu	přelom	k1gInSc6
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
oblast	oblast	k1gFnSc4
obsadili	obsadit	k5eAaPmAgMnP
germánští	germánský	k2eAgMnPc1d1
Vandalové	Vandal	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
roku	rok	k1gInSc2
429	#num#	k4
byla	být	k5eAaImAgFnS
začleněna	začlenit	k5eAaPmNgFnS
do	do	k7c2
Vizigótské	vizigótský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
6	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
pak	pak	k6eAd1
jihovýchodní	jihovýchodní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
Andalusie	Andalusie	k1gFnSc2
ovládla	ovládnout	k5eAaPmAgFnS
Byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
711	#num#	k4
pak	pak	k6eAd1
Andalusii	Andalusie	k1gFnSc4
ovládli	ovládnout	k5eAaPmAgMnP
Arabové	Arab	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
750	#num#	k4
se	se	k3xPyFc4
Andalusie	Andalusie	k1gFnSc1
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Córdobského	Córdobský	k2eAgInSc2d1
emirátu	emirát	k1gInSc2
(	(	kIx(
<g/>
později	pozdě	k6eAd2
Córdobský	Córdobský	k2eAgInSc1d1
chalífát	chalífát	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
významnými	významný	k2eAgMnPc7d1
centry	centr	k1gMnPc7
politického	politický	k2eAgInSc2d1
<g/>
,	,	kIx,
vědeckého	vědecký	k2eAgInSc2d1
i	i	k8xC
kulturního	kulturní	k2eAgInSc2d1
života	život	k1gInSc2
<g/>
,	,	kIx,
jakými	jaký	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
byly	být	k5eAaImAgFnP
Córdoba	Córdoba	k1gFnSc1
<g/>
,	,	kIx,
Sevilla	Sevilla	k1gFnSc1
<g/>
,	,	kIx,
Jaén	Jaén	k1gNnSc1
či	či	k8xC
Granada	Granada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Arabové	Arab	k1gMnPc1
však	však	k9
obohatili	obohatit	k5eAaPmAgMnP
i	i	k9
zdejší	zdejší	k2eAgNnSc4d1
zemědělství	zemědělství	k1gNnSc4
(	(	kIx(
<g/>
zavedli	zavést	k5eAaPmAgMnP
například	například	k6eAd1
pěstování	pěstování	k1gNnSc4
rýže	rýže	k1gFnSc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
subtropických	subtropický	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivá	jednotlivý	k2eAgNnPc4d1
města	město	k1gNnPc4
se	se	k3xPyFc4
po	po	k7c6
rozpadu	rozpad	k1gInSc6
Cordobského	Cordobský	k2eAgInSc2d1
chalifátu	chalifát	k1gInSc2
roku	rok	k1gInSc2
1031	#num#	k4
<g/>
,	,	kIx,
stala	stát	k5eAaPmAgFnS
středisky	středisko	k1gNnPc7
menších	malý	k2eAgInPc2d2
států	stát	k1gInPc2
(	(	kIx(
<g/>
taifas	taifasit	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
i	i	k9
nadále	nadále	k6eAd1
nesl	nést	k5eAaImAgInS
označení	označení	k1gNnSc4
Córdobský	Córdobský	k2eAgInSc1d1
emirát	emirát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
muslimů	muslim	k1gMnPc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Las	laso	k1gNnPc2
Navas	Navas	k1gInSc1
de	de	k?
Tolosa	Tolosa	k1gFnSc1
roku	rok	k1gInSc2
1212	#num#	k4
se	se	k3xPyFc4
Córdobského	Córdobský	k2eAgInSc2d1
emirátu	emirát	k1gInSc2
zmocnil	zmocnit	k5eAaPmAgInS
emirát	emirát	k1gInSc1
Granada	Granada	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
už	už	k9
roku	rok	k1gInSc2
1236	#num#	k4
ho	on	k3xPp3gInSc4
ovládla	ovládnout	k5eAaPmAgFnS
Kastilie	Kastilie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
roku	rok	k1gInSc2
1246	#num#	k4
ovládla	ovládnout	k5eAaPmAgFnS
i	i	k9
Jaén	Jaén	k1gNnSc4
a	a	k8xC
roku	rok	k1gInSc2
1248	#num#	k4
také	také	k6eAd1
Sevillu	Sevilla	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1462	#num#	k4
získala	získat	k5eAaPmAgFnS
Kastilie	Kastilie	k1gFnSc1
Gibraltar	Gibraltar	k1gInSc4
a	a	k8xC
roku	rok	k1gInSc2
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1492	#num#	k4
padla	padnout	k5eAaImAgFnS,k5eAaPmAgFnS
i	i	k9
Granada	Granada	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
zakončena	zakončen	k2eAgMnSc4d1
reconquista	reconquist	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
začalo	začít	k5eAaPmAgNnS
pronásledování	pronásledování	k1gNnSc1
a	a	k8xC
vyhánění	vyhánění	k1gNnSc1
muslimského	muslimský	k2eAgInSc2d1
(	(	kIx(
<g/>
Maurové	Maurové	k?
<g/>
,	,	kIx,
pokud	pokud	k8xS
přijali	přijmout	k5eAaPmAgMnP
křesťanství	křesťanství	k1gNnSc4
Moriskové	Moriskový	k2eAgFnSc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
židovského	židovský	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalšímu	další	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
správní	správní	k2eAgFnSc7d1
reformou	reforma	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
1833	#num#	k4
se	se	k3xPyFc4
pak	pak	k6eAd1
Andalusie	Andalusie	k1gFnSc1
členila	členit	k5eAaImAgFnS
na	na	k7c4
4	#num#	k4
provincie	provincie	k1gFnSc2
<g/>
:	:	kIx,
Království	království	k1gNnSc1
Córdoba	Córdoba	k1gFnSc1
<g/>
,	,	kIx,
Království	království	k1gNnSc1
Jaén	Jaéna	k1gFnPc2
<g/>
,	,	kIx,
Království	království	k1gNnSc4
Granada	Granada	k1gFnSc1
a	a	k8xC
Království	království	k1gNnSc1
Sevilla	Sevilla	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1833	#num#	k4
pak	pak	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
správní	správní	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
Andalusie	Andalusie	k1gFnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c6
současných	současný	k2eAgInPc6d1
8	#num#	k4
provincií	provincie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
federalistické	federalistický	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
První	první	k4xOgFnSc2
španělské	španělský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
na	na	k7c6
území	území	k1gNnSc6
Andalusie	Andalusie	k1gFnSc2
ustaveny	ustavit	k5eAaPmNgInP
spolkové	spolkový	k2eAgInPc1d1
státy	stát	k1gInPc1
Horní	horní	k2eAgFnSc2d1
Andalusie	Andalusie	k1gFnSc2
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Andalucía	Andalucí	k2eAgFnSc1d1
Alta	Alta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Dolní	dolní	k2eAgFnPc1d1
Andalusie	Andalusie	k1gFnPc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Andalucía	Andalucí	k2eAgFnSc1d1
Baja	Baja	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
kvůli	kvůli	k7c3
kolapsu	kolaps	k1gInSc3
republiky	republika	k1gFnSc2
nakonec	nakonec	k6eAd1
nebylo	být	k5eNaImAgNnS
realizováno	realizovat	k5eAaBmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
pádu	pád	k1gInSc6
diktátorského	diktátorský	k2eAgInSc2d1
režimu	režim	k1gInSc2
generála	generál	k1gMnSc4
Francisca	Franciscus	k1gMnSc4
Franca	Franca	k?
a	a	k8xC
obnovení	obnovení	k1gNnSc1
demokracie	demokracie	k1gFnSc2
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
Andalusie	Andalusie	k1gFnSc1
koncem	koncem	k7c2
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
rozsáhlou	rozsáhlý	k2eAgFnSc4d1
autonomii	autonomie	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
bylo	být	k5eAaImAgNnS
11	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1982	#num#	k4
potvrzeno	potvrdit	k5eAaPmNgNnS
schválením	schválení	k1gNnSc7
autonomního	autonomní	k2eAgInSc2d1
statutu	statut	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
Andalusie	Andalusie	k1gFnSc2
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
hraničí	hraničit	k5eAaImIp3nS
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
Extremadurou	Extremadura	k1gFnSc7
a	a	k8xC
Kastilií	Kastilie	k1gFnSc7
–	–	k?
La	la	k1gNnSc7
Manchou	Mancha	k1gMnSc7
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
s	s	k7c7
Murcií	Murcie	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
se	s	k7c7
Středozemním	středozemní	k2eAgNnSc7d1
mořem	moře	k1gNnSc7
a	a	k8xC
Gibraltarem	Gibraltar	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
s	s	k7c7
Atlantským	atlantský	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
s	s	k7c7
Portugalskem	Portugalsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
sever	sever	k1gInSc4
Andalusie	Andalusie	k1gFnSc2
zasahuje	zasahovat	k5eAaImIp3nS
pohoří	pohoří	k1gNnSc1
Sierra	Sierra	k1gFnSc1
Morena	Morena	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
Sierra	Sierra	k1gFnSc1
de	de	k?
Segura	Segura	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
pak	pak	k6eAd1
pohoří	pohoří	k1gNnSc1
Cordillera	Cordiller	k1gMnSc2
Penibética	Penibéticus	k1gMnSc2
se	s	k7c7
Sierrou	Sierra	k1gFnSc7
Nevadou	Nevada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
řeky	řeka	k1gFnSc2
Guadalquivir	Guadalquivira	k1gFnPc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
nížina	nížina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přirozenou	přirozený	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Kastilií	Kastilie	k1gFnSc7
–	–	k?
La	la	k1gNnSc1
Manchou	Mancha	k1gMnSc7
tvoří	tvořit	k5eAaImIp3nS
průsmyk	průsmyk	k1gInSc4
Despeñ	Despeñ	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Alhambra	Alhambra	k1gFnSc1
<g/>
,	,	kIx,
Granada	Granada	k1gFnSc1
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1833	#num#	k4
člení	členit	k5eAaImIp3nS
na	na	k7c4
8	#num#	k4
provincií	provincie	k1gFnPc2
zavedených	zavedený	k2eAgFnPc2d1
Javierem	Javier	k1gInSc7
de	de	k?
Burgos	Burgos	k1gInSc1
<g/>
:	:	kIx,
Almería	Almería	k1gFnSc1
<g/>
,	,	kIx,
Cádiz	Cádiz	k1gInSc1
<g/>
,	,	kIx,
Córdoba	Córdoba	k1gFnSc1
<g/>
,	,	kIx,
Granada	Granada	k1gFnSc1
<g/>
,	,	kIx,
Huelva	Huelva	k1gFnSc1
<g/>
,	,	kIx,
Jaén	Jaén	k1gNnSc1
<g/>
,	,	kIx,
Málaga	Málaga	k1gFnSc1
<g/>
,	,	kIx,
Sevilla	Sevilla	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Andaluské	andaluský	k2eAgFnPc1d1
provincie	provincie	k1gFnPc1
</s>
<s>
ProvincieObyvatelstvoRozloha	ProvincieObyvatelstvoRozloha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
<g/>
ObcíSoudních	ObcíSoudní	k2eAgInPc2d1
okresů	okres	k1gInPc2
</s>
<s>
Almería	Almería	k6eAd1
<g/>
667	#num#	k4
6358	#num#	k4
7741028	#num#	k4
</s>
<s>
Cádiz	Cádiz	k1gInSc1
<g/>
1	#num#	k4
220	#num#	k4
4677	#num#	k4
4364414	#num#	k4
</s>
<s>
Córdoba	Córdoba	k1gFnSc1
<g/>
798	#num#	k4
82213	#num#	k4
5507512	#num#	k4
</s>
<s>
Granada	Granada	k1gFnSc1
<g/>
901	#num#	k4
22012	#num#	k4
5311689	#num#	k4
</s>
<s>
Huelva	Huelva	k6eAd1
<g/>
507	#num#	k4
91510	#num#	k4
148796	#num#	k4
</s>
<s>
Jaén	Jaén	k1gInSc1
<g/>
667	#num#	k4
43813	#num#	k4
4899710	#num#	k4
</s>
<s>
Málaga	Málaga	k1gFnSc1
<g/>
1	#num#	k4
563	#num#	k4
2617	#num#	k4
30810111	#num#	k4
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
<g/>
1	#num#	k4
875	#num#	k4
46214	#num#	k4
04210515	#num#	k4
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
Andalusie	Andalusie	k1gFnSc2
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
je	být	k5eAaImIp3nS
známá	známý	k2eAgFnSc1d1
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
maurskou	maurský	k2eAgFnSc4d1
architekturu	architektura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
stavbám	stavba	k1gFnPc3
patří	patřit	k5eAaImIp3nP
paláce	palác	k1gInSc2
Alhambra	Alhambr	k1gInSc2
v	v	k7c6
Granadě	Granada	k1gFnSc6
<g/>
,	,	kIx,
mešita	mešita	k1gFnSc1
v	v	k7c6
Córdobě	Córdoba	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
věže	věž	k1gFnPc1
Torre	torr	k1gInSc5
del	del	k?
Oro	Oro	k1gMnSc2
a	a	k8xC
Giralda	Girald	k1gMnSc2
v	v	k7c6
Seville	Sevilla	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poblíž	poblíž	k7c2
Córdoby	Córdoba	k1gFnSc2
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
ruiny	ruina	k1gFnPc1
Medina	Medina	k1gFnSc1
Azahara	Azahara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Sevilla	Sevilla	k1gFnSc1
</s>
<s>
Málaga	Málaga	k1gFnSc1
</s>
<s>
Córdoba	Córdoba	k1gFnSc1
</s>
<s>
Granada	Granada	k1gFnSc1
</s>
<s>
Cádiz	Cádiz	k1gInSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
hustoty	hustota	k1gFnSc2
osídlení	osídlení	k1gNnSc2
Andalusie	Andalusie	k1gFnSc2
(	(	kIx(
<g/>
dle	dle	k7c2
obcí	obec	k1gFnPc2
<g/>
)	)	kIx)
ukazuje	ukazovat	k5eAaImIp3nS
koncentraci	koncentrace	k1gFnSc4
obyvatelstva	obyvatelstvo	k1gNnSc2
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
a	a	k8xC
v	v	k7c6
nížinách	nížina	k1gFnPc6
řeky	řeka	k1gFnSc2
Guadalquivir	Guadalquivir	k1gInSc1
a	a	k8xC
vylidnění	vylidnění	k1gNnSc1
vnitrozemských	vnitrozemský	k2eAgFnPc2d1
hraničních	hraniční	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
</s>
<s>
Vývoj	vývoj	k1gInSc1
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
Andalusie	Andalusie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1787	#num#	k4
</s>
<s>
1842	#num#	k4
</s>
<s>
1860	#num#	k4
</s>
<s>
1887	#num#	k4
</s>
<s>
1900	#num#	k4
</s>
<s>
1910	#num#	k4
</s>
<s>
1920	#num#	k4
</s>
<s>
1930	#num#	k4
</s>
<s>
1850	#num#	k4
157	#num#	k4
</s>
<s>
2300	#num#	k4
020	#num#	k4
</s>
<s>
2965508	#num#	k4
</s>
<s>
3380	#num#	k4
846	#num#	k4
</s>
<s>
3544769	#num#	k4
</s>
<s>
3800	#num#	k4
299	#num#	k4
</s>
<s>
4221686	#num#	k4
</s>
<s>
4627148	#num#	k4
</s>
<s>
1940	#num#	k4
</s>
<s>
1950	#num#	k4
</s>
<s>
1960	#num#	k4
</s>
<s>
1970	#num#	k4
</s>
<s>
1981	#num#	k4
</s>
<s>
1991	#num#	k4
</s>
<s>
2001	#num#	k4
</s>
<s>
2010	#num#	k4
</s>
<s>
5255120	#num#	k4
</s>
<s>
5647244	#num#	k4
</s>
<s>
5940	#num#	k4
047	#num#	k4
</s>
<s>
5991	#num#	k4
076	#num#	k4
</s>
<s>
6440	#num#	k4
985	#num#	k4
</s>
<s>
6940	#num#	k4
522	#num#	k4
</s>
<s>
7357558	#num#	k4
</s>
<s>
8370	#num#	k4
975	#num#	k4
</s>
<s>
Protože	protože	k8xS
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
sloužil	sloužit	k5eAaImAgInS
přístav	přístav	k1gInSc1
Sevilla	Sevilla	k1gFnSc1
jako	jako	k8xS,k8xC
vstupní	vstupní	k2eAgFnSc1d1
brána	brána	k1gFnSc1
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
obsahuje	obsahovat	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
španělština	španělština	k1gFnSc1
hodně	hodně	k6eAd1
rysů	rys	k1gInPc2
andaluského	andaluský	k2eAgInSc2d1
dialektu	dialekt	k1gInSc2
španělštiny	španělština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Předsedové	předseda	k1gMnPc1
vlád	vláda	k1gFnPc2
Andalusie	Andalusie	k1gFnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1978	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1979	#num#	k4
Plácido	Plácida	k1gFnSc5
Fernández	Fernández	k1gMnSc1
Viagas	Viagas	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1979	#num#	k4
–	–	k?
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1984	#num#	k4
Rafael	Rafaela	k1gFnPc2
Escuredo	Escuredo	k1gNnSc1
Rodríguez	Rodríguez	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1984	#num#	k4
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1990	#num#	k4
José	José	k1gNnPc2
Rodríguez	Rodrígueza	k1gFnPc2
de	de	k?
la	la	k1gNnSc1
Borbolla	Borbolla	k1gMnSc1
y	y	k?
Camoyán	Camoyán	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1990	#num#	k4
–	–	k?
duben	duben	k1gInSc1
2009	#num#	k4
Manuel	Manuel	k1gMnSc1
María	María	k1gMnSc1
Cháves	Cháves	k1gMnSc1
González	González	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
</s>
<s>
od	od	k7c2
dubna	duben	k1gInSc2
2009	#num#	k4
José	José	k1gNnPc2
Antonio	Antonio	k1gMnSc1
Griñ	Griñ	k1gMnSc1
(	(	kIx(
<g/>
PSOE	PSOE	kA
<g/>
)	)	kIx)
</s>
<s>
Znak	znak	k1gInSc1
Andalusie	Andalusie	k1gFnSc2
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
má	mít	k5eAaImIp3nS
znak	znak	k1gInSc1
<g/>
,	,	kIx,
přijatý	přijatý	k2eAgInSc1d1
usnesením	usnesení	k1gNnSc7
shromáždění	shromáždění	k1gNnSc1
autonomistů	autonomista	k1gMnPc2
v	v	k7c6
Rondě	ronda	k1gFnSc6
1918	#num#	k4
<g/>
:	:	kIx,
En	En	k1gFnSc1
campo	campa	k1gFnSc5
de	de	k?
sinople	sinople	k6eAd1
<g/>
,	,	kIx,
una	una	k?
faja	fajum	k1gNnSc2
de	de	k?
plata	plato	k1gNnSc2
<g/>
,	,	kIx,
y	y	k?
brochante	brochant	k1gMnSc5
sobre	sobr	k1gMnSc5
todo	toda	k1gMnSc5
la	la	k1gNnSc2
figura	figura	k1gFnSc1
de	de	k?
Hércules	Hércules	k1gInSc1
prominente	prominent	k1gMnSc5
entre	entr	k1gInSc5
dos	dos	k?
columnas	columnas	k1gInSc1
de	de	k?
plata	plato	k1gNnSc2
con	con	k?
capiteles	capiteles	k1gMnSc1
de	de	k?
oro	oro	k?
<g/>
,	,	kIx,
sujetando	sujetando	k6eAd1
con	con	k?
sus	sus	k?
manos	manosa	k1gFnPc2
y	y	k?
<g />
.	.	kIx.
</s>
<s hack="1">
por	por	k?
las	laso	k1gNnPc2
melenas	melenas	k1gInSc1
a	a	k8xC
dos	dos	k?
leones	leones	k1gInSc1
al	ala	k1gFnPc2
natural	natural	k?
<g/>
,	,	kIx,
y	y	k?
sobre	sobr	k1gMnSc5
las	laso	k1gNnPc2
dos	dos	k?
columnas	columnas	k1gMnSc1
un	un	k?
arco	arco	k1gMnSc1
de	de	k?
medio	medio	k1gMnSc1
punto	punto	k1gNnSc1
cargado	cargada	k1gFnSc5
con	con	k?
las	laso	k1gNnPc2
palabras	palabras	k1gMnSc1
latinas	latinas	k1gMnSc1
"	"	kIx"
<g/>
Dominator	Dominator	k1gMnSc1
Hercules	Hercules	k1gMnSc1
Fundator	Fundator	k1gMnSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
en	en	k?
punta	punto	k1gNnSc2
<g/>
,	,	kIx,
la	la	k1gNnSc1
leyenda	leyend	k1gMnSc2
en	en	k?
letras	letras	k1gMnSc1
de	de	k?
oro	oro	k?
"	"	kIx"
<g/>
Andalucía	Andalucía	k1gMnSc1
por	por	k?
sí	sí	k?
<g/>
,	,	kIx,
para	para	k2eAgMnSc1d1
Españ	Españ	k1gMnSc1
y	y	k?
la	la	k1gNnPc2
Humanidad	Humanidad	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
V	v	k7c6
zeleném	zelený	k2eAgNnSc6d1
poli	pole	k1gNnSc6
stříbrné	stříbrná	k1gFnSc2
břevno	břevno	k1gNnSc4
<g/>
,	,	kIx,
přese	přese	k7c4
vše	všechen	k3xTgNnSc4
postava	postava	k1gFnSc1
Herkula	Herkul	k1gMnSc2
zepředu	zepředu	k6eAd1
mezi	mezi	k7c7
dvěma	dva	k4xCgInPc7
stříbrnými	stříbrný	k2eAgInPc7d1
sloupy	sloup	k1gInPc7
se	s	k7c7
zlatými	zlatý	k2eAgFnPc7d1
hlavicemi	hlavice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
něj	on	k3xPp3gInSc2
leží	ležet	k5eAaImIp3nP
dva	dva	k4xCgMnPc1
přirození	přirozený	k2eAgMnPc1d1
lvi	lev	k1gMnPc1
<g/>
,	,	kIx,
sloupy	sloup	k1gInPc1
jsou	být	k5eAaImIp3nP
spojeny	spojit	k5eAaPmNgInP
obloukem	oblouk	k1gInSc7
s	s	k7c7
nápisem	nápis	k1gInSc7
DOMINATOR	DOMINATOR	kA
HÉRCULES	HÉRCULES	kA
FUNDATOR	FUNDATOR	kA
<g/>
,	,	kIx,
dole	dole	k6eAd1
zlatý	zlatý	k2eAgInSc1d1
nápis	nápis	k1gInSc1
ANDALÚCIA	ANDALÚCIA	kA
POR	POR	kA
SÍ	SÍ	kA
<g/>
,	,	kIx,
PARA	para	k2eAgInPc4d1
ESPAŇA	ESPAŇA	kA
Y	Y	kA
LA	la	k1gNnPc2
HUMANITAD	HUMANITAD	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
fakticky	fakticky	k6eAd1
je	být	k5eAaImIp3nS
nápis	nápis	k1gInSc1
ve	v	k7c6
třech	tři	k4xCgInPc6
řádcích	řádek	k1gInPc6
na	na	k7c6
panelu	panel	k1gInSc6
<g/>
,	,	kIx,
opakujícím	opakující	k2eAgInSc6d1
barvy	barva	k1gFnPc4
štítu	štít	k1gInSc2
<g/>
,	,	kIx,
kolem	kolem	k6eAd1
se	s	k7c7
zlatou	zlatý	k2eAgFnSc7d1
kartuší	kartuš	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
se	se	k3xPyFc4
znak	znak	k1gInSc1
užívá	užívat	k5eAaImIp3nS
bez	bez	k7c2
štítu	štít	k1gInSc2
jako	jako	k8xC,k8xS
odznak	odznak	k1gInSc4
nebo	nebo	k8xC
v	v	k7c6
identicky	identicky	k6eAd1
zbarveném	zbarvený	k2eAgInSc6d1
listu	list	k1gInSc6
vlajky	vlajka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS
i	i	k9
heraldicky	heraldicky	k6eAd1
upravená	upravený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
znaku	znak	k1gInSc2
s	s	k7c7
modrým	modrý	k2eAgInSc7d1
štítem	štít	k1gInSc7
a	a	k8xC
zlatými	zlatý	k2eAgFnPc7d1
literami	litera	k1gFnPc7
–	–	k?
jde	jít	k5eAaImIp3nS
zřejmě	zřejmě	k6eAd1
o	o	k7c4
návrh	návrh	k1gInSc4
Vicenta	Vicento	k1gNnSc2
Cadenase	Cadenas	k1gInSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
verze	verze	k1gFnPc1
znaku	znak	k1gInSc2
ukazují	ukazovat	k5eAaImIp3nP
Herkula	Herkul	k1gMnSc4
–	–	k?
jeho	jeho	k3xOp3gInPc4
atributy	atribut	k1gInPc4
jsou	být	k5eAaImIp3nP
svalnaté	svalnatý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
a	a	k8xC
lví	lví	k2eAgFnSc2d1
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
je	být	k5eAaImIp3nS
oděn	oděn	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloupy	sloup	k1gInPc7
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
důležité	důležitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
slavné	slavný	k2eAgInPc1d1
Herkulovy	Herkulův	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
také	také	k9
objevují	objevovat	k5eAaImIp3nP
na	na	k7c6
státním	státní	k2eAgInSc6d1
znaku	znak	k1gInSc6
a	a	k8xC
vlajce	vlajka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andalusie	Andalusie	k1gFnSc1
je	být	k5eAaImIp3nS
region	region	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
vládne	vládnout	k5eAaImIp3nS
úžině	úžina	k1gFnSc3
Gibraltarské	gibraltarský	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibraltarská	gibraltarský	k2eAgFnSc1d1
skála	skála	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
jejím	její	k3xOp3gInSc7
protipólem	protipól	k1gInSc7
za	za	k7c7
úžinou	úžina	k1gFnSc7
<g/>
,	,	kIx,
Džebel	Džebel	k1gInSc1
al	ala	k1gFnPc2
Musa	Musa	k1gFnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
známy	znám	k2eAgInPc1d1
ve	v	k7c6
starověku	starověk	k1gInSc6
právě	právě	k6eAd1
jako	jako	k8xC,k8xS
Herkulovy	Herkulův	k2eAgInPc1d1
sloupy	sloup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc4
kvůli	kvůli	k7c3
báji	báj	k1gFnSc3
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Herkules	Herkules	k1gMnSc1
zde	zde	k6eAd1
plnil	plnit	k5eAaImAgMnS
další	další	k2eAgInSc4d1
z	z	k7c2
úkolů	úkol	k1gInPc2
při	při	k7c6
získáváni	získávat	k5eAaImNgMnP
zlatých	zlatý	k2eAgFnPc2d1
jablek	jablko	k1gNnPc2
Hesperidek	Hesperidka	k1gFnPc2
–	–	k?
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgInS
průliv	průliv	k1gInSc1
<g/>
,	,	kIx,
spojující	spojující	k2eAgFnSc1d1
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
s	s	k7c7
oceánem	oceán	k1gInSc7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
prorazil	prorazit	k5eAaPmAgInS
mohutnou	mohutný	k2eAgFnSc4d1
skálu	skála	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
tarasila	tarasit	k5eAaImAgFnS
výjezd	výjezd	k1gInSc4
ze	z	k7c2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
skálu	skála	k1gFnSc4
pak	pak	k6eAd1
vztyčil	vztyčit	k5eAaPmAgMnS
po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
úžiny	úžina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Cadiz	Cadiza	k1gFnPc2
<g/>
,	,	kIx,
od	od	k7c2
jehož	jenž	k3xRgInSc2,k3xOyRp3gInSc2
znaku	znak	k1gInSc2
je	být	k5eAaImIp3nS
krajský	krajský	k2eAgInSc1d1
znak	znak	k1gInSc1
odvozen	odvodit	k5eAaPmNgInS
<g/>
,	,	kIx,
nazývá	nazývat	k5eAaImIp3nS
Herkula	Herkul	k1gMnSc4
zakladatelem	zakladatel	k1gMnSc7
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Znak	znak	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
článkem	článek	k1gInSc7
6.2	6.2	k4
Autonomního	autonomní	k2eAgInSc2d1
statutu	statut	k1gInSc2
Andalusie	Andalusie	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1981	#num#	k4
schválen	schválit	k5eAaPmNgInS
prvním	první	k4xOgInSc7
článkem	článek	k1gInSc7
(	(	kIx(
<g/>
artículo	artícula	k1gFnSc5
primero	primera	k1gFnSc5
<g/>
)	)	kIx)
zákona	zákon	k1gInSc2
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
z	z	k7c2
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
O	o	k7c6
hymně	hymna	k1gFnSc6
a	a	k8xC
znaku	znak	k1gInSc6
Andalusie	Andalusie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
potvrzen	potvrdit	k5eAaPmNgInS
i	i	k9
novým	nový	k2eAgInSc7d1
autonomním	autonomní	k2eAgInSc7d1
statutem	statut	k1gInSc7
z	z	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autorem	autor	k1gMnSc7
koncepce	koncepce	k1gFnSc2
znaku	znak	k1gInSc2
(	(	kIx(
<g/>
či	či	k8xC
spíše	spíše	k9
emblému	emblém	k1gInSc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
vůdce	vůdce	k1gMnSc1
andaluských	andaluský	k2eAgMnPc2d1
autonomistů	autonomista	k1gMnPc2
Blaz	Blaz	k1gMnSc1
Infante	infant	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
Instituto	Institut	k2eAgNnSc1d1
de	de	k?
Estadistica	Estadistic	k2eAgMnSc4d1
de	de	k?
Andalucía	Andalucíus	k1gMnSc4
(	(	kIx(
<g/>
IEA	IEA	kA
<g/>
)	)	kIx)
<g/>
↑	↑	k?
DE	DE	k?
CADENAS	CADENAS	kA
<g/>
,	,	kIx,
Ampelio	Ampelio	k6eAd1
Alonso	Alonsa	k1gFnSc5
<g/>
;	;	kIx,
DE	DE	k?
CADENAS	CADENAS	kA
<g/>
,	,	kIx,
Vicente	Vicent	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heráldica	Heráldica	k1gFnSc1
de	de	k?
las	laso	k1gNnPc2
Comunidades	Comunidades	k1gMnSc1
Autónomas	Autónomas	k1gMnSc1
y	y	k?
de	de	k?
las	laso	k1gNnPc2
capitales	capitales	k1gInSc1
de	de	k?
provincia	provincia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Madrid	Madrid	k1gInSc1
<g/>
:	:	kIx,
Ediciones	Ediciones	k1gMnSc1
Hidalgía	Hidalgía	k1gMnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
84	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6040	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Region	region	k1gInSc1
Granada	Granada	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Andalusie	Andalusie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Andalusie	Andalusie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Andalusie	Andalusie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
na	na	k7c4
OpenStreetMap	OpenStreetMap	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
regionální	regionální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Stručný	stručný	k2eAgInSc4d1
popis	popis	k1gInSc4
Andalusie	Andalusie	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gFnPc2
památek	památka	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Chráněné	chráněný	k2eAgFnSc3d1
přírodní	přírodní	k2eAgFnSc3d1
oblasti	oblast	k1gFnSc3
Andalusie	Andalusie	k1gFnSc2
<g/>
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
originálu	originál	k1gInSc2
andaluského	andaluský	k2eAgInSc2d1
znaku	znak	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	být	k5eAaImIp3nS
navrhl	navrhnout	k5eAaPmAgMnS
Blaz	Blaz	k1gInSc4
Infante	infant	k1gMnSc5
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc1
historie	historie	k1gFnSc1
a	a	k8xC
text	text	k1gInSc1
autonomní	autonomní	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
regionální	regionální	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Junta	junta	k1gFnSc1
de	de	k?
Andalusía	Andalusía	k1gFnSc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Španělsko	Španělsko	k1gNnSc1
–	–	k?
Españ	Españ	k1gInSc2
–	–	k?
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgMnSc1d1
společenstvía	společenstvía	k1gMnSc1
jejich	jejich	k3xOp3gNnPc4
správní	správní	k2eAgNnPc4d1
centra	centrum	k1gNnPc4
</s>
<s>
Andalusie	Andalusie	k1gFnSc1
(	(	kIx(
<g/>
Sevilla	Sevilla	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Aragonie	Aragonie	k1gFnSc1
(	(	kIx(
<g/>
Zaragoza	Zaragoza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Asturie	Asturie	k1gFnSc1
(	(	kIx(
<g/>
Oviedo	Oviedo	k1gNnSc1
–	–	k?
Uviéu	Uviéus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Baleáry	Baleáry	k1gFnPc1
(	(	kIx(
<g/>
Palma	palma	k1gFnSc1
de	de	k?
Mallorca	Mallorca	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Baskicko	Baskicko	k1gNnSc1
(	(	kIx(
<g/>
Vitoria-Gasteiz	Vitoria-Gasteiz	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Extremadura	Extremadura	k1gFnSc1
(	(	kIx(
<g/>
Mérida	Mérida	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Galicie	Galicie	k1gFnSc1
(	(	kIx(
<g/>
Santiago	Santiago	k1gNnSc1
de	de	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Compostela	Compostela	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
(	(	kIx(
<g/>
Las	laso	k1gNnPc2
Palmas	Palmasa	k1gFnPc2
de	de	k?
Gran	Gran	k1gNnSc1
Canaria	Canarium	k1gNnSc2
a	a	k8xC
Santa	Santa	k1gMnSc1
Cruz	Cruz	k1gMnSc1
de	de	k?
Tenerife	Tenerif	k1gInSc5
<g/>
)	)	kIx)
•	•	k?
Kantábrie	Kantábrie	k1gFnSc1
(	(	kIx(
<g/>
Santander	Santander	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kastilie	Kastilie	k1gFnSc1
a	a	k8xC
León	León	k1gInSc1
(	(	kIx(
<g/>
Valladolid	Valladolid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kastilie	Kastilie	k1gFnSc1
–	–	k?
La	la	k1gNnSc1
Mancha	Mancha	k1gFnSc1
(	(	kIx(
<g/>
Toledo	Toledo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Katalánsko	Katalánsko	k1gNnSc1
(	(	kIx(
<g/>
Barcelona	Barcelona	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
La	la	k1gNnSc1
Rioja	Rioja	k1gMnSc1
(	(	kIx(
<g/>
Logroñ	Logroñ	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Madrid	Madrid	k1gInSc1
(	(	kIx(
<g/>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Murcie	Murcie	k1gFnSc1
(	(	kIx(
<g/>
Murcia	Murcia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Navarra	Navarra	k1gFnSc1
(	(	kIx(
<g/>
Pamplona	Pamplona	k1gFnSc1
–	–	k?
Iruñ	Iruñ	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Valencie	Valencie	k1gFnSc1
(	(	kIx(
<g/>
Valencie	Valencie	k1gFnSc1
–	–	k?
Valè	Valè	k1gNnPc1
<g/>
)	)	kIx)
Autonomní	autonomní	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Ceuta	Ceuta	k1gFnSc1
•	•	k?
Melilla	Melilla	k1gFnSc1
Přímá	přímý	k2eAgFnSc1d1
správa	správa	k1gFnSc1
ústřední	ústřední	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
</s>
<s>
Španělské	španělský	k2eAgFnPc1d1
severoafrické	severoafrický	k2eAgFnPc1d1
državy	država	k1gFnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128663	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4001909-3	4001909-3	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82008465	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
147299881	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82008465	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
