<s>
Muchomůrka	Muchomůrka	k?
růžovka	růžovka	k1gFnSc1
(	(	kIx(
<g/>
Amanita	Amanita	k1gFnSc1
rubescens	rubescensa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
často	často	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
jen	jen	k9
růžovka	růžovka	k1gFnSc1
či	či	k8xC
masák	masák	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výtečná	výtečný	k2eAgFnSc1d1
jedlá	jedlý	k2eAgFnSc1d1
houba	houba	k1gFnSc1
z	z	k7c2
čeledi	čeleď	k1gFnSc2
muchomůrkovitých	muchomůrkovitý	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>