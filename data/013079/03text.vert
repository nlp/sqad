<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
růžovka	růžovka	k1gFnSc1	růžovka
(	(	kIx(	(
<g/>
Amanita	Amanita	k1gFnSc1	Amanita
rubescens	rubescensa	k1gFnPc2	rubescensa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
jen	jen	k9	jen
růžovka	růžovka	k1gFnSc1	růžovka
či	či	k8xC	či
masák	masák	k1gInSc1	masák
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
výtečná	výtečný	k2eAgFnSc1d1	výtečná
jedlá	jedlý	k2eAgFnSc1d1	jedlá
houba	houba	k1gFnSc1	houba
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
muchomůrkovitých	muchomůrkovitý	k2eAgMnPc2d1	muchomůrkovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
odrůda	odrůda	k1gFnSc1	odrůda
této	tento	k3xDgFnSc2	tento
houby	houba	k1gFnSc2	houba
se	s	k7c7	s
žlutým	žlutý	k2eAgInSc7d1	žlutý
prstenem	prsten	k1gInSc7	prsten
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
klasifikována	klasifikován	k2eAgFnSc1d1	klasifikována
jako	jako	k8xS	jako
Amanita	Amanit	k2eAgFnSc1d1	Amanita
rubescens	rubescens	k1gInSc1	rubescens
var.	var.	k?	var.
annulosulphurea	annulosulphurea	k1gFnSc1	annulosulphurea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
==	==	k?	==
</s>
</p>
<p>
<s>
Klobouk	klobouk	k1gInSc1	klobouk
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
5-15	[number]	k4	5-15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
je	být	k5eAaImIp3nS	být
polokulovitý	polokulovitý	k2eAgInSc1d1	polokulovitý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
sklenutý	sklenutý	k2eAgMnSc1d1	sklenutý
až	až	k8xS	až
rozložený	rozložený	k2eAgMnSc1d1	rozložený
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
červenavě	červenavě	k6eAd1	červenavě
hnědý	hnědý	k2eAgInSc1d1	hnědý
<g/>
,	,	kIx,	,
řidčeji	řídce	k6eAd2	řídce
bělavý	bělavý	k2eAgInSc4d1	bělavý
<g/>
,	,	kIx,	,
žlutohnědý	žlutohnědý	k2eAgInSc4d1	žlutohnědý
nebo	nebo	k8xC	nebo
červenavě	červenavě	k6eAd1	červenavě
šedý	šedý	k2eAgInSc1d1	šedý
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
snadno	snadno	k6eAd1	snadno
stíratelnými	stíratelný	k2eAgInPc7d1	stíratelný
<g/>
,	,	kIx,	,
špinavě	špinavě	k6eAd1	špinavě
bílými	bílý	k2eAgInPc7d1	bílý
až	až	k8xS	až
šedorůžovými	šedorůžový	k2eAgInPc7d1	šedorůžový
zbytky	zbytek	k1gInPc7	zbytek
plachetky	plachetka	k1gFnSc2	plachetka
<g/>
.	.	kIx.	.
</s>
<s>
Lupeny	lupen	k1gInPc1	lupen
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
červenavě	červenavě	k6eAd1	červenavě
skvrnité	skvrnitý	k2eAgInPc1d1	skvrnitý
<g/>
;	;	kIx,	;
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
u	u	k7c2	u
třeně	třeň	k1gInSc2	třeň
volné	volný	k2eAgNnSc1d1	volné
<g/>
.	.	kIx.	.
</s>
<s>
Třeň	třeň	k1gInSc1	třeň
je	být	k5eAaImIp3nS	být
bělavý	bělavý	k2eAgInSc1d1	bělavý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
krytý	krytý	k2eAgMnSc1d1	krytý
růžovými	růžový	k2eAgFnPc7d1	růžová
skvrnkami	skvrnka	k1gFnPc7	skvrnka
<g/>
,	,	kIx,	,
k	k	k7c3	k
bázi	báze	k1gFnSc3	báze
stejnoměrně	stejnoměrně	k6eAd1	stejnoměrně
ztlustlý	ztlustlý	k2eAgInSc1d1	ztlustlý
<g/>
,	,	kIx,	,
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
s	s	k7c7	s
bradavičnatou	bradavičnatý	k2eAgFnSc7d1	bradavičnatá
širokou	široký	k2eAgFnSc7d1	široká
hlízou	hlíza	k1gFnSc7	hlíza
<g/>
.	.	kIx.	.
</s>
<s>
Prsten	prsten	k1gInSc1	prsten
je	být	k5eAaImIp3nS	být
převislý	převislý	k2eAgInSc1d1	převislý
<g/>
,	,	kIx,	,
široký	široký	k2eAgInSc1d1	široký
<g/>
,	,	kIx,	,
podobný	podobný	k2eAgInSc1d1	podobný
sukni	sukně	k1gFnSc3	sukně
<g/>
,	,	kIx,	,
vyrůstající	vyrůstající	k2eAgFnSc1d1	vyrůstající
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
třeně	třeň	k1gInSc2	třeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
je	být	k5eAaImIp3nS	být
prsten	prsten	k1gInSc1	prsten
přitisknut	přitisknout	k5eAaPmNgInS	přitisknout
k	k	k7c3	k
lupenům	lupen	k1gInPc3	lupen
<g/>
,	,	kIx,	,
od	od	k7c2	od
nichž	jenž	k3xRgInPc2	jenž
získává	získávat	k5eAaImIp3nS	získávat
rýhování	rýhování	k1gNnSc1	rýhování
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
u	u	k7c2	u
A.	A.	kA	A.
rubescens	rubescens	k1gInSc1	rubescens
var.	var.	k?	var.
annulosuphurea	annulosuphurea	k6eAd1	annulosuphurea
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Dužnina	dužnina	k1gFnSc1	dužnina
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
pomalu	pomalu	k6eAd1	pomalu
červenající	červenající	k2eAgFnSc1d1	červenající
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
slabou	slabý	k2eAgFnSc4d1	slabá
nenápadnou	nápadný	k2eNgFnSc4d1	nenápadná
vůni	vůně	k1gFnSc4	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Výtrusný	výtrusný	k2eAgInSc1d1	výtrusný
prach	prach	k1gInSc1	prach
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Pochva	pochva	k1gFnSc1	pochva
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Možnost	možnost	k1gFnSc1	možnost
záměny	záměna	k1gFnSc2	záměna
==	==	k?	==
</s>
</p>
<p>
<s>
Růžovku	růžovka	k1gFnSc4	růžovka
lze	lze	k6eAd1	lze
splést	splést	k5eAaPmF	splést
s	s	k7c7	s
prudce	prudko	k6eAd1	prudko
jedovatou	jedovatý	k2eAgFnSc7d1	jedovatá
muchomůrkou	muchomůrkou	k?	muchomůrkou
tygrovanou	tygrovaný	k2eAgFnSc7d1	tygrovaná
(	(	kIx(	(
<g/>
panterovou	panterův	k2eAgFnSc7d1	panterova
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Určit	určit	k5eAaPmF	určit
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
každý	každý	k3xTgInSc1	každý
exemplář	exemplář	k1gInSc1	exemplář
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
muchomůrky	muchomůrky	k?	muchomůrky
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k1gInSc4	růst
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgInPc1d1	hlavní
rozlišovací	rozlišovací	k2eAgInPc1d1	rozlišovací
body	bod	k1gInPc1	bod
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prsten	prsten	k1gInSc1	prsten
u	u	k7c2	u
růžovky	růžovka	k1gFnSc2	růžovka
je	být	k5eAaImIp3nS	být
vroubkovaný	vroubkovaný	k2eAgMnSc1d1	vroubkovaný
<g/>
,	,	kIx,	,
u	u	k7c2	u
muchomůrky	muchomůrky	k?	muchomůrky
tygrovité	tygrovitý	k2eAgInPc4d1	tygrovitý
je	být	k5eAaImIp3nS	být
hladký	hladký	k2eAgInSc1d1	hladký
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Po	po	k7c6	po
otlaku	otlak	k1gInSc6	otlak
<g/>
,	,	kIx,	,
naříznutí	naříznutí	k1gNnSc6	naříznutí
nebo	nebo	k8xC	nebo
stažení	stažení	k1gNnSc6	stažení
kůže	kůže	k1gFnSc2	kůže
klobouku	klobouk	k1gInSc2	klobouk
maso	maso	k1gNnSc1	maso
růžovky	růžovka	k1gFnSc2	růžovka
nabíhá	nabíhat	k5eAaImIp3nS	nabíhat
do	do	k7c2	do
růžova	růžovo	k1gNnSc2	růžovo
<g/>
,	,	kIx,	,
u	u	k7c2	u
muchomůrky	muchomůrky	k?	muchomůrky
tygrovité	tygrovitý	k2eAgNnSc1d1	tygrovitý
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pochva	pochva	k1gFnSc1	pochva
u	u	k7c2	u
růžovky	růžovka	k1gFnSc2	růžovka
chybí	chybět	k5eAaImIp3nS	chybět
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
muchomůrka	muchomůrka	k?	muchomůrka
tygrovaná	tygrovaný	k2eAgFnSc1d1	tygrovaná
má	mít	k5eAaImIp3nS	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
pochvu	pochva	k1gFnSc4	pochva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
lidově	lidově	k6eAd1	lidově
nazývá	nazývat	k5eAaImIp3nS	nazývat
kalich	kalich	k1gInSc4	kalich
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
růžovka	růžovka	k1gFnSc1	růžovka
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
hojná	hojný	k2eAgFnSc1d1	hojná
houba	houba	k1gFnSc1	houba
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
<g/>
,	,	kIx,	,
smíšených	smíšený	k2eAgInPc6d1	smíšený
i	i	k8xC	i
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Růžovka	růžovka	k1gFnSc1	růžovka
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
hub	houba	k1gFnPc2	houba
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
<g/>
,	,	kIx,	,
mnohdy	mnohdy	k6eAd1	mnohdy
jsou	být	k5eAaImIp3nP	být
sušeny	sušen	k2eAgInPc4d1	sušen
nadrobno	nadrobno	k6eAd1	nadrobno
nakrájené	nakrájený	k2eAgInPc4d1	nakrájený
plátky	plátek	k1gInPc4	plátek
plodnic	plodnice	k1gFnPc2	plodnice
<g/>
.	.	kIx.	.
</s>
<s>
Sbírá	sbírat	k5eAaImIp3nS	sbírat
se	se	k3xPyFc4	se
však	však	k9	však
až	až	k9	až
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Masák	masák	k1gMnSc1	masák
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
chutný	chutný	k2eAgInSc1d1	chutný
jen	jen	k9	jen
osmažený	osmažený	k2eAgInSc1d1	osmažený
na	na	k7c4	na
přírodno	přírodno	k1gNnSc4	přírodno
na	na	k7c6	na
másle	máslo	k1gNnSc6	máslo
<g/>
,	,	kIx,	,
či	či	k8xC	či
sádle	sádlo	k1gNnSc6	sádlo
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
kmínem	kmín	k1gInSc7	kmín
a	a	k8xC	a
solí	sůl	k1gFnSc7	sůl
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
chutí	chuť	k1gFnSc7	chuť
skutečně	skutečně	k6eAd1	skutečně
připomíná	připomínat	k5eAaImIp3nS	připomínat
smažené	smažený	k2eAgNnSc4d1	smažené
maso	maso	k1gNnSc4	maso
<g/>
.	.	kIx.	.
</s>
<s>
Muchomůrka	Muchomůrka	k?	Muchomůrka
růžovka	růžovka	k1gFnSc1	růžovka
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
syrova	syrův	k2eAgNnPc4d1	syrův
jedovatá	jedovatý	k2eAgNnPc4d1	jedovaté
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hemolytické	hemolytický	k2eAgInPc4d1	hemolytický
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
degradují	degradovat	k5eAaBmIp3nP	degradovat
tepelnou	tepelný	k2eAgFnSc7d1	tepelná
úpravou	úprava	k1gFnSc7	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KEIZER	KEIZER	kA	KEIZER
<g/>
,	,	kIx,	,
Gerrit	Gerrit	k1gInSc1	Gerrit
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
REBO	REBO	kA	REBO
Productions	Productions	k1gInSc1	Productions
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
288	[number]	k4	288
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85815	[number]	k4	85815
<g/>
-	-	kIx~	-
<g/>
95	[number]	k4	95
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
157	[number]	k4	157
<g/>
-	-	kIx~	-
<g/>
158	[number]	k4	158
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GARNWEIDNER	GARNWEIDNER	kA	GARNWEIDNER
<g/>
,	,	kIx,	,
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
.	.	kIx.	.
</s>
<s>
Houby	houba	k1gFnPc5	houba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
82	[number]	k4	82
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
muchomůrka	muchomůrka	k?	muchomůrka
růžovka	růžovka	k1gFnSc1	růžovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
muchomůrka	muchomůrka	k?	muchomůrka
růžovka	růžovka	k1gFnSc1	růžovka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgInPc1d1	detailní
snímky	snímek	k1gInPc1	snímek
panterové	panterové	k?	panterové
a	a	k8xC	a
růžovky	růžovka	k1gFnPc1	růžovka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
články	článek	k1gInPc1	článek
o	o	k7c6	o
houbách	houba	k1gFnPc6	houba
</s>
</p>
