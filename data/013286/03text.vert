<p>
<s>
Hledání	hledání	k1gNnSc1	hledání
Aljašky	Aljaška	k1gFnSc2	Aljaška
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Looking	Looking	k1gInSc4	Looking
for	forum	k1gNnPc2	forum
Alaska	Alasek	k1gMnSc2	Alasek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
první	první	k4xOgInSc4	první
román	román	k1gInSc4	román
amerického	americký	k2eAgMnSc2d1	americký
autora	autor	k1gMnSc2	autor
Johna	John	k1gMnSc2	John
Greena	Green	k1gMnSc2	Green
<g/>
.	.	kIx.	.
</s>
<s>
Vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
a	a	k8xC	a
v	v	k7c6	v
ČR	ČR	kA	ČR
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vydal	vydat	k5eAaPmAgInS	vydat
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Veronika	Veronika	k1gFnSc1	Veronika
Volhejnová	Volhejnová	k1gFnSc1	Volhejnová
<g/>
.	.	kIx.	.
</s>
<s>
Knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
řadící	řadící	k2eAgInPc4d1	řadící
se	se	k3xPyFc4	se
do	do	k7c2	do
žánru	žánr	k1gInSc2	žánr
literatury	literatura	k1gFnSc2	literatura
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgFnSc4d1	dospívající
<g/>
,	,	kIx,	,
zařadila	zařadit	k5eAaPmAgFnS	zařadit
Americká	americký	k2eAgFnSc1d1	americká
knihovnická	knihovnický	k2eAgFnSc1d1	knihovnická
asociace	asociace	k1gFnSc1	asociace
mezi	mezi	k7c4	mezi
deset	deset	k4xCc4	deset
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
knih	kniha	k1gFnPc2	kniha
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgNnSc4d1	dospívající
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
rodičů	rodič	k1gMnPc2	rodič
knihu	kniha	k1gFnSc4	kniha
oceňovala	oceňovat	k5eAaImAgFnS	oceňovat
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
rodičovské	rodičovský	k2eAgFnSc2d1	rodičovská
organizace	organizace	k1gFnSc2	organizace
si	se	k3xPyFc3	se
však	však	k9	však
vymohly	vymoct	k5eAaPmAgFnP	vymoct
stažení	stažení	k1gNnSc4	stažení
knihy	kniha	k1gFnSc2	kniha
z	z	k7c2	z
některých	některý	k3yIgFnPc2	některý
knihoven	knihovna	k1gFnPc2	knihovna
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
syrově	syrově	k6eAd1	syrově
líčí	líčit	k5eAaImIp3nS	líčit
život	život	k1gInSc4	život
dospívajících	dospívající	k2eAgFnPc2d1	dospívající
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
nechutné	chutný	k2eNgNnSc1d1	nechutné
a	a	k8xC	a
pornografické	pornografický	k2eAgNnSc1d1	pornografické
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
podstatou	podstata	k1gFnSc7	podstata
života	život	k1gInSc2	život
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
hledá	hledat	k5eAaImIp3nS	hledat
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
důležité	důležitý	k2eAgFnPc4d1	důležitá
otázky	otázka	k1gFnPc4	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
–	–	k?	–
Předtím	předtím	k6eAd1	předtím
a	a	k8xC	a
Potom	potom	k6eAd1	potom
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
nesmělý	smělý	k2eNgMnSc1d1	nesmělý
chlapec	chlapec	k1gMnSc1	chlapec
Miles	Miles	k1gMnSc1	Miles
Halter	Halter	k1gMnSc1	Halter
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
zálibou	záliba	k1gFnSc7	záliba
je	být	k5eAaImIp3nS	být
hledání	hledání	k1gNnSc1	hledání
posledních	poslední	k2eAgNnPc2d1	poslední
slov	slovo	k1gNnPc2	slovo
slavných	slavný	k2eAgFnPc2d1	slavná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Odejde	odejít	k5eAaPmIp3nS	odejít
z	z	k7c2	z
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
od	od	k7c2	od
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
studovat	studovat	k5eAaImF	studovat
internátní	internátní	k2eAgFnSc4d1	internátní
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
Culver	Culver	k1gMnSc1	Culver
Creek	Creek	k1gMnSc1	Creek
v	v	k7c6	v
Alabamě	Alabama	k1gFnSc6	Alabama
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
hledat	hledat	k5eAaImF	hledat
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
"	"	kIx"	"
<g/>
velké	velká	k1gFnPc4	velká
Možná	možná	k6eAd1	možná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
internátní	internátní	k2eAgFnSc6d1	internátní
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
spolužačky	spolužačka	k1gFnSc2	spolužačka
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
jménem	jméno	k1gNnSc7	jméno
Aljaška	Aljaška	k1gFnSc1	Aljaška
Youngová	Youngový	k2eAgFnSc1d1	Youngová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
skrývá	skrývat	k5eAaImIp3nS	skrývat
tajemství	tajemství	k1gNnSc4	tajemství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jí	on	k3xPp3gFnSc7	on
změnilo	změnit	k5eAaPmAgNnS	změnit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
