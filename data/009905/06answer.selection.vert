<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
nemoc	nemoc	k1gFnSc4	nemoc
nebyl	být	k5eNaImAgInS	být
odveden	odvést	k5eAaPmNgMnS	odvést
do	do	k7c2	do
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
nemusel	muset	k5eNaImAgMnS	muset
proto	proto	k8xC	proto
bojovat	bojovat	k5eAaImF	bojovat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
touto	tento	k3xDgFnSc7	tento
válkou	válka	k1gFnSc7	válka
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
následky	následek	k1gInPc7	následek
velmi	velmi	k6eAd1	velmi
ovlivněn	ovlivněn	k2eAgMnSc1d1	ovlivněn
<g/>
.	.	kIx.	.
</s>
