<s>
Alexandr	Alexandr	k1gMnSc1
Rumjancev	Rumjancva	k1gFnPc2
(	(	kIx(
<g/>
rychlobruslař	rychlobruslař	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
RumjancevCelé	RumjancevCelý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
А	А	k?
В	В	k?
Р	Р	k1gInSc1
Vadimovič	Vadimovič	k1gMnSc1
Rumjancev	Rumjancva	k1gFnPc2
Datum	datum	k1gNnSc4
narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1986	#num#	k4
(	(	kIx(
<g/>
34	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Archangelsk	Archangelsk	k1gInSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
SFSR	SFSR	kA
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
Rychlobruslařská	rychlobruslařský	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
Reprezentace	reprezentace	k1gFnSc2
</s>
<s>
od	od	k7c2
2009	#num#	k4
Světový	světový	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
od	od	k7c2
2009	#num#	k4
Status	status	k1gInSc1
</s>
<s>
aktivní	aktivní	k2eAgFnSc1d1
Celková	celkový	k2eAgFnSc1d1
vítězství	vítězství	k1gNnSc1
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
5000	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Stíh	stíh	k1gInSc1
<g/>
.	.	kIx.
závod	závod	k1gInSc1
</s>
<s>
1	#num#	k4
(	(	kIx(
<g/>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Infobox	Infobox	k1gInSc1
aktualizován	aktualizován	k2eAgInSc1d1
dne	den	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2019	#num#	k4
Inzell	Inzell	k1gInSc1
</s>
<s>
stíh	stíh	k1gInSc1
<g/>
.	.	kIx.
závod	závod	k1gInSc1
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
2021	#num#	k4
Heerenveen	Heerenveen	k1gInSc1
</s>
<s>
10	#num#	k4
000	#num#	k4
m	m	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2018	#num#	k4
Kolomna	Kolomn	k1gInSc2
</s>
<s>
5000	#num#	k4
m	m	kA
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2018	#num#	k4
Kolomna	Kolomn	k1gInSc2
</s>
<s>
stíh	stíh	k1gInSc1
<g/>
.	.	kIx.
závod	závod	k1gInSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
2020	#num#	k4
Heerenveen	Heerenveen	k1gInSc1
</s>
<s>
stíh	stíh	k1gInSc1
<g/>
.	.	kIx.
závod	závod	k1gInSc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Vadimovič	Vadimovič	k1gMnSc1
Rumjancev	Rumjancev	k1gFnSc1
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
А	А	k?
В	В	k?
Р	Р	k?
<g/>
;	;	kIx,
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1986	#num#	k4
Archangelsk	Archangelsk	k1gInSc1
<g/>
,	,	kIx,
Ruská	ruský	k2eAgFnSc1d1
SFSR	SFSR	kA
<g/>
,	,	kIx,
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ruský	ruský	k2eAgMnSc1d1
rychlobruslař	rychlobruslař	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
Světovém	světový	k2eAgInSc6d1
poháru	pohár	k1gInSc6
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
představil	představit	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
,	,	kIx,
pravidelně	pravidelně	k6eAd1
v	v	k7c6
něm	on	k3xPp3gInSc6
začal	začít	k5eAaPmAgInS
nastupovat	nastupovat	k5eAaImF
až	až	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Zimních	zimní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2010	#num#	k4
skončil	skončit	k5eAaPmAgMnS
v	v	k7c6
závodě	závod	k1gInSc6
na	na	k7c4
10	#num#	k4
km	km	kA
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
na	na	k7c6
poloviční	poloviční	k2eAgFnSc6d1
distanci	distance	k1gFnSc6
byl	být	k5eAaImAgInS
diskvalifikován	diskvalifikován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
také	také	k9
ZOH	ZOH	kA
2014	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
jedenáctý	jedenáctý	k4xOgMnSc1
na	na	k7c6
trati	trať	k1gFnSc6
5000	#num#	k4
m	m	kA
a	a	k8xC
šestý	šestý	k4xOgMnSc1
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
2015	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
desetikilometrovém	desetikilometrový	k2eAgInSc6d1
závodě	závod	k1gInSc6
pátý	pátý	k4xOgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
Mistrovství	mistrovství	k1gNnSc2
Evropy	Evropa	k1gFnSc2
2018	#num#	k4
si	se	k3xPyFc3
přivezl	přivézt	k5eAaPmAgMnS
stříbrné	stříbrný	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
z	z	k7c2
tratě	trať	k1gFnSc2
5000	#num#	k4
m	m	kA
a	a	k8xC
ze	z	k7c2
stíhacího	stíhací	k2eAgInSc2d1
závodu	závod	k1gInSc2
družstev	družstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
MS	MS	kA
2019	#num#	k4
vybojoval	vybojovat	k5eAaPmAgInS
s	s	k7c7
ruským	ruský	k2eAgInSc7d1
týmem	tým	k1gInSc7
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
bronz	bronz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
v	v	k7c6
celkovém	celkový	k2eAgNnSc6d1
hodnocení	hodnocení	k1gNnSc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c6
závodech	závod	k1gInPc6
na	na	k7c6
dlouhých	dlouhý	k2eAgFnPc6d1
tratích	trať	k1gFnPc6
5000	#num#	k4
m	m	kA
/	/	kIx~
10	#num#	k4
000	#num#	k4
m.	m.	k?
Na	na	k7c4
ME	ME	kA
2020	#num#	k4
získal	získat	k5eAaPmAgInS
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
ve	v	k7c6
stíhacím	stíhací	k2eAgInSc6d1
závodě	závod	k1gInSc6
družstev	družstvo	k1gNnPc2
a	a	k8xC
ze	z	k7c2
světového	světový	k2eAgInSc2d1
šampionátu	šampionát	k1gInSc2
2021	#num#	k4
si	se	k3xPyFc3
přivezl	přivézt	k5eAaPmAgMnS
bronz	bronz	k1gInSc4
z	z	k7c2
tratě	trať	k1gFnSc2
10	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2017	#num#	k4
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
na	na	k7c4
ZOH	ZOH	kA
v	v	k7c6
Soči	Soči	k1gNnSc6
byl	být	k5eAaImAgInS
součástí	součást	k1gFnSc7
ruského	ruský	k2eAgInSc2d1
dopingového	dopingový	k2eAgInSc2d1
skandálu	skandál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
obou	dva	k4xCgInPc6
závodech	závod	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
tam	tam	k6eAd1
absolvoval	absolvovat	k5eAaPmAgInS
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zpětně	zpětně	k6eAd1
diskvalifikován	diskvalifikovat	k5eAaBmNgMnS
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
doživotně	doživotně	k6eAd1
vyloučen	vyloučen	k2eAgInSc1d1
z	z	k7c2
olympijských	olympijský	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
hromadném	hromadný	k2eAgNnSc6d1
odvolání	odvolání	k1gNnSc6
ruských	ruský	k2eAgMnPc2d1
sportovců	sportovec	k1gMnPc2
byla	být	k5eAaImAgFnS
na	na	k7c6
začátku	začátek	k1gInSc6
února	únor	k1gInSc2
2018	#num#	k4
jeho	jeho	k3xOp3gFnSc2
diskvalifikace	diskvalifikace	k1gFnSc2
a	a	k8xC
vyloučení	vyloučení	k1gNnSc2
z	z	k7c2
OH	OH	kA
Mezinárodní	mezinárodní	k2eAgFnSc7d1
sportovní	sportovní	k2eAgFnSc7d1
arbitráží	arbitráž	k1gFnSc7
kvůli	kvůli	k7c3
nedostatku	nedostatek	k1gInSc3
důkazů	důkaz	k1gInPc2
zrušena	zrušen	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zubkov	Zubkov	k1gInSc1
a	a	k8xC
Fatkulinová	Fatkulinový	k2eAgFnSc1d1
musí	muset	k5eAaImIp3nS
kvůli	kvůli	k7c3
dopingu	doping	k1gInSc3
vrátit	vrátit	k5eAaPmF
medaile	medaile	k1gFnPc4
ze	z	k7c2
Soči	Soči	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idnes	Idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2017-11-24	2017-11-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Přelomové	přelomový	k2eAgNnSc4d1
rozhodnutí	rozhodnutí	k1gNnSc4
arbitráže	arbitráž	k1gFnSc2
<g/>
:	:	kIx,
39	#num#	k4
Rusům	Rus	k1gMnPc3
zrušila	zrušit	k5eAaPmAgFnS
doživotní	doživotní	k2eAgInPc4d1
tresty	trest	k1gInPc4
za	za	k7c4
doping	doping	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceskatelevize	Ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-02-01	2018-02-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Rumjancev	Rumjancva	k1gFnPc2
na	na	k7c4
speedskatingnews	speedskatingnews	k1gInSc4
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Rusko	Rusko	k1gNnSc1
</s>
