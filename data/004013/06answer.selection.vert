<s>
Londýn	Londýn	k1gInSc1	Londýn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
London	London	k1gMnSc1	London
s	s	k7c7	s
výslovností	výslovnost	k1gFnSc7	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Severního	severní	k2eAgNnSc2d1	severní
Irska	Irsko	k1gNnSc2	Irsko
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
země	zem	k1gFnSc2	zem
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Temže	Temže	k1gFnSc2	Temže
<g/>
.	.	kIx.	.
</s>
