<p>
<s>
Anna	Anna	k1gFnSc1	Anna
Hřímalá	Hřímalý	k2eAgFnSc1d1	Hřímalý
<g/>
,	,	kIx,	,
křtěná	křtěný	k2eAgFnSc1d1	křtěná
Anna	Anna	k1gFnSc1	Anna
Johanna	Johanna	k1gFnSc1	Johanna
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1841	[number]	k4	1841
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
1897	[number]	k4	1897
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
klavíristka	klavíristka	k1gFnSc1	klavíristka
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
děl	dělo	k1gNnPc2	dělo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
do	do	k7c2	do
ruštiny	ruština	k1gFnSc2	ruština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Anna	Anna	k1gFnSc1	Anna
Hřímalá	Hřímalý	k2eAgFnSc1d1	Hřímalý
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
hudební	hudební	k2eAgInSc1d1	hudební
slovník	slovník	k1gInSc1	slovník
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
Hřímalý	Hřímalý	k2eAgInSc1d1	Hřímalý
</s>
</p>
<p>
<s>
Databáze	databáze	k1gFnPc1	databáze
Národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
Hřímalá	Hřímalý	k2eAgFnSc1d1	Hřímalý
Anna	Anna	k1gFnSc1	Anna
</s>
</p>
