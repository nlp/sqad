<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
česká	český	k2eAgFnSc1d1	Česká
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
překládala	překládat	k5eAaImAgFnS	překládat
díla	dílo	k1gNnSc2	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Vrchlického	Vrchlický	k1gMnSc2	Vrchlický
do	do	k7c2	do
ruštiny	ruština	k1gFnSc2	ruština
<g/>
?	?	kIx.	?
</s>
