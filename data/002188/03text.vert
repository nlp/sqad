<s>
Nashville	Nashville	k6eAd1	Nashville
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Tennessee	Tennesse	k1gFnSc2	Tennesse
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1779	[number]	k4	1779
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Cumberland	Cumberlanda	k1gFnPc2	Cumberlanda
v	v	k7c4	v
Davidson	Davidson	k1gNnSc4	Davidson
County	Counta	k1gFnSc2	Counta
v	v	k7c6	v
severo-střední	severotřední	k2eAgFnSc6d1	severo-střední
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
střediskem	středisko	k1gNnSc7	středisko
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
americké	americký	k2eAgFnSc2d1	americká
country	country	k2eAgFnSc2d1	country
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
hudebního	hudební	k2eAgNnSc2d1	hudební
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
a	a	k8xC	a
dopravního	dopravní	k2eAgInSc2d1	dopravní
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
1362,5	[number]	k4	1362,5
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
tu	tu	k6eAd1	tu
okolo	okolo	k7c2	okolo
607	[number]	k4	607
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
<g/>
,	,	kIx,	,
po	po	k7c6	po
Memphisu	Memphis	k1gInSc6	Memphis
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
největší	veliký	k2eAgFnSc2d3	veliký
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1,4	[number]	k4	1,4
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
New	New	k1gFnSc2	New
Orleansu	Orleans	k1gInSc2	Orleans
se	se	k3xPyFc4	se
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgNnSc4	žádný
jiné	jiný	k2eAgNnSc4d1	jiné
město	město	k1gNnSc4	město
USA	USA	kA	USA
nemůže	moct	k5eNaImIp3nS	moct
rovnat	rovnat	k5eAaImF	rovnat
Nashville	Nashville	k1gInSc4	Nashville
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
nahrávacím	nahrávací	k2eAgNnSc7d1	nahrávací
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
,	,	kIx,	,
živými	živý	k2eAgFnPc7d1	živá
vystoupeními	vystoupení	k1gNnPc7	vystoupení
anebo	anebo	k8xC	anebo
mnohými	mnohý	k2eAgInPc7d1	mnohý
hudebními	hudební	k2eAgInPc7d1	hudební
kluby	klub	k1gInPc7	klub
a	a	k8xC	a
bary	bar	k1gInPc7	bar
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
představení	představení	k1gNnSc1	představení
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
konat	konat	k5eAaImF	konat
v	v	k7c6	v
Tennessee	Tennessee	k1gFnSc6	Tennessee
Performing	Performing	k1gInSc1	Performing
Arts	Artsa	k1gFnPc2	Artsa
Center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
každoročně	každoročně	k6eAd1	každoročně
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
tisíce	tisíc	k4xCgInPc4	tisíc
turistů	turist	k1gMnPc2	turist
z	z	k7c2	z
celých	celá	k1gFnPc2	celá
USA	USA	kA	USA
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
největšího	veliký	k2eAgInSc2d3	veliký
festivalu	festival	k1gInSc2	festival
country	country	k2eAgFnSc2d1	country
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
CMA	CMA	kA	CMA
Music	Music	k1gMnSc1	Music
Festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
601	[number]	k4	601
222	[number]	k4	222
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
60,5	[number]	k4	60,5
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
28,4	[number]	k4	28,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,1	[number]	k4	3,1
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
5,1	[number]	k4	5,1
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
2,5	[number]	k4	2,5
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
10,0	[number]	k4	10,0
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nashvillu	Nashvill	k1gInSc6	Nashvill
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
2	[number]	k4	2
kluby	klub	k1gInPc7	klub
hrající	hrající	k2eAgFnSc2d1	hrající
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
americké	americký	k2eAgFnSc2d1	americká
soutěže	soutěž	k1gFnSc2	soutěž
NHL	NHL	kA	NHL
a	a	k8xC	a
NFL	NFL	kA	NFL
<g/>
.	.	kIx.	.
</s>
<s>
Obrovskou	obrovský	k2eAgFnSc7d1	obrovská
podporou	podpora	k1gFnSc7	podpora
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
slavných	slavný	k2eAgFnPc2d1	slavná
osobností	osobnost	k1gFnPc2	osobnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
pyšnit	pyšnit	k5eAaImF	pyšnit
klub	klub	k1gInSc1	klub
Nashville	Nashvillo	k1gNnSc6	Nashvillo
Predators	Predators	k1gInSc1	Predators
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
domácí	domácí	k2eAgNnSc4d1	domácí
utkání	utkání	k1gNnSc4	utkání
v	v	k7c6	v
Bridgestone	Bridgeston	k1gInSc5	Bridgeston
Arena	Areen	k2eAgMnSc4d1	Areen
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
17.113	[number]	k4	17.113
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
získává	získávat	k5eAaImIp3nS	získávat
respekt	respekt	k1gInSc4	respekt
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
NHL	NHL	kA	NHL
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
černého	černý	k2eAgMnSc4d1	černý
koně	kůň	k1gMnSc4	kůň
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
NFL	NFL	kA	NFL
bojuje	bojovat	k5eAaImIp3nS	bojovat
tým	tým	k1gInSc1	tým
Tennessee	Tennessee	k1gNnSc1	Tennessee
Titans	Titans	k1gInSc1	Titans
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
domácí	domácí	k2eAgNnSc4d1	domácí
utkání	utkání	k1gNnSc4	utkání
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
LP	LP	kA	LP
Field	Field	k1gInSc1	Field
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
69.143	[number]	k4	69.143
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
8	[number]	k4	8
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
Nashvillu	Nashvill	k1gInSc2	Nashvill
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
na	na	k7c4	na
větší	veliký	k2eAgInPc4d2	veliký
úspěchy	úspěch	k1gInPc4	úspěch
zatím	zatím	k6eAd1	zatím
stále	stále	k6eAd1	stále
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Nashville	Nashvill	k1gInSc6	Nashvill
žije	žít	k5eAaImIp3nS	žít
(	(	kIx(	(
<g/>
či	či	k8xC	či
žilo	žít	k5eAaImAgNnS	žít
<g/>
)	)	kIx)	)
mnoho	mnoho	k4c4	mnoho
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Johnny	Johnen	k2eAgFnPc1d1	Johnna
Cash	cash	k1gFnPc1	cash
<g/>
,	,	kIx,	,
Jack	Jack	k1gMnSc1	Jack
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
Sheryl	Sheryl	k1gInSc4	Sheryl
Crow	Crow	k1gFnSc2	Crow
<g/>
,	,	kIx,	,
Jimi	on	k3xPp3gInPc7	on
Hendrix	Hendrix	k1gInSc4	Hendrix
<g/>
,	,	kIx,	,
Faith	Faith	k1gMnSc1	Faith
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
Dolly	Dolla	k1gFnPc1	Dolla
Parton	Parton	k1gInSc1	Parton
<g/>
,	,	kIx,	,
Shania	Shanium	k1gNnPc1	Shanium
Twain	Twain	k1gMnSc1	Twain
<g/>
,	,	kIx,	,
Taylor	Taylor	k1gMnSc1	Taylor
Swift	Swift	k1gMnSc1	Swift
<g/>
,	,	kIx,	,
Carrie	Carrie	k1gFnSc1	Carrie
Underwood	underwood	k1gInSc1	underwood
<g/>
,	,	kIx,	,
Reese	Reese	k1gFnSc1	Reese
Witherspoon	Witherspoon	k1gInSc1	Witherspoon
<g/>
.	.	kIx.	.
</s>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Miley	Mile	k2eAgFnPc1d1	Mile
Cyrus	Cyrus	k1gInSc4	Cyrus
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
odtud	odtud	k6eAd1	odtud
i	i	k9	i
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Kesha	Kesha	k1gFnSc1	Kesha
<g/>
.	.	kIx.	.
</s>
<s>
Belfast	Belfast	k1gInSc1	Belfast
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Caen	Caen	k1gInSc1	Caen
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Edmonton	Edmonton	k1gInSc1	Edmonton
<g/>
,	,	kIx,	,
Alberta	Alberta	k1gFnSc1	Alberta
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
Girona	Girona	k1gFnSc1	Girona
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
,	,	kIx,	,
Sasko-Anhaltsko	Sasko-Anhaltsko	k1gNnSc1	Sasko-Anhaltsko
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Mendoza	Mendoza	k1gFnSc1	Mendoza
<g/>
,	,	kIx,	,
Argentina	Argentina	k1gFnSc1	Argentina
Pernik	Pernik	k1gInSc1	Pernik
<g/>
,	,	kIx,	,
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
Tchaj-jüan	Tchajüana	k1gFnPc2	Tchaj-jüana
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
</s>
