<p>
<s>
Omán	Omán	k1gInSc1	Omán
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Sultanát	sultanát	k1gInSc1	sultanát
Omán	Omán	k1gInSc1	Omán
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
س	س	k?	س
ع	ع	k?	ع
<g/>
,	,	kIx,	,
Salṭ	Salṭ	k1gMnSc1	Salṭ
'	'	kIx"	'
<g/>
Umā	Umā	k1gMnSc1	Umā
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
arabský	arabský	k2eAgInSc1d1	arabský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
při	při	k7c6	při
Arabském	arabský	k2eAgNnSc6d1	arabské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
Ománském	ománský	k2eAgInSc6d1	ománský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc2	hranice
410	[number]	k4	410
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Jemenem	Jemen	k1gInSc7	Jemen
(	(	kIx(	(
<g/>
288	[number]	k4	288
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	s	k7c7	s
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
(	(	kIx(	(
<g/>
676	[number]	k4	676
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
309	[number]	k4	309
500	[number]	k4	500
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ománu	Omán	k1gInSc2	Omán
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
exklávy	exkláva	k1gFnPc1	exkláva
Madha	Madh	k1gMnSc2	Madh
a	a	k8xC	a
Musandam	Musandam	k1gInSc4	Musandam
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
součástí	součást	k1gFnSc7	součást
neshod	neshoda	k1gFnPc2	neshoda
mezi	mezi	k7c7	mezi
Ománem	Omán	k1gInSc7	Omán
a	a	k8xC	a
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
údajně	údajně	k6eAd1	údajně
ratifikovaly	ratifikovat	k5eAaBmAgInP	ratifikovat
a	a	k8xC	a
podepsaly	podepsat	k5eAaPmAgInP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
konečném	konečný	k2eAgNnSc6d1	konečné
ustanovení	ustanovení	k1gNnSc6	ustanovení
společných	společný	k2eAgFnPc2d1	společná
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
ohledně	ohledně	k7c2	ohledně
dohody	dohoda	k1gFnSc2	dohoda
ovšem	ovšem	k9	ovšem
nebyly	být	k5eNaImAgFnP	být
doposud	doposud	k6eAd1	doposud
zveřejněny	zveřejněn	k2eAgFnPc1d1	zveřejněna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žijí	žít	k5eAaImIp3nP	žít
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Početně	početně	k6eAd1	početně
nejzastoupenějším	zastoupený	k2eAgNnSc7d3	zastoupený
etnikem	etnikum	k1gNnSc7	etnikum
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
následují	následovat	k5eAaImIp3nP	následovat
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
–	–	k?	–
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
(	(	kIx(	(
<g/>
Bengálci	Bengálec	k1gMnPc1	Bengálec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
(	(	kIx(	(
<g/>
Balúčové	Balúčové	k2eAgMnSc1d1	Balúčové
<g/>
)	)	kIx)	)
a	a	k8xC	a
Srí	Srí	k1gFnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
a	a	k8xC	a
též	též	k9	též
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Valná	valný	k2eAgFnSc1d1	valná
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
jsou	být	k5eAaImIp3nP	být
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
nejrozšířenější	rozšířený	k2eAgMnSc1d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
ibádíjovská	ibádíjovský	k2eAgFnSc1d1	ibádíjovský
větev	větev	k1gFnSc1	větev
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyznávanější	vyznávaný	k2eAgNnSc1d3	vyznávaný
náboženství	náboženství	k1gNnSc1	náboženství
mimo	mimo	k6eAd1	mimo
islámu	islám	k1gInSc2	islám
je	být	k5eAaImIp3nS	být
hinduismus	hinduismus	k1gInSc1	hinduismus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dorozumívací	dorozumívací	k2eAgInPc4d1	dorozumívací
jazyky	jazyk	k1gInPc4	jazyk
patří	patřit	k5eAaImIp3nS	patřit
úřední	úřední	k2eAgFnSc1d1	úřední
arabština	arabština	k1gFnSc1	arabština
a	a	k8xC	a
hojně	hojně	k6eAd1	hojně
používaná	používaný	k2eAgFnSc1d1	používaná
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
je	být	k5eAaImIp3nS	být
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
sultánem	sultán	k1gMnSc7	sultán
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
Kábús	Kábús	k1gInSc1	Kábús
bin	bin	k?	bin
Saíd	Saíd	k1gInSc1	Saíd
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
al-Saídů	al-Saíd	k1gInPc2	al-Saíd
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
monarchy	monarcha	k1gMnSc2	monarcha
je	být	k5eAaImIp3nS	být
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Kábus	Kábus	k1gMnSc1	Kábus
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
průzkumu	průzkum	k1gInSc2	průzkum
australské	australský	k2eAgFnSc2d1	australská
organizace	organizace	k1gFnSc2	organizace
Vision	vision	k1gInSc1	vision
of	of	k?	of
Humanity	humanita	k1gFnPc1	humanita
<g/>
,	,	kIx,	,
sestavující	sestavující	k2eAgInSc1d1	sestavující
seznam	seznam	k1gInSc1	seznam
zemí	zem	k1gFnPc2	zem
podle	podle	k7c2	podle
světového	světový	k2eAgInSc2d1	světový
indexu	index	k1gInSc2	index
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
označily	označit	k5eAaPmAgFnP	označit
Omán	Omán	k1gInSc4	Omán
21	[number]	k4	21
<g/>
.	.	kIx.	.
nejbezpečnější	bezpečný	k2eAgFnSc1d3	nejbezpečnější
zemí	zem	k1gFnSc7	zem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
obsadil	obsadit	k5eAaPmAgMnS	obsadit
25	[number]	k4	25
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
a	a	k8xC	a
22	[number]	k4	22
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2010	[number]	k4	2010
program	program	k1gInSc1	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
vydal	vydat	k5eAaPmAgInS	vydat
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
rozvoji	rozvoj	k1gInSc6	rozvoj
135	[number]	k4	135
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
v	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgFnPc1	první
známky	známka	k1gFnPc1	známka
civilizace	civilizace	k1gFnSc2	civilizace
pocházejí	pocházet	k5eAaImIp3nP	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
přinejmenším	přinejmenším	k6eAd1	přinejmenším
před	před	k7c7	před
5	[number]	k4	5
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
území	území	k1gNnPc2	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ománu	Omán	k1gInSc2	Omán
označovalo	označovat	k5eAaImAgNnS	označovat
odlišnými	odlišný	k2eAgInPc7d1	odlišný
názvy	název	k1gInPc7	název
–	–	k?	–
nejznámější	známý	k2eAgMnSc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
Majan	Majan	k1gMnSc1	Majan
nebo	nebo	k8xC	nebo
Megan	Megan	k1gMnSc1	Megan
či	či	k8xC	či
Mezoun	Mezoun	k1gMnSc1	Mezoun
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
název	název	k1gInSc4	název
Magan	Magana	k1gFnPc2	Magana
poukazují	poukazovat	k5eAaImIp3nP	poukazovat
nalezené	nalezený	k2eAgFnPc4d1	nalezená
sumerské	sumerský	k2eAgFnPc4d1	sumerská
tabulky	tabulka	k1gFnPc4	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
označením	označení	k1gNnSc7	označení
Magan	Magan	k1gInSc1	Magan
byly	být	k5eAaImAgFnP	být
myšleny	myslet	k5eAaImNgInP	myslet
staré	starý	k2eAgInPc1d1	starý
měděné	měděný	k2eAgInPc1d1	měděný
doly	dol	k1gInPc1	dol
<g/>
.	.	kIx.	.
</s>
<s>
Mezoun	Mezoun	k1gInSc1	Mezoun
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
muzn	muzna	k1gFnPc2	muzna
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamenalo	znamenat	k5eAaImAgNnS	znamenat
bohatý	bohatý	k2eAgMnSc1d1	bohatý
na	na	k7c4	na
tekoucí	tekoucí	k2eAgFnSc4d1	tekoucí
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInSc1d1	dnešní
název	název	k1gInSc1	název
Omán	Omán	k1gInSc1	Omán
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
pocházet	pocházet	k5eAaImF	pocházet
od	od	k7c2	od
arabských	arabský	k2eAgInPc2d1	arabský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
přesouvaly	přesouvat	k5eAaImAgFnP	přesouvat
z	z	k7c2	z
jemenské	jemenský	k2eAgFnSc2d1	Jemenská
oblasti	oblast	k1gFnSc2	oblast
Uman	Umana	k1gFnPc2	Umana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doba	doba	k1gFnSc1	doba
kamenná	kamenný	k2eAgFnSc1d1	kamenná
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
známé	známý	k2eAgNnSc1d1	známé
místo	místo	k1gNnSc1	místo
lidského	lidský	k2eAgNnSc2d1	lidské
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
je	být	k5eAaImIp3nS	být
Watajah	Watajah	k1gInSc1	Watajah
<g/>
,	,	kIx,	,
situovaný	situovaný	k2eAgInSc1d1	situovaný
na	na	k7c6	na
území	území	k1gNnSc6	území
Maskatského	maskatský	k2eAgInSc2d1	maskatský
guvernorátu	guvernorát	k1gInSc2	guvernorát
<g/>
.	.	kIx.	.
</s>
<s>
Datuje	datovat	k5eAaImIp3nS	datovat
se	se	k3xPyFc4	se
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
před	před	k7c4	před
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
</s>
</p>
<p>
<s>
a	a	k8xC	a
řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
nejstarší	starý	k2eAgNnPc4d3	nejstarší
lidská	lidský	k2eAgNnPc4d1	lidské
osídlení	osídlení	k1gNnPc4	osídlení
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
zde	zde	k6eAd1	zde
nalezli	naleznout	k5eAaPmAgMnP	naleznout
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
nejranější	raný	k2eAgFnPc1d3	nejranější
spadaly	spadat	k5eAaImAgFnP	spadat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc2d1	další
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
nálezy	nález	k1gInPc1	nález
sestávaly	sestávat	k5eAaImAgInP	sestávat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
kamenných	kamenný	k2eAgInPc2d1	kamenný
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
zvířecích	zvířecí	k2eAgFnPc2d1	zvířecí
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
skořápek	skořápka	k1gFnPc2	skořápka
a	a	k8xC	a
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
ohniště	ohniště	k1gNnSc2	ohniště
<g/>
.	.	kIx.	.
</s>
<s>
Datují	datovat	k5eAaImIp3nP	datovat
se	se	k3xPyFc4	se
do	do	k7c2	do
roku	rok	k1gInSc2	rok
7	[number]	k4	7
615	[number]	k4	615
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
nálezy	nález	k1gInPc7	nález
byly	být	k5eAaImAgFnP	být
pazourkové	pazourkový	k2eAgInPc4d1	pazourkový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
zaostřené	zaostřený	k2eAgInPc4d1	zaostřený
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
škrabky	škrabek	k1gInPc4	škrabek
a	a	k8xC	a
ručně	ručně	k6eAd1	ručně
dělaná	dělaný	k2eAgFnSc1d1	dělaná
keramika	keramika	k1gFnSc1	keramika
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
hor	hora	k1gFnPc2	hora
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
nástěnné	nástěnný	k2eAgInPc1d1	nástěnný
kresby	kresba	k1gFnPc4	kresba
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
kresby	kresba	k1gFnPc1	kresba
se	se	k3xPyFc4	se
našly	najít	k5eAaPmAgFnP	najít
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
koutech	kout	k1gInPc6	kout
Ománu	Omán	k1gInSc2	Omán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
Vádí	vádí	k1gNnSc2	vádí
Sahtan	Sahtan	k1gInSc1	Sahtan
a	a	k8xC	a
Vádí	vádí	k1gNnSc1	vádí
Baní	baně	k1gFnPc2	baně
Charús	Charúsa	k1gFnPc2	Charúsa
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Rustak	Rustak	k1gInSc1	Rustak
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnPc1	kresba
zobrazovaly	zobrazovat	k5eAaImAgFnP	zobrazovat
člověka	člověk	k1gMnSc4	člověk
nesoucího	nesoucí	k2eAgInSc2d1	nesoucí
zbraně	zbraň	k1gFnPc1	zbraň
čelícího	čelící	k2eAgInSc2d1	čelící
divokým	divoký	k2eAgNnPc3d1	divoké
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
místem	místo	k1gNnSc7	místo
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgNnSc1d1	kamenné
je	být	k5eAaImIp3nS	být
Sivan	Sivan	k1gInSc4	Sivan
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Hajma	Hajmum	k1gNnSc2	Hajmum
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgInP	objevit
hroty	hrot	k1gInPc1	hrot
šípů	šíp	k1gInPc2	šíp
<g/>
,	,	kIx,	,
nože	nůž	k1gInSc2	nůž
<g/>
,	,	kIx,	,
dláta	dláto	k1gNnSc2	dláto
a	a	k8xC	a
zakulacené	zakulacený	k2eAgInPc4d1	zakulacený
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
lidé	člověk	k1gMnPc1	člověk
používali	používat	k5eAaImAgMnP	používat
k	k	k7c3	k
hodu	hod	k1gInSc3	hod
po	po	k7c6	po
zvířatech	zvíře	k1gNnPc6	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Předislámský	předislámský	k2eAgInSc1d1	předislámský
Omán	Omán	k1gInSc1	Omán
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
do	do	k7c2	do
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
byl	být	k5eAaImAgInS	být
Omán	Omán	k1gInSc1	Omán
kontrolován	kontrolovat	k5eAaImNgInS	kontrolovat
či	či	k8xC	či
ovlivňován	ovlivňovat	k5eAaImNgInS	ovlivňovat
třemi	tři	k4xCgFnPc7	tři
perskými	perský	k2eAgFnPc7d1	perská
dynastiemi	dynastie	k1gFnPc7	dynastie
–	–	k?	–
Achaimenovci	Achaimenovec	k1gMnPc7	Achaimenovec
<g/>
,	,	kIx,	,
Parthy	Parth	k1gMnPc7	Parth
a	a	k8xC	a
Sásánovci	Sásánovec	k1gMnPc7	Sásánovec
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
jej	on	k3xPp3gMnSc4	on
kontrolovali	kontrolovat	k5eAaImAgMnP	kontrolovat
Achaimenovci	Achaimenovec	k1gMnPc1	Achaimenovec
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
250	[number]	k4	250
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
převzali	převzít	k5eAaPmAgMnP	převzít
moc	moc	k6eAd1	moc
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
Parthové	Parthová	k1gFnSc2	Parthová
a	a	k8xC	a
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
až	až	k9	až
k	k	k7c3	k
Ománu	Omán	k1gInSc3	Omán
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ochrany	ochrana	k1gFnSc2	ochrana
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
tras	trasa	k1gFnPc2	trasa
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
Parthové	Parth	k1gMnPc1	Parth
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
osadili	osadit	k5eAaPmAgMnP	osadit
je	on	k3xPp3gMnPc4	on
vlastními	vlastní	k2eAgMnPc7d1	vlastní
vojáky	voják	k1gMnPc7	voják
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
nahradili	nahradit	k5eAaPmAgMnP	nahradit
Parthy	Partha	k1gFnPc4	Partha
Sásánovci	Sásánovec	k1gMnSc3	Sásánovec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
udrželi	udržet	k5eAaPmAgMnP	udržet
oblast	oblast	k1gFnSc4	oblast
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
až	až	k6eAd1	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vzestupu	vzestup	k1gInSc2	vzestup
islámu	islám	k1gInSc2	islám
o	o	k7c4	o
pár	pár	k4xCyI	pár
staletí	staletí	k1gNnPc2	staletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příchod	příchod	k1gInSc1	příchod
islámu	islám	k1gInSc2	islám
===	===	k?	===
</s>
</p>
<p>
<s>
Islám	islám	k1gInSc1	islám
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c4	na
ománské	ománský	k2eAgNnSc4d1	Ománské
území	území	k1gNnSc4	území
ještě	ještě	k9	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
proroka	prorok	k1gMnSc2	prorok
Mohameda	Mohamed	k1gMnSc2	Mohamed
<g/>
.	.	kIx.	.
</s>
<s>
Ománci	Ománek	k1gMnPc1	Ománek
byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
prvními	první	k4xOgFnPc7	první
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přijali	přijmout	k5eAaPmAgMnP	přijmout
novou	nový	k2eAgFnSc4d1	nová
víru	víra	k1gFnSc4	víra
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
<s>
Konverze	konverze	k1gFnSc1	konverze
Ománců	Ománec	k1gMnPc2	Ománec
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
připisována	připisován	k2eAgMnSc4d1	připisován
Amru	Amra	k1gMnSc4	Amra
íbn	íbn	k?	íbn
al-Asovi	al-As	k1gMnSc3	al-As
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
630	[number]	k4	630
poslán	poslán	k2eAgInSc1d1	poslán
Mohamedem	Mohamed	k1gMnSc7	Mohamed
jakožto	jakožto	k8xS	jakožto
vyslanec	vyslanec	k1gMnSc1	vyslanec
za	za	k7c7	za
tehdejšími	tehdejší	k2eAgMnPc7d1	tehdejší
spoluvládci	spoluvládce	k1gMnPc7	spoluvládce
Ománu	Omán	k1gInSc2	Omán
<g/>
,	,	kIx,	,
Džajfarem	Džajfar	k1gInSc7	Džajfar
a	a	k8xC	a
Ábdem	Ábd	k1gInSc7	Ábd
<g/>
.	.	kIx.	.
</s>
<s>
Amr	Amr	k?	Amr
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
dvojici	dvojice	k1gFnSc4	dvojice
k	k	k7c3	k
přijmutí	přijmutí	k1gNnSc3	přijmutí
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ibádíjovským	ibádíjovský	k2eAgInSc7d1	ibádíjovský
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
vládl	vládnout	k5eAaImAgInS	vládnout
zvolený	zvolený	k2eAgMnSc1d1	zvolený
vládce	vládce	k1gMnSc1	vládce
zvaný	zvaný	k2eAgMnSc1d1	zvaný
imám	imám	k1gMnSc1	imám
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc1	Omán
hrál	hrát	k5eAaImAgInS	hrát
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
následovala	následovat	k5eAaImAgFnS	následovat
po	po	k7c6	po
Mohamedově	Mohamedův	k2eAgFnSc6d1	Mohamedova
smrti	smrt	k1gFnSc6	smrt
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
letech	léto	k1gNnPc6	léto
islámské	islámský	k2eAgFnSc2d1	islámská
expanze	expanze	k1gFnSc2	expanze
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
i	i	k8xC	i
moři	moře	k1gNnSc6	moře
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc2	Persie
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
začal	začít	k5eAaPmAgInS	začít
provozovat	provozovat	k5eAaImF	provozovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
obchodní	obchodní	k2eAgFnPc4d1	obchodní
a	a	k8xC	a
námořní	námořní	k2eAgFnPc4d1	námořní
aktivity	aktivita	k1gFnPc4	aktivita
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
Ománci	Ománek	k1gMnPc1	Ománek
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
cestovali	cestovat	k5eAaImAgMnP	cestovat
až	až	k8xS	až
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
asijských	asijský	k2eAgInPc2d1	asijský
přístavů	přístav	k1gInPc2	přístav
s	s	k7c7	s
poselstvím	poselství	k1gNnSc7	poselství
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
661	[number]	k4	661
až	až	k6eAd1	až
750	[number]	k4	750
vládli	vládnout	k5eAaImAgMnP	vládnout
zemi	zem	k1gFnSc4	zem
Umajjovci	Umajjovec	k1gMnSc3	Umajjovec
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
750	[number]	k4	750
až	až	k9	až
931	[number]	k4	931
Abbásovci	Abbásovec	k1gMnPc7	Abbásovec
<g/>
,	,	kIx,	,
931	[number]	k4	931
až	až	k9	až
932	[number]	k4	932
Karmatiané	Karmatianý	k2eAgInPc1d1	Karmatianý
<g/>
,	,	kIx,	,
932	[number]	k4	932
až	až	k9	až
933	[number]	k4	933
znovu	znovu	k6eAd1	znovu
Abbásovci	Abbásovec	k1gMnPc1	Abbásovec
<g/>
,	,	kIx,	,
933	[number]	k4	933
až	až	k9	až
934	[number]	k4	934
podruhé	podruhé	k6eAd1	podruhé
Karmatiané	Karmatianý	k2eAgInPc1d1	Karmatianý
<g/>
,	,	kIx,	,
934	[number]	k4	934
až	až	k9	až
967	[number]	k4	967
definitivně	definitivně	k6eAd1	definitivně
Abbásovci	Abbásovec	k1gMnPc1	Abbásovec
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
dynastie	dynastie	k1gFnSc2	dynastie
Bujidů	Bujid	k1gMnPc2	Bujid
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1053	[number]	k4	1053
<g/>
,	,	kIx,	,
odkdy	odkdy	k6eAd1	odkdy
začali	začít	k5eAaPmAgMnP	začít
vládnout	vládnout	k5eAaImF	vládnout
Seldžukové	Seldžuk	k1gMnPc1	Seldžuk
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1154	[number]	k4	1154
<g/>
.	.	kIx.	.
</s>
<s>
Vypuzením	vypuzení	k1gNnSc7	vypuzení
Seldžuků	Seldžuk	k1gInPc2	Seldžuk
začala	začít	k5eAaPmAgFnS	začít
vláda	vláda	k1gFnSc1	vláda
ománské	ománský	k2eAgFnSc2d1	ománská
dynastie	dynastie	k1gFnSc2	dynastie
Nabhánovců	Nabhánovec	k1gMnPc2	Nabhánovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
Omán	Omán	k1gInSc1	Omán
prosperující	prosperující	k2eAgInSc1d1	prosperující
a	a	k8xC	a
námořně-orientovaný	námořněrientovaný	k2eAgInSc1d1	námořně-orientovaný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ománské	ománský	k2eAgFnPc1d1	ománská
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
dhauy	dhauy	k1gInPc1	dhauy
<g/>
,	,	kIx,	,
pluly	plout	k5eAaImAgInP	plout
do	do	k7c2	do
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
destinací	destinace	k1gFnPc2	destinace
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Ománské	ománský	k2eAgNnSc1d1	Ománské
město	město	k1gNnSc1	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
přístav	přístav	k1gInSc1	přístav
Suhár	Suhár	k1gInSc1	Suhár
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgNnPc3d3	nejdůležitější
městům	město	k1gNnPc3	město
Arabského	arabský	k2eAgInSc2d1	arabský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Invaze	invaze	k1gFnSc1	invaze
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
dynastie	dynastie	k1gFnSc2	dynastie
Jarúba	Jarúba	k1gMnSc1	Jarúba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
snažilo	snažit	k5eAaImAgNnS	snažit
rozšířit	rozšířit	k5eAaPmF	rozšířit
svůj	svůj	k3xOyFgInSc4	svůj
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Ománu	Omán	k1gInSc2	Omán
v	v	k7c6	v
Arabském	arabský	k2eAgNnSc6d1	arabské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Portugalské	portugalský	k2eAgNnSc1d1	portugalské
vojsko	vojsko	k1gNnSc1	vojsko
vpadlo	vpadnout	k5eAaPmAgNnS	vpadnout
do	do	k7c2	do
Ománu	Omán	k1gInSc2	Omán
a	a	k8xC	a
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
část	část	k1gFnSc4	část
pobřežního	pobřežní	k2eAgNnSc2d1	pobřežní
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1508	[number]	k4	1508
až	až	k9	až
1648	[number]	k4	1648
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
140	[number]	k4	140
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kontrolovala	kontrolovat	k5eAaImAgFnS	kontrolovat
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
říše	říše	k1gFnSc1	říše
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
i	i	k8xC	i
ománské	ománský	k2eAgNnSc4d1	Ománské
město	město	k1gNnSc4	město
Maskat	Maskat	k1gInSc4	Maskat
<g/>
.	.	kIx.	.
</s>
<s>
Portugalci	Portugalec	k1gMnPc1	Portugalec
sem	sem	k6eAd1	sem
přicestovali	přicestovat	k5eAaPmAgMnP	přicestovat
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
mořeplavci	mořeplavec	k1gMnSc6	mořeplavec
Vasco	Vasco	k6eAd1	Vasco
da	da	k?	da
Gamovi	Gama	k1gMnSc3	Gama
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zde	zde	k6eAd1	zde
učinil	učinit	k5eAaPmAgMnS	učinit
mnoho	mnoho	k4c4	mnoho
objevů	objev	k1gInPc2	objev
včetně	včetně	k7c2	včetně
námořní	námořní	k2eAgFnSc2d1	námořní
trasy	trasa	k1gFnSc2	trasa
do	do	k7c2	do
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Portugalci	Portugalec	k1gMnPc1	Portugalec
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
základny	základna	k1gFnPc4	základna
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
svých	svůj	k3xOyFgFnPc2	svůj
tras	trasa	k1gFnPc2	trasa
<g/>
,	,	kIx,	,
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
svá	svůj	k3xOyFgNnPc4	svůj
opevněná	opevněný	k2eAgNnPc4d1	opevněné
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Pozůstatky	pozůstatek	k1gInPc4	pozůstatek
těchto	tento	k3xDgNnPc2	tento
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgInPc2d1	postavený
v	v	k7c6	v
koloniálních	koloniální	k2eAgInPc6d1	koloniální
architektonickém	architektonický	k2eAgInSc6d1	architektonický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1624	[number]	k4	1624
až	až	k9	až
1744	[number]	k4	1744
vládla	vládnout	k5eAaImAgFnS	vládnout
části	část	k1gFnPc4	část
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ománu	Omán	k1gInSc2	Omán
dynastie	dynastie	k1gFnSc2	dynastie
Jarúba	Jarúba	k1gFnSc1	Jarúba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
mnoho	mnoho	k4c1	mnoho
historických	historický	k2eAgFnPc2d1	historická
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
pevností	pevnost	k1gFnPc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Jarúba	Jarúba	k1gMnSc1	Jarúba
pocházel	pocházet	k5eAaImAgMnS	pocházet
i	i	k9	i
Sultán	sultán	k1gMnSc1	sultán
bin	bin	k?	bin
Saíf	Saíf	k1gMnSc1	Saíf
al-Jarúbi	al-Jarúb	k1gFnSc2	al-Jarúb
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
porazil	porazit	k5eAaPmAgInS	porazit
portugalská	portugalský	k2eAgNnPc4d1	portugalské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
vlády	vláda	k1gFnSc2	vláda
dynastie	dynastie	k1gFnSc2	dynastie
Jarúba	Jarúba	k1gFnSc1	Jarúba
propukla	propuknout	k5eAaPmAgFnS	propuknout
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
dohady	dohad	k1gInPc7	dohad
a	a	k8xC	a
spory	spor	k1gInPc7	spor
ohledně	ohledně	k7c2	ohledně
zvolení	zvolení	k1gNnSc2	zvolení
nového	nový	k2eAgMnSc2d1	nový
imáma	imám	k1gMnSc2	imám
<g/>
.	.	kIx.	.
<g/>
Portugalci	Portugalec	k1gMnPc1	Portugalec
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
Osmany	Osman	k1gMnPc4	Osman
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k8xC	však
zemi	zem	k1gFnSc6	zem
nekontrolovali	kontrolovat	k5eNaImAgMnP	kontrolovat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
o	o	k7c6	o
století	století	k1gNnSc6	století
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
<g/>
,	,	kIx,	,
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
Osmany	Osman	k1gMnPc4	Osman
příslušníci	příslušník	k1gMnPc1	příslušník
jemenského	jemenský	k2eAgInSc2d1	jemenský
kmene	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc4	jejichž
vůdce	vůdce	k1gMnSc1	vůdce
započal	započnout	k5eAaPmAgMnS	započnout
současnou	současný	k2eAgFnSc4d1	současná
linii	linie	k1gFnSc4	linie
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
sultánů	sultán	k1gMnPc2	sultán
dynastie	dynastie	k1gFnSc2	dynastie
al-Busaídů	al-Busaíd	k1gMnPc2	al-Busaíd
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Omán	Omán	k1gInSc1	Omán
nezávislý	závislý	k2eNgInSc1d1	nezávislý
a	a	k8xC	a
oproštěný	oproštěný	k2eAgMnSc1d1	oproštěný
od	od	k7c2	od
kontroly	kontrola	k1gFnSc2	kontrola
cizích	cizí	k2eAgFnPc2d1	cizí
mocností	mocnost	k1gFnPc2	mocnost
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
krátké	krátký	k2eAgFnSc2d1	krátká
perské	perský	k2eAgFnSc2d1	perská
invaze	invaze	k1gFnSc2	invaze
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
Peršanům	Peršan	k1gMnPc3	Peršan
podařilo	podařit	k5eAaPmAgNnS	podařit
na	na	k7c4	na
čas	čas	k1gInSc4	čas
získat	získat	k5eAaPmF	získat
některá	některý	k3yIgNnPc4	některý
přímořská	přímořský	k2eAgNnPc4d1	přímořské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Východoafrická	východoafrický	k2eAgFnSc1d1	východoafrická
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
dynastie	dynastie	k1gFnSc1	dynastie
Saídů	Saíd	k1gInPc2	Saíd
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
Britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vytlačovala	vytlačovat	k5eAaImAgNnP	vytlačovat
ománská	ománský	k2eAgNnPc1d1	Ománské
vojska	vojsko	k1gNnPc1	vojsko
imáma	imám	k1gMnSc2	imám
Saífa	Saíf	k1gMnSc2	Saíf
bin	bin	k?	bin
Sultána	sultán	k1gMnSc2	sultán
Portugalce	Portugalec	k1gMnSc2	Portugalec
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
překážkou	překážka	k1gFnSc7	překážka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
pevnost	pevnost	k1gFnSc1	pevnost
Fort	Fort	k?	Fort
Jesus	Jesus	k1gInSc1	Jesus
<g/>
,	,	kIx,	,
sídlo	sídlo	k1gNnSc1	sídlo
portugalské	portugalský	k2eAgFnSc2d1	portugalská
posádky	posádka	k1gFnSc2	posádka
v	v	k7c6	v
Mombase	Mombasa	k1gFnSc6	Mombasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1698	[number]	k4	1698
ji	on	k3xPp3gFnSc4	on
Saíf	Saíf	k1gMnSc1	Saíf
po	po	k7c6	po
dvouletém	dvouletý	k2eAgNnSc6d1	dvouleté
obléhání	obléhání	k1gNnSc6	obléhání
dobyl	dobýt	k5eAaPmAgInS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Stejným	stejný	k2eAgInSc7d1	stejný
rokem	rok	k1gInSc7	rok
vzniká	vznikat	k5eAaImIp3nS	vznikat
Zanzibarský	zanzibarský	k2eAgInSc1d1	zanzibarský
sultanát	sultanát	k1gInSc1	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Ománci	Ománek	k1gMnPc1	Ománek
snadno	snadno	k6eAd1	snadno
vyhánějí	vyhánět	k5eAaImIp3nP	vyhánět
Portugalce	Portugalec	k1gMnPc4	Portugalec
ze	z	k7c2	z
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
regionů	region	k1gInPc2	region
severně	severně	k6eAd1	severně
od	od	k7c2	od
Mosambiku	Mosambik	k1gInSc2	Mosambik
<g/>
.	.	kIx.	.
<g/>
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
byl	být	k5eAaImAgInS	být
cennou	cenný	k2eAgFnSc7d1	cenná
kořistí	kořist	k1gFnSc7	kořist
<g/>
,	,	kIx,	,
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
trhu	trh	k1gInSc3	trh
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgInS	stávat
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
tím	ten	k3xDgNnSc7	ten
důležitější	důležitý	k2eAgFnSc1d2	důležitější
částí	část	k1gFnPc2	část
Ománské	ománský	k2eAgFnSc2d1	ománská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
kapitole	kapitola	k1gFnSc6	kapitola
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
převzali	převzít	k5eAaPmAgMnP	převzít
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
příslušníci	příslušník	k1gMnPc1	příslušník
dynastie	dynastie	k1gFnSc2	dynastie
al-Busaídů	al-Busaíd	k1gMnPc2	al-Busaíd
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatelem	zakladatel	k1gMnSc7	zakladatel
dynastie	dynastie	k1gFnSc2	dynastie
byl	být	k5eAaImAgMnS	být
Ahmad	Ahmad	k1gInSc4	Ahmad
bin	bin	k?	bin
Saíd	Saíd	k1gInSc1	Saíd
<g/>
,	,	kIx,	,
zvolený	zvolený	k2eAgInSc1d1	zvolený
roku	rok	k1gInSc2	rok
1741	[number]	k4	1741
imámem	imám	k1gMnSc7	imám
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1775	[number]	k4	1775
jeho	jeho	k3xOp3gFnSc4	jeho
syn	syn	k1gMnSc1	syn
Saíd	Saíd	k1gMnSc1	Saíd
bin	bin	k?	bin
Ahmad	Ahmad	k1gInSc1	Ahmad
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
chtěla	chtít	k5eAaImAgFnS	chtít
udržet	udržet	k5eAaPmF	udržet
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
titul	titul	k1gInSc1	titul
sultána	sultán	k1gMnSc2	sultán
dědičným	dědičný	k2eAgFnPc3d1	dědičná
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
učinili	učinit	k5eAaPmAgMnP	učinit
Britové	Brit	k1gMnPc1	Brit
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
svůj	svůj	k3xOyFgInSc4	svůj
protektorát	protektorát	k1gInSc4	protektorát
a	a	k8xC	a
sultáni	sultán	k1gMnPc1	sultán
tak	tak	k6eAd1	tak
získali	získat	k5eAaPmAgMnP	získat
jejich	jejich	k3xOp3gFnSc4	jejich
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdní	pozdní	k2eAgFnSc6d1	pozdní
fázi	fáze	k1gFnSc6	fáze
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
pomoc	pomoc	k1gFnSc1	pomoc
užitečná	užitečný	k2eAgFnSc1d1	užitečná
především	především	k9	především
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
imámům	imám	k1gMnPc3	imám
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
být	být	k5eAaImF	být
opět	opět	k6eAd1	opět
voleni	volit	k5eAaImNgMnP	volit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
tradičními	tradiční	k2eAgInPc7d1	tradiční
ománskými	ománský	k2eAgInPc7d1	ománský
zvyky	zvyk	k1gInPc7	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Imám	imám	k1gMnSc1	imám
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
vnitrozemní	vnitrozemní	k2eAgFnSc4d1	vnitrozemní
oblast	oblast	k1gFnSc4	oblast
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ománu	Omán	k1gInSc2	Omán
<g/>
,	,	kIx,	,
ománský	ománský	k2eAgInSc1d1	ománský
imámát	imámát	k1gInSc1	imámát
<g/>
,	,	kIx,	,
s	s	k7c7	s
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nazvá	Nazvá	k1gFnSc1	Nazvá
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
moc	moc	k1gFnSc1	moc
pramenila	pramenit	k5eAaImAgFnS	pramenit
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
lokálními	lokální	k2eAgInPc7d1	lokální
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
čas	čas	k1gInSc4	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
na	na	k7c4	na
sultánem	sultán	k1gMnSc7	sultán
kontrolovaná	kontrolovaný	k2eAgNnPc1d1	kontrolované
pobřežní	pobřežní	k2eAgNnPc1d1	pobřežní
území	území	k1gNnPc1	území
<g/>
.	.	kIx.	.
<g/>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
ománských	ománský	k2eAgMnPc2d1	ománský
sultánů	sultán	k1gMnPc2	sultán
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Saíd	Saíd	k1gMnSc1	Saíd
bin	bin	k?	bin
Sultán	sultán	k1gMnSc1	sultán
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
roku	rok	k1gInSc2	rok
1837	[number]	k4	1837
učinit	učinit	k5eAaPmF	učinit
Zanzibar	Zanzibar	k1gInSc4	Zanzibar
svým	svůj	k3xOyFgNnSc7	svůj
hlavním	hlavní	k2eAgNnSc7d1	hlavní
sídlem	sídlo	k1gNnSc7	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Saíd	Saíd	k1gMnSc1	Saíd
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
impozantní	impozantní	k2eAgInPc4d1	impozantní
paláce	palác	k1gInPc4	palác
a	a	k8xC	a
zahrady	zahrada	k1gFnPc4	zahrada
a	a	k8xC	a
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
místní	místní	k2eAgFnSc2d1	místní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
vývozem	vývoz	k1gInSc7	vývoz
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
indiga	indigo	k1gNnSc2	indigo
a	a	k8xC	a
hřebíčku	hřebíček	k1gInSc2	hřebíček
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
naopak	naopak	k6eAd1	naopak
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
část	část	k1gFnSc4	část
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
trhu	trh	k1gInSc2	trh
díky	díky	k7c3	díky
snahám	snaha	k1gFnPc3	snaha
Britů	Brit	k1gMnPc2	Brit
o	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
již	již	k6eAd1	již
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
Ománem	Omán	k1gInSc7	Omán
a	a	k8xC	a
Zanzibarem	Zanzibar	k1gInSc7	Zanzibar
nefungovalo	fungovat	k5eNaImAgNnS	fungovat
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
dvěma	dva	k4xCgInPc7	dva
syny	syn	k1gMnPc7	syn
Majídem	Majíd	k1gMnSc7	Majíd
a	a	k8xC	a
Thuvajnim	Thuvajni	k1gNnSc7	Thuvajni
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
mocná	mocný	k2eAgFnSc1d1	mocná
britská	britský	k2eAgFnSc1d1	britská
diplomacie	diplomacie	k1gFnSc1	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
Majíd	Majíd	k6eAd1	Majíd
dědí	dědit	k5eAaImIp3nS	dědit
zanzibarský	zanzibarský	k2eAgInSc4d1	zanzibarský
trůn	trůn	k1gInSc4	trůn
společně	společně	k6eAd1	společně
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
území	území	k1gNnSc2	území
východní	východní	k2eAgFnSc2d1	východní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Thuvajni	Thuvajň	k1gMnSc3	Thuvajň
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vládcem	vládce	k1gMnSc7	vládce
Maskatu	Maskat	k1gInSc2	Maskat
a	a	k8xC	a
Ománu	Omán	k1gInSc2	Omán
<g/>
.	.	kIx.	.
</s>
<s>
Thuvajni	Thuvajn	k1gMnPc1	Thuvajn
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
si	se	k3xPyFc3	se
udrželi	udržet	k5eAaPmAgMnP	udržet
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
Ománem	Omán	k1gInSc7	Omán
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1856	[number]	k4	1856
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Maskat	Maskat	k1gInSc1	Maskat
a	a	k8xC	a
Omán	Omán	k1gInSc1	Omán
===	===	k?	===
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
navázal	navázat	k5eAaPmAgInS	navázat
styky	styk	k1gInPc4	styk
s	s	k7c7	s
dobovými	dobový	k2eAgFnPc7d1	dobová
velmocemi	velmoc	k1gFnPc7	velmoc
jako	jako	k8xS	jako
byla	být	k5eAaImAgFnS	být
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
nebo	nebo	k8xC	nebo
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Omán	Omán	k1gInSc4	Omán
do	do	k7c2	do
období	období	k1gNnSc2	období
úpadku	úpadek	k1gInSc2	úpadek
a	a	k8xC	a
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
<g/>
Pátrání	pátrání	k1gNnSc1	pátrání
po	po	k7c6	po
ropě	ropa	k1gFnSc6	ropa
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Ománu	Omán	k1gInSc2	Omán
začala	začít	k5eAaPmAgFnS	začít
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
společnost	společnost	k1gFnSc1	společnost
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Arcy	Arca	k1gFnPc1	Arca
Exploration	Exploration	k1gInSc4	Exploration
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
Anglo-Persian	Anglo-Persian	k1gMnSc1	Anglo-Persian
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
geologické	geologický	k2eAgInPc1d1	geologický
průzkumy	průzkum	k1gInPc1	průzkum
skončily	skončit	k5eAaPmAgInP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
události	událost	k1gFnPc1	událost
včetně	včetně	k7c2	včetně
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přerušily	přerušit	k5eAaPmAgInP	přerušit
průzkumy	průzkum	k1gInPc1	průzkum
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
již	již	k6eAd1	již
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
vrt	vrt	k1gInSc1	vrt
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ropný	ropný	k2eAgInSc1d1	ropný
vrt	vrt	k1gInSc1	vrt
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
v	v	k7c6	v
Jíbalu	Jíbal	k1gInSc6	Jíbal
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgInP	následovat
vrty	vrt	k1gInPc1	vrt
v	v	k7c6	v
Natíhu	Natíh	k1gInSc6	Natíh
a	a	k8xC	a
Fahúdu	Fahúd	k1gInSc6	Fahúd
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
obchodním	obchodní	k2eAgNnSc6d1	obchodní
měřítku	měřítko	k1gNnSc6	měřítko
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
<g/>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
je	být	k5eAaImIp3nS	být
moc	moc	k1gFnSc4	moc
imáma	imám	k1gMnSc2	imám
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
spojenců	spojenec	k1gMnPc2	spojenec
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
Britové	Brit	k1gMnPc1	Brit
nuceni	nutit	k5eAaImNgMnP	nutit
sjednat	sjednat	k5eAaPmF	sjednat
Síbskou	Síbská	k1gFnSc4	Síbská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
sultán	sultán	k1gMnSc1	sultán
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
autonomii	autonomie	k1gFnSc4	autonomie
vnitrozemního	vnitrozemní	k2eAgInSc2d1	vnitrozemní
regionu	region	k1gInSc2	region
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
v	v	k7c4	v
Nazvá	Nazvý	k2eAgNnPc4d1	Nazvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
tato	tento	k3xDgFnSc1	tento
smlouva	smlouva	k1gFnSc1	smlouva
nestačí	stačit	k5eNaBmIp3nS	stačit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
imám	imám	k1gMnSc1	imám
a	a	k8xC	a
silný	silný	k2eAgMnSc1d1	silný
kmenový	kmenový	k2eAgMnSc1d1	kmenový
vůdce	vůdce	k1gMnSc1	vůdce
získají	získat	k5eAaPmIp3nP	získat
pomoc	pomoc	k1gFnSc4	pomoc
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
na	na	k7c4	na
zřízení	zřízení	k1gNnSc4	zřízení
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
silám	síla	k1gFnPc3	síla
sultána	sultána	k1gFnSc1	sultána
Saída	Saíd	k1gMnSc2	Saíd
bin	bin	k?	bin
Tajmúra	Tajmúr	k1gInSc2	Tajmúr
podařilo	podařit	k5eAaPmAgNnS	podařit
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Britů	Brit	k1gMnPc2	Brit
zabránit	zabránit	k5eAaPmF	zabránit
vzpouře	vzpoura	k1gFnSc3	vzpoura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesla	přinést	k5eAaPmAgFnS	přinést
jinou	jiný	k2eAgFnSc4d1	jiná
hrozbu	hrozba	k1gFnSc4	hrozba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
marxistického	marxistický	k2eAgNnSc2d1	marxistické
partyzánského	partyzánský	k2eAgNnSc2d1	partyzánské
hnutí	hnutí	k1gNnSc2	hnutí
–	–	k?	–
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
Ománu	Omán	k1gInSc2	Omán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dafárské	Dafárský	k2eAgNnSc1d1	Dafárský
povstání	povstání	k1gNnSc1	povstání
===	===	k?	===
</s>
</p>
<p>
<s>
Dafárské	Dafárský	k2eAgNnSc1d1	Dafárský
povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc4	povstání
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Ománu	Omán	k1gInSc2	Omán
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Dafár	Dafár	k1gInSc1	Dafár
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1962	[number]	k4	1962
až	až	k9	až
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Dafárské	Dafárský	k2eAgNnSc1d1	Dafárský
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
trpělo	trpět	k5eAaImAgNnS	trpět
kvůli	kvůli	k7c3	kvůli
nedobré	dobrý	k2eNgFnSc3d1	nedobrá
hospodářské	hospodářský	k2eAgFnSc3d1	hospodářská
situaci	situace	k1gFnSc3	situace
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
severním	severní	k2eAgInPc3d1	severní
regionům	region	k1gInPc3	region
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
náchylné	náchylný	k2eAgNnSc1d1	náchylné
k	k	k7c3	k
vlnám	vlna	k1gFnPc3	vlna
arabského	arabský	k2eAgInSc2d1	arabský
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
si	se	k3xPyFc3	se
kladly	klást	k5eAaImAgFnP	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
svržení	svržení	k1gNnSc2	svržení
vládců	vládce	k1gMnPc2	vládce
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Dafáru	Dafár	k1gInSc6	Dafár
se	se	k3xPyFc4	se
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
na	na	k7c6	na
svrhnutí	svrhnutí	k1gNnSc6	svrhnutí
sultána	sultán	k1gMnSc2	sultán
Saída	Saíd	k1gMnSc2	Saíd
bin	bin	k?	bin
Tajmúra	Tajmúr	k1gMnSc2	Tajmúr
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
souznělo	souznít	k5eAaImAgNnS	souznít
s	s	k7c7	s
akcemi	akce	k1gFnPc7	akce
marxistů	marxista	k1gMnPc2	marxista
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Jižním	jižní	k2eAgInSc6d1	jižní
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
Dafárská	Dafárský	k2eAgFnSc1d1	Dafárský
fronta	fronta	k1gFnSc1	fronta
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
tvořená	tvořený	k2eAgFnSc1d1	tvořená
zejména	zejména	k9	zejména
arabskými	arabský	k2eAgMnPc7d1	arabský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
s	s	k7c7	s
konzervativním	konzervativní	k2eAgInSc7d1	konzervativní
pohledem	pohled	k1gInSc7	pohled
na	na	k7c4	na
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Dafárská	Dafárský	k2eAgFnSc1d1	Dafárský
fronta	fronta	k1gFnSc1	fronta
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
získala	získat	k5eAaPmAgFnS	získat
vůdce	vůdce	k1gMnSc4	vůdce
–	–	k?	–
šejky	šejk	k1gMnPc4	šejk
–	–	k?	–
místních	místní	k2eAgInPc2d1	místní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
prosadili	prosadit	k5eAaPmAgMnP	prosadit
marxisté	marxista	k1gMnPc1	marxista
a	a	k8xC	a
<g/>
,	,	kIx,	,
podporovaní	podporovaný	k2eAgMnPc1d1	podporovaný
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
navázali	navázat	k5eAaPmAgMnP	navázat
užší	úzký	k2eAgFnSc3d2	užší
spolupráci	spolupráce	k1gFnSc3	spolupráce
s	s	k7c7	s
Jižním	jižní	k2eAgInSc7d1	jižní
Jemenem	Jemen	k1gInSc7	Jemen
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
operace	operace	k1gFnPc4	operace
byl	být	k5eAaImAgInS	být
hornatý	hornatý	k2eAgInSc1d1	hornatý
jih	jih	k1gInSc1	jih
Dafáru	Dafár	k1gInSc2	Dafár
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
konzervativnějšího	konzervativní	k2eAgNnSc2d2	konzervativnější
smýšlení	smýšlení	k1gNnSc2	smýšlení
se	se	k3xPyFc4	se
od	od	k7c2	od
marxistů	marxista	k1gMnPc2	marxista
odvrátili	odvrátit	k5eAaPmAgMnP	odvrátit
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
nového	nový	k2eAgMnSc2d1	nový
sultána	sultán	k1gMnSc2	sultán
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
dostal	dostat	k5eAaPmAgMnS	dostat
Tajmúrův	Tajmúrův	k2eAgMnSc1d1	Tajmúrův
syn	syn	k1gMnSc1	syn
Kábús	Kábúsa	k1gFnPc2	Kábúsa
bin	bin	k?	bin
Saíd	Saíd	k1gMnSc1	Saíd
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sultán	sultán	k1gMnSc1	sultán
Kábús	Kábúsa	k1gFnPc2	Kábúsa
bin	bin	k?	bin
Saíd	Saíd	k1gInSc1	Saíd
získal	získat	k5eAaPmAgInS	získat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
a	a	k8xC	a
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
podporu	podpora	k1gFnSc4	podpora
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rovněž	rovněž	k9	rovněž
negativně	negativně	k6eAd1	negativně
vnímaly	vnímat	k5eAaImAgInP	vnímat
levicová	levicový	k2eAgNnPc1d1	levicové
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
vylodily	vylodit	k5eAaPmAgInP	vylodit
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc1	tři
tisíce	tisíc	k4xCgInPc1	tisíc
pozemních	pozemní	k2eAgMnPc2d1	pozemní
jednotek	jednotka	k1gFnPc2	jednotka
íránského	íránský	k2eAgMnSc2d1	íránský
šáha	šáh	k1gMnSc2	šáh
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
íránskými	íránský	k2eAgFnPc7d1	íránská
vzdušnými	vzdušný	k2eAgFnPc7d1	vzdušná
silami	síla	k1gFnPc7	síla
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
s	s	k7c7	s
ománskou	ománský	k2eAgFnSc7d1	ománská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
se	se	k3xPyFc4	se
dopravila	dopravit	k5eAaPmAgNnP	dopravit
i	i	k9	i
vojska	vojsko	k1gNnPc1	vojsko
z	z	k7c2	z
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
zajaty	zajmout	k5eAaPmNgFnP	zajmout
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
povstalců	povstalec	k1gMnPc2	povstalec
a	a	k8xC	a
po	po	k7c6	po
podrobení	podrobení	k1gNnSc6	podrobení
výcviku	výcvik	k1gInSc2	výcvik
byly	být	k5eAaImAgFnP	být
včleněni	včlenit	k5eAaPmNgMnP	včlenit
do	do	k7c2	do
Sultánových	sultánův	k2eAgFnPc2d1	sultánova
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
zřekli	zřeknout	k5eAaPmAgMnP	zřeknout
dřívějších	dřívější	k2eAgInPc2d1	dřívější
postojů	postoj	k1gInPc2	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
až	až	k9	až
1976	[number]	k4	1976
byli	být	k5eAaImAgMnP	být
rebelové	rebel	k1gMnPc1	rebel
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
zamezil	zamezit	k5eAaPmAgMnS	zamezit
novým	nový	k2eAgInPc3d1	nový
nepokojům	nepokoj	k1gInPc3	nepokoj
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
sultán	sultán	k1gMnSc1	sultán
Kábús	Kábús	k1gInSc4	Kábús
dotovat	dotovat	k5eAaBmF	dotovat
postižený	postižený	k2eAgInSc4d1	postižený
region	region	k1gInSc4	region
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
rozvinout	rozvinout	k5eAaPmF	rozvinout
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
–	–	k?	–
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
nemocnice	nemocnice	k1gFnSc2	nemocnice
a	a	k8xC	a
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Dafár	Dafár	k1gMnSc1	Dafár
svou	svůj	k3xOyFgFnSc7	svůj
úrovní	úroveň	k1gFnSc7	úroveň
rovnal	rovnat	k5eAaImAgMnS	rovnat
ostatním	ostatní	k2eAgFnPc3d1	ostatní
oblastem	oblast	k1gFnPc3	oblast
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
všemu	všecek	k3xTgNnSc3	všecek
napomohla	napomoct	k5eAaPmAgFnS	napomoct
i	i	k9	i
finanční	finanční	k2eAgFnSc1d1	finanční
výpomoc	výpomoc	k1gFnSc1	výpomoc
od	od	k7c2	od
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Abú	abú	k1gMnSc1	abú
Zabí	Zabí	k1gMnSc1	Zabí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
sultána	sultána	k1gFnSc1	sultána
Kábúse	Kábúse	k1gFnSc1	Kábúse
===	===	k?	===
</s>
</p>
<p>
<s>
Kábús	Kábús	k6eAd1	Kábús
se	se	k3xPyFc4	se
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
nejen	nejen	k6eAd1	nejen
sultánem	sultán	k1gMnSc7	sultán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
jeho	on	k3xPp3gNnSc2	on
panování	panování	k1gNnSc2	panování
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
z	z	k7c2	z
Maskat	Maskat	k1gInSc1	Maskat
a	a	k8xC	a
Omán	Omán	k1gInSc1	Omán
na	na	k7c4	na
Sultanát	sultanát	k1gInSc4	sultanát
Omán	Omán	k1gInSc1	Omán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
formálně	formálně	k6eAd1	formálně
uznává	uznávat	k5eAaImIp3nS	uznávat
Sultanát	sultanát	k1gInSc1	sultanát
Omán	Omán	k1gInSc1	Omán
dědičnou	dědičný	k2eAgFnSc7d1	dědičná
absolutistickou	absolutistický	k2eAgFnSc7d1	absolutistická
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Kábús	Kábús	k6eAd1	Kábús
modernizuje	modernizovat	k5eAaBmIp3nS	modernizovat
zemi	zem	k1gFnSc4	zem
využitím	využití	k1gNnSc7	využití
zásob	zásoba	k1gFnPc2	zásoba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2011	[number]	k4	2011
proběhly	proběhnout	k5eAaPmAgInP	proběhnout
pouliční	pouliční	k2eAgInPc1d1	pouliční
nepokoje	nepokoj	k1gInPc1	nepokoj
v	v	k7c6	v
přístavním	přístavní	k2eAgNnSc6d1	přístavní
městě	město	k1gNnSc6	město
Suhár	Suhár	k1gMnSc1	Suhár
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
tisíc	tisíc	k4xCgInSc1	tisíc
demonstrantů	demonstrant	k1gMnPc2	demonstrant
požadovalo	požadovat	k5eAaImAgNnS	požadovat
politické	politický	k2eAgFnPc4d1	politická
změny	změna	k1gFnPc4	změna
a	a	k8xC	a
lepší	dobrý	k2eAgFnPc4d2	lepší
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Vypálili	vypálit	k5eAaPmAgMnP	vypálit
policejní	policejní	k2eAgFnSc4d1	policejní
stanici	stanice	k1gFnSc4	stanice
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
státní	státní	k2eAgInPc1d1	státní
úřady	úřad	k1gInPc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
zastřelila	zastřelit	k5eAaPmAgFnS	zastřelit
nejméně	málo	k6eAd3	málo
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
Arabským	arabský	k2eAgNnSc7d1	arabské
mořem	moře	k1gNnSc7	moře
a	a	k8xC	a
Ománským	ománský	k2eAgInSc7d1	ománský
a	a	k8xC	a
Perským	perský	k2eAgInSc7d1	perský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nP	sousedit
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
arabskými	arabský	k2eAgInPc7d1	arabský
emiráty	emirát	k1gInPc7	emirát
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc2	hranice
410	[number]	k4	410
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jemenem	Jemen	k1gInSc7	Jemen
(	(	kIx(	(
<g/>
288	[number]	k4	288
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
a	a	k8xC	a
Saúdskou	saúdský	k2eAgFnSc7d1	Saúdská
Arábií	Arábie	k1gFnSc7	Arábie
(	(	kIx(	(
<g/>
676	[number]	k4	676
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
státu	stát	k1gInSc2	stát
podle	podle	k7c2	podle
ománských	ománský	k2eAgInPc2d1	ománský
zdrojů	zdroj	k1gInPc2	zdroj
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
212	[number]	k4	212
460	[number]	k4	460
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prameny	pramen	k1gInPc1	pramen
(	(	kIx(	(
<g/>
CIA	CIA	kA	CIA
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
až	až	k9	až
309	[number]	k4	309
500	[number]	k4	500
km2	km2	k4	km2
kvůli	kvůli	k7c3	kvůli
nejasnému	jasný	k2eNgNnSc3d1	nejasné
stanovení	stanovení	k1gNnSc3	stanovení
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
pouštních	pouštní	k2eAgFnPc6d1	pouštní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ománu	Omán	k1gInSc2	Omán
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
exklávy	exkláva	k1gFnPc1	exkláva
Madha	Madh	k1gMnSc2	Madh
a	a	k8xC	a
Musandam	Musandam	k1gInSc4	Musandam
a	a	k8xC	a
několik	několik	k4yIc4	několik
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Masíra	Masír	k1gMnSc2	Masír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
100	[number]	k4	100
až	až	k9	až
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
oblast	oblast	k1gFnSc1	oblast
al-Batína	al-Batín	k1gInSc2	al-Batín
je	být	k5eAaImIp3nS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
,	,	kIx,	,
od	od	k7c2	od
vnitrozemní	vnitrozemní	k2eAgFnSc2d1	vnitrozemní
pouště	poušť	k1gFnSc2	poušť
Rub	rub	k1gInSc1	rub
al-Chálí	al-Chálet	k5eAaPmIp3nS	al-Chálet
ji	on	k3xPp3gFnSc4	on
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
vysoké	vysoký	k2eAgNnSc1d1	vysoké
vápencové	vápencový	k2eAgNnSc1d1	vápencové
pohoří	pohoří	k1gNnSc1	pohoří
Al-Hadžar	Al-Hadžara	k1gFnPc2	Al-Hadžara
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
Arabská	arabský	k2eAgFnSc1d1	arabská
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
dosahující	dosahující	k2eAgFnPc1d1	dosahující
místy	místy	k6eAd1	místy
výšky	výška	k1gFnPc4	výška
až	až	k9	až
1	[number]	k4	1
400	[number]	k4	400
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
a	a	k8xC	a
horská	horský	k2eAgFnSc1d1	horská
oblast	oblast	k1gFnSc1	oblast
Karra	Karra	k1gFnSc1	Karra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Velkolepost	velkolepost	k1gFnSc1	velkolepost
a	a	k8xC	a
rozmanitost	rozmanitost	k1gFnSc1	rozmanitost
krajiny	krajina	k1gFnSc2	krajina
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
geologické	geologický	k2eAgFnSc2d1	geologická
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
klimatu	klima	k1gNnSc2	klima
posledních	poslední	k2eAgInPc2d1	poslední
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jedinečné	jedinečný	k2eAgInPc1d1	jedinečný
skalní	skalní	k2eAgInPc1d1	skalní
výčnělky	výčnělek	k1gInPc1	výčnělek
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Al-Hadžar	Al-Hadžara	k1gFnPc2	Al-Hadžara
<g/>
,	,	kIx,	,
Hukfu	Hukf	k1gInSc2	Hukf
a	a	k8xC	a
Dafáru	Dafár	k1gInSc2	Dafár
jsou	být	k5eAaImIp3nP	být
rájem	ráj	k1gInSc7	ráj
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgMnPc4d1	mezinárodní
geology	geolog	k1gMnPc4	geolog
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
geologické	geologický	k2eAgInPc1d1	geologický
záznamy	záznam	k1gInPc1	záznam
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
přibližně	přibližně	k6eAd1	přibližně
825	[number]	k4	825
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
alespoň	alespoň	k9	alespoň
tři	tři	k4xCgNnPc1	tři
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
ledem	led	k1gInSc7	led
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
poněkud	poněkud	k6eAd1	poněkud
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
zeměpisné	zeměpisný	k2eAgFnSc3d1	zeměpisná
šířce	šířka	k1gFnSc3	šířka
a	a	k8xC	a
podnebí	podnebí	k1gNnSc3	podnebí
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc4	Omán
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
cípu	cíp	k1gInSc6	cíp
Arabské	arabský	k2eAgFnSc2d1	arabská
tabule	tabule	k1gFnSc2	tabule
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
pomalu	pomalu	k6eAd1	pomalu
tlačí	tlačit	k5eAaImIp3nS	tlačit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Hory	hora	k1gFnSc2	hora
Al-Hadžar	Al-Hadžara	k1gFnPc2	Al-Hadžara
a	a	k8xC	a
potopená	potopený	k2eAgNnPc1d1	potopené
údolí	údolí	k1gNnPc1	údolí
Musandamu	Musandam	k1gInSc2	Musandam
jsou	být	k5eAaImIp3nP	být
toho	ten	k3xDgNnSc2	ten
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
geologicky	geologicky	k6eAd1	geologicky
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
položeno	položit	k5eAaPmNgNnS	položit
při	při	k7c6	při
okraji	okraj	k1gInSc6	okraj
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
nálezy	nález	k1gInPc1	nález
tmavě	tmavě	k6eAd1	tmavě
zabarvených	zabarvený	k2eAgInPc2d1	zabarvený
ofiolitů	ofiolit	k1gInPc2	ofiolit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
horniny	hornina	k1gFnPc1	hornina
z	z	k7c2	z
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
bohaté	bohatý	k2eAgFnSc2d1	bohatá
na	na	k7c4	na
měď	měď	k1gFnSc4	měď
a	a	k8xC	a
chrom	chrom	k1gInSc4	chrom
<g/>
.	.	kIx.	.
<g/>
Vnitrozemské	vnitrozemský	k2eAgFnPc1d1	vnitrozemská
pláně	pláň	k1gFnPc1	pláň
jsou	být	k5eAaImIp3nP	být
plné	plný	k2eAgFnPc1d1	plná
mladších	mladý	k2eAgFnPc2d2	mladší
<g/>
,	,	kIx,	,
usazených	usazený	k2eAgFnPc2d1	usazená
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
štěrku	štěrk	k1gInSc2	štěrk
<g/>
,	,	kIx,	,
dunového	dunový	k2eAgInSc2d1	dunový
písku	písek	k1gInSc2	písek
a	a	k8xC	a
solných	solný	k2eAgFnPc2d1	solná
zvětralin	zvětralina	k1gFnPc2	zvětralina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
usazenin	usazenina	k1gFnPc2	usazenina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgNnPc6	který
jsou	být	k5eAaImIp3nP	být
ukryty	ukryt	k2eAgInPc1d1	ukryt
ománské	ománský	k2eAgInPc1d1	ománský
uhlovodíkové	uhlovodíkový	k2eAgInPc1d1	uhlovodíkový
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Starověká	starověký	k2eAgFnSc1d1	starověká
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
z	z	k7c2	z
několika	několik	k4yIc2	několik
solných	solný	k2eAgFnPc2d1	solná
hor	hora	k1gFnPc2	hora
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
ku	k	k7c3	k
příkladu	příklad	k1gInSc3	příklad
Karat	Karat	k2eAgInSc1d1	Karat
Kibrit	Kibrit	k1gInSc1	Kibrit
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c4	v
utváření	utváření	k1gNnSc4	utváření
mnoha	mnoho	k4c2	mnoho
ropných	ropný	k2eAgMnPc2d1	ropný
a	a	k8xC	a
plynových	plynový	k2eAgNnPc2d1	plynové
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klima	klima	k1gNnSc1	klima
===	===	k?	===
</s>
</p>
<p>
<s>
Ománské	ománský	k2eAgNnSc1d1	Ománské
klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
topografií	topografie	k1gFnSc7	topografie
různorodé	různorodý	k2eAgInPc1d1	různorodý
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlhkými	vlhký	k2eAgFnPc7d1	vlhká
přímořskými	přímořský	k2eAgFnPc7d1	přímořská
oblastmi	oblast	k1gFnPc7	oblast
a	a	k8xC	a
tropicky	tropicky	k6eAd1	tropicky
teplým	teplý	k2eAgNnSc7d1	teplé
suchým	suchý	k2eAgNnSc7d1	suché
pouštním	pouštní	k2eAgNnSc7d1	pouštní
vnitrozemím	vnitrozemí	k1gNnSc7	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
jsou	být	k5eAaImIp3nP	být
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
zpravidla	zpravidla	k6eAd1	zpravidla
nepravidelné	pravidelný	k2eNgFnPc1d1	nepravidelná
a	a	k8xC	a
nepatrné	patrný	k2eNgFnPc1d1	patrný
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
položená	položený	k2eAgFnSc1d1	položená
provincie	provincie	k1gFnSc1	provincie
Dafár	Dafár	k1gInSc1	Dafár
zachytává	zachytávat	k5eAaImIp3nS	zachytávat
monzuny	monzun	k1gInPc4	monzun
z	z	k7c2	z
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
mezi	mezi	k7c7	mezi
červnem	červen	k1gInSc7	červen
a	a	k8xC	a
zářím	září	k1gNnSc7	září
<g/>
.	.	kIx.	.
</s>
<s>
Vnitrozemní	vnitrozemní	k2eAgFnPc1d1	vnitrozemní
letní	letní	k2eAgFnPc1d1	letní
teploty	teplota	k1gFnPc1	teplota
mohou	moct	k5eAaImIp3nP	moct
stoupnout	stoupnout	k5eAaPmF	stoupnout
až	až	k9	až
na	na	k7c4	na
54	[number]	k4	54
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
turistů	turist	k1gMnPc2	turist
zemi	zem	k1gFnSc4	zem
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
během	během	k7c2	během
mírněji	mírně	k6eAd2	mírně
teplých	teplý	k2eAgInPc2d1	teplý
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
říjnem	říjen	k1gInSc7	říjen
až	až	k8xS	až
dubnem	duben	k1gInSc7	duben
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
návštěvníky	návštěvník	k1gMnPc7	návštěvník
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
GCC	GCC	kA	GCC
<g/>
)	)	kIx)	)
upřednostňují	upřednostňovat	k5eAaImIp3nP	upřednostňovat
měsíce	měsíc	k1gInPc4	měsíc
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
období	období	k1gNnSc1	období
monzunů	monzun	k1gInPc2	monzun
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
Dafáru	Dafár	k1gInSc2	Dafár
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
panují	panovat	k5eAaImIp3nP	panovat
vůbec	vůbec	k9	vůbec
jedny	jeden	k4xCgFnPc1	jeden
z	z	k7c2	z
nejteplejších	teplý	k2eAgFnPc2d3	nejteplejší
nocí	noc	k1gFnPc2	noc
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ani	ani	k8xC	ani
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
denní	denní	k2eAgFnSc4d1	denní
dobu	doba	k1gFnSc4	doba
letní	letní	k2eAgFnSc2d1	letní
teploty	teplota	k1gFnSc2	teplota
neklesají	klesat	k5eNaImIp3nP	klesat
pod	pod	k7c7	pod
33	[number]	k4	33
°	°	k?	°
<g/>
C.	C.	kA	C.
<g/>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Maskat	Maskat	k1gInSc1	Maskat
má	mít	k5eAaImIp3nS	mít
horké	horký	k2eAgNnSc4d1	horké
tropické	tropický	k2eAgNnSc4d1	tropické
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
roční	roční	k2eAgFnSc7d1	roční
teplotou	teplota	k1gFnSc7	teplota
bezmála	bezmála	k6eAd1	bezmála
29	[number]	k4	29
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
mohou	moct	k5eAaImIp3nP	moct
teploty	teplota	k1gFnPc1	teplota
vystoupit	vystoupit	k5eAaPmF	vystoupit
na	na	k7c4	na
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
června	červen	k1gInSc2	červen
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
21	[number]	k4	21
°	°	k?	°
<g/>
C	C	kA	C
nejchladnějšího	chladný	k2eAgInSc2d3	nejchladnější
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
weather	weathra	k1gFnPc2	weathra
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
===	===	k?	===
Životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
===	===	k?	===
</s>
</p>
<p>
<s>
Pouštní	pouštní	k2eAgFnPc1d1	pouštní
křoviny	křovina	k1gFnPc1	křovina
a	a	k8xC	a
traviny	travina	k1gFnPc1	travina
<g/>
,	,	kIx,	,
společné	společný	k2eAgNnSc1d1	společné
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
jižní	jižní	k2eAgFnSc4d1	jižní
část	část	k1gFnSc4	část
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
<g/>
.	.	kIx.	.
</s>
<s>
Vegetace	vegetace	k1gFnSc1	vegetace
je	být	k5eAaImIp3nS	být
rozptýlená	rozptýlený	k2eAgFnSc1d1	rozptýlená
po	po	k7c6	po
vnitrozemské	vnitrozemský	k2eAgFnSc6d1	vnitrozemská
náhorní	náhorní	k2eAgFnSc6d1	náhorní
plošině	plošina	k1gFnSc6	plošina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
z	z	k7c2	z
valné	valný	k2eAgFnSc2d1	valná
části	část	k1gFnSc2	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
štěrková	štěrkový	k2eAgFnSc1d1	štěrková
poušť	poušť	k1gFnSc1	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Jejímu	její	k3xOp3gInSc3	její
růstu	růst	k1gInSc3	růst
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
monzunových	monzunový	k2eAgFnPc2d1	monzunová
srážek	srážka	k1gFnPc2	srážka
v	v	k7c6	v
Dafáru	Dafár	k1gInSc6	Dafár
a	a	k8xC	a
horách	hora	k1gFnPc6	hora
v	v	k7c6	v
období	období	k1gNnSc6	období
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
zde	zde	k6eAd1	zde
kokosové	kokosový	k2eAgFnPc1d1	kokosová
palmy	palma	k1gFnPc1	palma
(	(	kIx(	(
<g/>
především	především	k9	především
Dafár	Dafár	k1gInSc1	Dafár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odrůdy	odrůda	k1gFnPc1	odrůda
akácie	akácie	k1gFnPc1	akácie
<g/>
,	,	kIx,	,
oleandry	oleandr	k1gInPc1	oleandr
a	a	k8xC	a
kadidlovníky	kadidlovník	k1gInPc1	kadidlovník
(	(	kIx(	(
<g/>
vyvýšeniny	vyvýšenina	k1gFnPc1	vyvýšenina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohoří	k1gNnSc1	pohoří
Al-Hadžar	Al-Hadžara	k1gFnPc2	Al-Hadžara
je	být	k5eAaImIp3nS	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
místem	místo	k1gNnSc7	místo
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
tahr	tahr	k1gInSc1	tahr
arabský	arabský	k2eAgInSc1d1	arabský
<g/>
.	.	kIx.	.
<g/>
Zdejšími	zdejší	k2eAgMnPc7d1	zdejší
původními	původní	k2eAgMnPc7d1	původní
savci	savec	k1gMnPc7	savec
jsou	být	k5eAaImIp3nP	být
leopardi	leopard	k1gMnPc1	leopard
<g/>
,	,	kIx,	,
hyeny	hyena	k1gFnPc1	hyena
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
zajíci	zajíc	k1gMnPc1	zajíc
<g/>
,	,	kIx,	,
přímorožci	přímorožec	k1gMnPc1	přímorožec
a	a	k8xC	a
kozorožci	kozorožec	k1gMnPc1	kozorožec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
původní	původní	k2eAgMnPc4d1	původní
ptáky	pták	k1gMnPc4	pták
patří	patřit	k5eAaImIp3nP	patřit
supi	sup	k1gMnPc1	sup
<g/>
,	,	kIx,	,
orli	orel	k1gMnPc1	orel
<g/>
,	,	kIx,	,
čápi	čáp	k1gMnPc1	čáp
<g/>
,	,	kIx,	,
dropi	drop	k1gMnPc1	drop
<g/>
,	,	kIx,	,
koroptev	koroptev	k1gFnSc1	koroptev
arabská	arabský	k2eAgFnSc1d1	arabská
<g/>
,	,	kIx,	,
vlha	vlha	k1gFnSc1	vlha
<g/>
,	,	kIx,	,
sokoli	sokol	k1gMnPc1	sokol
a	a	k8xC	a
strdimilové	strdimil	k1gMnPc1	strdimil
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
Ománu	Omán	k1gInSc2	Omán
9	[number]	k4	9
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
5	[number]	k4	5
zástupců	zástupce	k1gMnPc2	zástupce
ptactva	ptactvo	k1gNnSc2	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
19	[number]	k4	19
druhy	druh	k1gInPc7	druh
rostlin	rostlina	k1gFnPc2	rostlina
rovněž	rovněž	k9	rovněž
visela	viset	k5eAaImAgFnS	viset
hrozba	hrozba	k1gFnSc1	hrozba
vymizení	vymizení	k1gNnSc4	vymizení
<g/>
.	.	kIx.	.
</s>
<s>
Sultán	sultán	k1gMnSc1	sultán
Kábús	Kábúsa	k1gFnPc2	Kábúsa
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
vyhubením	vyhubení	k1gNnSc7	vyhubení
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
několik	několik	k4yIc4	několik
výnosů	výnos	k1gInPc2	výnos
<g/>
,	,	kIx,	,
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc1	Omán
trpí	trpět	k5eAaImIp3nS	trpět
hned	hned	k9	hned
několika	několik	k4yIc7	několik
ekologickými	ekologický	k2eAgInPc7d1	ekologický
problémy	problém	k1gInPc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
znečištění	znečištění	k1gNnSc1	znečištění
pláží	pláž	k1gFnPc2	pláž
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
přímořských	přímořský	k2eAgFnPc2d1	přímořská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
proplouvání	proplouvání	k1gNnPc2	proplouvání
ropných	ropný	k2eAgInPc2d1	ropný
tankerů	tanker	k1gInPc2	tanker
Hormuzským	Hormuzský	k2eAgInSc7d1	Hormuzský
průlivem	průliv	k1gInSc7	průliv
a	a	k8xC	a
Ománským	ománský	k2eAgInSc7d1	ománský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
<g/>
,	,	kIx,	,
neméně	málo	k6eNd2	málo
závažným	závažný	k2eAgInSc7d1	závažný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
udržování	udržování	k1gNnSc1	udržování
přísunu	přísun	k1gInSc2	přísun
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
oblastí	oblast	k1gFnPc2	oblast
a	a	k8xC	a
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
disponuje	disponovat	k5eAaBmIp3nS	disponovat
pouhým	pouhý	k2eAgInSc7d1	pouhý
jedním	jeden	k4xCgInSc7	jeden
krychlovým	krychlový	k2eAgInSc7d1	krychlový
kilometrem	kilometr	k1gInSc7	kilometr
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
94	[number]	k4	94
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
užito	užít	k5eAaPmNgNnS	užít
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
2	[number]	k4	2
%	%	kIx~	%
při	při	k7c6	při
průmyslové	průmyslový	k2eAgFnSc6d1	průmyslová
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
30	[number]	k4	30
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
venkovských	venkovský	k2eAgMnPc2d1	venkovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
čisté	čistý	k2eAgFnSc3d1	čistá
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
nečetné	četný	k2eNgFnPc1d1	nečetná
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
nedostatečnému	dostatečný	k2eNgInSc3d1	nedostatečný
zásobování	zásobování	k1gNnSc4	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Forma	forma	k1gFnSc1	forma
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
je	být	k5eAaImIp3nS	být
absolutní	absolutní	k2eAgFnPc4d1	absolutní
monarchie	monarchie	k1gFnPc4	monarchie
a	a	k8xC	a
sultanát	sultanát	k1gInSc4	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Titul	titul	k1gInSc1	titul
sultána	sultán	k1gMnSc2	sultán
je	být	k5eAaImIp3nS	být
dědičný	dědičný	k2eAgInSc1d1	dědičný
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
sultánem	sultán	k1gMnSc7	sultán
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1970	[number]	k4	1970
Kábús	Kábús	k1gInSc1	Kábús
bin	bin	k?	bin
Saíd	Saíd	k1gInSc1	Saíd
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
od	od	k7c2	od
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1972	[number]	k4	1972
zastává	zastávat	k5eAaImIp3nS	zastávat
i	i	k9	i
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
jmenování	jmenování	k1gNnSc3	jmenování
<g/>
.	.	kIx.	.
</s>
<s>
Sultanát	sultanát	k1gInSc1	sultanát
disponuje	disponovat	k5eAaBmIp3nS	disponovat
dvoukomorovým	dvoukomorový	k2eAgInSc7d1	dvoukomorový
sborem	sbor	k1gInSc7	sbor
rádců	rádce	k1gMnPc2	rádce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
Rada	rada	k1gFnSc1	rada
Ománu	Omán	k1gInSc2	Omán
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
Ománu	Omán	k1gInSc2	Omán
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Státní	státní	k2eAgFnSc4d1	státní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc1d1	horní
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
Madžlis	Madžlis	k1gFnSc1	Madžlis
al-Daula	al-Daula	k1gFnSc1	al-Daula
<g/>
)	)	kIx)	)
a	a	k8xC	a
Poradní	poradní	k2eAgNnSc1d1	poradní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
<g/>
,	,	kIx,	,
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
Madžlis	Madžlis	k1gFnSc1	Madžlis
aš-Šura	aš-Šura	k1gFnSc1	aš-Šura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
zpochybňovat	zpochybňovat	k5eAaImF	zpochybňovat
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
vládních	vládní	k2eAgMnPc2d1	vládní
ministrů	ministr	k1gMnPc2	ministr
a	a	k8xC	a
doporučit	doporučit	k5eAaPmF	doporučit
změny	změna	k1gFnPc4	změna
nových	nový	k2eAgInPc2d1	nový
zákonů	zákon	k1gInPc2	zákon
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
doporučení	doporučení	k1gNnPc1	doporučení
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
úpravám	úprava	k1gFnPc3	úprava
navrhovaných	navrhovaný	k2eAgFnPc2d1	navrhovaná
usnesení	usnesení	k1gNnPc2	usnesení
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
obou	dva	k4xCgFnPc2	dva
komor	komora	k1gFnPc2	komora
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
justice	justice	k1gFnSc1	justice
===	===	k?	===
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
systém	systém	k1gInSc1	systém
si	se	k3xPyFc3	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
islámském	islámský	k2eAgNnSc6d1	islámské
a	a	k8xC	a
anglickém	anglický	k2eAgNnSc6d1	anglické
obecném	obecný	k2eAgNnSc6d1	obecné
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
Soudy	soud	k1gInPc1	soud
Šaría	Šarí	k1gInSc2	Šarí
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
Centrálním	centrální	k2eAgInSc7d1	centrální
soudním	soudní	k2eAgInSc7d1	soudní
dvorem	dvůr	k1gInSc7	dvůr
v	v	k7c6	v
Maskatu	Maskat	k1gInSc6	Maskat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
čtyři	čtyři	k4xCgInPc1	čtyři
soudy	soud	k1gInPc1	soud
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c4	v
Nazvá	Nazvý	k2eAgNnPc4d1	Nazvý
<g/>
,	,	kIx,	,
Salále	Salál	k1gInSc6	Salál
<g/>
,	,	kIx,	,
Suháru	Suhár	k1gInSc6	Suhár
a	a	k8xC	a
Súru	súra	k1gFnSc4	súra
<g/>
.	.	kIx.	.
</s>
<s>
Náboženští	náboženský	k2eAgMnPc1d1	náboženský
soudci	soudce	k1gMnPc1	soudce
(	(	kIx(	(
<g/>
kadi	kad	k1gMnPc1	kad
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jmenováni	jmenovat	k5eAaBmNgMnP	jmenovat
sultánem	sultán	k1gMnSc7	sultán
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
určováni	určovat	k5eAaImNgMnP	určovat
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vilájet	vilájet	k5eAaImF	vilájet
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Centrálního	centrální	k2eAgInSc2d1	centrální
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
odvolat	odvolat	k5eAaPmF	odvolat
k	k	k7c3	k
sultánovi	sultán	k1gMnSc3	sultán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
disponuje	disponovat	k5eAaBmIp3nS	disponovat
pravomocí	pravomoc	k1gFnSc7	pravomoc
udělení	udělení	k1gNnSc2	udělení
milosti	milost	k1gFnSc2	milost
<g/>
.	.	kIx.	.
</s>
<s>
Soudy	soud	k1gInPc1	soud
Šaría	Šaríum	k1gNnSc2	Šaríum
<g/>
,	,	kIx,	,
dbající	dbající	k2eAgFnSc2d1	dbající
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
islámského	islámský	k2eAgNnSc2d1	islámské
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
považují	považovat	k5eAaImIp3nP	považovat
svědectví	svědectví	k1gNnSc4	svědectví
jednoho	jeden	k4xCgMnSc2	jeden
muže	muž	k1gMnSc2	muž
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
žen	žena	k1gFnPc2	žena
za	za	k7c4	za
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
rovněž	rovněž	k9	rovněž
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
užívaný	užívaný	k2eAgInSc1d1	užívaný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
záležitostí	záležitost	k1gFnPc2	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Obchodní	obchodní	k2eAgInPc1d1	obchodní
spory	spor	k1gInPc1	spor
mohou	moct	k5eAaImIp3nP	moct
vyřešit	vyřešit	k5eAaPmF	vyřešit
Úřady	úřad	k1gInPc1	úřad
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
obchodních	obchodní	k2eAgInPc2d1	obchodní
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
sultán	sultán	k1gMnSc1	sultán
Kábús	Kábús	k1gInSc4	Kábús
základní	základní	k2eAgInSc4d1	základní
zákon	zákon	k1gInSc4	zákon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
občanům	občan	k1gMnPc3	občan
Ománu	Omán	k1gInSc2	Omán
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
jejich	jejich	k3xOp3gNnPc4	jejich
základní	základní	k2eAgNnPc4d1	základní
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgNnSc1d1	základní
právo	právo	k1gNnSc1	právo
utvrzuje	utvrzovat	k5eAaImIp3nS	utvrzovat
nezávislost	nezávislost	k1gFnSc4	nezávislost
justice	justice	k1gFnSc2	justice
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
zbaví	zbavit	k5eAaPmIp3nS	zbavit
porotců	porotce	k1gMnPc2	porotce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
vládní	vládní	k2eAgFnSc2d1	vládní
moci	moc	k1gFnSc2	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Sultanát	sultanát	k1gInSc1	sultanát
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
ústavu	ústava	k1gFnSc4	ústava
nebo	nebo	k8xC	nebo
legislaturu	legislatura	k1gFnSc4	legislatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
dal	dát	k5eAaPmAgMnS	dát
sultán	sultán	k1gMnSc1	sultán
pokyn	pokyn	k1gInSc4	pokyn
ke	k	k7c3	k
zformování	zformování	k1gNnSc3	zformování
ministerského	ministerský	k2eAgInSc2d1	ministerský
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
zodpovědného	zodpovědný	k2eAgInSc2d1	zodpovědný
za	za	k7c4	za
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
vládní	vládní	k2eAgNnPc4d1	vládní
ministerstva	ministerstvo	k1gNnPc4	ministerstvo
a	a	k8xC	a
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
sultán	sultán	k1gMnSc1	sultán
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ustanovit	ustanovit	k5eAaPmF	ustanovit
Státní	státní	k2eAgFnSc4d1	státní
konzultativní	konzultativní	k2eAgFnSc4d1	konzultativní
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
55	[number]	k4	55
jmenovaných	jmenovaný	k2eAgMnPc2d1	jmenovaný
představitelů	představitel	k1gMnPc2	představitel
z	z	k7c2	z
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
soukromého	soukromý	k2eAgInSc2d1	soukromý
sektoru	sektor	k1gInSc2	sektor
a	a	k8xC	a
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
Poradním	poradní	k2eAgNnSc7d1	poradní
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
59	[number]	k4	59
<g/>
člennou	členný	k2eAgFnSc7d1	členná
radou	rada	k1gFnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
ke	k	k7c3	k
spoluúčasti	spoluúčast	k1gFnSc3	spoluúčast
ománského	ománský	k2eAgInSc2d1	ománský
lidu	lid	k1gInSc2	lid
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
sultán	sultán	k1gMnSc1	sultán
Kábús	Kábús	k1gInSc4	Kábús
počet	počet	k1gInSc4	počet
křesel	křeslo	k1gNnPc2	křeslo
z	z	k7c2	z
59	[number]	k4	59
na	na	k7c4	na
80	[number]	k4	80
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
sultán	sultán	k1gMnSc1	sultán
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
základní	základní	k2eAgNnSc4d1	základní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Rada	rada	k1gFnSc1	rada
Ománu	Omán	k1gInSc2	Omán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
počet	počet	k1gInSc1	počet
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
Poradním	poradní	k2eAgNnSc6d1	poradní
shromáždění	shromáždění	k1gNnSc6	shromáždění
navýšen	navýšit	k5eAaPmNgInS	navýšit
na	na	k7c6	na
83	[number]	k4	83
<g/>
.	.	kIx.	.
<g/>
Volební	volební	k2eAgNnSc1d1	volební
právo	právo	k1gNnSc1	právo
získávají	získávat	k5eAaImIp3nP	získávat
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
po	po	k7c6	po
dosažení	dosažení	k1gNnSc6	dosažení
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
příslušníci	příslušník	k1gMnPc1	příslušník
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
právo	právo	k1gNnSc4	právo
volby	volba	k1gFnSc2	volba
nemají	mít	k5eNaImIp3nP	mít
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
Ománci	Ománek	k1gMnPc1	Ománek
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
tohoto	tento	k3xDgNnSc2	tento
práva	právo	k1gNnSc2	právo
využívali	využívat	k5eAaPmAgMnP	využívat
jen	jen	k9	jen
kmenoví	kmenový	k2eAgMnPc1d1	kmenový
vůdci	vůdce	k1gMnPc1	vůdce
<g/>
,	,	kIx,	,
obchodníci	obchodník	k1gMnPc1	obchodník
a	a	k8xC	a
vzdělanci	vzdělanec	k1gMnPc1	vzdělanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Vůči	vůči	k7c3	vůči
konfliktům	konflikt	k1gInPc3	konflikt
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
a	a	k8xC	a
Středním	střední	k2eAgInSc6d1	střední
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
Omán	Omán	k1gInSc1	Omán
staví	stavit	k5eAaPmIp3nS	stavit
umírněně	umírněně	k6eAd1	umírněně
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádné	mimořádný	k2eAgInPc1d1	mimořádný
vztahy	vztah	k1gInPc1	vztah
země	zem	k1gFnSc2	zem
udržuje	udržovat	k5eAaImIp3nS	udržovat
s	s	k7c7	s
členskými	členský	k2eAgInPc7d1	členský
státy	stát	k1gInPc7	stát
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
(	(	kIx(	(
<g/>
GCC	GCC	kA	GCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgFnPc4	jenž
má	mít	k5eAaImIp3nS	mít
tradiční	tradiční	k2eAgFnPc4d1	tradiční
silné	silný	k2eAgFnPc4d1	silná
vazby	vazba	k1gFnPc4	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Členy	člen	k1gInPc1	člen
GCC	GCC	kA	GCC
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k6eAd1	mimo
Ománu	Omán	k1gInSc2	Omán
také	také	k9	také
Bahrajn	Bahrajn	k1gInSc1	Bahrajn
<g/>
,	,	kIx,	,
Katar	katar	k1gInSc1	katar
<g/>
,	,	kIx,	,
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
arabské	arabský	k2eAgInPc1d1	arabský
emiráty	emirát	k1gInPc1	emirát
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
podporuje	podporovat	k5eAaImIp3nS	podporovat
společné	společný	k2eAgFnPc4d1	společná
akce	akce	k1gFnPc4	akce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Arabské	arabský	k2eAgFnSc2d1	arabská
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
současného	současný	k2eAgInSc2d1	současný
vývoje	vývoj	k1gInSc2	vývoj
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
podporuje	podporovat	k5eAaImIp3nS	podporovat
nynější	nynější	k2eAgInPc4d1	nynější
vládní	vládní	k2eAgInPc4d1	vládní
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
stability	stabilita	k1gFnSc2	stabilita
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
odsuzuje	odsuzovat	k5eAaImIp3nS	odsuzovat
praktiky	praktika	k1gFnPc4	praktika
Izraele	Izrael	k1gInSc2	Izrael
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
Ománu	Omán	k1gInSc2	Omán
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
napětí	napětí	k1gNnSc4	napětí
na	na	k7c6	na
okupovaných	okupovaný	k2eAgNnPc6d1	okupované
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Vybízí	vybízet	k5eAaImIp3nP	vybízet
k	k	k7c3	k
naplnění	naplnění	k1gNnSc3	naplnění
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
rezolucí	rezoluce	k1gFnPc2	rezoluce
a	a	k8xC	a
zřízení	zřízení	k1gNnSc6	zřízení
samostatného	samostatný	k2eAgInSc2d1	samostatný
Palestinského	palestinský	k2eAgInSc2d1	palestinský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Katarem	katar	k1gInSc7	katar
jedinou	jediný	k2eAgFnSc7d1	jediná
zemí	zem	k1gFnSc7	zem
GCC	GCC	kA	GCC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
udržuje	udržovat	k5eAaImIp3nS	udržovat
alespoň	alespoň	k9	alespoň
malé	malý	k2eAgInPc4d1	malý
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
udržuje	udržovat	k5eAaImIp3nS	udržovat
dobré	dobrý	k2eAgInPc4d1	dobrý
vztahy	vztah	k1gInPc4	vztah
i	i	k9	i
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
jinými	jiný	k2eAgInPc7d1	jiný
státy	stát	k1gInPc7	stát
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
od	od	k7c2	od
Čínské	čínský	k2eAgFnSc2d1	čínská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
východě	východ	k1gInSc6	východ
po	po	k7c4	po
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
podepsal	podepsat	k5eAaPmAgInS	podepsat
Omán	Omán	k1gInSc1	Omán
i	i	k9	i
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
volném	volný	k2eAgInSc6d1	volný
obchodu	obchod	k1gInSc6	obchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
plnil	plnit	k5eAaImAgInS	plnit
roli	role	k1gFnSc4	role
diplomatického	diplomatický	k2eAgInSc2d1	diplomatický
mostu	most	k1gInSc2	most
při	při	k7c6	při
rozhovorech	rozhovor	k1gInPc6	rozhovor
týkajících	týkající	k2eAgInPc2d1	týkající
se	se	k3xPyFc4	se
konfliktů	konflikt	k1gInPc2	konflikt
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Sýrii	Sýrie	k1gFnSc3	Sýrie
a	a	k8xC	a
Jemenu	Jemen	k1gInSc3	Jemen
(	(	kIx(	(
<g/>
na	na	k7c6	na
ománské	ománský	k2eAgFnSc6d1	ománská
půdě	půda	k1gFnSc6	půda
jednali	jednat	k5eAaImAgMnP	jednat
např.	např.	kA	např.
jemenští	jemenský	k2eAgMnPc1d1	jemenský
Hútíové	Hútíus	k1gMnPc1	Hútíus
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
proti	proti	k7c3	proti
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávanému	uznávaný	k2eAgMnSc3d1	uznávaný
prezidentovi	prezident	k1gMnSc3	prezident
Hádímu	Hádí	k1gMnSc3	Hádí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
v	v	k7c6	v
minulém	minulý	k2eAgNnSc6d1	Minulé
století	století	k1gNnSc6	století
i	i	k9	i
během	během	k7c2	během
irácko-íránské	irácko-íránský	k2eAgFnSc2d1	irácko-íránská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgInS	účastnit
se	se	k3xPyFc4	se
též	též	k9	též
jednání	jednání	k1gNnSc3	jednání
o	o	k7c6	o
íránském	íránský	k2eAgInSc6d1	íránský
jaderném	jaderný	k2eAgInSc6d1	jaderný
programu	program	k1gInSc6	program
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
katarská	katarský	k2eAgFnSc1d1	katarská
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
Omán	Omán	k1gInSc4	Omán
tradiční	tradiční	k2eAgInSc4d1	tradiční
neutrální	neutrální	k2eAgInSc4d1	neutrální
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
organizacích	organizace	k1gFnPc6	organizace
====	====	k?	====
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
IMF	IMF	kA	IMF
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UNESCO	Unesco	k1gNnSc1	Unesco
(	(	kIx(	(
<g/>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
UNIDO	UNIDO	kA	UNIDO
(	(	kIx(	(
<g/>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
pro	pro	k7c4	pro
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
WHO	WHO	kA	WHO
(	(	kIx(	(
<g/>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
WTO	WTO	kA	WTO
(	(	kIx(	(
<g/>
Světová	světový	k2eAgFnSc1d1	světová
obchodní	obchodní	k2eAgFnSc1d1	obchodní
organizace	organizace	k1gFnSc1	organizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
organizací	organizace	k1gFnPc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Interpol	interpol	k1gInSc1	interpol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
členem	člen	k1gMnSc7	člen
regionálních	regionální	k2eAgFnPc2d1	regionální
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Arabská	arabský	k2eAgFnSc1d1	arabská
liga	liga	k1gFnSc1	liga
a	a	k8xC	a
GCC	GCC	kA	GCC
(	(	kIx(	(
<g/>
Rada	rada	k1gFnSc1	rada
spolupráce	spolupráce	k1gFnSc2	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
====	====	k?	====
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
navázal	navázat	k5eAaPmAgInS	navázat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
oba	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
vzájemně	vzájemně	k6eAd1	vzájemně
vyměnily	vyměnit	k5eAaPmAgInP	vyměnit
nóty	nóta	k1gFnPc4	nóta
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
stagnují	stagnovat	k5eAaImIp3nP	stagnovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
Omán	Omán	k1gInSc4	Omán
navštívila	navštívit	k5eAaPmAgFnS	navštívit
náměstkyně	náměstkyně	k1gFnSc1	náměstkyně
ministra	ministr	k1gMnSc2	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
doposud	doposud	k6eAd1	doposud
nepodepsaly	podepsat	k5eNaPmAgInP	podepsat
žádné	žádný	k3yNgFnPc4	žádný
bilaterální	bilaterální	k2eAgFnPc4d1	bilaterální
dohody	dohoda	k1gFnPc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
obchodu	obchod	k1gInSc2	obchod
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
rok	rok	k1gInSc1	rok
od	od	k7c2	od
roku	rok	k1gInSc2	rok
navyšuje	navyšovat	k5eAaImIp3nS	navyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Omán	Omán	k1gInSc4	Omán
český	český	k2eAgMnSc1d1	český
politický	politický	k2eAgMnSc1d1	politický
konzultant	konzultant	k1gMnSc1	konzultant
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgInS	vést
jednání	jednání	k1gNnSc4	jednání
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
ománským	ománský	k2eAgInSc7d1	ománský
protějškem	protějšek	k1gInSc7	protějšek
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
položil	položit	k5eAaPmAgMnS	položit
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
své	svůj	k3xOyFgNnSc4	svůj
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
<g/>
,	,	kIx,	,
turisté	turist	k1gMnPc1	turist
tak	tak	k6eAd1	tak
případně	případně	k6eAd1	případně
musí	muset	k5eAaImIp3nS	muset
na	na	k7c4	na
blízké	blízký	k2eAgNnSc4d1	blízké
velvyslanectví	velvyslanectví	k1gNnSc4	velvyslanectví
ČR	ČR	kA	ČR
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
Omán	Omán	k1gInSc1	Omán
nemá	mít	k5eNaImIp3nS	mít
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
<g/>
,	,	kIx,	,
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
sousedním	sousední	k2eAgNnSc6d1	sousední
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Sultánovy	sultánův	k2eAgFnPc1d1	sultánova
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Sultan	Sultan	k1gInSc1	Sultan
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Armed	Armed	k1gMnSc1	Armed
Forces	Forces	k1gMnSc1	Forces
<g/>
,	,	kIx,	,
zkr.	zkr.	kA	zkr.
SAF	SAF	kA	SAF
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
složek	složka	k1gFnPc2	složka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Královská	královský	k2eAgFnSc1d1	královská
armáda	armáda	k1gFnSc1	armáda
Ománu	Omán	k1gInSc2	Omán
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Royal	Royal	k1gInSc1	Royal
Army	Arma	k1gFnSc2	Arma
of	of	k?	of
Oman	oman	k1gInSc1	oman
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Královské	královský	k2eAgNnSc1d1	královské
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Ománu	Omán	k1gInSc2	Omán
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Royal	Royal	k1gInSc1	Royal
Navy	Navy	k?	Navy
of	of	k?	of
Oman	oman	k1gInSc1	oman
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Královské	královský	k2eAgFnPc1d1	královská
vzdušné	vzdušný	k2eAgFnPc1d1	vzdušná
síly	síla	k1gFnPc1	síla
Ománu	Omán	k1gInSc2	Omán
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Royal	Royal	k1gInSc1	Royal
Air	Air	k1gFnSc2	Air
Force	force	k1gFnSc2	force
of	of	k?	of
Oman	oman	k1gInSc1	oman
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
disponovaly	disponovat	k5eAaBmAgFnP	disponovat
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
Ománu	Omán	k1gInSc2	Omán
celkově	celkově	k6eAd1	celkově
41	[number]	k4	41
700	[number]	k4	700
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
podružených	podružený	k2eAgFnPc2d1	podružený
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
armáda	armáda	k1gFnSc1	armáda
čítala	čítat	k5eAaImAgFnS	čítat
25	[number]	k4	25
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
tvořilo	tvořit	k5eAaImAgNnS	tvořit
ji	on	k3xPp3gFnSc4	on
přes	přes	k7c4	přes
100	[number]	k4	100
hlavních	hlavní	k2eAgInPc2d1	hlavní
bitevních	bitevní	k2eAgInPc2d1	bitevní
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
37	[number]	k4	37
tanků	tank	k1gInPc2	tank
typu	typ	k1gInSc2	typ
Scorpion	Scorpion	k1gInSc1	Scorpion
<g/>
.	.	kIx.	.
</s>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
personál	personál	k1gInSc1	personál
čítal	čítat	k5eAaImAgInS	čítat
4	[number]	k4	4
100	[number]	k4	100
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
disponoval	disponovat	k5eAaBmAgInS	disponovat
40	[number]	k4	40
bojovými	bojový	k2eAgInPc7d1	bojový
letouny	letoun	k1gInPc7	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
mělo	mít	k5eAaImAgNnS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
posádku	posádka	k1gFnSc4	posádka
o	o	k7c4	o
4	[number]	k4	4
200	[number]	k4	200
lidech	lid	k1gInPc6	lid
se	s	k7c7	s
13	[number]	k4	13
hlídkovými	hlídkový	k2eAgFnPc7d1	hlídková
loděmi	loď	k1gFnPc7	loď
<g/>
.	.	kIx.	.
</s>
<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
domobrany	domobrana	k1gFnPc1	domobrana
zvané	zvaný	k2eAgFnPc1d1	zvaná
Firqats	Firqats	k1gInSc4	Firqats
jsou	být	k5eAaImIp3nP	být
polovojenskou	polovojenský	k2eAgFnSc7d1	polovojenská
složkou	složka	k1gFnSc7	složka
a	a	k8xC	a
čítaly	čítat	k5eAaImAgInP	čítat
4	[number]	k4	4
000	[number]	k4	000
bojovníků	bojovník	k1gMnPc2	bojovník
anizovaných	anizovaný	k2eAgMnPc2d1	anizovaný
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
kmenových	kmenový	k2eAgFnPc6d1	kmenová
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
polovojenskými	polovojenský	k2eAgFnPc7d1	polovojenská
složkami	složka	k1gFnPc7	složka
jsou	být	k5eAaImIp3nP	být
pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
policejní	policejní	k2eAgFnSc1d1	policejní
hlídka	hlídka	k1gFnSc1	hlídka
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
400	[number]	k4	400
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
menší	malý	k2eAgFnSc1d2	menší
policejní	policejní	k2eAgFnSc1d1	policejní
letecké	letecký	k2eAgNnSc4d1	letecké
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byly	být	k5eAaImAgInP	být
vojenské	vojenský	k2eAgInPc1d1	vojenský
výdaje	výdaj	k1gInPc1	výdaj
země	zem	k1gFnSc2	zem
procentuálně	procentuálně	k6eAd1	procentuálně
vůči	vůči	k7c3	vůči
hrubému	hrubý	k2eAgInSc3d1	hrubý
domácímu	domácí	k2eAgInSc3d1	domácí
produktu	produkt	k1gInSc3	produkt
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
11,4	[number]	k4	11,4
%	%	kIx~	%
až	až	k9	až
11,8	[number]	k4	11,8
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgInS	vydat
Omán	Omán	k1gInSc1	Omán
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
1,545	[number]	k4	1,545
miliardy	miliarda	k4xCgFnSc2	miliarda
ománských	ománský	k2eAgMnPc2d1	ománský
riálů	riál	k1gInPc2	riál
čili	čili	k8xC	čili
4,003	[number]	k4	4,003
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
dobrovolná	dobrovolný	k2eAgFnSc1d1	dobrovolná
<g/>
,	,	kIx,	,
branná	branný	k2eAgFnSc1d1	Branná
povinnost	povinnost	k1gFnSc1	povinnost
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
===	===	k?	===
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
jedenáct	jedenáct	k4xCc4	jedenáct
guvernorátů	guvernorát	k1gInPc2	guvernorát
(	(	kIx(	(
<g/>
množné	množný	k2eAgNnSc4d1	množné
číslo	číslo	k1gNnSc4	číslo
<g/>
:	:	kIx,	:
muhafazat	muhafazat	k5eAaImF	muhafazat
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgNnSc4d1	jednotné
číslo	číslo	k1gNnSc4	číslo
<g/>
:	:	kIx,	:
muhafazat	muhafazat	k5eAaImF	muhafazat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
vilájety	vilájet	k1gInPc4	vilájet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ad-Dáchílija	ad-Dáchílija	k1gFnSc1	ad-Dáchílija
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ad-Zahíra	ad-Zahír	k1gMnSc4	ad-Zahír
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
al-Batína	al-Batína	k1gFnSc1	al-Batína
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
al-Batína	al-Batína	k1gFnSc1	al-Batína
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
al-Burajmi	al-Buraj	k1gFnPc7	al-Buraj
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
al-Wusta	al-Wusta	k1gFnSc1	al-Wusta
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
aš-Šarkíja	aš-Šarkíja	k1gFnSc1	aš-Šarkíja
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
aš-Šarkíja	aš-Šarkíja	k1gFnSc1	aš-Šarkíja
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dafár	Dafár	k1gInSc1	Dafár
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Maskat	Maskat	k1gInSc1	Maskat
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Musandam	Musandam	k1gInSc1	Musandam
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Ománu	Omán	k1gInSc2	Omán
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
ekonomik	ekonomika	k1gFnPc2	ekonomika
se	s	k7c7	s
středně	středně	k6eAd1	středně
vysokými	vysoký	k2eAgInPc7d1	vysoký
příjmy	příjem	k1gInPc7	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
69,48	[number]	k4	69,48
miliardy	miliarda	k4xCgFnSc2	miliarda
amerických	americký	k2eAgMnPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
tak	tak	k6eAd1	tak
připadalo	připadat	k5eAaPmAgNnS	připadat
23	[number]	k4	23
900	[number]	k4	900
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
činil	činit	k5eAaImAgInS	činit
reálný	reálný	k2eAgInSc1d1	reálný
růst	růst	k1gInSc1	růst
hrubého	hrubý	k2eAgInSc2d1	hrubý
domácího	domácí	k2eAgInSc2d1	domácí
produktu	produkt	k1gInSc2	produkt
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c4	na
17	[number]	k4	17
%	%	kIx~	%
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
oporou	opora	k1gFnSc7	opora
je	být	k5eAaImIp3nS	být
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
zpracování	zpracování	k1gNnSc1	zpracování
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
různé	různý	k2eAgInPc1d1	různý
projekty	projekt	k1gInPc1	projekt
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
snížit	snížit	k5eAaPmF	snížit
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
ubývajícím	ubývající	k2eAgNnSc6d1	ubývající
nerostném	nerostný	k2eAgNnSc6d1	nerostné
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
zmenšit	zmenšit	k5eAaPmF	zmenšit
přínos	přínos	k1gInSc4	přínos
tohoto	tento	k3xDgInSc2	tento
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
produkce	produkce	k1gFnSc2	produkce
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc2	produkce
ropy	ropa	k1gFnSc2	ropa
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
<g/>
Diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
cílí	cílit	k5eAaImIp3nS	cílit
především	především	k9	především
na	na	k7c4	na
turismus	turismus	k1gInSc4	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
je	být	k5eAaImIp3nS	být
země	zem	k1gFnSc2	zem
i	i	k8xC	i
důležitým	důležitý	k2eAgInSc7d1	důležitý
uzlem	uzel	k1gInSc7	uzel
námořní	námořní	k2eAgFnSc2d1	námořní
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc1d1	národní
měna	měna	k1gFnSc1	měna
<g/>
,	,	kIx,	,
ománský	ománský	k2eAgInSc1d1	ománský
rial	rial	k1gInSc1	rial
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejsilnějším	silný	k2eAgFnPc3d3	nejsilnější
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
stabilitě	stabilita	k1gFnSc3	stabilita
<g/>
.	.	kIx.	.
<g/>
Elektřinu	elektřina	k1gFnSc4	elektřina
Omán	Omán	k1gInSc4	Omán
nedováží	dovážit	k5eNaPmIp3nS	dovážit
ani	ani	k8xC	ani
nevyváží	vyvážit	k5eNaPmIp3nS	vyvážit
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
výroba	výroba	k1gFnSc1	výroba
(	(	kIx(	(
<g/>
13,58	[number]	k4	13,58
mld.	mld.	k?	mld.
kWh	kwh	kA	kwh
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
spotřeba	spotřeba	k1gFnSc1	spotřeba
(	(	kIx(	(
<g/>
11,36	[number]	k4	11,36
mld.	mld.	k?	mld.
kWh	kwh	kA	kwh
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Denní	denní	k2eAgFnSc1d1	denní
produkce	produkce	k1gFnSc1	produkce
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
806	[number]	k4	806
tisíc	tisíc	k4xCgInPc2	tisíc
barelů	barel	k1gInPc2	barel
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
kolem	kolem	k7c2	kolem
24	[number]	k4	24
miliard	miliarda	k4xCgFnPc2	miliarda
m3	m3	k4	m3
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
předním	přední	k2eAgFnPc3d1	přední
ománským	ománský	k2eAgFnPc3d1	ománská
společnostem	společnost	k1gFnPc3	společnost
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
Omantel	Omantel	k1gInSc1	Omantel
(	(	kIx(	(
<g/>
telekomunikace	telekomunikace	k1gFnPc1	telekomunikace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petroleum	Petroleum	k1gInSc1	Petroleum
Development	Development	k1gInSc1	Development
Oman	oman	k1gInSc1	oman
(	(	kIx(	(
<g/>
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Oman	oman	k1gInSc1	oman
Oil	Oil	k1gFnSc2	Oil
Company	Compana	k1gFnSc2	Compana
(	(	kIx(	(
<g/>
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oman	oman	k1gInSc1	oman
Air	Air	k1gFnSc2	Air
(	(	kIx(	(
<g/>
letecká	letecký	k2eAgFnSc1d1	letecká
doprava	doprava	k1gFnSc1	doprava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
trvající	trvající	k2eAgFnSc1d1	trvající
katarská	katarský	k2eAgFnSc1d1	katarská
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
krize	krize	k1gFnSc1	krize
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
Ománu	Omán	k1gInSc2	Omán
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
přístavů	přístav	k1gInPc2	přístav
dalších	další	k2eAgInPc2d1	další
členů	člen	k1gInPc2	člen
GCC	GCC	kA	GCC
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
ománské	ománský	k2eAgFnPc1d1	ománská
Katařanům	Katařan	k1gMnPc3	Katařan
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
<g/>
,	,	kIx,	,
plyne	plynout	k5eAaImIp3nS	plynout
tudy	tudy	k6eAd1	tudy
tudíž	tudíž	k8xC	tudíž
část	část	k1gFnSc1	část
katarského	katarský	k2eAgInSc2d1	katarský
importu	import	k1gInSc2	import
i	i	k8xC	i
exportu	export	k1gInSc2	export
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
na	na	k7c4	na
polovinu	polovina	k1gFnSc4	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
tvoří	tvořit	k5eAaImIp3nS	tvořit
jen	jen	k6eAd1	jen
necelá	celý	k2eNgFnSc1d1	necelá
2	[number]	k4	2
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
produkty	produkt	k1gInPc7	produkt
patří	patřit	k5eAaImIp3nS	patřit
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
datle	datle	k1gFnPc1	datle
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
velbloudi	velbloud	k1gMnPc1	velbloud
<g/>
,	,	kIx,	,
vojtěšky	vojtěška	k1gFnPc1	vojtěška
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
nejrychleji	rychle	k6eAd3	rychle
rostoucím	rostoucí	k2eAgNnPc3d1	rostoucí
odvětvím	odvětví	k1gNnPc3	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
oblasti	oblast	k1gFnSc2	oblast
se	se	k3xPyFc4	se
považují	považovat	k5eAaImIp3nP	považovat
Dafár	Dafár	k1gInSc1	Dafár
a	a	k8xC	a
al-Batína	al-Batína	k1gFnSc1	al-Batína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
má	mít	k5eAaImIp3nS	mít
dostatečně	dostatečně	k6eAd1	dostatečně
velký	velký	k2eAgInSc1d1	velký
potenciál	potenciál	k1gInSc1	potenciál
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
případný	případný	k2eAgInSc4d1	případný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
půdy	půda	k1gFnSc2	půda
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
v	v	k7c6	v
první	první	k4xOgFnSc6	první
řadě	řada	k1gFnSc6	řada
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dostupnosti	dostupnost	k1gFnSc6	dostupnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
přímořské	přímořský	k2eAgFnPc4d1	přímořská
oblasti	oblast	k1gFnPc4	oblast
al-Batína	al-Batín	k1gInSc2	al-Batín
a	a	k8xC	a
Šumajlíja	Šumajlíj	k1gInSc2	Šumajlíj
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc4	pěstování
čehokoli	cokoli	k3yInSc2	cokoli
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgFnSc2d1	obtížná
<g/>
.	.	kIx.	.
<g/>
Vody	voda	k1gFnSc2	voda
Ománského	ománský	k2eAgInSc2d1	ománský
zálivu	záliv	k1gInSc2	záliv
oplývají	oplývat	k5eAaImIp3nP	oplývat
množstvím	množství	k1gNnSc7	množství
různých	různý	k2eAgFnPc2d1	různá
ryb	ryba	k1gFnPc2	ryba
–	–	k?	–
barakud	barakuda	k1gFnPc2	barakuda
<g/>
,	,	kIx,	,
krevet	kreveta	k1gFnPc2	kreveta
<g/>
,	,	kIx,	,
humrů	humr	k1gMnPc2	humr
<g/>
,	,	kIx,	,
makrel	makrela	k1gFnPc2	makrela
<g/>
,	,	kIx,	,
raků	rak	k1gMnPc2	rak
<g/>
,	,	kIx,	,
sardinek	sardinka	k1gFnPc2	sardinka
<g/>
,	,	kIx,	,
tuňáků	tuňák	k1gMnPc2	tuňák
a	a	k8xC	a
žraloků	žralok	k1gMnPc2	žralok
<g/>
.	.	kIx.	.
</s>
<s>
Úlovky	úlovek	k1gInPc1	úlovek
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
měly	mít	k5eAaImAgFnP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
416	[number]	k4	416
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
sardinky	sardinka	k1gFnPc4	sardinka
<g/>
.	.	kIx.	.
</s>
<s>
Rybolov	rybolov	k1gInSc1	rybolov
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
přibližně	přibližně	k6eAd1	přibližně
26	[number]	k4	26
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vývoz	vývoz	k1gInSc1	vývoz
rybích	rybí	k2eAgInPc2d1	rybí
produktů	produkt	k1gInPc2	produkt
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
částky	částka	k1gFnSc2	částka
51,3	[number]	k4	51,3
milionu	milion	k4xCgInSc2	milion
USD	USD	kA	USD
(	(	kIx(	(
<g/>
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
době	doba	k1gFnSc6	doba
putovaly	putovat	k5eAaImAgFnP	putovat
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
zvýšení	zvýšení	k1gNnSc2	zvýšení
kvality	kvalita	k1gFnSc2	kvalita
přístavů	přístav	k1gInPc2	přístav
a	a	k8xC	a
celkového	celkový	k2eAgInSc2d1	celkový
rozvoje	rozvoj	k1gInSc2	rozvoj
rybolovu	rybolov	k1gInSc2	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Ománská	ománský	k2eAgFnSc1d1	ománská
vláda	vláda	k1gFnSc1	vláda
dotuje	dotovat	k5eAaBmIp3nS	dotovat
náklady	náklad	k1gInPc4	náklad
lodí	loď	k1gFnPc2	loď
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
zaměstnanosti	zaměstnanost	k1gFnSc2	zaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byly	být	k5eAaImAgInP	být
zprovozněny	zprovozněn	k2eAgInPc1d1	zprovozněn
tři	tři	k4xCgInPc1	tři
nové	nový	k2eAgInPc1d1	nový
rybářské	rybářský	k2eAgInPc1d1	rybářský
přístavy	přístav	k1gInPc1	přístav
(	(	kIx(	(
<g/>
Buchá	buchat	k5eAaImIp3nS	buchat
v	v	k7c6	v
Musandamu	Musandam	k1gInSc6	Musandam
<g/>
,	,	kIx,	,
Kurijat	Kurijat	k1gInSc1	Kurijat
a	a	k8xC	a
Šinas	Šinas	k1gInSc1	Šinas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
pojme	pojmout	k5eAaPmIp3nS	pojmout
až	až	k9	až
1	[number]	k4	1
000	[number]	k4	000
malých	malý	k2eAgFnPc2d1	malá
rybářských	rybářský	k2eAgFnPc2d1	rybářská
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
sektor	sektor	k1gInSc1	sektor
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
rozvojové	rozvojový	k2eAgFnSc2d1	rozvojová
strategie	strategie	k1gFnSc2	strategie
sultána	sultán	k1gMnSc2	sultán
Kábúse	Kábús	k1gMnSc2	Kábús
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
–	–	k?	–
<g/>
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přední	přední	k2eAgNnSc4d1	přední
odvětví	odvětví	k1gNnSc4	odvětví
spojené	spojený	k2eAgNnSc4d1	spojené
s	s	k7c7	s
diverzifikací	diverzifikace	k1gFnSc7	diverzifikace
státních	státní	k2eAgInPc2d1	státní
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
snížením	snížení	k1gNnSc7	snížení
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
ropě	ropa	k1gFnSc6	ropa
a	a	k8xC	a
zemním	zemní	k2eAgInSc6d1	zemní
plynu	plyn	k1gInSc6	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
pomoci	pomoct	k5eAaPmF	pomoct
naplňování	naplňování	k1gNnSc4	naplňování
potřeb	potřeba	k1gFnPc2	potřeba
sociálního	sociální	k2eAgInSc2d1	sociální
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
své	svůj	k3xOyFgInPc4	svůj
zdroje	zdroj	k1gInPc4	zdroj
na	na	k7c4	na
hotové	hotový	k2eAgInPc4d1	hotový
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejdůležitějším	důležitý	k2eAgFnPc3d3	nejdůležitější
složkám	složka	k1gFnPc3	složka
patří	patřit	k5eAaImIp3nS	patřit
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
rafinace	rafinace	k1gFnSc1	rafinace
surové	surový	k2eAgFnSc2d1	surová
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
produkce	produkce	k1gFnSc2	produkce
jak	jak	k8xS	jak
přírodního	přírodní	k2eAgNnSc2d1	přírodní
<g/>
,	,	kIx,	,
tak	tak	k9	tak
zkapalněného	zkapalněný	k2eAgInSc2d1	zkapalněný
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
a	a	k8xC	a
těžba	těžba	k1gFnSc1	těžba
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
ocelářství	ocelářství	k1gNnSc2	ocelářství
<g/>
,	,	kIx,	,
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
optických	optický	k2eAgNnPc2d1	optické
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
ze	z	k7c2	z
36	[number]	k4	36
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Továrny	továrna	k1gFnPc1	továrna
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Suháru	Suhár	k1gInSc6	Suhár
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
v	v	k7c6	v
al-Burajmi	al-Buraj	k1gFnPc7	al-Buraj
<g/>
,	,	kIx,	,
Nazvá	Nazvý	k2eAgFnSc1d1	Nazvý
<g/>
,	,	kIx,	,
Salále	Salála	k1gFnSc6	Salála
a	a	k8xC	a
Súru	súra	k1gFnSc4	súra
<g/>
.	.	kIx.	.
</s>
<s>
Expanzi	expanze	k1gFnSc4	expanze
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
závislém	závislý	k2eAgNnSc6d1	závislé
na	na	k7c6	na
zemním	zemní	k2eAgNnSc6d1	zemní
plynu	plyn	k1gInSc6	plyn
podporuje	podporovat	k5eAaImIp3nS	podporovat
jeho	jeho	k3xOp3gInSc4	jeho
dovoz	dovoz	k1gInSc4	dovoz
z	z	k7c2	z
těžebních	těžební	k2eAgFnPc2d1	těžební
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Firmy	firma	k1gFnPc1	firma
operující	operující	k2eAgFnPc1d1	operující
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
průmyslech	průmysl	k1gInPc6	průmysl
jsou	být	k5eAaImIp3nP	být
vládou	vláda	k1gFnSc7	vláda
osvobozené	osvobozený	k2eAgFnSc2d1	osvobozená
od	od	k7c2	od
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	on	k3xPp3gMnPc4	on
pobízí	pobízet	k5eAaImIp3nS	pobízet
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
rozvoji	rozvoj	k1gInSc3	rozvoj
a	a	k8xC	a
expanzi	expanze	k1gFnSc3	expanze
<g/>
.	.	kIx.	.
<g/>
V	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
7	[number]	k4	7
<g/>
.	.	kIx.	.
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
plán	plán	k1gInSc1	plán
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
tvorbu	tvorba	k1gFnSc4	tvorba
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
atraktivní	atraktivní	k2eAgNnSc4d1	atraktivní
investiční	investiční	k2eAgNnSc4d1	investiční
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
své	svůj	k3xOyFgFnSc2	svůj
strategie	strategie	k1gFnSc2	strategie
týkající	týkající	k2eAgInSc4d1	týkající
se	se	k3xPyFc4	se
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
sektoru	sektor	k1gInSc2	sektor
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
vláda	vláda	k1gFnSc1	vláda
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
(	(	kIx(	(
<g/>
IT	IT	kA	IT
<g/>
)	)	kIx)	)
a	a	k8xC	a
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
instituce	instituce	k1gFnSc1	instituce
The	The	k1gFnSc1	The
Knowledge	Knowledge	k1gFnSc1	Knowledge
Oasis	Oasis	k1gFnSc1	Oasis
Muscat	Muscat	k1gFnSc1	Muscat
(	(	kIx(	(
<g/>
KOM	koma	k1gFnPc2	koma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ománské	ománský	k2eAgInPc1d1	ománský
podniky	podnik	k1gInPc1	podnik
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
svůj	svůj	k3xOyFgInSc4	svůj
technologický	technologický	k2eAgInSc4d1	technologický
potenciál	potenciál	k1gInSc4	potenciál
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
spolupráce	spolupráce	k1gFnSc2	spolupráce
se	s	k7c7	s
společnostmi	společnost	k1gFnPc7	společnost
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
továren	továrna	k1gFnPc2	továrna
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
KOM	koma	k1gFnPc2	koma
v	v	k7c6	v
guvernorátu	guvernorát	k1gInSc6	guvernorát
Maskat	Maskat	k1gInSc1	Maskat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
má	mít	k5eAaImIp3nS	mít
sekundární	sekundární	k2eAgInSc1d1	sekundární
sektor	sektor	k1gInSc1	sektor
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
HDP	HDP	kA	HDP
navíc	navíc	k6eAd1	navíc
15	[number]	k4	15
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
ománských	ománský	k2eAgInPc2d1	ománský
podniků	podnik	k1gInPc2	podnik
odměněno	odměnit	k5eAaPmNgNnS	odměnit
pohárem	pohár	k1gInSc7	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Soutěž	soutěž	k1gFnSc1	soutěž
má	mít	k5eAaImIp3nS	mít
povzbudit	povzbudit	k5eAaPmF	povzbudit
ománské	ománský	k2eAgFnPc4d1	ománská
firmy	firma	k1gFnPc4	firma
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
kroku	krok	k1gInSc2	krok
se	se	k3xPyFc4	se
zahraničím	zahraničit	k5eAaPmIp1nS	zahraničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nerostné	nerostný	k2eAgFnPc4d1	nerostná
suroviny	surovina	k1gFnPc4	surovina
vyskytující	vyskytující	k2eAgFnPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
patří	patřit	k5eAaImIp3nS	patřit
dolomit	dolomit	k1gInSc1	dolomit
<g/>
,	,	kIx,	,
chromit	chromit	k1gInSc1	chromit
<g/>
,	,	kIx,	,
kobalt	kobalt	k1gInSc1	kobalt
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
sádrovec	sádrovec	k1gInSc1	sádrovec
<g/>
,	,	kIx,	,
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
zinek	zinek	k1gInSc1	zinek
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Surovinová	surovinový	k2eAgNnPc4d1	surovinové
naleziště	naleziště	k1gNnPc4	naleziště
brzy	brzy	k6eAd1	brzy
obklopily	obklopit	k5eAaPmAgInP	obklopit
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
podniky	podnik	k1gInPc1	podnik
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
rozvoje	rozvoj	k1gInSc2	rozvoj
strategie	strategie	k1gFnSc2	strategie
státu	stát	k1gInSc2	stát
a	a	k8xC	a
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
přispění	přispění	k1gNnSc3	přispění
k	k	k7c3	k
hrubému	hrubý	k2eAgInSc3d1	hrubý
domácímu	domácí	k2eAgInSc3d1	domácí
produktu	produkt	k1gInSc3	produkt
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
Ománce	Ománec	k1gInPc4	Ománec
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
měď	měď	k1gFnSc1	měď
se	se	k3xPyFc4	se
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
těží	těžet	k5eAaImIp3nS	těžet
po	po	k7c4	po
tisíce	tisíc	k4xCgInPc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Rovněž	rovněž	k6eAd1	rovněž
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
některé	některý	k3yIgInPc1	některý
projekty	projekt	k1gInPc1	projekt
včetně	včetně	k7c2	včetně
studie	studie	k1gFnSc2	studie
proveditelnosti	proveditelnost	k1gFnSc2	proveditelnost
využití	využití	k1gNnSc2	využití
zásob	zásoba	k1gFnPc2	zásoba
křemenné	křemenný	k2eAgFnSc2d1	křemenná
rudy	ruda	k1gFnSc2	ruda
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vádí	vádí	k1gNnSc2	vádí
Buwa	Buw	k1gInSc2	Buw
a	a	k8xC	a
Abutan	Abutan	k1gInSc1	Abutan
(	(	kIx(	(
<g/>
region	region	k1gInSc1	region
al-Wusta	al-Wust	k1gMnSc2	al-Wust
<g/>
)	)	kIx)	)
odhadované	odhadovaný	k2eAgFnPc1d1	odhadovaná
na	na	k7c4	na
28	[number]	k4	28
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komunikace	komunikace	k1gFnPc4	komunikace
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
aktivních	aktivní	k2eAgInPc2d1	aktivní
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,2	[number]	k4	3,2
milionu	milion	k4xCgInSc2	milion
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
na	na	k7c4	na
270	[number]	k4	270
tisíc	tisíc	k4xCgInPc2	tisíc
pevných	pevný	k2eAgFnPc2d1	pevná
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Internetová	internetový	k2eAgFnSc1d1	internetová
doména	doména	k1gFnSc1	doména
Ománu	Omán	k1gInSc2	Omán
je	být	k5eAaImIp3nS	být
.	.	kIx.	.
<g/>
om	om	k?	om
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
internetových	internetový	k2eAgMnPc2d1	internetový
uživatelů	uživatel	k1gMnPc2	uživatel
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
465	[number]	k4	465
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vysílaly	vysílat	k5eAaImAgInP	vysílat
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
AM	AM	kA	AM
3	[number]	k4	3
<g/>
,	,	kIx,	,
FM	FM	kA	FM
9	[number]	k4	9
a	a	k8xC	a
shortwave	shortwavat	k5eAaPmIp3nS	shortwavat
2	[number]	k4	2
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
25	[number]	k4	25
opakovačů	opakovač	k1gInPc2	opakovač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
poskytovatelem	poskytovatel	k1gMnSc7	poskytovatel
internetových	internetový	k2eAgFnPc2d1	internetová
služeb	služba	k1gFnPc2	služba
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
Omantel	Omantel	k1gInSc1	Omantel
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
státem	stát	k1gInSc7	stát
ze	z	k7c2	z
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Připojení	připojení	k1gNnSc1	připojení
přes	přes	k7c4	přes
Skype	Skyp	k1gInSc5	Skyp
je	být	k5eAaImIp3nS	být
blokováno	blokovat	k5eAaImNgNnS	blokovat
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
komunikační	komunikační	k2eAgInPc1d1	komunikační
programy	program	k1gInPc1	program
použít	použít	k5eAaPmF	použít
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
ovšem	ovšem	k9	ovšem
může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
místními	místní	k2eAgInPc7d1	místní
předpisy	předpis	k1gInPc7	předpis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
přilákat	přilákat	k5eAaPmF	přilákat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
turisty	turist	k1gMnPc4	turist
a	a	k8xC	a
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
investory	investor	k1gMnPc4	investor
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počet	počet	k1gInSc1	počet
turistů	turist	k1gMnPc2	turist
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2005	[number]	k4	2005
a	a	k8xC	a
2015	[number]	k4	2015
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS	zdvojnásobit
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
HDP	HDP	kA	HDP
6,6	[number]	k4	6,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
turismu	turismus	k1gInSc2	turismus
je	být	k5eAaImIp3nS	být
přesto	přesto	k8xC	přesto
opatrný	opatrný	k2eAgMnSc1d1	opatrný
<g/>
,	,	kIx,	,
turismus	turismus	k1gInSc1	turismus
samotný	samotný	k2eAgInSc1d1	samotný
byl	být	k5eAaImAgInS	být
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
vládou	vláda	k1gFnSc7	vláda
dřívějších	dřívější	k2eAgInPc2d1	dřívější
sultánů	sultán	k1gMnPc2	sultán
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
turisté	turist	k1gMnPc1	turist
musí	muset	k5eAaImIp3nP	muset
obdržet	obdržet	k5eAaPmF	obdržet
vízum	vízum	k1gNnSc4	vízum
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
občané	občan	k1gMnPc1	občan
států	stát	k1gInPc2	stát
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
občané	občan	k1gMnPc1	občan
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
tvoří	tvořit	k5eAaImIp3nP	tvořit
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
každý	každý	k3xTgMnSc1	každý
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
je	být	k5eAaImIp3nS	být
původem	původ	k1gInSc7	původ
z	z	k7c2	z
asijského	asijský	k2eAgInSc2d1	asijský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
GCC	GCC	kA	GCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
pátý	pátý	k4xOgMnSc1	pátý
je	být	k5eAaImIp3nS	být
Evropan	Evropan	k1gMnSc1	Evropan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
zemi	zem	k1gFnSc4	zem
okolo	okolo	k7c2	okolo
612	[number]	k4	612
000	[number]	k4	000
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
si	se	k3xPyFc3	se
našlo	najít	k5eAaPmAgNnS	najít
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Ománu	Omán	k1gInSc2	Omán
přes	přes	k7c4	přes
2	[number]	k4	2
a	a	k8xC	a
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Světové	světový	k2eAgFnSc2d1	světová
rady	rada	k1gFnSc2	rada
cestování	cestování	k1gNnSc2	cestování
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
(	(	kIx(	(
<g/>
World	World	k1gMnSc1	World
Travel	Travel	k1gMnSc1	Travel
and	and	k?	and
Tourism	Tourism	k1gMnSc1	Tourism
Council	Council	k1gMnSc1	Council
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
WTTC	WTTC	kA	WTTC
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
však	však	k9	však
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
očekávat	očekávat	k5eAaImF	očekávat
nárůst	nárůst	k1gInSc1	nárůst
přispění	přispění	k1gNnSc2	přispění
sektoru	sektor	k1gInSc2	sektor
cestovní	cestovní	k2eAgFnSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
k	k	k7c3	k
hrubému	hrubý	k2eAgInSc3d1	hrubý
domácímu	domácí	k2eAgInSc3d1	domácí
produktu	produkt	k1gInSc3	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Omán	Omán	k1gInSc1	Omán
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
své	svůj	k3xOyFgInPc4	svůj
obchodní	obchodní	k2eAgInPc4d1	obchodní
vztahy	vztah	k1gInPc4	vztah
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
sousedy	soused	k1gMnPc7	soused
a	a	k8xC	a
činí	činit	k5eAaImIp3nS	činit
kroky	krok	k1gInPc4	krok
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
pamětihodností	pamětihodnost	k1gFnPc2	pamětihodnost
a	a	k8xC	a
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
WTTC	WTTC	kA	WTTC
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
objem	objem	k1gInSc1	objem
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
8,84	[number]	k4	8,84
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
palác	palác	k1gInSc1	palác
Al	ala	k1gFnPc2	ala
Alam	Alam	k1gInSc1	Alam
<g/>
,	,	kIx,	,
Mešita	mešita	k1gFnSc1	mešita
sultána	sultán	k1gMnSc2	sultán
Kábúse	Kábús	k1gMnSc2	Kábús
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
mešity	mešita	k1gFnSc2	mešita
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
pevností	pevnost	k1gFnPc2	pevnost
-	-	kIx~	-
an-Nachal	an-Nachat	k5eAaImAgInS	an-Nachat
<g/>
,	,	kIx,	,
Nazvá	Nazvá	k1gFnSc1	Nazvá
<g/>
,	,	kIx,	,
Bahla	Bahla	k1gFnSc1	Bahla
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Bahla	Bahla	k1gFnSc1	Bahla
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
třemi	tři	k4xCgMnPc7	tři
památkami	památka	k1gFnPc7	památka
zapsána	zapsat	k5eAaPmNgNnP	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Pátou	pátý	k4xOgFnSc7	pátý
památkou	památka	k1gFnSc7	památka
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
bylo	být	k5eAaImAgNnS	být
útočiště	útočiště	k1gNnSc1	útočiště
přímorožce	přímorožec	k1gMnSc2	přímorožec
arabského	arabský	k2eAgMnSc2d1	arabský
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
památka	památka	k1gFnSc1	památka
UNESCO	Unesco	k1gNnSc1	Unesco
vyškrtnuto	vyškrtnout	k5eAaPmNgNnS	vyškrtnout
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
.	.	kIx.	.
<g/>
Prostředí	prostředí	k1gNnSc1	prostředí
města	město	k1gNnSc2	město
Salála	Salál	k1gMnSc2	Salál
<g/>
,	,	kIx,	,
přístavu	přístav	k1gInSc2	přístav
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
země	zem	k1gFnSc2	zem
známém	známý	k1gMnSc6	známý
jako	jako	k8xC	jako
Dafár	Dafár	k1gInSc4	Dafár
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
další	další	k2eAgInSc1d1	další
vyhledávaným	vyhledávaný	k2eAgNnSc7d1	vyhledávané
místem	místo	k1gNnSc7	místo
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
po	po	k7c6	po
období	období	k1gNnSc6	období
charíf	charíf	k1gInSc1	charíf
(	(	kIx(	(
<g/>
období	období	k1gNnSc1	období
monzunů	monzun	k1gInPc2	monzun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
Ománu	Omán	k1gInSc3	Omán
nachází	nacházet	k5eAaImIp3nS	nacházet
tři	tři	k4xCgInPc4	tři
heliporty	heliport	k1gInPc4	heliport
a	a	k8xC	a
128	[number]	k4	128
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
má	mít	k5eAaImIp3nS	mít
deset	deset	k4xCc4	deset
zpevněnou	zpevněný	k2eAgFnSc7d1	zpevněná
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
nikoliv	nikoliv	k9	nikoliv
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
vozovek	vozovka	k1gFnPc2	vozovka
je	být	k5eAaImIp3nS	být
42	[number]	k4	42
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
16	[number]	k4	16
500	[number]	k4	500
kilometrů	kilometr	k1gInPc2	kilometr
spadá	spadat	k5eAaImIp3nS	spadat
mezi	mezi	k7c4	mezi
zpevněné	zpevněný	k2eAgFnPc4d1	zpevněná
a	a	k8xC	a
zbylých	zbylý	k2eAgNnPc2d1	zbylé
25	[number]	k4	25
800	[number]	k4	800
kilometrů	kilometr	k1gInPc2	kilometr
k	k	k7c3	k
nezpevněným	zpevněný	k2eNgFnPc3d1	nezpevněná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
plynovody	plynovod	k1gInPc1	plynovod
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
délka	délka	k1gFnSc1	délka
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
4	[number]	k4	4
126	[number]	k4	126
kilometrů	kilometr	k1gInPc2	kilometr
a	a	k8xC	a
ropovody	ropovod	k1gInPc4	ropovod
<g/>
,	,	kIx,	,
s	s	k7c7	s
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
3	[number]	k4	3
558	[number]	k4	558
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ománské	ománský	k2eAgNnSc1d1	Ománské
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
chemického	chemický	k2eAgInSc2d1	chemický
tankeru	tanker	k1gInSc2	tanker
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgFnSc2d1	cestovní
lodi	loď	k1gFnSc2	loď
a	a	k8xC	a
cestovní	cestovní	k2eAgFnSc2d1	cestovní
<g/>
/	/	kIx~	/
<g/>
nákladní	nákladní	k2eAgFnSc2d1	nákladní
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgInPc4d1	významný
přístavy	přístav	k1gInPc4	přístav
a	a	k8xC	a
terminály	terminál	k1gInPc4	terminál
patří	patřit	k5eAaImIp3nS	patřit
Miná	Miná	k1gFnSc1	Miná
Kábús	Kábús	k1gInSc1	Kábús
a	a	k8xC	a
Salála	Salála	k1gFnSc1	Salála
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc1	Omán
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
členy	člen	k1gInPc7	člen
GCC	GCC	kA	GCC
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gulf	Gulf	k1gMnSc1	Gulf
Railway	Railwaa	k1gFnSc2	Railwaa
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
všech	všecek	k3xTgInPc2	všecek
šest	šest	k4xCc4	šest
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
propojit	propojit	k5eAaPmF	propojit
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
odhadem	odhad	k1gInSc7	odhad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
tří	tři	k4xCgInPc2	tři
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Většinovým	většinový	k2eAgNnSc7d1	většinové
neboli	neboli	k8xC	neboli
majoritním	majoritní	k2eAgNnSc7d1	majoritní
etnikem	etnikum	k1gNnSc7	etnikum
jsou	být	k5eAaImIp3nP	být
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
(	(	kIx(	(
<g/>
Balúčové	Balúčové	k2eAgMnSc1d1	Balúčové
<g/>
)	)	kIx)	)
a	a	k8xC	a
Srí	Srí	k1gFnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
přistěhovaleckou	přistěhovalecký	k2eAgFnSc4d1	přistěhovalecká
komunitu	komunita	k1gFnSc4	komunita
tvoří	tvořit	k5eAaImIp3nP	tvořit
Indové	Ind	k1gMnPc1	Ind
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k6eAd1	kolem
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
rozloženo	rozložit	k5eAaPmNgNnS	rozložit
nerovnoměrně	rovnoměrně	k6eNd1	rovnoměrně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pouštní	pouštní	k2eAgFnPc4d1	pouštní
oblasti	oblast	k1gFnPc4	oblast
jsou	být	k5eAaImIp3nP	být
takřka	takřka	k6eAd1	takřka
neobydleny	obydlen	k2eNgFnPc1d1	neobydlena
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
umístěné	umístěný	k2eAgInPc4d1	umístěný
guvernoráty	guvernorát	k1gInPc4	guvernorát
Severní	severní	k2eAgFnSc1d1	severní
al-Batína	al-Batína	k1gFnSc1	al-Batína
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc1d1	jižní
al-Batína	al-Batína	k1gFnSc1	al-Batína
a	a	k8xC	a
východně	východně	k6eAd1	východně
od	od	k7c2	od
nich	on	k3xPp3gInPc2	on
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblast	oblast	k1gFnSc4	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Maskatu	Maskat	k1gInSc2	Maskat
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvíce	hodně	k6eAd3	hodně
zalidněným	zalidněný	k2eAgFnPc3d1	zalidněná
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
mateřské	mateřský	k2eAgInPc1d1	mateřský
jazyky	jazyk	k1gInPc1	jazyk
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
–	–	k?	–
balúčština	balúčština	k1gFnSc1	balúčština
<g/>
,	,	kIx,	,
urdština	urdština	k1gFnSc1	urdština
a	a	k8xC	a
různé	různý	k2eAgInPc1d1	různý
indické	indický	k2eAgInPc1d1	indický
dialekty	dialekt	k1gInPc1	dialekt
<g/>
.	.	kIx.	.
</s>
<s>
Gramotnost	gramotnost	k1gFnSc1	gramotnost
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
81	[number]	k4	81
%	%	kIx~	%
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgMnSc1d2	vyšší
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
chlapců	chlapec	k1gMnPc2	chlapec
či	či	k8xC	či
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
86,8	[number]	k4	86,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
nižší	nízký	k2eAgMnSc1d2	nižší
u	u	k7c2	u
dívek	dívka	k1gFnPc2	dívka
nebo	nebo	k8xC	nebo
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
73,5	[number]	k4	73,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Míra	Míra	k1gFnSc1	Míra
populačního	populační	k2eAgInSc2d1	populační
růstu	růst	k1gInSc2	růst
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
The	The	k1gFnSc2	The
World	World	k1gMnSc1	World
Factbook	Factbook	k1gInSc1	Factbook
na	na	k7c6	na
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
do	do	k7c2	do
14	[number]	k4	14
let	léto	k1gNnPc2	léto
tvoří	tvořit	k5eAaImIp3nS	tvořit
43	[number]	k4	43
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
,	,	kIx,	,
od	od	k7c2	od
14	[number]	k4	14
do	do	k7c2	do
64	[number]	k4	64
let	léto	k1gNnPc2	léto
54	[number]	k4	54
%	%	kIx~	%
a	a	k8xC	a
pouhá	pouhý	k2eAgFnSc1d1	pouhá
3	[number]	k4	3
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
nad	nad	k7c7	nad
65	[number]	k4	65
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
věk	věk	k1gInSc1	věk
u	u	k7c2	u
muže	muž	k1gMnSc2	muž
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
25	[number]	k4	25
a	a	k8xC	a
26	[number]	k4	26
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
72	[number]	k4	72
%	%	kIx~	%
Ománců	Ománec	k1gInPc2	Ománec
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
a	a	k8xC	a
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
roční	roční	k2eAgInSc1d1	roční
dvouprocentní	dvouprocentní	k2eAgInSc1d1	dvouprocentní
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
plodnosti	plodnost	k1gFnSc2	plodnost
(	(	kIx(	(
<g/>
fertility	fertilita	k1gFnSc2	fertilita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
2,87	[number]	k4	2,87
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc1	Omán
hostí	hostit	k5eAaImIp3nS	hostit
přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
uprchlíků	uprchlík	k1gMnPc2	uprchlík
ze	z	k7c2	z
sousedního	sousední	k2eAgInSc2d1	sousední
Jemenu	Jemen	k1gInSc2	Jemen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
probíhá	probíhat	k5eAaImIp3nS	probíhat
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
je	být	k5eAaImIp3nS	být
občanům	občan	k1gMnPc3	občan
Ománu	Omán	k1gInSc2	Omán
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
bezplatně	bezplatně	k6eAd1	bezplatně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
cizinci	cizinec	k1gMnPc1	cizinec
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
musejí	muset	k5eAaImIp3nP	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
úrovně	úroveň	k1gFnPc4	úroveň
regionálně	regionálně	k6eAd1	regionálně
i	i	k8xC	i
mezinárodně	mezinárodně	k6eAd1	mezinárodně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
Dětského	dětský	k2eAgInSc2d1	dětský
fondu	fond	k1gInSc2	fond
OSN	OSN	kA	OSN
a	a	k8xC	a
Rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
programu	program	k1gInSc2	program
OSN	OSN	kA	OSN
dostál	dostát	k5eAaPmAgInS	dostát
Ománský	ománský	k2eAgInSc1d1	ománský
sultanát	sultanát	k1gInSc1	sultanát
uznání	uznání	k1gNnSc2	uznání
za	za	k7c4	za
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1999	[number]	k4	1999
připadalo	připadat	k5eAaImAgNnS	připadat
na	na	k7c4	na
1	[number]	k4	1
000	[number]	k4	000
obyvatel	obyvatel	k1gMnSc1	obyvatel
odhadem	odhad	k1gInSc7	odhad
1,3	[number]	k4	1,3
lékaře	lékař	k1gMnPc4	lékař
a	a	k8xC	a
2,2	[number]	k4	2,2
nemocničních	nemocniční	k2eAgNnPc2d1	nemocniční
lůžek	lůžko	k1gNnPc2	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
mělo	mít	k5eAaImAgNnS	mít
89	[number]	k4	89
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
přístup	přístup	k1gInSc1	přístup
ke	k	k7c3	k
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
péči	péče	k1gFnSc3	péče
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
měla	mít	k5eAaImAgFnS	mít
přibližně	přibližně	k6eAd1	přibližně
třetina	třetina	k1gFnSc1	třetina
populace	populace	k1gFnSc1	populace
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
čisté	čistý	k2eAgFnSc3d1	čistá
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
92	[number]	k4	92
%	%	kIx~	%
mělo	mít	k5eAaImAgNnS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
možnost	možnost	k1gFnSc4	možnost
hygieny	hygiena	k1gFnSc2	hygiena
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
žijících	žijící	k2eAgMnPc2d1	žijící
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
<g/>
/	/	kIx~	/
<g/>
HIV	HIV	kA	HIV
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
na	na	k7c4	na
1	[number]	k4	1
300	[number]	k4	300
<g/>
,	,	kIx,	,
roční	roční	k2eAgInSc1d1	roční
nárůst	nárůst	k1gInSc1	nárůst
činí	činit	k5eAaImIp3nS	činit
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
úmrtí	úmrtí	k1gNnSc2	úmrtí
je	být	k5eAaImIp3nS	být
nízký	nízký	k2eAgInSc1d1	nízký
<g/>
.	.	kIx.	.
</s>
<s>
Antikoncepce	antikoncepce	k1gFnSc1	antikoncepce
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
nemocnicích	nemocnice	k1gFnPc6	nemocnice
<g/>
,	,	kIx,	,
prodejnách	prodejna	k1gFnPc6	prodejna
a	a	k8xC	a
lékárnách	lékárna	k1gFnPc6	lékárna
<g/>
.	.	kIx.	.
</s>
<s>
Potrat	potrat	k1gInSc1	potrat
je	být	k5eAaImIp3nS	být
nelegální	legální	k2eNgFnSc4d1	nelegální
<g/>
,	,	kIx,	,
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ohrožen	ohrožen	k2eAgInSc1d1	ohrožen
život	život	k1gInSc1	život
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
<g/>
Pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
a	a	k8xC	a
léčby	léčba	k1gFnSc2	léčba
probíhal	probíhat	k5eAaImAgInS	probíhat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
ve	v	k7c6	v
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
devět	devět	k4xCc4	devět
státních	státní	k2eAgFnPc2d1	státní
poliklinik	poliklinika	k1gFnPc2	poliklinika
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
dvanáctilůžková	dvanáctilůžkový	k2eAgFnSc1d1	dvanáctilůžkový
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
personál	personál	k1gInSc4	personál
tvořili	tvořit	k5eAaImAgMnP	tvořit
misionáři	misionář	k1gMnPc1	misionář
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
stálo	stát	k5eAaImAgNnS	stát
14	[number]	k4	14
nemocnic	nemocnice	k1gFnPc2	nemocnice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
již	již	k9	již
47	[number]	k4	47
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
stejného	stejný	k2eAgNnSc2d1	stejné
desetiletí	desetiletí	k1gNnSc2	desetiletí
se	se	k3xPyFc4	se
navýšil	navýšit	k5eAaPmAgInS	navýšit
počet	počet	k1gInSc1	počet
lékařů	lékař	k1gMnPc2	lékař
z	z	k7c2	z
294	[number]	k4	294
na	na	k7c4	na
994	[number]	k4	994
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
sester	sestra	k1gFnPc2	sestra
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
čtyřnásobil	čtyřnásobit	k5eAaImAgMnS	čtyřnásobit
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
857	[number]	k4	857
a	a	k8xC	a
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	léto	k1gNnPc6	léto
3	[number]	k4	3
512	[number]	k4	512
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1987	[number]	k4	1987
měla	mít	k5eAaImAgFnS	mít
lékařská	lékařský	k2eAgFnSc1d1	lékařská
komunita	komunita	k1gFnSc1	komunita
možnost	možnost	k1gFnSc4	možnost
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
účastnit	účastnit	k5eAaImF	účastnit
semináře	seminář	k1gInPc4	seminář
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
rozšířit	rozšířit	k5eAaPmF	rozšířit
si	se	k3xPyFc3	se
tak	tak	k9	tak
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
Ománců	Ománec	k1gInPc2	Ománec
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
ibadíjovské	ibadíjovský	k2eAgFnSc3d1	ibadíjovský
odnoži	odnož	k1gFnSc3	odnož
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
činí	činit	k5eAaImIp3nS	činit
unikátní	unikátní	k2eAgInSc1d1	unikátní
muslimský	muslimský	k2eAgInSc1d1	muslimský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zde	zde	k6eAd1	zde
nepřevládají	převládat	k5eNaImIp3nP	převládat
ani	ani	k8xC	ani
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
šíitští	šíitský	k2eAgMnPc1d1	šíitský
muslimové	muslim	k1gMnPc1	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
těchto	tento	k3xDgFnPc2	tento
zbylých	zbylý	k2eAgFnPc2d1	zbylá
dvou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
islámu	islám	k1gInSc2	islám
tvoří	tvořit	k5eAaImIp3nP	tvořit
společně	společně	k6eAd1	společně
s	s	k7c7	s
hinduisty	hinduista	k1gMnPc7	hinduista
většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
zbylé	zbylý	k2eAgFnSc2d1	zbylá
čtvrtiny	čtvrtina	k1gFnSc2	čtvrtina
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Sunnité	sunnita	k1gMnPc1	sunnita
obývají	obývat	k5eAaImIp3nP	obývat
zejména	zejména	k9	zejména
město	město	k1gNnSc4	město
Súr	súra	k1gFnPc2	súra
včetně	včetně	k7c2	včetně
jeho	on	k3xPp3gNnSc2	on
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
oblast	oblast	k1gFnSc1	oblast
Dafár	Dafár	k1gInSc1	Dafár
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
největší	veliký	k2eAgFnSc4d3	veliký
neibadíjovskou	ibadíjovský	k2eNgFnSc4d1	ibadíjovský
skupinu	skupina	k1gFnSc4	skupina
věřících	věřící	k1gMnPc2	věřící
<g/>
.	.	kIx.	.
</s>
<s>
Šíité	šíita	k1gMnPc1	šíita
žijí	žít	k5eAaImIp3nP	žít
především	především	k9	především
při	při	k7c6	při
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
oblasti	oblast	k1gFnSc6	oblast
al-Batína	al-Batín	k1gInSc2	al-Batín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
buddhismus	buddhismus	k1gInSc4	buddhismus
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prvky	prvek	k1gInPc4	prvek
ibádíji	ibádí	k6eAd2	ibádí
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jediného	jediný	k2eAgMnSc4d1	jediný
legitimního	legitimní	k2eAgMnSc4d1	legitimní
vůdce	vůdce	k1gMnSc4	vůdce
ibádíjovského	ibádíjovský	k2eAgInSc2d1	ibádíjovský
islámu	islám	k1gInSc2	islám
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
imám	imám	k1gMnSc1	imám
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukách	ruka	k1gFnPc6	ruka
svírá	svírat	k5eAaImIp3nS	svírat
politickou	politický	k2eAgFnSc4d1	politická
i	i	k8xC	i
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Imáma	imám	k1gMnSc4	imám
volí	volit	k5eAaImIp3nP	volit
šejkové	šejk	k1gMnPc1	šejk
a	a	k8xC	a
rada	rada	k1gFnSc1	rada
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
neodborníků	neodborník	k1gMnPc2	neodborník
<g/>
.	.	kIx.	.
<g/>
Omán	Omán	k1gInSc1	Omán
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
nábožensky	nábožensky	k6eAd1	nábožensky
konzervativní	konzervativní	k2eAgFnPc4d1	konzervativní
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
zdejšími	zdejší	k2eAgFnPc7d1	zdejší
různorodými	různorodý	k2eAgFnPc7d1	různorodá
náboženskými	náboženský	k2eAgFnPc7d1	náboženská
skupinami	skupina	k1gFnPc7	skupina
však	však	k9	však
panuje	panovat	k5eAaImIp3nS	panovat
velká	velký	k2eAgFnSc1d1	velká
tolerance	tolerance	k1gFnSc1	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
konverzi	konverze	k1gFnSc6	konverze
muslimů	muslim	k1gMnPc2	muslim
na	na	k7c4	na
jiné	jiný	k2eAgNnSc4d1	jiné
náboženství	náboženství	k1gNnSc4	náboženství
bude	být	k5eAaImBp3nS	být
vnímána	vnímat	k5eAaImNgFnS	vnímat
negativně	negativně	k6eAd1	negativně
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
není	být	k5eNaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Ománská	ománský	k2eAgFnSc1d1	ománská
kultura	kultura	k1gFnSc1	kultura
má	mít	k5eAaImIp3nS	mít
pevné	pevný	k2eAgInPc4d1	pevný
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
islámu	islám	k1gInSc6	islám
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
odnož	odnož	k1gFnSc1	odnož
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
ibádíja	ibádíja	k1gFnSc1	ibádíja
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
název	název	k1gInSc4	název
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
zakladateli	zakladatel	k1gMnSc6	zakladatel
Abdulláhu	Abdulláh	k1gMnSc6	Abdulláh
íbn	íbn	k?	íbn
Ibádu	Ibád	k1gInSc2	Ibád
žijícím	žijící	k2eAgMnSc7d1	žijící
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
Ománci	Ománek	k1gMnPc1	Ománek
nejsou	být	k5eNaImIp3nP	být
ibádíjovští	ibádíjovský	k2eAgMnPc1d1	ibádíjovský
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
zde	zde	k6eAd1	zde
i	i	k9	i
muslimové	muslim	k1gMnPc1	muslim
sunnitští	sunnitský	k2eAgMnPc1d1	sunnitský
a	a	k8xC	a
šíitští	šíitský	k2eAgMnPc1d1	šíitský
<g/>
.	.	kIx.	.
</s>
<s>
Ománci	Ománek	k1gMnPc1	Ománek
jsou	být	k5eAaImIp3nP	být
tolerantní	tolerantní	k2eAgFnSc4d1	tolerantní
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
vyznavačům	vyznavač	k1gMnPc3	vyznavač
jiných	jiný	k2eAgFnPc2d1	jiná
větví	větev	k1gFnPc2	větev
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
i	i	k9	i
k	k	k7c3	k
vyznavačům	vyznavač	k1gMnPc3	vyznavač
jiných	jiný	k2eAgNnPc2d1	jiné
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
smějí	smát	k5eAaImIp3nP	smát
praktikovat	praktikovat	k5eAaImF	praktikovat
svou	svůj	k3xOyFgFnSc4	svůj
víru	víra	k1gFnSc4	víra
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
kostelech	kostel	k1gInPc6	kostel
a	a	k8xC	a
chrámech	chrám	k1gInPc6	chrám
<g/>
.	.	kIx.	.
<g/>
Islám	islám	k1gInSc1	islám
si	se	k3xPyFc3	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c4	na
plnění	plnění	k1gNnSc4	plnění
pěti	pět	k4xCc2	pět
pilířů	pilíř	k1gInPc2	pilíř
islámu	islám	k1gInSc2	islám
a	a	k8xC	a
hadísu	hadís	k1gInSc2	hadís
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
splnění	splnění	k1gNnSc4	splnění
těchto	tento	k3xDgFnPc2	tento
povinností	povinnost	k1gFnPc2	povinnost
si	se	k3xPyFc3	se
muslim	muslim	k1gMnSc1	muslim
zajistí	zajistit	k5eAaPmIp3nS	zajistit
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
nebi	nebe	k1gNnSc6	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Vakf	Vakf	k1gMnSc1	Vakf
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
náboženské	náboženský	k2eAgFnPc4d1	náboženská
dotace	dotace	k1gFnPc4	dotace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
majetku	majetek	k1gInSc2	majetek
nebo	nebo	k8xC	nebo
příjmu	příjem	k1gInSc2	příjem
<g/>
,	,	kIx,	,
spravovaných	spravovaný	k2eAgInPc2d1	spravovaný
určeným	určený	k2eAgNnSc7d1	určené
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stará	starat	k5eAaImIp3nS	starat
o	o	k7c4	o
údržbu	údržba	k1gFnSc4	údržba
mešit	mešita	k1gFnPc2	mešita
a	a	k8xC	a
prospěch	prospěch	k1gInSc4	prospěch
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zakát	Zakát	k1gInSc1	Zakát
je	být	k5eAaImIp3nS	být
dobročinná	dobročinný	k2eAgFnSc1d1	dobročinná
daň	daň	k1gFnSc1	daň
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
potřebným	potřebný	k2eAgInSc7d1	potřebný
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nP	muset
ji	on	k3xPp3gFnSc4	on
platit	platit	k5eAaImF	platit
každý	každý	k3xTgMnSc1	každý
muslim	muslim	k1gMnSc1	muslim
<g/>
,	,	kIx,	,
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
možnostmi	možnost	k1gFnPc7	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
muslimové	muslim	k1gMnPc1	muslim
jsou	být	k5eAaImIp3nP	být
zavázáni	zavázat	k5eAaPmNgMnP	zavázat
k	k	k7c3	k
půstu	půst	k1gInSc3	půst
během	během	k7c2	během
Ramadánu	ramadán	k1gInSc2	ramadán
<g/>
,	,	kIx,	,
dalšího	další	k1gNnSc2	další
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
29	[number]	k4	29
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
islámský	islámský	k2eAgInSc4d1	islámský
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
muslimové	muslim	k1gMnPc1	muslim
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
od	od	k7c2	od
kouření	kouření	k1gNnSc2	kouření
<g/>
,	,	kIx,	,
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc2	pití
v	v	k7c6	v
hodinách	hodina	k1gFnPc6	hodina
půstu	půst	k1gInSc2	půst
(	(	kIx(	(
<g/>
od	od	k7c2	od
svítání	svítání	k1gNnSc2	svítání
do	do	k7c2	do
soumraku	soumrak	k1gInSc2	soumrak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ramadán	ramadán	k1gInSc1	ramadán
postupuje	postupovat	k5eAaImIp3nS	postupovat
o	o	k7c4	o
10	[number]	k4	10
až	až	k9	až
11	[number]	k4	11
dní	den	k1gInPc2	den
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
řídí	řídit	k5eAaImIp3nS	řídit
lunárním	lunární	k2eAgInSc7d1	lunární
kalendářem	kalendář	k1gInSc7	kalendář
<g/>
.	.	kIx.	.
<g/>
Rok	rok	k1gInSc1	rok
co	co	k8xS	co
rok	rok	k1gInSc1	rok
cestují	cestovat	k5eAaImIp3nP	cestovat
poutníci	poutník	k1gMnPc1	poutník
do	do	k7c2	do
Medíny	Medína	k1gFnSc2	Medína
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
navštívení	navštívení	k1gNnSc2	navštívení
prorokovy	prorokův	k2eAgFnSc2d1	Prorokova
hrobky	hrobka	k1gFnSc2	hrobka
před	před	k7c7	před
cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
svatá	svatý	k2eAgNnPc4d1	svaté
místa	místo	k1gNnPc4	místo
v	v	k7c6	v
Mekce	Mekka	k1gFnSc6	Mekka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
takto	takto	k6eAd1	takto
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
cestovalo	cestovat	k5eAaImAgNnS	cestovat
přibližně	přibližně	k6eAd1	přibližně
19	[number]	k4	19
tisíc	tisíc	k4xCgInPc2	tisíc
muslimů	muslim	k1gMnPc2	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Pouť	pouť	k1gFnSc1	pouť
ke	k	k7c3	k
svatému	svatý	k2eAgNnSc3d1	svaté
místu	místo	k1gNnSc3	místo
je	být	k5eAaImIp3nS	být
organizována	organizován	k2eAgFnSc1d1	organizována
a	a	k8xC	a
koordinována	koordinován	k2eAgFnSc1d1	koordinována
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
poutníkům	poutník	k1gMnPc3	poutník
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oděv	oděv	k1gInSc1	oděv
===	===	k?	===
</s>
</p>
<p>
<s>
Muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
odívají	odívat	k5eAaImIp3nP	odívat
jednoduchým	jednoduchý	k2eAgNnSc7d1	jednoduché
rouchem	roucho	k1gNnSc7	roucho
bez	bez	k7c2	bez
límce	límec	k1gInSc2	límec
sahajícím	sahající	k2eAgInSc7d1	sahající
ke	k	k7c3	k
kotníkům	kotník	k1gInPc3	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
rouchu	roucho	k1gNnSc3	roucho
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
rukávy	rukáv	k1gInPc7	rukáv
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
dišdaša	dišdaša	k1gFnSc1	dišdaša
(	(	kIx(	(
<g/>
dishdasha	dishdasha	k1gFnSc1	dishdasha
<g/>
)	)	kIx)	)
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
bílé	bílý	k2eAgFnPc4d1	bílá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc1d1	modrá
a	a	k8xC	a
hnědé	hnědý	k2eAgFnPc1d1	hnědá
barevné	barevný	k2eAgFnPc1d1	barevná
varianty	varianta	k1gFnPc1	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
rouchem	roucho	k1gNnSc7	roucho
se	se	k3xPyFc4	se
nosívá	nosívat	k5eAaImIp3nS	nosívat
jen	jen	k9	jen
spodní	spodní	k2eAgNnSc4d1	spodní
prádlo	prádlo	k1gNnSc4	prádlo
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
výstřihu	výstřih	k1gInSc2	výstřih
přišitý	přišitý	k2eAgInSc1d1	přišitý
střapec	střapec	k1gInSc1	střapec
–	–	k?	–
furacha	furacha	k1gFnSc1	furacha
(	(	kIx(	(
<g/>
furakha	furakha	k1gFnSc1	furakha
<g/>
)	)	kIx)	)
–	–	k?	–
lze	lze	k6eAd1	lze
naparfémovat	naparfémovat	k5eAaPmF	naparfémovat
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
ozdoba	ozdoba	k1gFnSc1	ozdoba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
formálních	formální	k2eAgFnPc6d1	formální
příležitostech	příležitost	k1gFnPc6	příležitost
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dišdaša	dišdaša	k6eAd1	dišdaša
překryta	překrýt	k5eAaPmNgFnS	překrýt
černým	černý	k2eAgNnSc7d1	černé
nebo	nebo	k8xC	nebo
béžovým	béžový	k2eAgInSc7d1	béžový
hávem	háv	k1gInSc7	háv
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
bišt	bišt	k2eAgInSc1d1	bišt
(	(	kIx(	(
<g/>
bisht	bisht	k1gInSc1	bisht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bišt	Bišt	k1gInSc1	Bišt
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
s	s	k7c7	s
propracovanými	propracovaný	k2eAgFnPc7d1	propracovaná
výšivkami	výšivka	k1gFnPc7	výšivka
ze	z	k7c2	z
zlatých	zlatý	k2eAgFnPc2d1	zlatá
a	a	k8xC	a
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
vláken	vlákna	k1gFnPc2	vlákna
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
možným	možný	k2eAgInSc7d1	možný
doplňkem	doplněk	k1gInSc7	doplněk
při	při	k7c6	při
formálním	formální	k2eAgNnSc6d1	formální
setkání	setkání	k1gNnSc6	setkání
je	být	k5eAaImIp3nS	být
hůl	hůl	k1gFnSc1	hůl
assa	assa	k1gFnSc1	assa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šal	šal	k1gInSc1	šal
(	(	kIx(	(
<g/>
Shal	Shal	k1gInSc1	Shal
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pruh	pruh	k1gInSc1	pruh
látky	látka	k1gFnSc2	látka
z	z	k7c2	z
vlny	vlna	k1gFnSc2	vlna
či	či	k8xC	či
bavlny	bavlna	k1gFnSc2	bavlna
sloužící	sloužící	k2eAgFnSc2d1	sloužící
k	k	k7c3	k
nošení	nošení	k1gNnSc3	nošení
dýky	dýka	k1gFnSc2	dýka
chandžar	chandžara	k1gFnPc2	chandžara
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
shalu	shal	k1gInSc2	shal
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
také	také	k9	také
sapta	sapta	k1gFnSc1	sapta
<g/>
,	,	kIx,	,
kožený	kožený	k2eAgInSc1d1	kožený
pásek	pásek	k1gInSc1	pásek
se	s	k7c7	s
stříbrnými	stříbrný	k2eAgInPc7d1	stříbrný
vzory	vzor	k1gInPc7	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Ománci	Ománek	k1gMnPc1	Ománek
často	často	k6eAd1	často
nosí	nosit	k5eAaImIp3nP	nosit
pokrývky	pokrývka	k1gFnPc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
–	–	k?	–
muzzar	muzzar	k1gInSc1	muzzar
je	být	k5eAaImIp3nS	být
kus	kus	k1gInSc4	kus
látky	látka	k1gFnSc2	látka
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
čtverce	čtverec	k1gInSc2	čtverec
zavinutý	zavinutý	k2eAgInSc4d1	zavinutý
do	do	k7c2	do
turbanu	turban	k1gInSc2	turban
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
vlněný	vlněný	k2eAgMnSc1d1	vlněný
nebo	nebo	k8xC	nebo
bavlněný	bavlněný	k2eAgMnSc1d1	bavlněný
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
turbanem	turban	k1gInSc7	turban
navíc	navíc	k6eAd1	navíc
nosí	nosit	k5eAaImIp3nP	nosit
pracně	pracně	k6eAd1	pracně
vyšívaný	vyšívaný	k2eAgInSc4d1	vyšívaný
čepec	čepec	k1gInSc4	čepec
zvaný	zvaný	k2eAgInSc1d1	zvaný
kummar	kummar	k1gInSc1	kummar
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
Ománci	Ománek	k1gMnPc1	Ománek
se	se	k3xPyFc4	se
obouvají	obouvat	k5eAaImIp3nP	obouvat
do	do	k7c2	do
sandálů	sandál	k1gInPc2	sandál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kuchyně	kuchyně	k1gFnSc2	kuchyně
===	===	k?	===
</s>
</p>
<p>
<s>
Napříč	napříč	k7c7	napříč
Ománem	Omán	k1gInSc7	Omán
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
různá	různý	k2eAgNnPc1d1	různé
jídla	jídlo	k1gNnPc1	jídlo
<g/>
,	,	kIx,	,
rozdílná	rozdílný	k2eAgNnPc1d1	rozdílné
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
v	v	k7c6	v
Salále	Salál	k1gInSc6	Salál
oproti	oproti	k7c3	oproti
severněji	severně	k6eAd2	severně
ležícímu	ležící	k2eAgInSc3d1	ležící
Maskatu	Maskat	k1gInSc3	Maskat
<g/>
.	.	kIx.	.
</s>
<s>
Společným	společný	k2eAgInSc7d1	společný
znakem	znak	k1gInSc7	znak
ománských	ománský	k2eAgFnPc2d1	ománská
regionálních	regionální	k2eAgFnPc2d1	regionální
kuchyní	kuchyně	k1gFnPc2	kuchyně
a	a	k8xC	a
symbolem	symbol	k1gInSc7	symbol
ománské	ománský	k2eAgFnSc2d1	ománská
kuchyně	kuchyně	k1gFnSc2	kuchyně
v	v	k7c6	v
celku	celek	k1gInSc6	celek
jsou	být	k5eAaImIp3nP	být
datle	datle	k1gFnPc1	datle
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
dobou	doba	k1gFnSc7	doba
pro	pro	k7c4	pro
jídlo	jídlo	k1gNnSc4	jídlo
je	být	k5eAaImIp3nS	být
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
večerní	večerní	k2eAgFnSc1d1	večerní
strava	strava	k1gFnSc1	strava
bývá	bývat	k5eAaImIp3nS	bývat
lehčí	lehký	k2eAgFnSc1d2	lehčí
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
ománské	ománský	k2eAgFnSc6d1	ománská
kuchyni	kuchyně	k1gFnSc6	kuchyně
zastává	zastávat	k5eAaImIp3nS	zastávat
rýže	rýže	k1gFnSc2	rýže
s	s	k7c7	s
vařeným	vařený	k2eAgNnSc7d1	vařené
masem	maso	k1gNnSc7	maso
a	a	k8xC	a
rozmanité	rozmanitý	k2eAgInPc4d1	rozmanitý
druhy	druh	k1gInPc4	druh
koření	koření	k1gNnSc2	koření
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
cibule	cibule	k1gFnPc1	cibule
<g/>
,	,	kIx,	,
česnek	česnek	k1gInSc1	česnek
a	a	k8xC	a
různé	různý	k2eAgFnPc1d1	různá
bylinky	bylinka	k1gFnPc1	bylinka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rybolovu	rybolov	k1gInSc3	rybolov
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
konzumovány	konzumován	k2eAgFnPc1d1	konzumována
ryby	ryba	k1gFnPc1	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
dále	daleko	k6eAd2	daleko
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
drůbeží	drůbeží	k2eAgNnSc1d1	drůbeží
a	a	k8xC	a
skopové	skopový	k2eAgNnSc1d1	skopové
<g/>
.	.	kIx.	.
</s>
<s>
Tenký	tenký	k2eAgInSc4d1	tenký
chléb	chléb	k1gInSc4	chléb
ruchal	ruchat	k5eAaImAgMnS	ruchat
(	(	kIx(	(
<g/>
rukhal	rukhat	k5eAaBmAgMnS	rukhat
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
u	u	k7c2	u
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
chodu	chod	k1gInSc2	chod
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
u	u	k7c2	u
snídaně	snídaně	k1gFnSc2	snídaně
s	s	k7c7	s
medem	med	k1gInSc7	med
nebo	nebo	k8xC	nebo
u	u	k7c2	u
večeře	večeře	k1gFnSc2	večeře
s	s	k7c7	s
kořeněnou	kořeněný	k2eAgFnSc7d1	kořeněná
směsí	směs	k1gFnSc7	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každodenní	každodenní	k2eAgFnSc1d1	každodenní
strava	strava	k1gFnSc1	strava
Ománců	Ománec	k1gMnPc2	Ománec
mnohdy	mnohdy	k6eAd1	mnohdy
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
rovněž	rovněž	k9	rovněž
polévky	polévka	k1gFnSc2	polévka
–	–	k?	–
zeleninovou	zeleninový	k2eAgFnSc4d1	zeleninová
polévku	polévka	k1gFnSc4	polévka
<g/>
,	,	kIx,	,
jehněčí	jehněčí	k2eAgFnSc4d1	jehněčí
<g/>
,	,	kIx,	,
drůbeží	drůbeží	k2eAgFnSc4d1	drůbeží
nebo	nebo	k8xC	nebo
luštěninovou	luštěninový	k2eAgFnSc4d1	luštěninová
–	–	k?	–
a	a	k8xC	a
saláty	salát	k1gInPc1	salát
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zeleninové	zeleninový	k2eAgInPc4d1	zeleninový
saláty	salát	k1gInPc4	salát
<g/>
,	,	kIx,	,
baklažánové	baklažánový	k2eAgInPc4d1	baklažánový
<g/>
,	,	kIx,	,
řeřichové	řeřichový	k2eAgInPc4d1	řeřichový
<g/>
,	,	kIx,	,
tuňákové	tuňákové	k?	tuňákové
nebo	nebo	k8xC	nebo
o	o	k7c4	o
saláty	salát	k1gInPc4	salát
ze	z	k7c2	z
sušených	sušený	k2eAgFnPc2d1	sušená
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgInPc4d1	hlavní
jídelní	jídelní	k2eAgInPc4d1	jídelní
chody	chod	k1gInPc4	chod
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
marak	marak	k6eAd1	marak
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zeleninová	zeleninový	k2eAgFnSc1d1	zeleninová
směs	směs	k1gFnSc1	směs
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kebab	kebab	k1gInSc1	kebab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
jídel	jídlo	k1gNnPc2	jídlo
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
jen	jen	k9	jen
za	za	k7c2	za
svátečních	sváteční	k2eAgFnPc2d1	sváteční
okolností	okolnost	k1gFnPc2	okolnost
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Ramadánu	ramadán	k1gInSc2	ramadán
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
příležitostech	příležitost	k1gFnPc6	příležitost
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nS	vařit
šuva	šuva	k1gFnSc1	šuva
(	(	kIx(	(
<g/>
shuwa	shuwa	k1gFnSc1	shuwa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
vícero	vícero	k1gNnSc4	vícero
způsobů	způsob	k1gInPc2	způsob
vaření	vaření	k1gNnPc2	vaření
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
na	na	k7c6	na
vaření	vaření	k1gNnSc6	vaření
podílí	podílet	k5eAaImIp3nS	podílet
celá	celý	k2eAgFnSc1d1	celá
ves	ves	k1gFnSc1	ves
<g/>
.	.	kIx.	.
</s>
<s>
Pečení	pečení	k1gNnSc1	pečení
krávy	kráva	k1gFnSc2	kráva
nebo	nebo	k8xC	nebo
kozy	koza	k1gFnSc2	koza
trvá	trvat	k5eAaImIp3nS	trvat
i	i	k9	i
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Svéráznou	svérázný	k2eAgFnSc4d1	svérázná
chuť	chuť	k1gFnSc4	chuť
jídlu	jídlo	k1gNnSc3	jídlo
šuva	šuva	k6eAd1	šuva
dodávají	dodávat	k5eAaImIp3nP	dodávat
právě	právě	k9	právě
bylinky	bylinka	k1gFnPc1	bylinka
a	a	k8xC	a
koření	koření	k1gNnPc1	koření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
laban	laban	k1gInSc1	laban
<g/>
,	,	kIx,	,
podmáslí	podmáslí	k1gNnSc1	podmáslí
se	s	k7c7	s
slanou	slaný	k2eAgFnSc7d1	slaná
chutí	chuť	k1gFnSc7	chuť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
jogurtové	jogurtový	k2eAgInPc1d1	jogurtový
nápoje	nápoj	k1gInPc1	nápoj
doprovázené	doprovázený	k2eAgInPc1d1	doprovázený
kardamomem	kardamom	k1gInSc7	kardamom
a	a	k8xC	a
pistáciovými	pistáciový	k2eAgInPc7d1	pistáciový
ořechy	ořech	k1gInPc7	ořech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Místní	místní	k2eAgFnSc1d1	místní
káva	káva	k1gFnSc1	káva
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kahva	kahva	k6eAd1	kahva
(	(	kIx(	(
<g/>
kahwa	kahwa	k6eAd1	kahwa
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
khava	khava	k6eAd1	khava
(	(	kIx(	(
<g/>
khawa	khawa	k6eAd1	khawa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
připravována	připravovat	k5eAaImNgFnS	připravovat
z	z	k7c2	z
čerstvě	čerstvě	k6eAd1	čerstvě
upražených	upražený	k2eAgNnPc2d1	upražené
kávových	kávový	k2eAgNnPc2d1	kávové
zrn	zrno	k1gNnPc2	zrno
a	a	k8xC	a
kardamomské	kardamomský	k2eAgFnSc2d1	kardamomský
moučky	moučka	k1gFnSc2	moučka
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
kávě	káva	k1gFnSc3	káva
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
pro	pro	k7c4	pro
dochucení	dochucení	k1gNnSc4	dochucení
a	a	k8xC	a
vyvážení	vyvážení	k1gNnSc4	vyvážení
hořkosti	hořkost	k1gFnSc2	hořkost
podávají	podávat	k5eAaImIp3nP	podávat
datle	datle	k1gFnPc1	datle
<g/>
,	,	kIx,	,
koření	kořenit	k5eAaImIp3nS	kořenit
kardamom	kardamom	k1gInSc1	kardamom
a	a	k8xC	a
sladká	sladký	k2eAgFnSc1d1	sladká
želatina	želatina	k1gFnSc1	želatina
halva	halva	k1gFnSc1	halva
(	(	kIx(	(
<g/>
halwa	halwa	k1gFnSc1	halwa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
hnědého	hnědý	k2eAgInSc2d1	hnědý
cukru	cukr	k1gInSc2	cukr
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
medu	med	k1gInSc2	med
a	a	k8xC	a
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
oříšky	oříšek	k1gInPc7	oříšek
<g/>
,	,	kIx,	,
čokoládou	čokoláda	k1gFnSc7	čokoláda
nebo	nebo	k8xC	nebo
s	s	k7c7	s
oleji	olej	k1gInPc7	olej
z	z	k7c2	z
růže	růž	k1gFnSc2	růž
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kávě	káva	k1gFnSc6	káva
se	se	k3xPyFc4	se
podávají	podávat	k5eAaImIp3nP	podávat
také	také	k9	také
lochemat	lochemat	k5eAaPmF	lochemat
(	(	kIx(	(
<g/>
lokhemat	lokhemat	k5eAaPmF	lokhemat
<g/>
)	)	kIx)	)
–	–	k?	–
smažené	smažený	k2eAgFnSc2d1	smažená
koule	koule	k1gFnSc2	koule
z	z	k7c2	z
mouky	mouka	k1gFnSc2	mouka
a	a	k8xC	a
droždí	droždí	k1gNnSc2	droždí
kořeněné	kořeněný	k2eAgFnSc2d1	kořeněná
kardamomem	kardamom	k1gInSc7	kardamom
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
limety	limeta	k1gFnPc1	limeta
a	a	k8xC	a
sirupy	sirup	k1gInPc1	sirup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sport	sport	k1gInSc1	sport
===	===	k?	===
</s>
</p>
<p>
<s>
Nejpopulárnějšími	populární	k2eAgInPc7d3	nejpopulárnější
sporty	sport	k1gInPc7	sport
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
jsou	být	k5eAaImIp3nP	být
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
hokej	hokej	k1gInSc4	hokej
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
volejbal	volejbal	k1gInSc4	volejbal
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
a	a	k8xC	a
tenisu	tenis	k1gInSc6	tenis
podobný	podobný	k2eAgInSc4d1	podobný
squash	squash	k1gInSc4	squash
<g/>
.	.	kIx.	.
</s>
<s>
Golf	golf	k1gInSc1	golf
mezi	mezi	k7c7	mezi
nejoblíbenějšími	oblíbený	k2eAgInPc7d3	nejoblíbenější
sporty	sport	k1gInPc7	sport
nefiguruje	figurovat	k5eNaImIp3nS	figurovat
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
terénu	terén	k1gInSc3	terén
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
často	často	k6eAd1	často
jeho	jeho	k3xOp3gNnSc4	jeho
provozování	provozování	k1gNnSc4	provozování
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
písečných	písečný	k2eAgFnPc2d1	písečná
pláží	pláž	k1gFnPc2	pláž
lze	lze	k6eAd1	lze
praktikovat	praktikovat	k5eAaImF	praktikovat
některé	některý	k3yIgInPc4	některý
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
jako	jako	k8xS	jako
například	například	k6eAd1	například
potápění	potápěný	k2eAgMnPc1d1	potápěný
<g/>
.	.	kIx.	.
</s>
<s>
Populární	populární	k2eAgMnSc1d1	populární
je	být	k5eAaImIp3nS	být
i	i	k9	i
speleologie	speleologie	k1gFnSc1	speleologie
(	(	kIx(	(
<g/>
jeskyňářství	jeskyňářství	k1gNnSc1	jeskyňářství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
jeskyň	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Madžlis	Madžlis	k1gFnSc3	Madžlis
al-Džhin	al-Džhin	k1gMnSc1	al-Džhin
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
jeskynním	jeskynní	k2eAgInSc7d1	jeskynní
komplexem	komplex	k1gInSc7	komplex
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
<g/>
Olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
se	se	k3xPyFc4	se
Omán	Omán	k1gInSc1	Omán
poprvé	poprvé	k6eAd1	poprvé
zúčastnil	zúčastnit	k5eAaPmAgInS	zúčastnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
objevuje	objevovat	k5eAaImIp3nS	objevovat
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
navíc	navíc	k6eAd1	navíc
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
letních	letní	k2eAgFnPc6d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Vyslal	vyslat	k5eAaPmAgMnS	vyslat
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
celkově	celkově	k6eAd1	celkově
36	[number]	k4	36
sportovců	sportovec	k1gMnPc2	sportovec
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ománci	Ománek	k1gMnPc1	Ománek
doposud	doposud	k6eAd1	doposud
nezískali	získat	k5eNaPmAgMnP	získat
žádné	žádný	k3yNgFnPc4	žádný
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Ománský	ománský	k2eAgInSc1d1	ománský
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
byl	být	k5eAaImAgInS	být
ustanoven	ustanovit	k5eAaPmNgInS	ustanovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
<g/>
Známým	známý	k2eAgMnSc7d1	známý
sportovcem	sportovec	k1gMnSc7	sportovec
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
národního	národní	k2eAgInSc2d1	národní
týmu	tým	k1gInSc2	tým
Ali	Ali	k1gMnSc3	Ali
Al-Habsi	Al-Habs	k1gMnSc3	Al-Habs
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
v	v	k7c6	v
dresech	dres	k1gInPc6	dres
anglického	anglický	k2eAgInSc2d1	anglický
prvoligového	prvoligový	k2eAgInSc2d1	prvoligový
klubu	klub	k1gInSc2	klub
Wigan	Wigan	k1gMnSc1	Wigan
Athletic	Athletice	k1gFnPc2	Athletice
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
školství	školství	k1gNnSc1	školství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
předmětem	předmět	k1gInSc7	předmět
úsilí	úsilí	k1gNnSc2	úsilí
ománské	ománský	k2eAgFnSc2d1	ománská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
usilovala	usilovat	k5eAaImAgFnS	usilovat
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
dostupnost	dostupnost	k1gFnSc4	dostupnost
vysokého	vysoký	k2eAgNnSc2d1	vysoké
úrovně	úroveň	k1gFnSc2	úroveň
vzdělání	vzdělání	k1gNnSc4	vzdělání
pro	pro	k7c4	pro
normální	normální	k2eAgMnPc4d1	normální
občany	občan	k1gMnPc4	občan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
o	o	k7c4	o
jejich	jejich	k3xOp3gFnSc4	jejich
vysokou	vysoký	k2eAgFnSc4d1	vysoká
připravenost	připravenost	k1gFnSc4	připravenost
a	a	k8xC	a
kvalifikovanost	kvalifikovanost	k1gFnSc1	kvalifikovanost
doplněním	doplnění	k1gNnSc7	doplnění
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
o	o	k7c4	o
národní	národní	k2eAgFnSc4d1	národní
kulturu	kultura	k1gFnSc4	kultura
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
obohacením	obohacení	k1gNnSc7	obohacení
o	o	k7c4	o
nejmodernější	moderní	k2eAgFnPc4d3	nejmodernější
metody	metoda	k1gFnPc4	metoda
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
výzkumů	výzkum	k1gInPc2	výzkum
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
pozornosti	pozornost	k1gFnSc3	pozornost
bylo	být	k5eAaImAgNnS	být
věnováno	věnovat	k5eAaPmNgNnS	věnovat
námořní	námořní	k2eAgInSc1d1	námořní
vědě	věda	k1gFnSc3	věda
<g/>
,	,	kIx,	,
nerostům	nerost	k1gInPc3	nerost
<g/>
,	,	kIx,	,
vodním	vodní	k2eAgInPc3d1	vodní
zdrojům	zdroj	k1gInPc3	zdroj
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Univerzita	univerzita	k1gFnSc1	univerzita
sultána	sultána	k1gFnSc1	sultána
Kábúse	Kábúse	k1gFnSc1	Kábúse
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
lékařských	lékařský	k2eAgInPc2d1	lékařský
<g/>
,	,	kIx,	,
strojařských	strojařský	k2eAgInPc2d1	strojařský
<g/>
,	,	kIx,	,
vědních	vědní	k2eAgInPc2d1	vědní
<g/>
,	,	kIx,	,
a	a	k8xC	a
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
kolejí	kolej	k1gFnPc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
až	až	k9	až
1997	[number]	k4	1997
studovalo	studovat	k5eAaImAgNnS	studovat
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
zabývajících	zabývající	k2eAgInPc2d1	zabývající
se	s	k7c7	s
strojařstvím	strojařství	k1gNnSc7	strojařství
a	a	k8xC	a
vědou	věda	k1gFnSc7	věda
jen	jen	k9	jen
13	[number]	k4	13
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
studujících	studující	k1gFnPc2	studující
<g/>
.	.	kIx.	.
<g/>
Institut	institut	k1gInSc1	institut
Zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
Věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
pod	pod	k7c4	pod
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Technická	technický	k2eAgFnSc1d1	technická
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Maskatu	Maskat	k1gInSc6	Maskat
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
oddělení	oddělení	k1gNnSc2	oddělení
laboratorní	laboratorní	k2eAgFnSc2d1	laboratorní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
,	,	kIx,	,
strojírenství	strojírenství	k1gNnSc2	strojírenství
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Ománské	ománský	k2eAgNnSc1d1	Ománské
Muzeum	muzeum	k1gNnSc1	muzeum
Biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
národní	národní	k2eAgInSc4d1	národní
herbář	herbář	k1gInSc4	herbář
a	a	k8xC	a
kolekci	kolekce	k1gFnSc4	kolekce
mušlí	mušle	k1gFnPc2	mušle
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
organizace	organizace	k1gFnPc1	organizace
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
městě	město	k1gNnSc6	město
Maskat	Maskat	k1gInSc1	Maskat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Oman	oman	k1gInSc1	oman
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Andromeda	Andromeda	k1gMnSc1	Andromeda
Oxford	Oxford	k1gInSc4	Oxford
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
světových	světový	k2eAgFnPc2d1	světová
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Hejný	Hejný	k1gMnSc1	Hejný
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
redakce	redakce	k1gFnSc1	redakce
Winklerová	Winklerová	k1gFnSc1	Winklerová
Dana	Dana	k1gFnSc1	Dana
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
Trojánková	Trojánková	k1gFnSc1	Trojánková
Margot	Margot	k1gMnSc1	Margot
<g/>
,	,	kIx,	,
Vosková	voskový	k2eAgFnSc1d1	vosková
Monika	Monika	k1gFnSc1	Monika
<g/>
.	.	kIx.	.
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Slovart	Slovart	k1gInSc1	Slovart
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7209	[number]	k4	7209
<g/>
-	-	kIx~	-
<g/>
249	[number]	k4	249
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TADEUSZE	TADEUSZE	kA	TADEUSZE
<g/>
,	,	kIx,	,
Moľdava	Moľdava	k1gFnSc1	Moľdava
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
<g/>
:	:	kIx,	:
Fragment	fragment	k1gInSc1	fragment
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
424	[number]	k4	424
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7200	[number]	k4	7200
<g/>
-	-	kIx~	-
<g/>
669	[number]	k4	669
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MÜLLER-WÖBCKE	MÜLLER-WÖBCKE	k?	MÜLLER-WÖBCKE
<g/>
,	,	kIx,	,
Birgit	Birgit	k1gInSc1	Birgit
<g/>
.	.	kIx.	.
</s>
<s>
Dubaj	Dubaj	k1gInSc1	Dubaj
<g/>
,	,	kIx,	,
Emiráty	emirát	k1gInPc1	emirát
<g/>
,	,	kIx,	,
Omán	Omán	k1gInSc1	Omán
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Alice	Alice	k1gFnSc2	Alice
Kavinová	Kavinová	k1gFnSc1	Kavinová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Jan	Jan	k1gMnSc1	Jan
Vašut	Vašut	k1gMnSc1	Vašut
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
128	[number]	k4	128
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7236	[number]	k4	7236
<g/>
-	-	kIx~	-
<g/>
688	[number]	k4	688
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kriminalita	kriminalita	k1gFnSc1	kriminalita
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
</s>
</p>
<p>
<s>
Prostituce	prostituce	k1gFnSc1	prostituce
v	v	k7c6	v
Ománu	Omán	k1gInSc6	Omán
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Omán	Omán	k1gInSc1	Omán
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Omán	Omán	k1gInSc1	Omán
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Omán	Omán	k1gInSc1	Omán
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
BBC	BBC	kA	BBC
News	News	k1gInSc1	News
<g/>
,	,	kIx,	,
profil	profil	k1gInSc1	profil
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
cestopis	cestopis	k1gInSc1	cestopis
a	a	k8xC	a
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
global	globat	k5eAaImAgMnS	globat
arab	arab	k1gMnSc1	arab
network	network	k1gInSc2	network
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc1	informace
a	a	k8xC	a
zpravodajství	zpravodajství	k1gNnPc1	zpravodajství
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Chiefa	Chief	k1gMnSc2	Chief
Coins	Coins	k1gInSc4	Coins
<g/>
,	,	kIx,	,
ománské	ománský	k2eAgFnPc4d1	ománská
mince	mince	k1gFnPc4	mince
a	a	k8xC	a
vládci	vládce	k1gMnPc1	vládce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
iStockphoto	iStockphota	k1gFnSc5	iStockphota
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
Ománu	Omán	k1gInSc2	Omán
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
nordog	nordog	k1gInSc1	nordog
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
tulacka	tulacka	k1gFnSc1	tulacka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
cestopis	cestopis	k1gInSc1	cestopis
a	a	k8xC	a
fotogalerie	fotogalerie	k1gFnSc1	fotogalerie
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oman	oman	k1gInSc1	oman
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oman	oman	k1gInSc1	oman
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Oman	oman	k1gInSc1	oman
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
Near	Near	k1gInSc1	Near
Eastern	Eastern	k1gMnSc1	Eastern
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Oman	oman	k1gInSc1	oman
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-03-07	[number]	k4	2011-03-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Oman	oman	k1gInSc1	oman
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-12	[number]	k4	2011-07-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Rijádu	Rijád	k1gInSc6	Rijád
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Omán	Omán	k1gInSc1	Omán
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011-10-01	[number]	k4	2011-10-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CRYSTAL	CRYSTAL	kA	CRYSTAL
<g/>
,	,	kIx,	,
Jill	Jill	k1gMnSc1	Jill
Ann	Ann	k1gMnSc1	Ann
<g/>
;	;	kIx,	;
PETERSON	PETERSON	kA	PETERSON
<g/>
,	,	kIx,	,
J.	J.	kA	J.
E	E	kA	E
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Oman	oman	k1gInSc1	oman
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Vláda	vláda	k1gFnSc1	vláda
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
informací	informace	k1gFnPc2	informace
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
</s>
</p>
