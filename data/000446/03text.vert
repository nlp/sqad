<s>
Ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
je	být	k5eAaImIp3nS	být
proton	proton	k1gInSc1	proton
subatomární	subatomární	k2eAgFnSc2d1	subatomární
částice	částice	k1gFnSc2	částice
s	s	k7c7	s
kladným	kladný	k2eAgInSc7d1	kladný
elementárním	elementární	k2eAgInSc7d1	elementární
elektrickým	elektrický	k2eAgInSc7d1	elektrický
nábojem	náboj	k1gInSc7	náboj
tj.	tj.	kA	tj.
1,6	[number]	k4	1,6
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
C	C	kA	C
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
938	[number]	k4	938
MeV	MeV	k1gFnPc2	MeV
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
1,67	[number]	k4	1,67
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
kg	kg	kA	kg
<g/>
,	,	kIx,	,
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
přibližně	přibližně	k6eAd1	přibližně
hmotnosti	hmotnost	k1gFnSc3	hmotnost
1836	[number]	k4	1836
elektronů	elektron	k1gInPc2	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
stabilní	stabilní	k2eAgFnSc4d1	stabilní
částici	částice	k1gFnSc4	částice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
rozpadat	rozpadat	k5eAaPmF	rozpadat
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
přes	přes	k7c4	přes
1035	[number]	k4	1035
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ověření	ověření	k1gNnSc1	ověření
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
mimo	mimo	k7c4	mimo
rozlišovací	rozlišovací	k2eAgFnPc4d1	rozlišovací
schopnosti	schopnost	k1gFnPc4	schopnost
současných	současný	k2eAgNnPc2d1	současné
experimentálních	experimentální	k2eAgNnPc2d1	experimentální
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
rozpad	rozpad	k1gInSc1	rozpad
protonu	proton	k1gInSc2	proton
nebyl	být	k5eNaImAgInS	být
dosud	dosud	k6eAd1	dosud
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
ale	ale	k9	ale
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
hmotná	hmotný	k2eAgFnSc1d1	hmotná
částice	částice	k1gFnSc1	částice
se	s	k7c7	s
spinem	spin	k1gInSc7	spin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
izospinem	izospino	k1gNnSc7	izospino
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc4	proton
řadíme	řadit	k5eAaImIp1nP	řadit
mezi	mezi	k7c7	mezi
fermiony	fermion	k1gInPc7	fermion
a	a	k8xC	a
baryony	baryon	k1gInPc7	baryon
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kvarků	kvark	k1gInPc2	kvark
u	u	k7c2	u
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc4	jeden
d	d	k?	d
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnSc7	interakce
<g/>
,	,	kIx,	,
zprostředkovanou	zprostředkovaný	k2eAgFnSc7d1	zprostředkovaná
gluony	gluon	k1gInPc1	gluon
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnPc1	měření
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvarky	kvark	k1gInPc1	kvark
i	i	k8xC	i
gluony	gluon	k1gInPc1	gluon
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
spinu	spin	k1gInSc3	spin
protonu	proton	k1gInSc2	proton
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
25	[number]	k4	25
<g/>
%	%	kIx~	%
a	a	k8xC	a
chybějící	chybějící	k2eAgInSc1d1	chybějící
příspěvek	příspěvek	k1gInSc1	příspěvek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
krizí	krize	k1gFnSc7	krize
protonového	protonový	k2eAgInSc2d1	protonový
spinu	spin	k1gInSc2	spin
<g/>
.	.	kIx.	.
</s>
<s>
Antičásticí	antičástice	k1gFnSc7	antičástice
protonu	proton	k1gInSc2	proton
je	být	k5eAaImIp3nS	být
antiproton	antiproton	k1gInSc1	antiproton
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
veliký	veliký	k2eAgInSc1d1	veliký
náboj	náboj	k1gInSc1	náboj
opačného	opačný	k2eAgNnSc2d1	opačné
znaménka	znaménko	k1gNnSc2	znaménko
<g/>
.	.	kIx.	.
</s>
<s>
Protony	proton	k1gInPc1	proton
a	a	k8xC	a
neutrony	neutron	k1gInPc1	neutron
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označují	označovat	k5eAaImIp3nP	označovat
společným	společný	k2eAgInSc7d1	společný
názvem	název	k1gInSc7	název
jako	jako	k8xS	jako
tzv.	tzv.	kA	tzv.
nukleony	nukleon	k1gInPc1	nukleon
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc1	proton
je	být	k5eAaImIp3nS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
neutronem	neutron	k1gInSc7	neutron
a	a	k8xC	a
elektronem	elektron	k1gInSc7	elektron
základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
částicí	částice	k1gFnSc7	částice
veškeré	veškerý	k3xTgFnSc2	veškerý
známé	známý	k2eAgFnSc2d1	známá
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
nejběžnějšího	běžný	k2eAgInSc2d3	nejběžnější
izotopu	izotop	k1gInSc2	izotop
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
proton	proton	k1gInSc1	proton
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
atomární	atomární	k2eAgNnPc1d1	atomární
jádra	jádro	k1gNnPc1	jádro
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
neutronů	neutron	k1gInPc2	neutron
přitahovaných	přitahovaný	k2eAgInPc2d1	přitahovaný
k	k	k7c3	k
sobě	se	k3xPyFc3	se
silnou	silný	k2eAgFnSc7d1	silná
interakcí	interakce	k1gFnSc7	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Protonové	protonový	k2eAgNnSc1d1	protonové
číslo	číslo	k1gNnSc1	číslo
určuje	určovat	k5eAaImIp3nS	určovat
chemické	chemický	k2eAgFnPc4d1	chemická
vlastnosti	vlastnost	k1gFnPc4	vlastnost
atomu	atom	k1gInSc2	atom
jako	jako	k8xC	jako
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
rovná	rovnat	k5eAaImIp3nS	rovnat
počet	počet	k1gInSc1	počet
elektronů	elektron	k1gInPc2	elektron
v	v	k7c6	v
obalu	obal	k1gInSc6	obal
neutrálního	neutrální	k2eAgInSc2d1	neutrální
atomu	atom	k1gInSc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
elektromagnetická	elektromagnetický	k2eAgFnSc1d1	elektromagnetická
síla	síla	k1gFnSc1	síla
je	být	k5eAaImIp3nS	být
řádově	řádově	k6eAd1	řádově
silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
náboj	náboj	k1gInSc1	náboj
protonu	proton	k1gInSc2	proton
stejně	stejně	k6eAd1	stejně
veliký	veliký	k2eAgInSc1d1	veliký
jako	jako	k8xC	jako
náboj	náboj	k1gInSc1	náboj
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
celková	celkový	k2eAgFnSc1d1	celková
odpudivost	odpudivost	k1gFnSc1	odpudivost
kladně	kladně	k6eAd1	kladně
či	či	k8xC	či
záporně	záporně	k6eAd1	záporně
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
větší	veliký	k2eAgInSc4d2	veliký
–	–	k?	–
atomy	atom	k1gInPc1	atom
by	by	kYmCp3nP	by
nebyly	být	k5eNaImAgInP	být
elektricky	elektricky	k6eAd1	elektricky
neutrální	neutrální	k2eAgInPc1d1	neutrální
<g/>
)	)	kIx)	)
způsobila	způsobit	k5eAaPmAgFnS	způsobit
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
rozpínavost	rozpínavost	k1gFnSc4	rozpínavost
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
těles	těleso	k1gNnPc2	těleso
přitahovaných	přitahovaný	k2eAgNnPc2d1	přitahované
k	k	k7c3	k
sobě	se	k3xPyFc3	se
gravitační	gravitační	k2eAgFnPc4d1	gravitační
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemii	chemie	k1gFnSc6	chemie
a	a	k8xC	a
biochemii	biochemie	k1gFnSc6	biochemie
je	být	k5eAaImIp3nS	být
proton	proton	k1gInSc1	proton
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
ion	ion	k1gInSc4	ion
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Proton	proton	k1gInSc4	proton
objevil	objevit	k5eAaPmAgMnS	objevit
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
v	v	k7c6	v
r.	r.	kA	r.
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnSc2	částice
alfa	alfa	k1gNnSc1	alfa
vystřelované	vystřelovaný	k2eAgNnSc1d1	vystřelované
do	do	k7c2	do
plynného	plynný	k2eAgInSc2d1	plynný
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
scintilačním	scintilační	k2eAgInSc6d1	scintilační
detektoru	detektor	k1gInSc6	detektor
jeví	jevit	k5eAaImIp3nS	jevit
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jádra	jádro	k1gNnSc2	jádro
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Rutherford	Rutherford	k1gMnSc1	Rutherford
určil	určit	k5eAaPmAgMnS	určit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zdrojem	zdroj	k1gInSc7	zdroj
jader	jádro	k1gNnPc2	jádro
vodíku	vodík	k1gInSc2	vodík
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
dusík	dusík	k1gInSc4	dusík
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
obsahovat	obsahovat	k5eAaImF	obsahovat
jádra	jádro	k1gNnSc2	jádro
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Myslel	myslet	k5eAaImAgInS	myslet
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jádra	jádro	k1gNnPc1	jádro
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgFnPc6	jenž
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
atomové	atomový	k2eAgNnSc4d1	atomové
číslo	číslo	k1gNnSc4	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
proton	proton	k1gInSc4	proton
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
řeckého	řecký	k2eAgNnSc2d1	řecké
protos	protos	k1gMnSc1	protos
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Protony	proton	k1gInPc1	proton
mají	mít	k5eAaImIp3nP	mít
vlastnost	vlastnost	k1gFnSc4	vlastnost
spin	spina	k1gFnPc2	spina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
při	při	k7c6	při
nukleární	nukleární	k2eAgFnSc6d1	nukleární
magnetické	magnetický	k2eAgFnSc6d1	magnetická
resonanční	resonanční	k2eAgFnSc6d1	resonanční
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
NMR	NMR	kA	NMR
se	se	k3xPyFc4	se
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
užívá	užívat	k5eAaImIp3nS	užívat
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
stínění	stínění	k1gNnSc2	stínění
okolo	okolo	k7c2	okolo
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
látky	látka	k1gFnSc2	látka
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
okolní	okolní	k2eAgInSc4d1	okolní
mrak	mrak	k1gInSc4	mrak
elektronů	elektron	k1gInPc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
mohou	moct	k5eAaImIp3nP	moct
získat	získat	k5eAaPmF	získat
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
uspořádání	uspořádání	k1gNnSc4	uspořádání
molekulární	molekulární	k2eAgFnSc2d1	molekulární
struktury	struktura	k1gFnSc2	struktura
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Nukleon	nukleon	k1gInSc1	nukleon
Baryon	Baryon	k1gInSc1	Baryon
Elektron	elektron	k1gInSc1	elektron
Neutron	neutron	k1gInSc4	neutron
Galerie	galerie	k1gFnSc2	galerie
proton	proton	k1gInSc1	proton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
proton	proton	k1gInSc1	proton
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
