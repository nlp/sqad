<s>
Baryum	baryum	k1gNnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
chemickém	chemický	k2eAgInSc6d1
prvku	prvek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Operace	operace	k1gFnSc1
Barium	barium	k1gNnSc1
-	-	kIx~
paradesantní	paradesantní	k2eAgInSc1d1
výsadek	výsadek	k1gInSc1
vyslaný	vyslaný	k2eAgInSc1d1
během	běh	k1gInSc7
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
z	z	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Baryum	baryum	k1gNnSc1
</s>
<s>
Ba	ba	k9
</s>
<s>
56	#num#	k4
</s>
<s>
↓	↓	k?
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
↓	↓	k?
</s>
<s>
Obecné	obecný	k2eAgNnSc1d1
</s>
<s>
Název	název	k1gInSc1
<g/>
,	,	kIx,
značka	značka	k1gFnSc1
<g/>
,	,	kIx,
číslo	číslo	k1gNnSc1
</s>
<s>
Baryum	baryum	k1gNnSc1
<g/>
,	,	kIx,
Ba	ba	k9
<g/>
,	,	kIx,
56	#num#	k4
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
lat.	lat.	k?
Barium	barium	k1gNnSc1
</s>
<s>
Skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
perioda	perioda	k1gFnSc1
<g/>
,	,	kIx,
blok	blok	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
perioda	perioda	k1gFnSc1
</s>
<s>
Chemická	chemický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
stříbrobílý	stříbrobílý	k2eAgInSc1d1
kov	kov	k1gInSc1
</s>
<s>
Identifikace	identifikace	k1gFnSc1
</s>
<s>
Registrační	registrační	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
CAS	CAS	kA
</s>
<s>
7440-39-3	7440-39-3	k4
</s>
<s>
Atomové	atomový	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
atomová	atomový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
137,33	137,33	k4
</s>
<s>
Atomový	atomový	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
222	#num#	k4
pm	pm	k?
</s>
<s>
Kovalentní	kovalentní	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
215	#num#	k4
pm	pm	k?
</s>
<s>
Van	van	k1gInSc1
der	drát	k5eAaImRp2nS
Waalsův	Waalsův	k2eAgInSc4d1
poloměr	poloměr	k1gInSc4
</s>
<s>
268	#num#	k4
pm	pm	k?
</s>
<s>
Oxidační	oxidační	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
</s>
<s>
+	+	kIx~
<g/>
II	II	kA
</s>
<s>
Elektronegativita	Elektronegativita	k1gFnSc1
(	(	kIx(
<g/>
Paulingova	Paulingův	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,89	0,89	k4
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Hustota	hustota	k1gFnSc1
</s>
<s>
3,51	3,51	k4
kg	kg	kA
<g/>
·	·	k?
<g/>
dm	dm	kA
<g/>
−	−	k?
<g/>
3	#num#	k4
</s>
<s>
Skupenství	skupenství	k1gNnSc1
</s>
<s>
pevné	pevný	k2eAgNnSc1d1
</s>
<s>
Tvrdost	tvrdost	k1gFnSc1
</s>
<s>
1,25	1,25	k4
</s>
<s>
Termické	termický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1
vodivost	vodivost	k1gFnSc1
</s>
<s>
18,4	18,4	k4
W	W	kA
<g/>
⋅	⋅	k?
<g/>
m	m	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
<g/>
⋅	⋅	k?
<g/>
K	K	kA
<g/>
−	−	k?
<g/>
1	#num#	k4
</s>
<s>
Termodynamické	termodynamický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Teplota	teplota	k1gFnSc1
tání	tání	k1gNnSc2
</s>
<s>
726,85	726,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
1	#num#	k4
000	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Teplota	teplota	k1gFnSc1
varu	var	k1gInSc2
</s>
<s>
1896,85	1896,85	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
2	#num#	k4
170	#num#	k4
K	k	k7c3
<g/>
)	)	kIx)
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Měrný	měrný	k2eAgInSc4d1
elektrický	elektrický	k2eAgInSc4d1
odpor	odpor	k1gInSc4
</s>
<s>
332	#num#	k4
nΩ	nΩ	k?
<g/>
·	·	k?
<g/>
m	m	kA
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1
chování	chování	k1gNnSc1
</s>
<s>
Paramagnetické	paramagnetický	k2eAgNnSc1d1
</s>
<s>
I	i	k9
</s>
<s>
V	v	k7c6
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
S	s	k7c7
</s>
<s>
T	T	kA
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
E	E	kA
(	(	kIx(
<g/>
MeV	MeV	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
P	P	kA
</s>
<s>
{{{	{{{	k?
<g/>
izotopy	izotop	k1gInPc1
<g/>
}}}	}}}	k?
</s>
<s>
Není	být	k5eNaImIp3nS
<g/>
-li	-li	k?
uvedeno	uvést	k5eAaPmNgNnS
jinak	jinak	k6eAd1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
použityjednotky	použityjednotka	k1gFnPc1
SI	si	k1gNnSc2
a	a	k8xC
STP	STP	kA
(	(	kIx(
<g/>
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
,	,	kIx,
100	#num#	k4
kPa	kPa	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sr	Sr	k?
<g/>
⋏	⋏	k?
</s>
<s>
Cesium	cesium	k1gNnSc1
≺	≺	k?
<g/>
Ba	ba	k9
<g/>
≻	≻	k?
Hafnium	hafnium	k1gNnSc1
</s>
<s>
⋎	⋎	k?
<g/>
Ra	ra	k0
</s>
<s>
Baryum	baryum	k1gNnSc1
(	(	kIx(
<g/>
chemická	chemický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Ba	ba	k9
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
Barium	barium	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pátým	pátý	k4xOgInSc7
prvkem	prvek	k1gInSc7
z	z	k7c2
řady	řada	k1gFnSc2
kovů	kov	k1gInPc2
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
měkký	měkký	k2eAgMnSc1d1
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgInSc1d1
a	a	k8xC
toxický	toxický	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgFnPc1d1
fyzikálně-chemické	fyzikálně-chemický	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
baryum	baryum	k1gNnSc1
</s>
<s>
Všechny	všechen	k3xTgFnPc1
rozpustné	rozpustný	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
barya	baryum	k1gNnSc2
jsou	být	k5eAaImIp3nP
prudce	prudko	k6eAd1
jedovaté	jedovatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
měkký	měkký	k2eAgInSc1d1
<g/>
,	,	kIx,
lehký	lehký	k2eAgInSc1d1
<g/>
,	,	kIx,
reaktivní	reaktivní	k2eAgInSc1d1
kov	kov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
svými	svůj	k3xOyFgFnPc7
vlastnostmi	vlastnost	k1gFnPc7
více	hodně	k6eAd2
podobá	podobat	k5eAaImIp3nS
vlastnostem	vlastnost	k1gFnPc3
alkalických	alkalický	k2eAgInPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kapalném	kapalný	k2eAgInSc6d1
amoniaku	amoniak	k1gInSc6
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
za	za	k7c2
vzniku	vznik	k1gInSc2
černého	černý	k2eAgInSc2d1
roztoku	roztok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baryum	baryum	k1gNnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
lepším	dobrý	k2eAgInPc3d2
vodičům	vodič	k1gInPc3
elektrického	elektrický	k2eAgInSc2d1
proudu	proud	k1gInSc2
a	a	k8xC
tepla	teplo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nejreaktivnější	reaktivní	k2eAgNnSc1d3
z	z	k7c2
kovů	kov	k1gInPc2
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
a	a	k8xC
reaktivitou	reaktivita	k1gFnSc7
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
alkalickým	alkalický	k2eAgInPc3d1
kovům	kov	k1gInPc3
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc4
reaktivita	reaktivita	k1gFnSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
dlouhodobě	dlouhodobě	k6eAd1
uchováváno	uchovávat	k5eAaImNgNnS
pouze	pouze	k6eAd1
pod	pod	k7c7
vrstvou	vrstva	k1gFnSc7
alifatických	alifatický	k2eAgInPc2d1
uhlovodíků	uhlovodík	k1gInPc2
(	(	kIx(
<g/>
jako	jako	k9
petrolej	petrolej	k1gInSc1
<g/>
,	,	kIx,
nafta	nafta	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
nimiž	jenž	k3xRgNnPc7
nereaguje	reagovat	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soli	sůl	k1gFnSc2
barya	baryum	k1gNnSc2
barví	barvit	k5eAaImIp3nS
plamen	plamen	k1gInSc1
zeleně	zeleň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
reaktivní	reaktivní	k2eAgInSc1d1
a	a	k8xC
v	v	k7c6
přírodě	příroda	k1gFnSc6
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
pouze	pouze	k6eAd1
barnaté	barnatý	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
Ba	ba	k9
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
laboratoři	laboratoř	k1gFnSc6
lze	lze	k6eAd1
připravit	připravit	k5eAaPmF
sloučeniny	sloučenina	k1gFnPc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
superbáze	superbáze	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
stroncium	stroncium	k1gNnSc4
baridový	baridový	k2eAgInSc1d1
anion	anion	k1gInSc1
Ba	ba	k9
<g/>
2	#num#	k4
<g/>
−	−	k?
<g/>
,	,	kIx,
takovéto	takovýto	k3xDgFnPc1
sloučeniny	sloučenina	k1gFnPc1
jsou	být	k5eAaImIp3nP
velmi	velmi	k6eAd1
nestabilní	stabilní	k2eNgFnSc1d1
a	a	k8xC
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejsilnější	silný	k2eAgNnPc4d3
redukční	redukční	k2eAgNnPc4d1
činidla	činidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baryum	baryum	k1gNnSc1
reaguje	reagovat	k5eAaBmIp3nS
za	za	k7c4
pokojové	pokojový	k2eAgFnPc4d1
teploty	teplota	k1gFnPc4
s	s	k7c7
vodou	voda	k1gFnSc7
i	i	k8xC
kyslíkem	kyslík	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vzduchu	vzduch	k1gInSc6
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
pokrývá	pokrývat	k5eAaImIp3nS
vrstvou	vrstva	k1gFnSc7
nažloutlého	nažloutlý	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
práškové	práškový	k2eAgNnSc1d1
baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
vzduchu	vzduch	k1gInSc6
schopno	schopen	k2eAgNnSc1d1
samovolného	samovolný	k2eAgNnSc2d1
vznícení	vznícení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
zahřátí	zahřátí	k1gNnSc6
se	se	k3xPyFc4
snadno	snadno	k6eAd1
slučuje	slučovat	k5eAaImIp3nS
s	s	k7c7
dusíkem	dusík	k1gInSc7
na	na	k7c4
nitrid	nitrid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
Ba	ba	k9
<g/>
3	#num#	k4
<g/>
N	N	kA
<g/>
2	#num#	k4
a	a	k8xC
s	s	k7c7
vodíkem	vodík	k1gInSc7
na	na	k7c4
hydrid	hydrid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
BaH	bah	k0
<g/>
2	#num#	k4
a	a	k8xC
i	i	k9
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
prvků	prvek	k1gInPc2
tvoří	tvořit	k5eAaImIp3nP
za	za	k7c2
vyšších	vysoký	k2eAgFnPc2d2
teplot	teplota	k1gFnPc2
sloučeniny	sloučenina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
zásadotvorný	zásadotvorný	k2eAgInSc4d1
prvek	prvek	k1gInSc4
a	a	k8xC
rozpouští	rozpouštět	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
běžných	běžný	k2eAgFnPc6d1
kyselinách	kyselina	k1gFnPc6
za	za	k7c2
tvorby	tvorba	k1gFnSc2
barnatých	barnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nerozpouští	rozpouštět	k5eNaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
roztocích	roztok	k1gInPc6
hydroxidů	hydroxid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historický	historický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
</s>
<s>
Těživec	těživec	k1gInSc1
(	(	kIx(
<g/>
baryt	baryt	k1gInSc1
<g/>
)	)	kIx)
BaSO	basa	k1gFnSc5
<g/>
4	#num#	k4
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
poznán	poznat	k5eAaPmNgInS
na	na	k7c6
základě	základ	k1gInSc6
objevu	objev	k1gInSc2
boloňského	boloňský	k2eAgMnSc2d1
obuvníka	obuvník	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1602	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zjistil	zjistit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
při	při	k7c6
žíhání	žíhání	k1gNnSc6
síranu	síran	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
s	s	k7c7
organickými	organický	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
začíná	začínat	k5eAaImIp3nS
látka	látka	k1gFnSc1
fosforeskovat	fosforeskovat	k5eAaImF
–	–	k?
boloňské	boloňský	k2eAgInPc4d1
fosfory	fosfor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1732	#num#	k4
objevil	objevit	k5eAaPmAgInS
William	William	k1gInSc1
Withering	Withering	k1gInSc1
uhličitan	uhličitan	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
v	v	k7c6
nerostu	nerost	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
později	pozdě	k6eAd2
po	po	k7c6
něm	on	k3xPp3gInSc6
nazván	nazván	k2eAgInSc4d1
witherit	witherit	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oxid	oxid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
byl	být	k5eAaImAgInS
objeven	objeven	k2eAgInSc1d1
roku	rok	k1gInSc2
1774	#num#	k4
Carlem	Carl	k1gMnSc7
Scheelem	Scheel	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
zprvu	zprvu	k6eAd1
nepoznal	poznat	k5eNaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
sloučeninu	sloučenina	k1gFnSc4
nové	nový	k2eAgFnSc2d1
zeminy	zemina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
těživcem	těživec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistil	zjistit	k5eAaPmAgMnS
to	ten	k3xDgNnSc4
teprve	teprve	k6eAd1
Johan	Johan	k1gMnSc1
Gottlieb	Gottliba	k1gFnPc2
Gahn	Gahn	k1gMnSc1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gInSc6
to	ten	k3xDgNnSc4
pak	pak	k6eAd1
potvrdil	potvrdit	k5eAaPmAgMnS
Carl	Carl	k1gMnSc1
Scheele	Scheel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tomto	tento	k3xDgInSc6
objevu	objev	k1gInSc6
dali	dát	k5eAaPmAgMnP
těživci	těživec	k1gInSc6
název	název	k1gInSc1
baryt	baryt	k1gInSc1
z	z	k7c2
řeckého	řecký	k2eAgNnSc2d1
barys	barys	k1gInSc1
–	–	k?
těžký	těžký	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
název	název	k1gInSc1
přešel	přejít	k5eAaPmAgInS
i	i	k9
na	na	k7c4
prvek	prvek	k1gInSc4
–	–	k?
baryum	baryum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Baryum	baryum	k1gNnSc4
poprvé	poprvé	k6eAd1
připravil	připravit	k5eAaPmAgMnS
sir	sir	k1gMnSc1
Humphry	Humphra	k1gFnSc2
Davy	Dav	k1gInPc4
roku	rok	k1gInSc2
1808	#num#	k4
elektrolýzou	elektrolýza	k1gFnSc7
barnatého	barnatý	k2eAgInSc2d1
amalgamu	amalgam	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
si	se	k3xPyFc3
připravil	připravit	k5eAaPmAgInS
elektrolýzou	elektrolýza	k1gFnSc7
slabě	slabě	k6eAd1
zvlhčeného	zvlhčený	k2eAgInSc2d1
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgMnSc4d1
za	za	k7c4
použití	použití	k1gNnSc4
rtuťové	rtuťový	k2eAgFnSc2d1
katody	katoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
</s>
<s>
Psilomelan	psilomelan	k1gInSc4
–	–	k?
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
Mn	Mn	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Mn	Mn	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
O	o	k7c4
<g/>
16	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
</s>
<s>
Největší	veliký	k2eAgMnSc1d3
producenti	producent	k1gMnPc1
barya	baryum	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
velké	velký	k2eAgFnSc3d1
reaktivitě	reaktivita	k1gFnSc3
je	být	k5eAaImIp3nS
v	v	k7c6
přírodě	příroda	k1gFnSc6
možné	možný	k2eAgFnSc6d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
prakticky	prakticky	k6eAd1
pouze	pouze	k6eAd1
se	s	k7c7
sloučeninami	sloučenina	k1gFnPc7
barya	baryum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
všech	všecek	k3xTgFnPc6
svých	svůj	k3xOyFgFnPc6
sloučeninách	sloučenina	k1gFnPc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
mocenství	mocenství	k1gNnSc6
Ba	ba	k9
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
barya	baryum	k1gNnSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
i	i	k8xC
okolním	okolní	k2eAgInSc6d1
vesmíru	vesmír	k1gInSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
vzácný	vzácný	k2eAgMnSc1d1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
to	ten	k3xDgNnSc1
již	již	k6eAd1
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vysokého	vysoký	k2eAgNnSc2d1
atomového	atomový	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemské	zemský	k2eAgFnSc6d1
kůře	kůra	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
množství	množství	k1gNnSc6
0,025	0,025	k4
<g/>
–	–	k?
<g/>
0,045	0,045	k4
%	%	kIx~
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
v	v	k7c6
zastoupení	zastoupení	k1gNnSc6
prvků	prvek	k1gInPc2
podle	podle	k7c2
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
procentuální	procentuální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
390	#num#	k4
ppm	ppm	k?
(	(	kIx(
<g/>
parts	partsa	k1gFnPc2
per	pero	k1gNnPc2
milion	milion	k4xCgInSc4
=	=	kIx~
počet	počet	k1gInSc4
částic	částice	k1gFnPc2
na	na	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
částic	částice	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
ve	v	k7c6
výskytu	výskyt	k1gInSc6
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
před	před	k7c4
stroncium	stroncium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mořské	mořský	k2eAgFnSc6d1
vodě	voda	k1gFnSc6
je	být	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc1
koncentrace	koncentrace	k1gFnSc1
pouze	pouze	k6eAd1
0,03	0,03	k4
mg	mg	kA
Ba	ba	k9
<g/>
/	/	kIx~
<g/>
l	l	kA
a	a	k8xC
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
připadá	připadat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
jeden	jeden	k4xCgInSc4
atom	atom	k1gInSc4
barya	baryum	k1gNnSc2
přibližně	přibližně	k6eAd1
8	#num#	k4
miliard	miliarda	k4xCgFnPc2
atomů	atom	k1gInPc2
vodíku	vodík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3
minerálem	minerál	k1gInSc7
barya	baryum	k1gNnSc2
je	být	k5eAaImIp3nS
síran	síran	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
<g/>
,	,	kIx,
baryt	baryt	k1gInSc1
neboli	neboli	k8xC
těživec	těživec	k1gInSc1
BaSO	basa	k1gFnSc5
<g/>
4	#num#	k4
a	a	k8xC
witherit	witherit	k1gInSc1
BaCO	BaCO	k1gFnSc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
<g/>
,	,	kIx,
méně	málo	k6eAd2
významné	významný	k2eAgFnPc1d1
<g/>
,	,	kIx,
minerály	minerál	k1gInPc1
baria	barium	k1gNnSc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
</s>
<s>
nitrobaryt	nitrobaryt	k1gInSc1
<g/>
,	,	kIx,
chemicky	chemicky	k6eAd1
dusičnan	dusičnan	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
NO	no	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
</s>
<s>
barylit	barylit	k1gInSc1
Ba	ba	k9
<g/>
4	#num#	k4
<g/>
Al	ala	k1gFnPc2
<g/>
4	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
7	#num#	k4
<g/>
O	o	k7c4
<g/>
24	#num#	k4
</s>
<s>
alstonit	alstonit	k1gInSc1
(	(	kIx(
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
Ba	ba	k9
<g/>
)	)	kIx)
<g/>
CO	co	k6eAd1
<g/>
3	#num#	k4
</s>
<s>
celsian	celsian	k1gInSc1
BaAl	BaAl	k1gInSc1
<g/>
2	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
8	#num#	k4
</s>
<s>
hagertyt	hagertyt	k1gInSc1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
Fe	Fe	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
6	#num#	k4
<g/>
Ti	ten	k3xDgMnPc1
<g/>
5	#num#	k4
<g/>
Mg	mg	kA
<g/>
)	)	kIx)
<g/>
O	o	k7c4
<g/>
19	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
harmotom	harmotom	k1gInSc1
(	(	kIx(
<g/>
Ba	ba	k9
<g/>
0.5	0.5	k4
<g/>
,	,	kIx,
<g/>
Ca	ca	kA
<g/>
0.5	0.5	k4
<g/>
,	,	kIx,
<g/>
Na	na	k7c6
<g/>
,	,	kIx,
<g/>
K	k	k7c3
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
Al	ala	k1gFnPc2
<g/>
5	#num#	k4
<g/>
Si	se	k3xPyFc3
<g/>
11	#num#	k4
<g/>
O	o	k7c4
<g/>
32	#num#	k4
<g/>
·	·	k?
<g/>
12	#num#	k4
<g/>
(	(	kIx(
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
)	)	kIx)
</s>
<s>
kukharenkoit	kukharenkoit	k1gInSc1
Ba	ba	k9
<g/>
3	#num#	k4
<g/>
CeF	CeF	k1gFnPc2
<g/>
(	(	kIx(
<g/>
CO	co	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
3	#num#	k4
</s>
<s>
psilomelan	psilomelan	k1gInSc1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
Mn	Mn	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Mn	Mn	k1gFnSc1
<g/>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
8	#num#	k4
<g/>
O	o	k7c4
<g/>
16	#num#	k4
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
4	#num#	k4
</s>
<s>
sanbornit	sanbornit	k5eAaPmF
BaSi	BaSi	k?
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
5	#num#	k4
</s>
<s>
todorokit	todorokit	k1gMnSc1
(	(	kIx(
<g/>
Mn	Mn	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Mg	mg	kA
<g/>
,	,	kIx,
<g/>
Ca	ca	kA
<g/>
,	,	kIx,
<g/>
Ba	ba	k9
<g/>
,	,	kIx,
<g/>
K	K	kA
<g/>
,	,	kIx,
<g/>
Na	na	k7c6
<g/>
)	)	kIx)
<g/>
2	#num#	k4
<g/>
Mn	Mn	k1gFnSc1
<g/>
3	#num#	k4
<g/>
O	o	k7c4
<g/>
12	#num#	k4
<g/>
·	·	k?
<g/>
3	#num#	k4
<g/>
H	H	kA
<g/>
2	#num#	k4
<g/>
O.	O.	kA
</s>
<s>
Baryt	baryt	k1gInSc1
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
ve	v	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
známá	známá	k1gFnSc1
jsou	být	k5eAaImIp3nP
ložiska	ložisko	k1gNnPc1
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
<g/>
,	,	kIx,
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
a	a	k8xC
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Česku	Česko	k1gNnSc6
se	se	k3xPyFc4
ložiska	ložisko	k1gNnSc2
barytu	baryt	k1gInSc2
vyskytují	vyskytovat	k5eAaImIp3nP
v	v	k7c6
okolí	okolí	k1gNnSc6
Příbrami	Příbram	k1gFnSc2
<g/>
,	,	kIx,
Harrachova	Harrachov	k1gInSc2
a	a	k8xC
Teplic	Teplice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Izotopy	izotop	k1gInPc1
</s>
<s>
Přírodní	přírodní	k2eAgNnSc1d1
baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
směsí	směs	k1gFnSc7
sedmi	sedm	k4xCc2
izotopů	izotop	k1gInPc2
v	v	k7c6
zastoupení	zastoupení	k1gNnSc6
130	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
0,106	0,106	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
132	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
0,101	0,101	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
134	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
2,417	2,417	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
135	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
6,592	6,592	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
136	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
7,854	7,854	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
137	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
11,232	11,232	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
138	#num#	k4
<g/>
Ba	ba	k9
(	(	kIx(
<g/>
71,698	71,698	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgMnPc2d1
35	#num#	k4
uměle	uměle	k6eAd1
připravených	připravený	k2eAgInPc2d1
izotopů	izotop	k1gInPc2
barya	baryum	k1gNnSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
radioaktivních	radioaktivní	k2eAgInPc2d1
<g/>
,	,	kIx,
protože	protože	k8xS
jejich	jejich	k3xOp3gInSc1
poločas	poločas	k1gInSc1
přeměny	přeměna	k1gFnSc2
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
ve	v	k7c6
zlomcích	zlomek	k1gInPc6
sekund	sekunda	k1gFnPc2
až	až	k8xS
milisekund	milisekunda	k1gFnPc2
<g/>
,	,	kIx,
výjimku	výjimka	k1gFnSc4
tvoří	tvořit	k5eAaImIp3nP
pouze	pouze	k6eAd1
izotop	izotop	k1gInSc4
133	#num#	k4
<g/>
Ba	ba	k9
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
poločas	poločas	k1gInSc4
přeměny	přeměna	k1gFnSc2
10,55	10,55	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Baryum	baryum	k1gNnSc4
také	také	k9
vzniká	vznikat	k5eAaImIp3nS
jako	jako	k9
jeden	jeden	k4xCgInSc1
z	z	k7c2
produktů	produkt	k1gInPc2
při	při	k7c6
výbuchu	výbuch	k1gInSc6
jaderné	jaderný	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
235U	235U	k4
+	+	kIx~
1	#num#	k4
<g/>
n	n	k0
→	→	k?
145	#num#	k4
<g/>
Ba	ba	k9
+	+	kIx~
88	#num#	k4
<g/>
Kr	Kr	k1gFnPc2
+	+	kIx~
3	#num#	k4
1	#num#	k4
<g/>
n	n	k0
</s>
<s>
Výroba	výroba	k1gFnSc1
</s>
<s>
Baryt	baryt	k1gInSc1
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
baryum	baryum	k1gNnSc1
se	se	k3xPyFc4
průmyslově	průmyslově	k6eAd1
nejčastěji	často	k6eAd3
vyrábí	vyrábět	k5eAaImIp3nS
z	z	k7c2
rudy	ruda	k1gFnSc2
barytu	baryt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prvním	první	k4xOgInSc6
kroku	krok	k1gInSc6
se	se	k3xPyFc4
ruda	ruda	k1gFnSc1
redukuje	redukovat	k5eAaBmIp3nS
uhlíkem	uhlík	k1gInSc7
na	na	k7c4
sulfid	sulfid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
a	a	k8xC
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhém	druhý	k4xOgInSc6
kroku	krok	k1gInSc6
probíhá	probíhat	k5eAaImIp3nS
reakce	reakce	k1gFnSc1
sulfidu	sulfid	k1gInSc2
barnatého	barnatý	k2eAgMnSc4d1
s	s	k7c7
vodou	voda	k1gFnSc7
a	a	k8xC
oxidem	oxid	k1gInSc7
uhličitý	uhličitý	k2eAgInSc1d1
za	za	k7c2
vzniku	vznik	k1gInSc2
uhličitanu	uhličitan	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
a	a	k8xC
sulfanu	sulfan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
třetím	třetí	k4xOgInSc6
kroku	krok	k1gInSc6
se	se	k3xPyFc4
uhličitan	uhličitan	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
tepelně	tepelně	k6eAd1
rozloží	rozložit	k5eAaPmIp3nS
na	na	k7c4
oxid	oxid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
a	a	k8xC
oxid	oxid	k1gInSc4
uhličitý	uhličitý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtvrtý	čtvrtý	k4xOgInSc4
krok	krok	k1gInSc4
je	být	k5eAaImIp3nS
vakuová	vakuový	k2eAgFnSc1d1
redukce	redukce	k1gFnSc1
oxidu	oxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
za	za	k7c2
vzniku	vznik	k1gInSc2
barya	baryum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
BaSO	basa	k1gFnSc5
<g/>
4	#num#	k4
+	+	kIx~
2	#num#	k4
C	C	kA
→	→	k?
BaS	bas	k1gInSc1
+	+	kIx~
2	#num#	k4
CO2	CO2	k1gFnPc2
</s>
<s>
BaS	bas	k1gInSc1
+	+	kIx~
H2O	H2O	k1gMnPc2
+	+	kIx~
CO2	CO2	k1gMnPc2
→	→	k?
BaCO	BaCO	k1gFnSc7
<g/>
3	#num#	k4
+	+	kIx~
H2S	H2S	k1gFnSc1
</s>
<s>
BaCO	BaCO	k?
<g/>
3	#num#	k4
→	→	k?
BaO	BaO	k1gMnSc1
+	+	kIx~
CO2	CO2	k1gMnSc1
</s>
<s>
3	#num#	k4
BaO	BaO	k1gFnSc1
+	+	kIx~
2	#num#	k4
Al	ala	k1gFnPc2
→	→	k?
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
Ba	ba	k9
</s>
<s>
Kovové	kovový	k2eAgNnSc1d1
baryum	baryum	k1gNnSc1
lze	lze	k6eAd1
také	také	k9
vyrobit	vyrobit	k5eAaPmF
<g/>
,	,	kIx,
ale	ale	k8xC
ne	ne	k9
v	v	k7c6
takové	takový	k3xDgFnSc6
čistotě	čistota	k1gFnSc6
<g/>
,	,	kIx,
elektrolýzou	elektrolýza	k1gFnSc7
taveniny	tavenina	k1gFnSc2
chloridu	chlorid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
ve	v	k7c6
směsi	směs	k1gFnSc6
s	s	k7c7
chloridem	chlorid	k1gInSc7
draselným	draselný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
produktem	produkt	k1gInSc7
této	tento	k3xDgFnSc2
reakce	reakce	k1gFnSc2
je	být	k5eAaImIp3nS
elementární	elementární	k2eAgInSc4d1
chlor	chlor	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
ihned	ihned	k6eAd1
dále	daleko	k6eAd2
zpracováván	zpracovávat	k5eAaImNgInS
v	v	k7c6
chemické	chemický	k2eAgFnSc6d1
výrobě	výroba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
elektrolýze	elektrolýza	k1gFnSc3
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
grafitová	grafitový	k2eAgFnSc1d1
anoda	anoda	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
vylučuje	vylučovat	k5eAaImIp3nS
chlor	chlor	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
železná	železný	k2eAgFnSc1d1
katoda	katoda	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
vylučuje	vylučovat	k5eAaImIp3nS
baryum	baryum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
přípravě	příprava	k1gFnSc3
elementárního	elementární	k2eAgNnSc2d1
barya	baryum	k1gNnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
využít	využít	k5eAaPmF
i	i	k9
samostatná	samostatný	k2eAgFnSc1d1
vakuová	vakuový	k2eAgFnSc1d1
redukce	redukce	k1gFnSc1
oxidu	oxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
hliníkem	hliník	k1gInSc7
nebo	nebo	k8xC
křemíkem	křemík	k1gInSc7
při	při	k7c6
1	#num#	k4
200	#num#	k4
°	°	k?
<g/>
C.	C.	kA
</s>
<s>
3	#num#	k4
BaO	BaO	k1gFnSc1
+	+	kIx~
2	#num#	k4
Al	ala	k1gFnPc2
→	→	k?
Al	ala	k1gFnPc2
<g/>
2	#num#	k4
<g/>
O	o	k7c4
<g/>
3	#num#	k4
+	+	kIx~
3	#num#	k4
Ba	ba	k9
</s>
<s>
3	#num#	k4
BaO	BaO	k1gFnPc2
+	+	kIx~
Si	se	k3xPyFc3
→	→	k?
BaSiO	BaSiO	k1gFnSc7
<g/>
3	#num#	k4
+	+	kIx~
2	#num#	k4
Ba	ba	k9
</s>
<s>
K	k	k7c3
malé	malý	k2eAgFnSc3d1
přípravě	příprava	k1gFnSc3
barya	baryum	k1gNnSc2
lze	lze	k6eAd1
také	také	k9
využít	využít	k5eAaPmF
tepelný	tepelný	k2eAgInSc4d1
rozklad	rozklad	k1gInSc4
azidu	azid	k1gInSc2
barnatého	barnatý	k2eAgMnSc2d1
na	na	k7c4
dusík	dusík	k1gInSc4
a	a	k8xC
baryum	baryum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Baryem	baryum	k1gNnSc7
obarvená	obarvený	k2eAgFnSc1d1
pyrotechnika	pyrotechnik	k1gMnSc4
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
barya	baryum	k1gNnSc2
<g/>
,	,	kIx,
například	například	k6eAd1
peroxidu	peroxid	k1gInSc2
BaO	BaO	k1gFnSc2
<g/>
2	#num#	k4
nebo	nebo	k8xC
dusičnanu	dusičnan	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
NO	no	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
se	se	k3xPyFc4
využívají	využívat	k5eAaImIp3nP,k5eAaPmIp3nP
při	při	k7c6
výrobě	výroba	k1gFnSc6
pyrotechnických	pyrotechnický	k2eAgInPc2d1
produktů	produkt	k1gInPc2
pro	pro	k7c4
jejich	jejich	k3xOp3gFnSc4
výraznou	výrazný	k2eAgFnSc4d1
barevnou	barevný	k2eAgFnSc4d1
reakci	reakce	k1gFnSc4
v	v	k7c6
plameni	plamen	k1gInSc6
-	-	kIx~
barví	barvit	k5eAaImIp3nS
plamen	plamen	k1gInSc1
světle	světlo	k1gNnSc6
zeleně	zeleň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Peroxid	peroxid	k1gInSc1
barnatý	barnatý	k2eAgMnSc1d1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
bělení	bělení	k1gNnSc3
hedvábí	hedvábí	k1gNnSc2
<g/>
,	,	kIx,
rostlinných	rostlinný	k2eAgNnPc2d1
vláken	vlákno	k1gNnPc2
a	a	k8xC
slámy	sláma	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
přípravě	příprava	k1gFnSc3
hydrogenperoxidu	hydrogenperoxid	k1gInSc2
a	a	k8xC
peruhličitanu	peruhličitan	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
přípravek	přípravek	k1gInSc1
k	k	k7c3
odbarvování	odbarvování	k1gNnSc3
olovnatých	olovnatý	k2eAgNnPc2d1
skel	sklo	k1gNnPc2
a	a	k8xC
jako	jako	k9
dezinfekční	dezinfekční	k2eAgInSc4d1
prostředek	prostředek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnPc1d1
uplatnění	uplatnění	k1gNnPc1
mají	mít	k5eAaImIp3nP
sloučeniny	sloučenina	k1gFnPc1
barya	baryum	k1gNnSc2
ve	v	k7c6
speciálních	speciální	k2eAgFnPc6d1
aplikacích	aplikace	k1gFnPc6
sklářského	sklářský	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
příkladem	příklad	k1gInSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
katodové	katodový	k2eAgFnPc4d1
trubice	trubice	k1gFnPc4
pro	pro	k7c4
výrobu	výroba	k1gFnSc4
obrazovek	obrazovka	k1gFnPc2
barevných	barevný	k2eAgInPc2d1
televizních	televizní	k2eAgInPc2d1
přijímačů	přijímač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgFnPc6
zářivkách	zářivka	k1gFnPc6
slouží	sloužit	k5eAaImIp3nS
elementární	elementární	k2eAgNnSc4d1
baryum	baryum	k1gNnSc4
jako	jako	k8xS,k8xC
getr	getr	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
látka	látka	k1gFnSc1
sloužící	sloužící	k1gFnSc2
k	k	k7c3
likvidaci	likvidace	k1gFnSc3
stopových	stopový	k2eAgNnPc2d1
množství	množství	k1gNnPc2
kyslíku	kyslík	k1gInSc2
a	a	k8xC
vodních	vodní	k2eAgFnPc2d1
par	para	k1gFnPc2
v	v	k7c6
inertním	inertní	k2eAgInSc6d1
plynu	plyn	k1gInSc6
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
je	být	k5eAaImIp3nS
svítidlo	svítidlo	k1gNnSc1
naplněno	naplnit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
BaSO	basa	k1gFnSc5
<g/>
4	#num#	k4
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgFnPc4d3
sloučeniny	sloučenina	k1gFnPc4
barya	baryum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
analytické	analytický	k2eAgFnSc6d1
chemii	chemie	k1gFnSc6
se	se	k3xPyFc4
nízké	nízký	k2eAgFnSc3d1
rozpustnosti	rozpustnost	k1gFnSc3
této	tento	k3xDgFnSc2
sloučeniny	sloučenina	k1gFnSc2
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
pro	pro	k7c4
gravimetrické	gravimetrický	k2eAgNnSc4d1
stanovení	stanovení	k1gNnSc4
obsahu	obsah	k1gInSc2
síranů	síran	k1gInPc2
ve	v	k7c6
vzorku	vzorek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
nízké	nízký	k2eAgInPc4d1
obsahy	obsah	k1gInPc4
síranů	síran	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vážení	vážení	k1gNnSc1
vysráženého	vysrážený	k2eAgInSc2d1
síranu	síran	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
neposkytuje	poskytovat	k5eNaImIp3nS
spolehlivé	spolehlivý	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vhodnější	vhodný	k2eAgFnSc1d2
metoda	metoda	k1gFnSc1
nefelometrická	nefelometrický	k2eAgFnSc1d1
<g/>
,	,	kIx,
hodnotící	hodnotící	k2eAgFnSc4d1
intenzitu	intenzita	k1gFnSc4
vzniklého	vzniklý	k2eAgInSc2d1
zákalu	zákal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
gumárenském	gumárenský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
plnivo	plnivo	k1gNnSc1
kaučukových	kaučukový	k2eAgInPc2d1
výrobků	výrobek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
současně	současně	k6eAd1
barví	barvit	k5eAaImIp3nS
výsledný	výsledný	k2eAgInSc1d1
produkt	produkt	k1gInSc1
bíle	bíle	k6eAd1
</s>
<s>
Suspenze	suspenze	k1gFnSc1
síranu	síran	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
ve	v	k7c6
vodě	voda	k1gFnSc6
vykazuje	vykazovat	k5eAaImIp3nS
značně	značně	k6eAd1
vysokou	vysoký	k2eAgFnSc4d1
hustotu	hustota	k1gFnSc4
a	a	k8xC
nalézá	nalézat	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
při	při	k7c6
těžbě	těžba	k1gFnSc6
ropy	ropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyčerpání	vyčerpání	k1gNnSc6
většiny	většina	k1gFnSc2
ropy	ropa	k1gFnSc2
z	z	k7c2
ložiska	ložisko	k1gNnSc2
se	se	k3xPyFc4
barytová	barytový	k2eAgFnSc1d1
suspenze	suspenze	k1gFnSc1
vtlačuje	vtlačovat	k5eAaImIp3nS
do	do	k7c2
vrtu	vrt	k1gInSc2
a	a	k8xC
vytlačuje	vytlačovat	k5eAaImIp3nS
zbytky	zbytek	k1gInPc4
lehčí	lehký	k2eAgFnSc2d2
ropy	ropa	k1gFnSc2
k	k	k7c3
povrchu	povrch	k1gInSc3
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
tak	tak	k6eAd1
prakticky	prakticky	k6eAd1
kompletní	kompletní	k2eAgInSc1d1
vytěžení	vytěžení	k1gNnSc4
vrtu	vrt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Baryum	baryum	k1gNnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
poměrně	poměrně	k6eAd1
velký	velký	k2eAgInSc4d1
atom	atom	k1gInSc4
schopno	schopen	k2eAgNnSc4d1
značné	značný	k2eAgFnPc4d1
absorpce	absorpce	k1gFnPc4
rentgenového	rentgenový	k2eAgNnSc2d1
záření	záření	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
rentgenovém	rentgenový	k2eAgNnSc6d1
snímkování	snímkování	k1gNnSc6
trávícího	trávící	k2eAgInSc2d1
traktu	trakt	k1gInSc2
vypije	vypít	k5eAaPmIp3nS
pacient	pacient	k1gMnSc1
suspenzi	suspenze	k1gFnSc4
síranu	síran	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
desítkách	desítka	k1gFnPc6
minut	minuta	k1gFnPc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
získat	získat	k5eAaPmF
velmi	velmi	k6eAd1
kvalitní	kvalitní	k2eAgInSc4d1
snímek	snímek	k1gInSc4
pacientova	pacientův	k2eAgInSc2d1
žaludku	žaludek	k1gInSc2
a	a	k8xC
střev	střevo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nízká	nízký	k2eAgFnSc1d1
rozpustnost	rozpustnost	k1gFnSc1
této	tento	k3xDgFnSc2
sloučeniny	sloučenina	k1gFnSc2
přitom	přitom	k6eAd1
zamezí	zamezit	k5eAaPmIp3nS
možnosti	možnost	k1gFnPc4
otravy	otrava	k1gMnSc2
pacienta	pacient	k1gMnSc2
toxickým	toxický	k2eAgInSc7d1
iontem	ion	k1gInSc7
Ba	ba	k9
<g/>
2	#num#	k4
<g/>
+	+	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Síran	síran	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
složkou	složka	k1gFnSc7
omítek	omítka	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yRgFnPc7,k3yIgFnPc7
jsou	být	k5eAaImIp3nP
pokrývány	pokrýván	k2eAgFnPc1d1
zdi	zeď	k1gFnPc1
rentgenových	rentgenový	k2eAgFnPc2d1
ordinací	ordinace	k1gFnPc2
a	a	k8xC
brání	bránit	k5eAaImIp3nS
tak	tak	k6eAd1
nechtěnému	chtěný	k2eNgNnSc3d1
ozáření	ozáření	k1gNnSc3
lékařského	lékařský	k2eAgInSc2d1
personálu	personál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
čistý	čistý	k2eAgInSc1d1
síran	síran	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
ideálně	ideálně	k6eAd1
bílý	bílý	k2eAgInSc1d1
a	a	k8xC
nalézá	nalézat	k5eAaImIp3nS
uplatnění	uplatnění	k1gNnSc4
při	při	k7c6
výrobě	výroba	k1gFnSc6
součástí	součást	k1gFnPc2
spektrometrických	spektrometrický	k2eAgNnPc2d1
zařízení	zařízení	k1gNnPc2
pro	pro	k7c4
měření	měření	k1gNnSc4
barevnosti	barevnost	k1gFnSc2
různých	různý	k2eAgFnPc2d1
látek	látka	k1gFnPc2
(	(	kIx(
<g/>
textilní	textilní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
,	,	kIx,
barviva	barvivo	k1gNnPc1
v	v	k7c6
automobilovém	automobilový	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1
barnatý	barnatý	k2eAgMnSc1d1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
k	k	k7c3
výrobě	výroba	k1gFnSc3
skel	sklo	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
keramickém	keramický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
k	k	k7c3
přípravě	příprava	k1gFnSc3
oxidu	oxid	k1gInSc2
a	a	k8xC
peroxidu	peroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
a	a	k8xC
je	být	k5eAaImIp3nS
složkou	složka	k1gFnSc7
otravných	otravný	k2eAgFnPc2d1
návnad	návnada	k1gFnPc2
na	na	k7c4
hlodavce	hlodavec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Titaničitan	Titaničitan	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
začíná	začínat	k5eAaImIp3nS
používat	používat	k5eAaImF
do	do	k7c2
baterií	baterie	k1gFnPc2
příští	příští	k2eAgFnSc2d1
generace	generace	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
sloužit	sloužit	k5eAaImF
k	k	k7c3
napájení	napájení	k1gNnSc3
elektromobilů	elektromobil	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Anorganické	anorganický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Hydrid	hydrid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
BaH	bah	k0
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vzniká	vznikat	k5eAaImIp3nS
reakcí	reakce	k1gFnSc7
elementárního	elementární	k2eAgInSc2d1
vodíku	vodík	k1gInSc2
s	s	k7c7
baryem	baryum	k1gNnSc7
při	při	k7c6
vyšší	vysoký	k2eAgFnSc6d2
teplotě	teplota	k1gFnSc6
a	a	k8xC
za	za	k7c2
o	o	k7c4
něco	něco	k3yInSc4
vyšší	vysoký	k2eAgFnPc4d2
teploty	teplota	k1gFnPc4
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlastnostmi	vlastnost	k1gFnPc7
se	se	k3xPyFc4
podobá	podobat	k5eAaImIp3nS
hydridům	hydrid	k1gInPc3
alkalických	alkalický	k2eAgMnPc2d1
kovů	kov	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Oxid	oxid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
BaO	BaO	k1gFnSc4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
práškovitá	práškovitý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
hygroskopická	hygroskopický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
mírném	mírný	k2eAgNnSc6d1
zahřívání	zahřívání	k1gNnSc6
přechází	přecházet	k5eAaImIp3nS
na	na	k7c4
peroxid	peroxid	k1gInSc4
barnatý	barnatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
vodou	voda	k1gFnSc7
reaguje	reagovat	k5eAaBmIp3nS
velmi	velmi	k6eAd1
živě	živě	k6eAd1
za	za	k7c2
vzniku	vznik	k1gInSc2
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgNnSc2d1
a	a	k8xC
velkého	velký	k2eAgNnSc2d1
množství	množství	k1gNnSc2
tepla	teplo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	s	k7c7
silným	silný	k2eAgNnSc7d1
žíháním	žíhání	k1gNnSc7
dusičnanu	dusičnan	k1gInSc2
nebo	nebo	k8xC
jodidu	jodid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
vyžíhá	vyžíhat	k5eAaBmIp3nS
špatně	špatně	k6eAd1
<g/>
,	,	kIx,
tak	tak	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
peroxid	peroxid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
jej	on	k3xPp3gMnSc4
lze	lze	k6eAd1
také	také	k6eAd1
připravit	připravit	k5eAaPmF
tpelným	tpelný	k2eAgInSc7d1
rozkladem	rozklad	k1gInSc7
uhličitanu	uhličitan	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
OH	OH	kA
<g/>
)	)	kIx)
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
bezvodém	bezvodý	k2eAgInSc6d1
stavu	stav	k1gInSc6
bílý	bílý	k2eAgInSc1d1
amorfní	amorfní	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vodě	voda	k1gFnSc6
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
nejlépe	dobře	k6eAd3
ze	z	k7c2
všech	všecek	k3xTgInPc2
hydroxidů	hydroxid	k1gInPc2
kovů	kov	k1gInPc2
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
(	(	kIx(
<g/>
3,84	3,84	k4
gramu	gram	k1gInSc2
hydroxidu	hydroxid	k1gInSc2
ve	v	k7c6
100	#num#	k4
gramech	gram	k1gInPc6
vody	voda	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
rozpustnost	rozpustnost	k1gFnSc1
s	s	k7c7
teplotou	teplota	k1gFnSc7
silně	silně	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roztok	roztok	k1gInSc1
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
<g/>
,	,	kIx,
barytová	barytový	k2eAgFnSc1d1
voda	voda	k1gFnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
laboratoři	laboratoř	k1gFnSc6
jako	jako	k8xC,k8xS
zkoumadlo	zkoumadlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniká	vznikat	k5eAaImIp3nS
rozpouštěním	rozpouštění	k1gNnSc7
oxidu	oxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Peroxid	peroxid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
BaO	BaO	k1gFnSc7
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bílý	bílý	k2eAgInSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vodě	voda	k1gFnSc6
málo	málo	k6eAd1
rozpustný	rozpustný	k2eAgInSc1d1
prášek	prášek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roztoku	roztok	k1gInSc6
může	moct	k5eAaImIp3nS
reagovat	reagovat	k5eAaBmF
jako	jako	k9
oxidační	oxidační	k2eAgNnSc4d1
i	i	k8xC
redukční	redukční	k2eAgNnSc4d1
činidlo	činidlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
teplotě	teplota	k1gFnSc6
700	#num#	k4
°	°	k?
<g/>
C	C	kA
odštěpuje	odštěpovat	k5eAaImIp3nS
kyslík	kyslík	k1gInSc4
a	a	k8xC
přechází	přecházet	k5eAaImIp3nS
na	na	k7c4
oxid	oxid	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peroxid	peroxid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
se	se	k3xPyFc4
připravuje	připravovat	k5eAaImIp3nS
zahříváním	zahřívání	k1gNnSc7
oxidu	oxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
asi	asi	k9
na	na	k7c4
500	#num#	k4
°	°	k?
<g/>
C	C	kA
nebo	nebo	k8xC
hořením	hoření	k1gNnSc7
barya	baryum	k1gNnSc2
v	v	k7c6
kyslíkové	kyslíkový	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Soli	sůl	k1gFnPc1
</s>
<s>
Větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
barnatých	barnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
se	se	k3xPyFc4
ve	v	k7c6
vodě	voda	k1gFnSc6
rozpuští	rozpuštit	k5eAaPmIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
část	část	k1gFnSc1
se	se	k3xPyFc4
rozpouští	rozpouštět	k5eAaImIp3nS
hůře	zle	k6eAd2
nebo	nebo	k8xC
vůbec	vůbec	k9
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
soli	sůl	k1gFnPc1
mají	mít	k5eAaImIp3nP
bílou	bílý	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
jsou	být	k5eAaImIp3nP
bezbarvé	bezbarvý	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
anion	anion	k1gInSc1
soli	sůl	k1gFnSc2
barevný	barevný	k2eAgMnSc1d1
(	(	kIx(
<g/>
manganistany	manganistan	k1gInPc1
<g/>
,	,	kIx,
chromany	chroman	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barnaté	barnatý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
jsou	být	k5eAaImIp3nP
lépe	dobře	k6eAd2
rozpustné	rozpustný	k2eAgFnPc1d1
než	než	k8xS
soli	sůl	k1gFnPc1
hořečnaté	hořečnatý	k2eAgFnPc1d1
<g/>
,	,	kIx,
vápenaté	vápenatý	k2eAgFnPc1d1
a	a	k8xC
strontnaté	strontnatý	k2eAgFnPc1d1
a	a	k8xC
všechny	všechen	k3xTgFnPc1
rozpustné	rozpustný	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
jsou	být	k5eAaImIp3nP
prudce	prudko	k6eAd1
jedovaté	jedovatý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barnaté	barnatý	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
snadno	snadno	k6eAd1
podvojné	podvojný	k2eAgFnPc4d1
soli	sůl	k1gFnPc4
a	a	k8xC
také	také	k9
komplexy	komplex	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
ale	ale	k9
nejsou	být	k5eNaImIp3nP
pro	pro	k7c4
stroncium	stroncium	k1gNnSc4
a	a	k8xC
i	i	k9
další	další	k2eAgInPc1d1
kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
typické	typický	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Fluorid	fluorid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
BaF	baf	k0
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
krystalická	krystalický	k2eAgFnSc1d1
<g/>
,	,	kIx,
ve	v	k7c6
vodě	voda	k1gFnSc6
málo	málo	k6eAd1
rozpustná	rozpustný	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	s	k7c7
srážením	srážení	k1gNnSc7
barnatých	barnatý	k2eAgInPc2d1
iontů	ion	k1gInPc2
ionty	ion	k1gInPc4
fluoridovými	fluoridový	k2eAgFnPc7d1
nebo	nebo	k8xC
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
či	či	k8xC
uhličitanu	uhličitan	k1gInSc2
barnatého	barnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
fluorovodíkovou	fluorovodíkový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Chlorid	chlorid	k1gInSc1
barnatý	barnatý	k2eAgInSc4d1
BaCl	BaCl	k1gInSc4
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
v	v	k7c6
bezvodém	bezvodý	k2eAgInSc6d1
stavu	stav	k1gInSc6
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
ke	k	k7c3
změkčování	změkčování	k1gNnSc3
kotelní	kotelní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
s	s	k7c7
obsahem	obsah	k1gInSc7
síranu	síran	k1gInSc2
vápenatého	vápenatý	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlorid	chlorid	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
má	mít	k5eAaImIp3nS
hořkou	hořký	k2eAgFnSc4d1
chuť	chuť	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
prudce	prudko	k6eAd1
jedovatý	jedovatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
protijed	protijed	k1gInSc4
po	po	k7c6
vypláchnutí	vypláchnutí	k1gNnSc6
žaludku	žaludek	k1gInSc2
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
1	#num#	k4
<g/>
%	%	kIx~
roztok	roztok	k1gInSc1
Glauberovy	Glauberův	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
Na	na	k7c4
<g/>
2	#num#	k4
<g/>
SO	So	kA
<g/>
4	#num#	k4
<g/>
·	·	k?
<g/>
10	#num#	k4
H	H	kA
<g/>
2	#num#	k4
<g/>
O	O	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
či	či	k8xC
uhličitanu	uhličitan	k1gInSc2
barnatého	barnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
chlorovodíkovou	chlorovodíkový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Bromid	bromid	k1gInSc1
barnatý	barnatý	k2eAgInSc4d1
BaBr	BaBr	k1gInSc4
<g/>
2	#num#	k4
a	a	k8xC
jodid	jodid	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
BaI	BaI	k1gFnSc7
<g/>
2	#num#	k4
jsou	být	k5eAaImIp3nP
bílé	bílý	k2eAgFnPc1d1
krystalické	krystalický	k2eAgFnPc1d1
látky	látka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
rozpouští	rozpouštět	k5eAaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
hydroxidu	hydroxid	k1gInSc2
či	či	k8xC
uhličitanu	uhličitan	k1gInSc2
barnatého	barnatý	k2eAgMnSc4d1
s	s	k7c7
kyselinou	kyselina	k1gFnSc7
bromovodíkovou	bromovodíkový	k2eAgFnSc7d1
popřípadě	popřípadě	k6eAd1
kyselinou	kyselina	k1gFnSc7
jodovodíkovou	jodovodíkový	k2eAgFnSc7d1
<g/>
.	.	kIx.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
Ba	ba	k9
<g/>
(	(	kIx(
<g/>
NO	no	k9
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
2	#num#	k4
je	být	k5eAaImIp3nS
bezbarvá	bezbarvý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
rozpouští	rozpouštět	k5eAaImIp3nS
ve	v	k7c6
vodě	voda	k1gFnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
prudce	prudko	k6eAd1
jedovatá	jedovatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roztoku	roztok	k1gInSc6
s	s	k7c7
alkalickými	alkalický	k2eAgInPc7d1
dusičnany	dusičnan	k1gInPc7
tvoří	tvořit	k5eAaImIp3nS
podvojné	podvojný	k2eAgFnSc3d1
soli	sůl	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejjednodušeji	jednoduše	k6eAd3
se	se	k3xPyFc4
vyrábí	vyrábět	k5eAaImIp3nS
rozpouštěním	rozpouštění	k1gNnSc7
uhličitanu	uhličitan	k1gInSc2
nebo	nebo	k8xC
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
dusičné	dusičný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4
barnatý	barnatý	k2eAgInSc4d1
BaCO	BaCO	k1gFnSc7
<g/>
3	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
nerozpustná	rozpustný	k2eNgFnSc1d1
<g/>
,	,	kIx,
práškovitá	práškovitý	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
nerost	nerost	k1gInSc1
witherit	witherit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	s	k7c7
srážením	srážení	k1gNnSc7
rozpustných	rozpustný	k2eAgFnPc2d1
barnatých	barnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
roztokem	roztok	k1gInSc7
uhličitanových	uhličitanový	k2eAgInPc2d1
aniontů	anion	k1gInPc2
nebo	nebo	k8xC
pohlcováním	pohlcování	k1gNnSc7
vzdušného	vzdušný	k2eAgInSc2d1
oxidu	oxid	k1gInSc2
uhličitého	uhličitý	k2eAgInSc2d1
hydroxidem	hydroxid	k1gInSc7
barnatým	barnatý	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Síran	síran	k1gInSc1
barnatý	barnatý	k2eAgInSc1d1
BaSO	basa	k1gFnSc5
<g/>
4	#num#	k4
je	být	k5eAaImIp3nS
bílá	bílý	k2eAgFnSc1d1
krystalická	krystalický	k2eAgFnSc1d1
látka	látka	k1gFnSc1
<g/>
,	,	kIx,
nerozpustná	rozpustný	k2eNgFnSc1d1
ve	v	k7c6
vodě	voda	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
jako	jako	k9
minerál	minerál	k1gInSc1
baryt	baryt	k1gInSc1
neboli	neboli	k8xC
těživec	těživec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravuje	připravovat	k5eAaImIp3nS
se	se	k3xPyFc4
reakcí	reakce	k1gFnSc7
uhličitanu	uhličitan	k1gInSc2
nebo	nebo	k8xC
hydroxidu	hydroxid	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
v	v	k7c6
kyselině	kyselina	k1gFnSc6
sírové	sírový	k2eAgFnSc2d1
nebo	nebo	k8xC
srážením	srážení	k1gNnSc7
roztoků	roztok	k1gInPc2
rozpustné	rozpustný	k2eAgFnSc2d1
barnaté	barnatý	k2eAgFnSc2d1
soli	sůl	k1gFnSc2
solí	sůl	k1gFnPc2
rozpustného	rozpustný	k2eAgInSc2d1
síranu	síran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Organické	organický	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
organické	organický	k2eAgFnPc4d1
sloučeniny	sloučenina	k1gFnPc4
barya	baryum	k1gNnSc2
patří	patřit	k5eAaImIp3nP
zejména	zejména	k9
barnaté	barnatý	k2eAgFnPc1d1
soli	sůl	k1gFnPc1
organických	organický	k2eAgFnPc2d1
kyselin	kyselina	k1gFnPc2
a	a	k8xC
barnaté	barnatý	k2eAgInPc4d1
alkoholáty	alkoholát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgFnPc3d1
barnatým	barnatý	k2eAgFnPc3d1
sloučeninám	sloučenina	k1gFnPc3
patří	patřit	k5eAaImIp3nS
organické	organický	k2eAgInPc4d1
komplexy	komplex	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
zvláštní	zvláštní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
organických	organický	k2eAgFnPc2d1
barnatých	barnatý	k2eAgFnPc2d1
sloučenin	sloučenina	k1gFnPc2
tvoří	tvořit	k5eAaImIp3nP
organokovové	organokovový	k2eAgFnPc1d1
sloučeniny	sloučenina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Zdravotní	zdravotní	k2eAgNnSc1d1
riziko	riziko	k1gNnSc1
</s>
<s>
Při	při	k7c6
vdechování	vdechování	k1gNnSc6
prachových	prachový	k2eAgFnPc2d1
částic	částice	k1gFnPc2
barya	baryum	k1gNnSc2
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
zánětům	zánět	k1gInPc3
průdušek	průduška	k1gFnPc2
a	a	k8xC
plic	plíce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
otravě	otrava	k1gFnSc3
chloridem	chlorid	k1gInSc7
barnatým	barnatý	k2eAgMnSc7d1
stačí	stačit	k5eAaBmIp3nS
asi	asi	k9
0,2	0,2	k4
g	g	kA
<g/>
,	,	kIx,
smrtelná	smrtelný	k2eAgFnSc1d1
dávka	dávka	k1gFnSc1
je	být	k5eAaImIp3nS
0,8	0,8	k4
až	až	k9
2	#num#	k4
g.	g.	k?
</s>
<s>
Příznaky	příznak	k1gInPc1
<g/>
:	:	kIx,
dráždění	dráždění	k1gNnSc1
trávicího	trávicí	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
<g/>
,	,	kIx,
slinění	slinění	k1gNnSc2
<g/>
,	,	kIx,
zvracení	zvracení	k1gNnSc2
<g/>
,	,	kIx,
krvácení	krvácení	k1gNnSc4
trávicího	trávicí	k2eAgNnSc2d1
ústrojí	ústrojí	k1gNnSc2
<g/>
,	,	kIx,
působí	působit	k5eAaImIp3nS
na	na	k7c4
nervový	nervový	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
na	na	k7c4
buňky	buňka	k1gFnPc4
kosterního	kosterní	k2eAgMnSc2d1
a	a	k8xC
srdečního	srdeční	k2eAgNnSc2d1
svalstva	svalstvo	k1gNnSc2
<g/>
,	,	kIx,
třes	třes	k1gInSc4
<g/>
,	,	kIx,
dýchací	dýchací	k2eAgFnPc4d1
potíže	potíž	k1gFnPc4
<g/>
,	,	kIx,
bolesti	bolest	k1gFnPc4
v	v	k7c6
celém	celý	k2eAgNnSc6d1
těle	tělo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Smrt	smrt	k1gFnSc1
nastává	nastávat	k5eAaImIp3nS
za	za	k7c2
plného	plný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
zástavou	zástava	k1gFnSc7
srdce	srdce	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
otravám	otrava	k1gFnPc3
baryem	baryum	k1gNnSc7
může	moct	k5eAaImIp3nS
dojít	dojít	k5eAaPmF
při	při	k7c6
použití	použití	k1gNnSc6
nekvalitního	kvalitní	k2eNgInSc2d1
síranu	síran	k1gInSc2
barnatého	barnatý	k2eAgInSc2d1
coby	coby	k?
kontrastní	kontrastní	k2eAgFnPc4d1
látky	látka	k1gFnPc4
pro	pro	k7c4
rentgen	rentgen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Protijedem	protijed	k1gInSc7
je	být	k5eAaImIp3nS
roztok	roztok	k1gInSc1
síranu	síran	k1gInSc2
sodného	sodný	k2eAgInSc2d1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.etymonline.com/word/barium	https://www.etymonline.com/word/barium	k1gNnSc1
<g/>
↑	↑	k?
http://www.nndc.bnl.gov/chart/	http://www.nndc.bnl.gov/chart/	k?
<g/>
↑	↑	k?
http://www.nndc.bnl.gov/chart/reCenter.jsp?z=56&	http://www.nndc.bnl.gov/chart/reCenter.jsp?z=56&	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Cotton	Cotton	k1gInSc1
F.	F.	kA
<g/>
A.	A.	kA
<g/>
,	,	kIx,
Wilkinson	Wilkinson	k1gMnSc1
J.	J.	kA
<g/>
:	:	kIx,
<g/>
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
souborné	souborný	k2eAgNnSc1d1
zpracování	zpracování	k1gNnSc1
pro	pro	k7c4
pokročilé	pokročilý	k1gMnPc4
<g/>
,	,	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1973	#num#	k4
</s>
<s>
Holzbecher	Holzbechra	k1gFnPc2
Z.	Z.	kA
<g/>
:	:	kIx,
<g/>
Analytická	analytický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
<g/>
,	,	kIx,
SNTL	SNTL	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1974	#num#	k4
</s>
<s>
Jursík	Jursík	k1gMnSc1
F.	F.	kA
<g/>
:	:	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
nekovů	nekov	k1gInPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7080-504-8	80-7080-504-8	k4
</s>
<s>
Dr	dr	kA
<g/>
.	.	kIx.
Heinrich	Heinrich	k1gMnSc1
Remy	remy	k1gNnSc2
<g/>
,	,	kIx,
Anorganická	anorganický	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc4
1961	#num#	k4
</s>
<s>
N.	N.	kA
N.	N.	kA
Greenwood	Greenwood	k1gInSc1
–	–	k?
A.	A.	kA
Earnshaw	Earnshaw	k1gFnSc2
<g/>
,	,	kIx,
Chemie	chemie	k1gFnSc1
prvků	prvek	k1gInPc2
1	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc2
1993	#num#	k4
ISBN	ISBN	kA
80-85427-38-9	80-85427-38-9	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
baryum	baryum	k1gNnSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
baryum	baryum	k1gNnSc4
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Periodická	periodický	k2eAgFnSc1d1
tabulka	tabulka	k1gFnSc1
prvků	prvek	k1gInPc2
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
H	H	kA
</s>
<s>
He	he	k0
</s>
<s>
Li	li	k8xS
</s>
<s>
Be	Be	k?
</s>
<s>
B	B	kA
</s>
<s>
C	C	kA
</s>
<s>
N	N	kA
</s>
<s>
O	o	k7c6
</s>
<s>
F	F	kA
</s>
<s>
Ne	ne	k9
</s>
<s>
Na	na	k7c6
</s>
<s>
Mg	mg	kA
</s>
<s>
Al	ala	k1gFnPc2
</s>
<s>
Si	se	k3xPyFc3
</s>
<s>
P	P	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Cl	Cl	k?
</s>
<s>
Ar	ar	k1gInSc1
</s>
<s>
K	k	k7c3
</s>
<s>
Ca	ca	kA
</s>
<s>
Sc	Sc	k?
</s>
<s>
Ti	ten	k3xDgMnPc1
</s>
<s>
V	v	k7c6
</s>
<s>
Cr	cr	k0
</s>
<s>
Mn	Mn	k?
</s>
<s>
Fe	Fe	k?
</s>
<s>
Co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
</s>
<s>
Ni	on	k3xPp3gFnSc4
</s>
<s>
Cu	Cu	k?
</s>
<s>
Zn	zn	kA
</s>
<s>
Ga	Ga	k?
</s>
<s>
Ge	Ge	k?
</s>
<s>
As	as	k9
</s>
<s>
Se	s	k7c7
</s>
<s>
Br	br	k0
</s>
<s>
Kr	Kr	k?
</s>
<s>
Rb	Rb	k?
</s>
<s>
Sr	Sr	k?
</s>
<s>
Y	Y	kA
</s>
<s>
Zr	Zr	k?
</s>
<s>
Nb	Nb	k?
</s>
<s>
Mo	Mo	k?
</s>
<s>
Tc	tc	k0
</s>
<s>
Ru	Ru	k?
</s>
<s>
Rh	Rh	k?
</s>
<s>
Pd	Pd	k?
</s>
<s>
Ag	Ag	k?
</s>
<s>
Cd	cd	kA
</s>
<s>
In	In	k?
</s>
<s>
Sn	Sn	k?
</s>
<s>
Sb	sb	kA
</s>
<s>
Te	Te	k?
</s>
<s>
I	i	k9
</s>
<s>
Xe	Xe	k?
</s>
<s>
Cs	Cs	k?
</s>
<s>
Ba	ba	k9
</s>
<s>
La	la	k1gNnSc1
</s>
<s>
Ce	Ce	k?
</s>
<s>
Pr	pr	k0
</s>
<s>
Nd	Nd	k?
</s>
<s>
Pm	Pm	k?
</s>
<s>
Sm	Sm	k?
</s>
<s>
Eu	Eu	k?
</s>
<s>
Gd	Gd	k?
</s>
<s>
Tb	Tb	k?
</s>
<s>
Dy	Dy	k?
</s>
<s>
Ho	on	k3xPp3gNnSc4
</s>
<s>
Er	Er	k?
</s>
<s>
Tm	Tm	k?
</s>
<s>
Yb	Yb	k?
</s>
<s>
Lu	Lu	k?
</s>
<s>
Hf	Hf	k?
</s>
<s>
Ta	ten	k3xDgFnSc1
</s>
<s>
W	W	kA
</s>
<s>
Re	re	k9
</s>
<s>
Os	osa	k1gFnPc2
</s>
<s>
Ir	Ir	k1gMnSc1
</s>
<s>
Pt	Pt	k?
</s>
<s>
Au	au	k0
</s>
<s>
Hg	Hg	k?
</s>
<s>
Tl	Tl	k?
</s>
<s>
Pb	Pb	k?
</s>
<s>
Bi	Bi	k?
</s>
<s>
Po	po	k7c6
</s>
<s>
At	At	k?
</s>
<s>
Rn	Rn	k?
</s>
<s>
Fr	fr	k0
</s>
<s>
Ra	ra	k0
</s>
<s>
Ac	Ac	k?
</s>
<s>
Th	Th	k?
</s>
<s>
Pa	Pa	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Np	Np	k?
</s>
<s>
Pu	Pu	k?
</s>
<s>
Am	Am	k?
</s>
<s>
Cm	cm	kA
</s>
<s>
Bk	Bk	k?
</s>
<s>
Cf	Cf	k?
</s>
<s>
Es	es	k1gNnSc1
</s>
<s>
Fm	Fm	k?
</s>
<s>
Md	Md	k?
</s>
<s>
No	no	k9
</s>
<s>
Lr	Lr	k?
</s>
<s>
Rf	Rf	k?
</s>
<s>
Db	db	kA
</s>
<s>
Sg	Sg	k?
</s>
<s>
Bh	Bh	k?
</s>
<s>
Hs	Hs	k?
</s>
<s>
Mt	Mt	k?
</s>
<s>
Ds	Ds	k?
</s>
<s>
Rg	Rg	k?
</s>
<s>
Cn	Cn	k?
</s>
<s>
Nh	Nh	k?
</s>
<s>
Fl	Fl	k?
</s>
<s>
Mc	Mc	k?
</s>
<s>
Lv	Lv	k?
</s>
<s>
Ts	ts	k0
</s>
<s>
Og	Og	k?
</s>
<s>
Alkalické	alkalický	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Kovy	kov	k1gInPc1
alkalických	alkalický	k2eAgFnPc2d1
zemin	zemina	k1gFnPc2
</s>
<s>
Lanthanoidy	Lanthanoida	k1gFnPc1
</s>
<s>
Aktinoidy	Aktinoida	k1gFnPc1
</s>
<s>
Přechodné	přechodný	k2eAgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Nepřechodné	přechodný	k2eNgInPc1d1
kovy	kov	k1gInPc1
</s>
<s>
Polokovy	Polokův	k2eAgFnPc1d1
</s>
<s>
Nekovy	nekov	k1gInPc1
</s>
<s>
Halogeny	halogen	k1gInPc1
</s>
<s>
Vzácné	vzácný	k2eAgInPc1d1
plyny	plyn	k1gInPc1
</s>
<s>
neznámé	známý	k2eNgNnSc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4140300-9	4140300-9	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
5754	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85011853	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85011853	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
</s>
