<s>
Petronas	Petronas	k1gInSc1	Petronas
Towers	Towersa	k1gFnPc2	Towersa
<g/>
,	,	kIx,	,
také	také	k9	také
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
Petronas	Petronas	k1gInSc1	Petronas
Twin	Twina	k1gFnPc2	Twina
Towers	Towersa	k1gFnPc2	Towersa
(	(	kIx(	(
<g/>
malajsky	malajsky	k6eAd1	malajsky
<g/>
:	:	kIx,	:
Menara	Menara	k1gFnSc1	Menara
Petronas	Petronas	k1gMnSc1	Petronas
či	či	k8xC	či
Menara	Menara	k1gFnSc1	Menara
Berkembar	Berkembara	k1gFnPc2	Berkembara
Petronas	Petronasa	k1gFnPc2	Petronasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dvojice	dvojice	k1gFnSc1	dvojice
postmoderních	postmoderní	k2eAgInPc2d1	postmoderní
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
v	v	k7c6	v
Kuala	Kual	k1gMnSc2	Kual
Lumpuru	Lumpur	k1gInSc6	Lumpur
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Malajsie	Malajsie	k1gFnSc2	Malajsie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
452	[number]	k4	452
m	m	kA	m
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k8xS	až
2004	[number]	k4	2004
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
budovy	budova	k1gFnPc4	budova
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byly	být	k5eAaImAgInP	být
mezi	mezi	k7c7	mezi
nejvyššími	vysoký	k2eAgInPc7d3	Nejvyšší
mrakodrapy	mrakodrap	k1gInPc7	mrakodrap
světa	svět	k1gInSc2	svět
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
v	v	k7c6	v
letech	let	k1gInPc6	let
1992	[number]	k4	1992
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
otevření	otevření	k1gNnSc1	otevření
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
návrhu	návrh	k1gInSc2	návrh
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
architekt	architekt	k1gMnSc1	architekt
argentinského	argentinský	k2eAgInSc2d1	argentinský
původu	původ	k1gInSc2	původ
César	César	k1gMnSc1	César
Antonio	Antonio	k1gMnSc1	Antonio
Pelli	Pelle	k1gFnSc4	Pelle
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
propojené	propojený	k2eAgFnPc4d1	propojená
mostem	most	k1gInSc7	most
mezi	mezi	k7c4	mezi
41	[number]	k4	41
<g/>
.	.	kIx.	.
a	a	k8xC	a
42	[number]	k4	42
<g/>
.	.	kIx.	.
poschodím	poschodí	k1gNnSc7	poschodí
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
170	[number]	k4	170
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
58	[number]	k4	58
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stejném	stejný	k2eAgNnSc6d1	stejné
poschodí	poschodí	k1gNnSc6	poschodí
je	být	k5eAaImIp3nS	být
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přestoupit	přestoupit	k5eAaPmF	přestoupit
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
výtahu	výtah	k1gInSc2	výtah
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgNnSc4d2	vyšší
podlaží	podlaží	k1gNnSc4	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Most	most	k1gInSc1	most
je	být	k5eAaImIp3nS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
návštěvníky	návštěvník	k1gMnPc4	návštěvník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vstupenky	vstupenka	k1gFnSc2	vstupenka
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
předem	předem	k6eAd1	předem
rezervovat	rezervovat	k5eAaBmF	rezervovat
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
světa	svět	k1gInSc2	svět
Seznam	seznam	k1gInSc4	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
staveb	stavba	k1gFnPc2	stavba
světa	svět	k1gInSc2	svět
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petronas	Petronasa	k1gFnPc2	Petronasa
Twin	Twino	k1gNnPc2	Twino
Towers	Towersa	k1gFnPc2	Towersa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
