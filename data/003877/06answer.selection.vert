<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
suši	suš	k1gFnSc6	suš
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
Nara	Nar	k1gInSc2	Nar
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
718	[number]	k4	718
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zákoníku	zákoník	k1gInSc2	zákoník
Yoro	Yoro	k1gMnSc1	Yoro
ritsuryo	ritsuryo	k1gMnSc1	ritsuryo
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
japonských	japonský	k2eAgMnPc2d1	japonský
písemných	písemný	k2eAgMnPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
jakési	jakýsi	k3yIgNnSc1	jakýsi
zakono-suši	zakonouše	k1gFnSc4	zakono-suše
jako	jako	k8xC	jako
způsob	způsob	k1gInSc4	způsob
odvádění	odvádění	k1gNnSc2	odvádění
naturální	naturální	k2eAgFnSc2d1	naturální
daně	daň	k1gFnSc2	daň
<g/>
;	;	kIx,	;
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
přípravy	příprava	k1gFnSc2	příprava
tohoto	tento	k3xDgInSc2	tento
suši	suš	k1gFnSc3	suš
ovšem	ovšem	k9	ovšem
uvedeny	uvést	k5eAaPmNgFnP	uvést
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
stále	stále	k6eAd1	stále
o	o	k7c4	o
typ	typ	k1gInSc4	typ
nare-suši	nareuše	k1gFnSc4	nare-suše
<g/>
.	.	kIx.	.
</s>
