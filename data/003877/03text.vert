<s>
Suši	suš	k1gFnSc3	suš
(	(	kIx(	(
<g/>
sushi	sushi	k6eAd1	sushi
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
寿	寿	k?	寿
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
jídel	jídlo	k1gNnPc2	jídlo
japonské	japonský	k2eAgFnSc2d1	japonská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
pojmem	pojem	k1gInSc7	pojem
představí	představit	k5eAaPmIp3nS	představit
zejména	zejména	k9	zejména
syrové	syrový	k2eAgNnSc1d1	syrové
rybí	rybí	k2eAgNnSc1d1	rybí
maso	maso	k1gNnSc1	maso
s	s	k7c7	s
rýží	rýže	k1gFnSc7	rýže
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
suši	suš	k1gFnSc3	suš
je	být	k5eAaImIp3nS	být
především	především	k9	především
vařená	vařený	k2eAgFnSc1d1	vařená
rýže	rýže	k1gFnSc1	rýže
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
speciální	speciální	k2eAgFnSc7d1	speciální
sladkokyselou	sladkokyselý	k2eAgFnSc7d1	sladkokyselá
omáčkou	omáčka	k1gFnSc7	omáčka
sušinoko	sušinoko	k6eAd1	sušinoko
připravenou	připravený	k2eAgFnSc7d1	připravená
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
slovo	slovo	k1gNnSc4	slovo
suši	suš	k1gFnSc3	suš
v	v	k7c6	v
doslovném	doslovný	k2eAgInSc6d1	doslovný
překladu	překlad	k1gInSc6	překlad
z	z	k7c2	z
japonštiny	japonština	k1gFnSc2	japonština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
kyselá	kyselý	k2eAgFnSc1d1	kyselá
rýže	rýže	k1gFnSc1	rýže
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Suši	suš	k1gFnSc3	suš
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
nare-suši	nareuše	k1gFnSc4	nare-suše
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
bylo	být	k5eAaImAgNnS	být
jídlo	jídlo	k1gNnSc1	jídlo
z	z	k7c2	z
ryb	ryba	k1gFnPc2	ryba
kvašených	kvašený	k2eAgFnPc2d1	kvašená
v	v	k7c6	v
rýži	rýže	k1gFnSc6	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Prosolené	prosolený	k2eAgFnPc1d1	prosolená
ryby	ryba	k1gFnPc1	ryba
se	se	k3xPyFc4	se
na	na	k7c4	na
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
vložily	vložit	k5eAaPmAgFnP	vložit
do	do	k7c2	do
vařené	vařený	k2eAgFnSc2d1	vařená
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
směsi	směs	k1gFnPc1	směs
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
obilí	obilí	k1gNnSc2	obilí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
probíhalo	probíhat	k5eAaImAgNnS	probíhat
mléčné	mléčný	k2eAgNnSc1d1	mléčné
kvašení	kvašení	k1gNnSc1	kvašení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
ryby	ryba	k1gFnPc4	ryba
konzervovalo	konzervovat	k5eAaBmAgNnS	konzervovat
a	a	k8xC	a
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
je	on	k3xPp3gMnPc4	on
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
skladovat	skladovat	k5eAaImF	skladovat
<g/>
.	.	kIx.	.
</s>
<s>
Ryby	Ryby	k1gFnPc4	Ryby
jím	jíst	k5eAaImIp1nS	jíst
také	také	k9	také
byly	být	k5eAaImAgFnP	být
obohaceny	obohatit	k5eAaPmNgFnP	obohatit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
živiny	živina	k1gFnPc4	živina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
syrových	syrový	k2eAgFnPc6d1	syrová
rybách	ryba	k1gFnPc6	ryba
nebyly	být	k5eNaImAgFnP	být
<g/>
.	.	kIx.	.
</s>
<s>
Rýže	rýže	k1gFnSc1	rýže
se	se	k3xPyFc4	se
nejedla	jíst	k5eNaImAgFnS	jíst
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
pouhý	pouhý	k2eAgInSc4d1	pouhý
konzervační	konzervační	k2eAgInSc4d1	konzervační
prostředek	prostředek	k1gInSc4	prostředek
vyhazovala	vyhazovat	k5eAaImAgFnS	vyhazovat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
uchovávání	uchovávání	k1gNnSc2	uchovávání
ryb	ryba	k1gFnPc2	ryba
pocházel	pocházet	k5eAaImAgMnS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
s	s	k7c7	s
monzumovým	monzumův	k2eAgNnSc7d1	monzumův
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ryby	ryba	k1gFnPc1	ryba
dostávaly	dostávat	k5eAaImAgFnP	dostávat
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
jen	jen	k9	jen
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
je	být	k5eAaImIp3nS	být
pochytat	pochytat	k5eAaPmF	pochytat
a	a	k8xC	a
uskladnit	uskladnit	k5eAaPmF	uskladnit
do	do	k7c2	do
zásoby	zásoba	k1gFnSc2	zásoba
na	na	k7c4	na
suchá	suchý	k2eAgNnPc4d1	suché
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
okolních	okolní	k2eAgFnPc2d1	okolní
zemí	zem	k1gFnPc2	zem
se	se	k3xPyFc4	se
šíril	šírit	k5eAaPmAgInS	šírit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
znalostí	znalost	k1gFnSc7	znalost
pěstování	pěstování	k1gNnSc2	pěstování
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
přes	přes	k7c4	přes
Čínu	Čína	k1gFnSc4	Čína
neznámo	neznámo	k6eAd1	neznámo
kdy	kdy	k6eAd1	kdy
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
období	období	k1gNnSc6	období
Džómon	Džómona	k1gFnPc2	Džómona
(	(	kIx(	(
<g/>
asi	asi	k9	asi
10	[number]	k4	10
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
př.n.l.	př.n.l.	k?	př.n.l.
až	až	k6eAd1	až
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
suši	suš	k1gFnSc6	suš
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
Nara	Nar	k1gInSc2	Nar
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
718	[number]	k4	718
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
ze	z	k7c2	z
zákoníku	zákoník	k1gInSc2	zákoník
Yoro	Yoro	k1gMnSc1	Yoro
ritsuryo	ritsuryo	k1gMnSc1	ritsuryo
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgMnPc2d3	nejstarší
japonských	japonský	k2eAgMnPc2d1	japonský
písemných	písemný	k2eAgMnPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
jakési	jakýsi	k3yIgNnSc1	jakýsi
zakono-suši	zakonouše	k1gFnSc4	zakono-suše
jako	jako	k8xC	jako
způsob	způsob	k1gInSc4	způsob
odvádění	odvádění	k1gNnSc2	odvádění
naturální	naturální	k2eAgFnSc2d1	naturální
daně	daň	k1gFnSc2	daň
<g/>
;	;	kIx,	;
žádné	žádný	k3yNgFnPc4	žádný
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
způsobu	způsob	k1gInSc2	způsob
přípravy	příprava	k1gFnSc2	příprava
tohoto	tento	k3xDgInSc2	tento
suši	suš	k1gFnSc3	suš
ovšem	ovšem	k9	ovšem
uvedeny	uvést	k5eAaPmNgFnP	uvést
nejsou	být	k5eNaImIp3nP	být
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
ale	ale	k8xC	ale
jde	jít	k5eAaImIp3nS	jít
stále	stále	k6eAd1	stále
o	o	k7c4	o
typ	typ	k1gInSc4	typ
nare-suši	nareuše	k1gFnSc4	nare-suše
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
klasického	klasický	k2eAgInSc2d1	klasický
suši	suš	k1gFnSc3	suš
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
až	až	k9	až
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
období	období	k1gNnSc6	období
Muromači	Muromač	k1gMnPc1	Muromač
(	(	kIx(	(
<g/>
1333	[number]	k4	1333
<g/>
-	-	kIx~	-
<g/>
1568	[number]	k4	1568
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
kulinářství	kulinářství	k1gNnSc1	kulinářství
obecně	obecně	k6eAd1	obecně
zažilo	zažít	k5eAaPmAgNnS	zažít
svůj	svůj	k3xOyFgInSc4	svůj
rozkvět	rozkvět	k1gInSc4	rozkvět
-	-	kIx~	-
zrodily	zrodit	k5eAaPmAgFnP	zrodit
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
školy	škola	k1gFnPc1	škola
vaření	vaření	k1gNnSc1	vaření
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
proslulý	proslulý	k2eAgInSc1d1	proslulý
japonský	japonský	k2eAgInSc1d1	japonský
čajový	čajový	k2eAgInSc1d1	čajový
obřad	obřad	k1gInSc1	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
fermentace	fermentace	k1gFnSc1	fermentace
nare-suši	nareuše	k1gFnSc4	nare-suše
byla	být	k5eAaImAgFnS	být
radikálně	radikálně	k6eAd1	radikálně
zkrácena	zkrátit	k5eAaPmNgFnS	zkrátit
a	a	k8xC	a
rýže	rýže	k1gFnSc1	rýže
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
přestat	přestat	k5eAaPmF	přestat
vyhazovat	vyhazovat	k5eAaImF	vyhazovat
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
konzumována	konzumovat	k5eAaBmNgFnS	konzumovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
částečně	částečně	k6eAd1	částečně
fermentovanou	fermentovaný	k2eAgFnSc7d1	fermentovaná
rybou	ryba	k1gFnSc7	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
posunu	posun	k1gInSc3	posun
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
rýžového	rýžový	k2eAgInSc2d1	rýžový
octa	ocet	k1gInSc2	ocet
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
přidání	přidání	k1gNnSc4	přidání
do	do	k7c2	do
suši	suš	k1gFnSc3	suš
dovolovalo	dovolovat	k5eAaImAgNnS	dovolovat
vypustit	vypustit	k5eAaPmF	vypustit
zdlouhavou	zdlouhavý	k2eAgFnSc4d1	zdlouhavá
fázi	fáze	k1gFnSc4	fáze
kvašení	kvašení	k1gNnSc2	kvašení
<g/>
.	.	kIx.	.
</s>
<s>
Kousky	kousek	k1gInPc1	kousek
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
ochutily	ochutit	k5eAaPmAgInP	ochutit
rýžovým	rýžový	k2eAgInSc7d1	rýžový
octem	ocet	k1gInSc7	ocet
<g/>
,	,	kIx,	,
přidala	přidat	k5eAaPmAgFnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
rýže	rýže	k1gFnSc1	rýže
a	a	k8xC	a
lisovaly	lisovat	k5eAaImAgInP	lisovat
se	se	k3xPyFc4	se
do	do	k7c2	do
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
krabiček	krabička	k1gFnPc2	krabička
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
názvy	název	k1gInPc4	název
jako	jako	k8xC	jako
hako-suši	hakouše	k1gFnSc4	hako-suše
(	(	kIx(	(
<g/>
krabičkové	krabičkový	k2eAgFnSc3d1	krabičková
suši	suš	k1gFnSc3	suš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oshi-suši	oshiuše	k1gFnSc4	oshi-suše
(	(	kIx(	(
<g/>
stlačované	stlačovaný	k2eAgFnSc3d1	stlačovaná
suši	suš	k1gFnSc3	suš
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
haja-suši	hajauše	k1gFnSc4	haja-suše
(	(	kIx(	(
<g/>
rychlé	rychlý	k2eAgFnSc3d1	rychlá
suši	suš	k1gFnSc3	suš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
nejznámější	známý	k2eAgInSc1d3	nejznámější
druh	druh	k1gInSc1	druh
suši	suš	k1gFnSc3	suš
<g/>
,	,	kIx,	,
nigiri-suši	nigiriuše	k1gFnSc3	nigiri-suše
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c4	v
Edu	Eda	k1gMnSc4	Eda
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	on	k3xPp3gMnSc4	on
otce	otec	k1gMnSc4	otec
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Hanaja	Hanaja	k1gMnSc1	Hanaja
Johei	Johe	k1gFnSc2	Johe
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
začal	začít	k5eAaPmAgMnS	začít
prodávat	prodávat	k5eAaImF	prodávat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
stáncích	stánek	k1gInPc6	stánek
jako	jako	k9	jako
rychlé	rychlý	k2eAgNnSc4d1	rychlé
občerstvení	občerstvení	k1gNnSc4	občerstvení
<g/>
:	:	kIx,	:
umístit	umístit	k5eAaPmF	umístit
kus	kus	k1gInSc4	kus
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
ryby	ryba	k1gFnSc2	ryba
na	na	k7c4	na
vrch	vrch	k1gInSc4	vrch
rýžové	rýžový	k2eAgFnSc2d1	rýžová
hnětenky	hnětenka	k1gFnSc2	hnětenka
zabralo	zabrat	k5eAaPmAgNnS	zabrat
pár	pár	k4xCyI	pár
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Přeskočil	přeskočit	k5eAaPmAgInS	přeskočit
tak	tak	k9	tak
krok	krok	k1gInSc1	krok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ryba	ryba	k1gFnSc1	ryba
s	s	k7c7	s
rýží	rýže	k1gFnSc7	rýže
s	s	k7c7	s
octem	ocet	k1gInSc7	ocet
na	na	k7c4	na
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
zmáčkla	zmáčknout	k5eAaPmAgFnS	zmáčknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
déle	dlouho	k6eAd2	dlouho
vydržela	vydržet	k5eAaPmAgFnS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
období	období	k1gNnSc6	období
Meidži	Meidž	k1gFnSc6	Meidž
(	(	kIx(	(
<g/>
1868	[number]	k4	1868
<g/>
-	-	kIx~	-
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
skončila	skončit	k5eAaPmAgFnS	skončit
politika	politika	k1gFnSc1	politika
národního	národní	k2eAgInSc2d1	národní
izolacionismu	izolacionismus	k1gInSc2	izolacionismus
<g/>
,	,	kIx,	,
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
technologie	technologie	k1gFnSc2	technologie
chlazení	chlazení	k1gNnSc2	chlazení
a	a	k8xC	a
také	také	k9	také
móda	móda	k1gFnSc1	móda
výraznějších	výrazný	k2eAgFnPc2d2	výraznější
chutí	chuť	k1gFnPc2	chuť
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
nových	nový	k2eAgFnPc2d1	nová
surovin	surovina	k1gFnPc2	surovina
v	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
suši	suš	k1gFnSc6	suš
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
maki	mak	k1gFnSc3	mak
suši	suš	k1gFnSc3	suš
a	a	k8xC	a
nigiri-suši	nigiriuše	k1gFnSc3	nigiri-suše
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
edská	edský	k2eAgFnSc1d1	edský
(	(	kIx(	(
<g/>
tokijská	tokijský	k2eAgFnSc1d1	Tokijská
<g/>
)	)	kIx)	)
specialita	specialita	k1gFnSc1	specialita
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
do	do	k7c2	do
zbytku	zbytek	k1gInSc2	zbytek
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Japonců	Japonec	k1gMnPc2	Japonec
také	také	k6eAd1	také
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
se	se	k3xPyFc4	se
suši	suš	k1gFnSc3	suš
dostalo	dostat	k5eAaPmAgNnS	dostat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
USA	USA	kA	USA
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
japonská	japonský	k2eAgFnSc1d1	japonská
restaurace	restaurace	k1gFnSc1	restaurace
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
r.	r.	kA	r.
1883	[number]	k4	1883
v	v	k7c6	v
San	San	k1gFnSc6	San
Francisku	Francisek	k1gInSc2	Francisek
<g/>
.	.	kIx.	.
</s>
<s>
Suši	suš	k1gFnSc6	suš
si	se	k3xPyFc3	se
brzy	brzy	k6eAd1	brzy
získalo	získat	k5eAaPmAgNnS	získat
oblibu	obliba	k1gFnSc4	obliba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavní	hlavní	k2eAgInSc1d1	hlavní
boom	boom	k1gInSc1	boom
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k6eAd1	až
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
rolka	rolka	k1gFnSc1	rolka
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
:	:	kIx,	:
drahý	drahý	k2eAgMnSc1d1	drahý
tuňák	tuňák	k1gMnSc1	tuňák
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
vzbuzující	vzbuzující	k2eAgFnPc1d1	vzbuzující
obavy	obava	k1gFnPc1	obava
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
syrovému	syrový	k2eAgInSc3d1	syrový
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgMnS	nahradit
avokádem	avokádo	k1gNnSc7	avokádo
a	a	k8xC	a
ingredience	ingredience	k1gFnSc1	ingredience
byly	být	k5eAaImAgFnP	být
obaleny	obalit	k5eAaPmNgFnP	obalit
rýží	rýže	k1gFnSc7	rýže
pokrytou	pokrytý	k2eAgFnSc7d1	pokrytá
nikoliv	nikoliv	k9	nikoliv
řasou	řasa	k1gFnSc7	řasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sezamovými	sezamový	k2eAgNnPc7d1	sezamové
semínky	semínko	k1gNnPc7	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
rolka	rolka	k1gFnSc1	rolka
se	se	k3xPyFc4	se
však	však	k9	však
stala	stát	k5eAaPmAgFnS	stát
populární	populární	k2eAgFnSc1d1	populární
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
včetně	včetně	k7c2	včetně
samotného	samotný	k2eAgNnSc2d1	samotné
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Suši	suš	k1gFnSc3	suš
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
oblíbeným	oblíbený	k2eAgInPc3d1	oblíbený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
netradičnost	netradičnost	k1gFnSc4	netradičnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
výživovou	výživový	k2eAgFnSc4d1	výživová
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Suši	suš	k1gFnSc3	suš
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
nišiki	nišik	k1gFnSc2	nišik
(	(	kIx(	(
<g/>
錦	錦	k?	錦
<g/>
,	,	kIx,	,
山	山	k?	山
Jamada	Jamada	k1gFnSc1	Jamada
nišiki	nišik	k1gFnSc2	nišik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uvaří	uvařit	k5eAaPmIp3nS	uvařit
a	a	k8xC	a
ještě	ještě	k6eAd1	ještě
teplá	teplý	k2eAgFnSc1d1	teplá
se	se	k3xPyFc4	se
smíchá	smíchat	k5eAaPmIp3nS	smíchat
s	s	k7c7	s
omáčkou	omáčka	k1gFnSc7	omáčka
sušinoko	sušinoko	k6eAd1	sušinoko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vychladnutí	vychladnutí	k1gNnSc6	vychladnutí
této	tento	k3xDgFnSc2	tento
základní	základní	k2eAgFnSc2d1	základní
ingredience	ingredience	k1gFnSc2	ingredience
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
přísada	přísada	k1gFnSc1	přísada
suši	suš	k1gFnSc3	suš
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
syrové	syrový	k2eAgFnPc1d1	syrová
ryby	ryba	k1gFnPc1	ryba
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
kultuře	kultura	k1gFnSc6	kultura
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
konzumovat	konzumovat	k5eAaBmF	konzumovat
ryby	ryba	k1gFnPc4	ryba
převážně	převážně	k6eAd1	převážně
tepelně	tepelně	k6eAd1	tepelně
upravované	upravovaný	k2eAgFnPc1d1	upravovaná
<g/>
,	,	kIx,	,
syrové	syrový	k2eAgFnPc1d1	syrová
ryby	ryba	k1gFnPc1	ryba
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
prospěšnější	prospěšný	k2eAgMnSc1d2	prospěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ryb	ryba	k1gFnPc2	ryba
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
suši	suš	k1gFnSc3	suš
používají	používat	k5eAaImIp3nP	používat
také	také	k9	také
krevety	kreveta	k1gFnPc1	kreveta
<g/>
,	,	kIx,	,
chobotnice	chobotnice	k1gFnPc1	chobotnice
<g/>
,	,	kIx,	,
sépie	sépie	k1gFnPc1	sépie
<g/>
,	,	kIx,	,
kaviár	kaviár	k1gInSc1	kaviár
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnPc1d1	mořská
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
<g/>
,	,	kIx,	,
avokádo	avokádo	k1gNnSc1	avokádo
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Suši	suš	k1gFnSc3	suš
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
zdrojem	zdroj	k1gInSc7	zdroj
mnohých	mnohé	k1gNnPc2	mnohé
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
tělo	tělo	k1gNnSc4	tělo
důležitých	důležitý	k2eAgFnPc2d1	důležitá
látek	látka	k1gFnPc2	látka
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
-	-	kIx~	-
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
jódu	jód	k1gInSc2	jód
a	a	k8xC	a
minerálů	minerál	k1gInPc2	minerál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k1gNnSc1	málo
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
kalorií	kalorie	k1gFnPc2	kalorie
<g/>
,	,	kIx,	,
hodí	hodit	k5eAaPmIp3nS	hodit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
i	i	k9	i
pro	pro	k7c4	pro
dietnější	dietní	k2eAgNnSc4d2	dietnější
stravování	stravování	k1gNnSc4	stravování
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Japonci	Japonec	k1gMnPc1	Japonec
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
že	že	k8xS	že
i	i	k9	i
konzumace	konzumace	k1gFnSc1	konzumace
kvalitního	kvalitní	k2eAgNnSc2d1	kvalitní
jídla	jídlo	k1gNnSc2	jídlo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
suši	suš	k1gFnSc6	suš
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
svůj	svůj	k3xOyFgInSc4	svůj
podíl	podíl	k1gInSc4	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
suši	suš	k1gFnSc3	suš
maki	mak	k1gFnSc2	mak
(	(	kIx(	(
<g/>
巻	巻	k?	巻
<g/>
,	,	kIx,	,
rolované	rolovaný	k2eAgNnSc4d1	rolované
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
suši	suš	k1gFnSc3	suš
temaki	temak	k1gFnSc2	temak
(	(	kIx(	(
<g/>
手	手	k?	手
<g/>
,	,	kIx,	,
ručně	ručně	k6eAd1	ručně
rolované	rolovaný	k2eAgFnPc1d1	rolovaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nanáší	nanášet	k5eAaImIp3nS	nanášet
se	se	k3xPyFc4	se
na	na	k7c4	na
plátky	plátek	k1gInPc4	plátek
mořské	mořský	k2eAgFnSc2d1	mořská
řasy	řasa	k1gFnSc2	řasa
nori	nor	k1gFnSc2	nor
(	(	kIx(	(
<g/>
海	海	k?	海
<g/>
)	)	kIx)	)
hotová	hotový	k2eAgFnSc1d1	hotová
rýže	rýže	k1gFnSc1	rýže
nišiki	nišik	k1gFnSc2	nišik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrstvu	vrstva	k1gFnSc4	vrstva
rýže	rýže	k1gFnSc2	rýže
se	se	k3xPyFc4	se
nanese	nanést	k5eAaPmIp3nS	nanést
velmi	velmi	k6eAd1	velmi
tenká	tenký	k2eAgFnSc1d1	tenká
vrstvička	vrstvička	k1gFnSc1	vrstvička
pasty	pasta	k1gFnSc2	pasta
z	z	k7c2	z
křenu	křen	k1gInSc2	křen
wasabi	wasab	k1gFnSc2	wasab
(	(	kIx(	(
<g/>
山	山	k?	山
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
polotovar	polotovar	k1gInSc4	polotovar
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vkládá	vkládat	k5eAaImIp3nS	vkládat
zelenina	zelenina	k1gFnSc1	zelenina
(	(	kIx(	(
<g/>
okurka	okurka	k1gFnSc1	okurka
<g/>
,	,	kIx,	,
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avokádo	avokádo	k1gNnSc1	avokádo
<g/>
,	,	kIx,	,
na	na	k7c4	na
kousky	kousek	k1gInPc4	kousek
nakrájený	nakrájený	k2eAgInSc4d1	nakrájený
syrový	syrový	k2eAgInSc4d1	syrový
<g/>
,	,	kIx,	,
či	či	k8xC	či
uzený	uzený	k2eAgMnSc1d1	uzený
losos	losos	k1gMnSc1	losos
<g/>
,	,	kIx,	,
míchaná	míchaný	k2eAgNnPc4d1	míchané
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kaviár	kaviár	k1gInSc1	kaviár
<g/>
.	.	kIx.	.
</s>
<s>
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
vrstva	vrstva	k1gFnSc1	vrstva
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
sroluje	srolovat	k5eAaPmIp3nS	srolovat
<g/>
,	,	kIx,	,
vychladí	vychladit	k5eAaPmIp3nS	vychladit
a	a	k8xC	a
nakrájí	nakrájet	k5eAaPmIp3nS	nakrájet
na	na	k7c4	na
2,5	[number]	k4	2,5
cm	cm	kA	cm
silná	silný	k2eAgNnPc4d1	silné
kolečka	kolečko	k1gNnPc4	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
spousta	spousta	k1gFnSc1	spousta
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
suši	suš	k1gFnSc3	suš
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Nigirizuši	Nigirizuch	k1gMnPc1	Nigirizuch
(	(	kIx(	(
<g/>
握	握	k?	握
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
tvoří	tvořit	k5eAaImIp3nP	tvořit
slovo	slovo	k1gNnSc4	slovo
suši	suš	k1gFnSc3	suš
příponu	přípona	k1gFnSc4	přípona
za	za	k7c7	za
jiným	jiný	k2eAgNnSc7d1	jiné
slovem	slovo	k1gNnSc7	slovo
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
se	s	k7c7	s
z	z	k7c2	z
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
váleček	váleček	k1gInSc4	váleček
ze	z	k7c2	z
suši	suš	k1gFnSc6	suš
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
potřený	potřený	k2eAgMnSc1d1	potřený
trochou	trocha	k1gFnSc7	trocha
wasabi	wasab	k1gFnSc2	wasab
(	(	kIx(	(
<g/>
namida	namida	k1gFnSc1	namida
な	な	k?	な
<g/>
)	)	kIx)	)
a	a	k8xC	a
obložený	obložený	k2eAgInSc1d1	obložený
kouskem	kousek	k1gInSc7	kousek
omelety	omeleta	k1gFnSc2	omeleta
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnSc2	ryba
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c4	pod
kategorii	kategorie	k1gFnSc4	kategorie
nigiri	nigir	k1gFnSc2	nigir
(	(	kIx(	(
<g/>
握	握	k?	握
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
gunkan	gunkan	k1gInSc4	gunkan
-	-	kIx~	-
maki-zuši	makiuše	k1gFnSc4	maki-zuše
(	(	kIx(	(
<g/>
軍	軍	k?	軍
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
proužek	proužek	k1gInSc1	proužek
nori	nor	k1gFnSc2	nor
(	(	kIx(	(
<g/>
海	海	k?	海
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obložený	obložený	k2eAgInSc1d1	obložený
rýží	rýže	k1gFnSc7	rýže
a	a	k8xC	a
sypkou	sypký	k2eAgFnSc7d1	sypká
přísadou	přísada	k1gFnSc7	přísada
(	(	kIx(	(
<g/>
ježovka	ježovka	k1gFnSc1	ježovka
<g/>
,	,	kIx,	,
kaviár	kaviár	k1gInSc1	kaviár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ručně	ručně	k6eAd1	ručně
smotaný	smotaný	k2eAgInSc1d1	smotaný
do	do	k7c2	do
válečku	váleček	k1gInSc2	váleček
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
druhy	druh	k1gInPc4	druh
podobné	podobný	k2eAgInPc4d1	podobný
nigiri-zuši	nigiriuše	k1gFnSc4	nigiri-zuše
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
temari-zuši	temariuše	k1gFnSc4	temari-zuše
(	(	kIx(	(
<g/>
手	手	k?	手
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
principu	princip	k1gInSc6	princip
jako	jako	k8xC	jako
nigiri	nigiri	k6eAd1	nigiri
samotné	samotný	k2eAgNnSc1d1	samotné
<g/>
,	,	kIx,	,
jenom	jenom	k9	jenom
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
rýže	rýže	k1gFnSc2	rýže
se	se	k3xPyFc4	se
formuje	formovat	k5eAaImIp3nS	formovat
koule	koule	k1gFnSc2	koule
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
druhy	druh	k1gInPc4	druh
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
sašimi	saši	k1gFnPc7	saši
(	(	kIx(	(
<g/>
刺	刺	k?	刺
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pokrm	pokrm	k1gInSc4	pokrm
ze	z	k7c2	z
syrových	syrový	k2eAgFnPc2d1	syrová
ryb	ryba	k1gFnPc2	ryba
naaranžovaných	naaranžovaný	k2eAgFnPc2d1	naaranžovaná
na	na	k7c6	na
talíři	talíř	k1gInSc6	talíř
<g/>
,	,	kIx,	,
čirashi-zuši	čirashiuše	k1gFnSc6	čirashi-zuše
(	(	kIx(	(
<g/>
散	散	k?	散
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
vrstvy	vrstva	k1gFnSc2	vrstva
okyselené	okyselený	k2eAgFnSc2d1	okyselená
rýže	rýže	k1gFnSc2	rýže
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
ryby	ryba	k1gFnPc1	ryba
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
vejce	vejce	k1gNnSc1	vejce
aj.	aj.	kA	aj.
Mezi	mezi	k7c7	mezi
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
méně	málo	k6eAd2	málo
známé	známý	k2eAgInPc1d1	známý
druhy	druh	k1gInPc1	druh
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
inari-zuši	inariuše	k1gFnSc4	inari-zuše
(	(	kIx(	(
<g/>
稲	稲	k?	稲
<g/>
,	,	kIx,	,
náplň	náplň	k1gFnSc1	náplň
zabalena	zabalen	k2eAgFnSc1d1	zabalena
v	v	k7c6	v
taštičce	taštička	k1gFnSc6	taštička
z	z	k7c2	z
tofu	tofu	k1gNnSc2	tofu
豆	豆	k?	豆
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oši-zuši	ošiuše	k1gFnSc4	oši-zuše
(	(	kIx(	(
<g/>
押	押	k?	押
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
po	po	k7c6	po
zformování	zformování	k1gNnSc6	zformování
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
dort	dort	k1gInSc1	dort
<g/>
.	.	kIx.	.
</s>
