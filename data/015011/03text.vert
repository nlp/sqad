<s>
Tereziánský	tereziánský	k2eAgInSc4d1
katastr	katastr	k1gInSc4
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
dvě	dva	k4xCgFnPc4
aktualizace	aktualizace	k1gFnPc4
rustikálního	rustikální	k2eAgInSc2d1
katastru	katastr	k1gInSc2
(	(	kIx(
<g/>
tj.	tj.	kA
soupisu	soupis	k1gInSc2
půdy	půda	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
měli	mít	k5eAaImAgMnP
poddaní	poddaný	k1gMnPc1
v	v	k7c6
dědičném	dědičný	k2eAgInSc6d1
nájmu	nájem	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
došlo	dojít	k5eAaPmAgNnS
za	za	k7c4
panování	panování	k1gNnSc4
Marie	Maria	k1gFnSc2
Terezie	Terezie	k1gFnSc2
v	v	k7c6
letech	let	k1gInPc6
1741	#num#	k4
<g/>
–	–	k?
<g/>
1748	#num#	k4
(	(	kIx(
<g/>
První	první	k4xOgInSc1
tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
rustikální	rustikální	k2eAgInSc1d1
<g/>
,	,	kIx,
též	též	k9
Třetí	třetí	k4xOgFnSc1
berní	berní	k2eAgFnSc1d1
rula	rula	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
1748	#num#	k4
<g/>
–	–	k?
<g/>
1757	#num#	k4
(	(	kIx(
<g/>
Druhý	druhý	k4xOgInSc1
tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
rustikální	rustikální	k2eAgInSc1d1
<g/>
,	,	kIx,
též	též	k9
Čtvrtá	čtvrtý	k4xOgFnSc1
berní	berní	k2eAgFnSc1d1
rula	rula	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
tímto	tento	k3xDgInSc7
názvem	název	k1gInSc7
byl	být	k5eAaImAgInS
užíván	užívat	k5eAaImNgInS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
katastr	katastr	k1gInSc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
je	být	k5eAaImIp3nS
znám	znám	k2eAgInSc1d1
pod	pod	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Rektifikační	rektifikační	k2eAgInSc1d1
akta	akta	k1gNnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vytváření	vytváření	k1gNnSc3
aktualizací	aktualizace	k1gFnPc2
původní	původní	k2eAgFnSc7d1
berní	berní	k1gFnSc7
ruly	rula	k1gFnSc2
vedly	vést	k5eAaImAgFnP
především	především	k9
dvě	dva	k4xCgFnPc1
okolnosti	okolnost	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
Berní	berní	k2eAgFnSc1d1
rula	rula	k1gFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
za	za	k7c4
pouhé	pouhý	k2eAgInPc4d1
tři	tři	k4xCgInPc4
roky	rok	k1gInPc4
(	(	kIx(
<g/>
1653	#num#	k4
<g/>
–	–	k?
<g/>
1656	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobilo	způsobit	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
dostala	dostat	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
chyb	chyba	k1gFnPc2
a	a	k8xC
nepřesností	nepřesnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
nedostatkem	nedostatek	k1gInSc7
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
nebrala	brát	k5eNaImAgFnS
v	v	k7c4
potaz	potaz	k1gInSc4
rozdílnou	rozdílný	k2eAgFnSc4d1
bonitu	bonita	k1gFnSc4
(	(	kIx(
<g/>
úrodnost	úrodnost	k1gFnSc4
<g/>
)	)	kIx)
půdy	půda	k1gFnSc2
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Berní	berní	k2eAgFnSc1d1
rula	rula	k1gFnSc1
zachycovala	zachycovat	k5eAaImAgFnS
pouze	pouze	k6eAd1
rustikální	rustikální	k2eAgFnSc4d1
(	(	kIx(
<g/>
poddanskou	poddanský	k2eAgFnSc4d1
<g/>
)	)	kIx)
půdu	půda	k1gFnSc4
<g/>
,	,	kIx,
nikoli	nikoli	k9
půdu	půda	k1gFnSc4
dominikální	dominikální	k2eAgFnSc4d1
(	(	kIx(
<g/>
panskou	panská	k1gFnSc4
<g/>
)	)	kIx)
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc4
tereziánský	tereziánský	k2eAgInSc4d1
katastr	katastr	k1gInSc4
z	z	k7c2
r.	r.	kA
1748	#num#	k4
zachycoval	zachycovat	k5eAaImAgInS
pouze	pouze	k6eAd1
rustikální	rustikální	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
a	a	k8xC
v	v	k7c6
otázce	otázka	k1gFnSc6
bonity	bonita	k1gFnSc2
odstranil	odstranit	k5eAaPmAgInS
pouze	pouze	k6eAd1
některé	některý	k3yIgInPc4
nejvýraznější	výrazný	k2eAgInPc4d3
nedostatky	nedostatek	k1gInPc4
berní	berní	k2eAgFnSc2d1
ruly	rula	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
druhý	druhý	k4xOgInSc4
tereziánský	tereziánský	k2eAgInSc4d1
katastr	katastr	k1gInSc4
z	z	k7c2
r.	r.	kA
1757	#num#	k4
zahrnul	zahrnout	k5eAaPmAgInS
jak	jak	k6eAd1
rustikální	rustikální	k2eAgInSc1d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
dominikální	dominikální	k2eAgFnSc4d1
půdu	půda	k1gFnSc4
a	a	k8xC
kvalitně	kvalitně	k6eAd1
zachytil	zachytit	k5eAaPmAgMnS
bonitu	bonita	k1gFnSc4
půdy	půda	k1gFnSc2
(	(	kIx(
<g/>
rozdělil	rozdělit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
do	do	k7c2
osmi	osm	k4xCc2
kategorií	kategorie	k1gFnPc2
podle	podle	k7c2
úrodnosti	úrodnost	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
dokonce	dokonce	k9
více	hodně	k6eAd2
než	než	k8xS
dnešní	dnešní	k2eAgInPc1d1
katastry	katastr	k1gInPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
tvorbě	tvorba	k1gFnSc6
katastru	katastr	k1gInSc2
docházelo	docházet	k5eAaImAgNnS
ojediněle	ojediněle	k6eAd1
i	i	k9
ke	k	k7c3
tvorbě	tvorba	k1gFnSc3
map	mapa	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
ale	ale	k9
nebyly	být	k5eNaImAgFnP
zpracovány	zpracovat	k5eAaPmNgFnP
podle	podle	k7c2
jednotného	jednotný	k2eAgInSc2d1
stylu	styl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Uložení	uložení	k1gNnSc1
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
se	se	k3xPyFc4
spolu	spolu	k6eAd1
s	s	k7c7
ostatní	ostatní	k2eAgFnSc7d1
pomocnou	pomocný	k2eAgFnSc7d1
dokumentací	dokumentace	k1gFnSc7
nachází	nacházet	k5eAaImIp3nS
pro	pro	k7c4
Čechy	Čech	k1gMnPc4
v	v	k7c6
Národním	národní	k2eAgInSc6d1
archivu	archiv	k1gInSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
v	v	k7c6
Moravském	moravský	k2eAgInSc6d1
zemském	zemský	k2eAgInSc6d1
archivu	archiv	k1gInSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumáře	sumář	k1gInSc2
byly	být	k5eAaImAgInP
vydány	vydat	k5eAaPmNgInP
v	v	k7c6
tištěné	tištěný	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
a	a	k8xC
edice	edice	k1gFnSc1
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
český	český	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
1	#num#	k4
<g/>
,	,	kIx,
Rustikál	rustikál	k1gInSc1
(	(	kIx(
<g/>
kraje	kraj	k1gInPc1
A-CH	A-CH	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Edd	Edda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleš	Aleš	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Archivní	archivní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
.	.	kIx.
323	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Edice	edice	k1gFnSc1
berních	berní	k2eAgInPc2d1
katastrů	katastr	k1gInPc2
českých	český	k2eAgInPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgInPc2d1
<g/>
,	,	kIx,
slezských	slezský	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
český	český	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
2	#num#	k4
<g/>
,	,	kIx,
Rustikál	rustikál	k1gInSc1
(	(	kIx(
<g/>
kraje	kraj	k1gInPc1
K-Ž	K-Ž	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sumář	sumář	k1gInSc4
a	a	k8xC
rejstřík	rejstřík	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Edd	Edda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aleš	Aleš	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Archivní	archivní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ČR	ČR	kA
<g/>
,	,	kIx,
1966	#num#	k4
<g/>
.	.	kIx.
524	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Edice	edice	k1gFnSc1
berních	berní	k2eAgInPc2d1
katastrů	katastr	k1gInPc2
českých	český	k2eAgInPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgInPc2d1
<g/>
,	,	kIx,
slezských	slezský	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
český	český	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
3	#num#	k4
<g/>
,	,	kIx,
Dominikál	dominikál	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Edd	Edda	k1gFnPc2
Aleš	Aleš	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Archivní	archivní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
1970	#num#	k4
<g/>
.	.	kIx.
653	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Edice	edice	k1gFnSc1
berních	berní	k2eAgInPc2d1
katastrů	katastr	k1gInPc2
českých	český	k2eAgInPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgInPc2d1
<g/>
,	,	kIx,
slezských	slezský	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
moravský	moravský	k2eAgInSc1d1
(	(	kIx(
<g/>
prameny	pramen	k1gInPc4
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
hosp	hosp	k1gInSc1
<g/>
.	.	kIx.
dějinám	dějiny	k1gFnPc3
Moravy	Morava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Edd	Edda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Radimský	Radimský	k2eAgMnSc1d1
a	a	k8xC
Miroslav	Miroslav	k1gMnSc1
Trantírek	Trantírka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Archivní	archivní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
ČSR	ČSR	kA
<g/>
,	,	kIx,
1962	#num#	k4
<g/>
.	.	kIx.
414	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
(	(	kIx(
<g/>
Edice	edice	k1gFnSc1
berních	berní	k2eAgInPc2d1
katastrů	katastr	k1gInPc2
českých	český	k2eAgInPc2d1
<g/>
,	,	kIx,
moravských	moravský	k2eAgInPc2d1
<g/>
,	,	kIx,
slezských	slezský	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sv.	sv.	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
HRADECKÝ	Hradecký	k1gMnSc1
<g/>
,	,	kIx,
Emil	Emil	k1gMnSc1
<g/>
:	:	kIx,
Tereziánský	tereziánský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
(	(	kIx(
<g/>
rozbor	rozbor	k1gInSc1
fondu	fond	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sborník	sborník	k1gInSc1
archivních	archivní	k2eAgFnPc2d1
prací	práce	k1gFnPc2
VI	VI	kA
<g/>
/	/	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
105	#num#	k4
-	-	kIx~
135	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
CHALUPA	Chalupa	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
:	:	kIx,
Venkovské	venkovský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
v	v	k7c6
tereziánských	tereziánský	k2eAgInPc6d1
katastrech	katastr	k1gInPc6
(	(	kIx(
<g/>
1700	#num#	k4
<g/>
-	-	kIx~
<g/>
1750	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sborník	sborník	k1gInSc1
Národního	národní	k2eAgInSc2d1
musea-A	musea-A	k?
1969	#num#	k4
</s>
<s>
PEKAŘ	Pekař	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
:	:	kIx,
České	český	k2eAgInPc1d1
katastry	katastr	k1gInPc1
1654	#num#	k4
-	-	kIx~
1789	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Historický	historický	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
1932	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
DRÁPELA	Drápela	k1gMnSc1
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
Václav	Václav	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
TEREZIÁNSKÝ	tereziánský	k2eAgInSc1d1
KATASTR	katastr	k1gInSc1
-	-	kIx~
TŘETÍ	třetí	k4xOgFnSc1
A	a	k8xC
ČTVRTÁ	čtvrtý	k4xOgFnSc1
BERNÍ	berní	k2eAgFnSc1d1
RULA	rula	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
:	:	kIx,
Geografický	geografický	k2eAgInSc1d1
ústav	ústav	k1gInSc1
PřF	PřF	k1gFnSc2
MU	MU	kA
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2005	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
WWW	WWW	kA
<g/>
:	:	kIx,
<	<	kIx(
<g/>
https://web.archive.org/web/20080316094058/http://www.geogr.muni.cz/ucebnice/dejiny/obsah.php?show=103	https://web.archive.org/web/20080316094058/http://www.geogr.muni.cz/ucebnice/dejiny/obsah.php?show=103	k4
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s>
ADAMEC	Adamec	k1gMnSc1
<g/>
,	,	kIx,
Radek	Radek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
vysvětlení	vysvětlení	k1gNnSc4
termínů	termín	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
mě	já	k3xPp1nSc4
zpočátku	zpočátku	k6eAd1
mátly	mást	k5eAaImAgFnP
<g/>
,	,	kIx,
tak	tak	k6eAd1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
chápu	chápat	k5eAaImIp1nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2000	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
WWW	WWW	kA
<g/>
:	:	kIx,
<	<	kIx(
<g/>
http://www.genebaze.cz/dv/term/RA_terminy.html	http://www.genebaze.cz/dv/term/RA_terminy.html	k1gInSc1
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s>
NOVÁK	Novák	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
Aba	Aba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčina	příčina	k1gFnSc1
a	a	k8xC
podmínky	podmínka	k1gFnPc1
podrobného	podrobný	k2eAgNnSc2d1
mapování	mapování	k1gNnSc2
polohopisného	polohopisný	k2eAgNnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupný	dostupný	k2eAgInSc1d1
z	z	k7c2
WWW	WWW	kA
<g/>
:	:	kIx,
<	<	kIx(
<g/>
http://hismap.wz.cz/pricina_podminky_mapovani.php	http://hismap.wz.cz/pricina_podminky_mapovani.php	k1gInSc1
<g/>
>	>	kIx)
<g/>
.	.	kIx.
</s>
<s>
VÝVOJ	vývoj	k1gInSc1
KATASTRÁLNÍCH	katastrální	k2eAgFnPc2d1
EVIDENCÍ	evidence	k1gFnPc2
NA	na	k7c4
ÚZEMÍ	území	k1gNnSc4
ČESKÉ	český	k2eAgFnSc2d1
REPUBLIKY	republika	k1gFnSc2
ING.	ing.	kA
LUMÍR	Lumír	k1gMnSc1
NEDVÍDEK	NEDVÍDEK	kA
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2010	#num#	k4
(	(	kIx(
<g/>
video	video	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Katastrální	katastrální	k2eAgInPc1d1
operáty	operát	k1gInPc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Berní	berní	k1gFnSc1
rula	rout	k5eAaImAgFnS,k5eAaPmAgFnS
-	-	kIx~
Čechy	Čechy	k1gFnPc4
Lánový	lánový	k2eAgInSc1d1
rejstřík	rejstřík	k1gInSc1
-	-	kIx~
Morava	Morava	k1gFnSc1
Karolínský	karolínský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
–	–	k?
Slezsko	Slezsko	k1gNnSc1
</s>
<s>
1741	#num#	k4
<g/>
–	–	k?
<g/>
1757	#num#	k4
Tereziánský	tereziánský	k2eAgInSc4d1
katastr	katastr	k1gInSc4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Josefinský	Josefinský	k2eAgInSc1d1
katastr	katastr	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Ekonomie	ekonomie	k1gFnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Novověk	novověk	k1gInSc1
</s>
