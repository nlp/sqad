<s>
George	Georg	k1gMnSc2	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Noel	Noel	k1gMnSc1	Noel
Byron	Byron	k1gMnSc1	Byron
<g/>
,	,	kIx,	,
lord	lord	k1gMnSc1	lord
Byron	Byron	k1gMnSc1	Byron
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1788	[number]	k4	1788
Londýn	Londýn	k1gInSc1	Londýn
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1824	[number]	k4	1824
Messolonghi	Messolonghi	k1gNnPc2	Messolonghi
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
filhelén	filhelén	k1gMnSc1	filhelén
a	a	k8xC	a
přední	přední	k2eAgMnSc1d1	přední
představitel	představitel	k1gMnSc1	představitel
romantismu	romantismus	k1gInSc2	romantismus
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
Childe	Child	k1gInSc5	Child
Haroldova	Haroldův	k2eAgFnSc1d1	Haroldova
pouť	pouť	k1gFnSc1	pouť
(	(	kIx(	(
<g/>
popsáno	popsat	k5eAaPmNgNnS	popsat
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
a	a	k8xC	a
satirický	satirický	k2eAgInSc4d1	satirický
veršovaný	veršovaný	k2eAgInSc4d1	veršovaný
román	román	k1gInSc4	román
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedokončený	dokončený	k2eNgMnSc1d1	nedokončený
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
evropských	evropský	k2eAgMnPc2d1	evropský
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Byron	Byron	k1gMnSc1	Byron
proslul	proslout	k5eAaPmAgMnS	proslout
nejen	nejen	k6eAd1	nejen
svým	svůj	k3xOyFgInSc7	svůj
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
extravagantní	extravagantní	k2eAgInPc4d1	extravagantní
výstřelky	výstřelek	k1gInPc4	výstřelek
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
milostných	milostný	k2eAgInPc2d1	milostný
skandálů	skandál	k1gInPc2	skandál
<g/>
,	,	kIx,	,
dluhy	dluh	k1gInPc4	dluh
<g/>
,	,	kIx,	,
rozvod	rozvod	k1gInSc4	rozvod
a	a	k8xC	a
obvinění	obvinění	k1gNnSc4	obvinění
z	z	k7c2	z
incestu	incest	k1gInSc2	incest
a	a	k8xC	a
sodomie	sodomie	k1gFnSc2	sodomie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
bývalá	bývalý	k2eAgFnSc1d1	bývalá
milenka	milenka	k1gFnSc1	milenka
lady	lady	k1gFnSc2	lady
Caroline	Carolin	k1gInSc5	Carolin
Lambová	Lambová	k1gFnSc1	Lambová
o	o	k7c6	o
Byronovi	Byron	k1gMnSc6	Byron
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
šílený	šílený	k2eAgMnSc1d1	šílený
<g/>
,	,	kIx,	,
zlovolný	zlovolný	k2eAgMnSc1d1	zlovolný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
ho	on	k3xPp3gInSc4	on
znát	znát	k5eAaImF	znát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byron	Byron	k1gMnSc1	Byron
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc1	vůdce
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
skupin	skupina	k1gFnPc2	skupina
italských	italský	k2eAgMnPc2d1	italský
karbonářů	karbonář	k1gMnPc2	karbonář
během	během	k7c2	během
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
bojů	boj	k1gInPc2	boj
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
během	během	k7c2	během
řecké	řecký	k2eAgFnSc2d1	řecká
války	válka	k1gFnSc2	válka
o	o	k7c4	o
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
pročež	pročež	k6eAd1	pročež
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
řeckého	řecký	k2eAgMnSc4d1	řecký
národního	národní	k2eAgMnSc4d1	národní
hrdinu	hrdina	k1gMnSc4	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
hojně	hojně	k6eAd1	hojně
přispíval	přispívat	k5eAaImAgMnS	přispívat
svými	svůj	k3xOyFgFnPc7	svůj
financemi	finance	k1gFnPc7	finance
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podpořil	podpořit	k5eAaPmAgMnS	podpořit
hnutí	hnutí	k1gNnSc4	hnutí
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
turecké	turecký	k2eAgFnSc3d1	turecká
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc7	svůj
obětí	oběť	k1gFnSc7	oběť
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
Řecku	Řecko	k1gNnSc3	Řecko
získat	získat	k5eAaPmF	získat
po	po	k7c6	po
staletém	staletý	k2eAgNnSc6d1	staleté
područenství	područenství	k1gNnSc6	područenství
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
znovunabýt	znovunabýt	k5eAaPmF	znovunabýt
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Messolonghi	Messolongh	k1gFnSc6	Messolongh
na	na	k7c6	na
řecké	řecký	k2eAgFnSc6d1	řecká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
skotského	skotský	k2eAgInSc2d1	skotský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
George	Georg	k1gMnSc2	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Byron	Byron	k1gMnSc1	Byron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
zdědil	zdědit	k5eAaPmAgInS	zdědit
titul	titul	k1gInSc4	titul
anglického	anglický	k2eAgMnSc2d1	anglický
barona	baron	k1gMnSc2	baron
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
baronem	baron	k1gMnSc7	baron
Byronem	Byron	k1gMnSc7	Byron
z	z	k7c2	z
Rochdale	Rochdala	k1gFnSc3	Rochdala
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
poslední	poslední	k2eAgFnSc2d1	poslední
vůle	vůle	k1gFnSc2	vůle
své	svůj	k3xOyFgFnSc2	svůj
zemřelé	zemřelý	k2eAgFnSc2d1	zemřelá
tchyně	tchyně	k1gFnSc2	tchyně
lady	lady	k1gFnSc2	lady
Milbanke	Milbanke	k1gFnSc1	Milbanke
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
George	Georg	k1gMnPc4	Georg
Gordon	Gordon	k1gMnSc1	Gordon
Noel	Noel	k1gMnSc1	Noel
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
baron	baron	k1gMnSc1	baron
Byron	Byron	k1gMnSc1	Byron
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
Ada	Ada	kA	Ada
Lovelace	Lovelace	k1gFnPc4	Lovelace
byla	být	k5eAaImAgFnS	být
matematička	matematička	k1gFnSc1	matematička
a	a	k8xC	a
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
Charlesem	Charles	k1gMnSc7	Charles
Babbagem	Babbag	k1gMnSc7	Babbag
na	na	k7c6	na
sestrojení	sestrojení	k1gNnSc6	sestrojení
analytického	analytický	k2eAgInSc2d1	analytický
stroje	stroj	k1gInSc2	stroj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
předchůdce	předchůdce	k1gMnSc1	předchůdce
dnešních	dnešní	k2eAgMnPc2d1	dnešní
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
Byronův	Byronův	k2eAgInSc1d1	Byronův
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
nemocí	nemoc	k1gFnSc7	nemoc
nohy	noha	k1gFnSc2	noha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ho	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
trápila	trápit	k5eAaImAgNnP	trápit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
chůva	chůva	k1gFnSc1	chůva
jej	on	k3xPp3gMnSc4	on
jako	jako	k9	jako
malého	malý	k1gMnSc4	malý
chlapce	chlapec	k1gMnSc4	chlapec
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Rád	rád	k6eAd1	rád
četl	číst	k5eAaImAgMnS	číst
historické	historický	k2eAgNnSc4d1	historické
a	a	k8xC	a
rytířské	rytířský	k2eAgInPc1d1	rytířský
romány	román	k1gInPc1	román
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
zřejmě	zřejmě	k6eAd1	zřejmě
pocházejí	pocházet	k5eAaImIp3nP	pocházet
rytířské	rytířský	k2eAgInPc1d1	rytířský
ideály	ideál	k1gInPc1	ideál
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
básníka	básník	k1gMnSc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
třináctého	třináctý	k4xOgInSc2	třináctý
roku	rok	k1gInSc2	rok
zdědil	zdědit	k5eAaPmAgMnS	zdědit
po	po	k7c6	po
prastrýci	prastrýc	k1gMnSc6	prastrýc
titul	titul	k1gInSc4	titul
barona	baron	k1gMnSc2	baron
a	a	k8xC	a
pozemky	pozemek	k1gInPc7	pozemek
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Newstadského	Newstadský	k2eAgNnSc2d1	Newstadský
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Starost	starost	k1gFnSc1	starost
nad	nad	k7c7	nad
ním	on	k3xPp3gNnSc7	on
převzala	převzít	k5eAaPmAgFnS	převzít
sestra	sestra	k1gFnSc1	sestra
jeho	jeho	k3xOp3gFnSc2	jeho
původní	původní	k2eAgFnSc2d1	původní
chůvy	chůva	k1gFnSc2	chůva
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
chůva	chůva	k1gFnSc1	chůva
byla	být	k5eAaImAgFnS	být
původkyní	původkyně	k1gFnSc7	původkyně
jeho	jeho	k3xOp3gFnSc2	jeho
předčasné	předčasný	k2eAgFnSc2d1	předčasná
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
iniciace	iniciace	k1gFnSc2	iniciace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
domu	dům	k1gInSc2	dům
vodila	vodit	k5eAaImAgFnS	vodit
různé	různý	k2eAgMnPc4d1	různý
hosty	host	k1gMnPc4	host
z	z	k7c2	z
okolních	okolní	k2eAgFnPc2d1	okolní
krčem	krčma	k1gFnPc2	krčma
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pramení	pramenit	k5eAaImIp3nS	pramenit
některé	některý	k3yIgFnPc4	některý
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
získal	získat	k5eAaPmAgMnS	získat
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
pokryteckému	pokrytecký	k2eAgInSc3d1	pokrytecký
náboženskému	náboženský	k2eAgInSc3d1	náboženský
moralismu	moralismus	k1gInSc3	moralismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
prestižní	prestižní	k2eAgFnSc2d1	prestižní
soukromé	soukromý	k2eAgFnSc2d1	soukromá
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Harrow	Harrow	k1gFnSc6	Harrow
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
spolužáky	spolužák	k1gMnPc7	spolužák
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
sportovními	sportovní	k2eAgInPc7d1	sportovní
výkony	výkon	k1gInPc7	výkon
<g/>
,	,	kIx,	,
sečtělostí	sečtělost	k1gFnSc7	sečtělost
a	a	k8xC	a
duševními	duševní	k2eAgFnPc7d1	duševní
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
dívky	dívka	k1gFnSc2	dívka
ze	z	k7c2	z
sousedního	sousední	k2eAgNnSc2d1	sousední
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
naprosto	naprosto	k6eAd1	naprosto
čistá	čistý	k2eAgFnSc1d1	čistá
a	a	k8xC	a
platonická	platonický	k2eAgFnSc1d1	platonická
láska	láska	k1gFnSc1	láska
zůstala	zůstat	k5eAaPmAgFnS	zůstat
nenaplněná	naplněný	k2eNgFnSc1d1	nenaplněná
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
dlouho	dlouho	k6eAd1	dlouho
jej	on	k3xPp3gInSc4	on
neopouštělo	opouštět	k5eNaImAgNnS	opouštět
zklamání	zklamání	k1gNnSc1	zklamání
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
první	první	k4xOgFnSc2	první
velké	velký	k2eAgFnSc2d1	velká
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
poznamenal	poznamenat	k5eAaPmAgInS	poznamenat
střet	střet	k1gInSc1	střet
snů	sen	k1gInPc2	sen
a	a	k8xC	a
představ	představa	k1gFnPc2	představa
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
realitou	realita	k1gFnSc7	realita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
studoval	studovat	k5eAaImAgMnS	studovat
Byron	Byron	k1gInSc4	Byron
na	na	k7c6	na
slavné	slavný	k2eAgFnSc6d1	slavná
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
Trinity	Trinit	k2eAgFnPc4d1	Trinita
College	Colleg	k1gFnPc4	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
našel	najít	k5eAaPmAgMnS	najít
nové	nový	k2eAgMnPc4d1	nový
přátele	přítel	k1gMnPc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pokoji	pokoj	k1gInSc6	pokoj
žil	žít	k5eAaImAgMnS	žít
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
převlečenou	převlečený	k2eAgFnSc7d1	převlečená
za	za	k7c4	za
chlapce	chlapec	k1gMnSc4	chlapec
(	(	kIx(	(
<g/>
inspirace	inspirace	k1gFnSc2	inspirace
pro	pro	k7c4	pro
poemu	poema	k1gFnSc4	poema
Lara	Lar	k1gInSc2	Lar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznámil	seznámit	k5eAaPmAgMnS	seznámit
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
nevlastní	vlastní	k2eNgFnSc7d1	nevlastní
sestrou	sestra	k1gFnSc7	sestra
Augustou	Augusta	k1gMnSc7	Augusta
Mary	Mary	k1gFnSc2	Mary
Leigh	Leigha	k1gFnPc2	Leigha
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
měl	mít	k5eAaImAgMnS	mít
velice	velice	k6eAd1	velice
úzký	úzký	k2eAgInSc4d1	úzký
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Cambridgi	Cambridge	k1gFnSc6	Cambridge
na	na	k7c6	na
koleji	kolej	k1gFnSc6	kolej
Trinity	Trinita	k1gFnSc2	Trinita
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Newsteadského	Newsteadský	k2eAgNnSc2d1	Newsteadský
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
láskou	láska	k1gFnSc7	láska
z	z	k7c2	z
dětství	dětství	k1gNnSc2	dětství
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
vdanou	vdaný	k2eAgFnSc4d1	vdaná
a	a	k8xC	a
zklamanou	zklamaný	k2eAgFnSc4d1	zklamaná
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
ho	on	k3xPp3gMnSc4	on
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
nedrželo	držet	k5eNaImAgNnS	držet
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
zadlužil	zadlužit	k5eAaPmAgMnS	zadlužit
a	a	k8xC	a
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
jeho	jeho	k3xOp3gFnSc1	jeho
zastávkou	zastávka	k1gFnSc7	zastávka
bylo	být	k5eAaImAgNnS	být
portugalské	portugalský	k2eAgNnSc1d1	portugalské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Lisabon	Lisabon	k1gInSc1	Lisabon
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
španělské	španělský	k2eAgFnSc2d1	španělská
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
,	,	kIx,	,
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Maltu	Malta	k1gFnSc4	Malta
<g/>
,	,	kIx,	,
do	do	k7c2	do
Albánie	Albánie	k1gFnSc2	Albánie
a	a	k8xC	a
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
Lamanšským	lamanšský	k2eAgInSc7d1	lamanšský
kanálem	kanál	k1gInSc7	kanál
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
poému	poéma	k1gFnSc4	poéma
Childe	Child	k1gInSc5	Child
Haroldova	Haroldův	k2eAgFnSc1d1	Haroldova
pouť	pouť	k1gFnSc1	pouť
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
po	po	k7c6	po
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
příbuzném	příbuzný	k1gMnSc6	příbuzný
zdědil	zdědit	k5eAaPmAgInS	zdědit
titul	titul	k1gInSc1	titul
baron	baron	k1gMnSc1	baron
Byron	Byron	k1gMnSc1	Byron
z	z	k7c2	z
Rochdale	Rochdala	k1gFnSc3	Rochdala
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
básník	básník	k1gMnSc1	básník
mohl	moct	k5eAaImAgMnS	moct
účastnit	účastnit	k5eAaImF	účastnit
zasedání	zasedání	k1gNnSc4	zasedání
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
několikrát	několikrát	k6eAd1	několikrát
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Hájil	hájit	k5eAaImAgMnS	hájit
práva	právo	k1gNnPc4	právo
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
nahrazována	nahrazován	k2eAgFnSc1d1	nahrazována
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
těmto	tento	k3xDgFnPc3	tento
snahám	snaha	k1gFnPc3	snaha
bouřili	bouřit	k5eAaImAgMnP	bouřit
a	a	k8xC	a
ničili	ničit	k5eAaImAgMnP	ničit
stroje	stroj	k1gInPc4	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
luddité	luddita	k1gMnPc1	luddita
(	(	kIx(	(
<g/>
luddité	luddita	k1gMnPc1	luddita
-	-	kIx~	-
od	od	k7c2	od
muže	muž	k1gMnSc2	muž
jménem	jméno	k1gNnSc7	jméno
Ludd	Ludd	k1gMnSc1	Ludd
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
začal	začít	k5eAaPmAgMnS	začít
stroje	stroj	k1gInPc4	stroj
ničit	ničit	k5eAaImF	ničit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byron	Byron	k1gMnSc1	Byron
také	také	k9	také
bojoval	bojovat	k5eAaImAgMnS	bojovat
proti	proti	k7c3	proti
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
přátele	přítel	k1gMnPc4	přítel
patřil	patřit	k5eAaImAgMnS	patřit
mladý	mladý	k2eAgMnSc1d1	mladý
básník	básník	k1gMnSc1	básník
Percy	Perca	k1gFnSc2	Perca
Bysshe	Byssh	k1gFnSc2	Byssh
Shelley	Shellea	k1gFnSc2	Shellea
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jednu	jeden	k4xCgFnSc4	jeden
dobu	doba	k1gFnSc4	doba
vegetarián	vegetarián	k1gMnSc1	vegetarián
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
zahálky	zahálka	k1gFnPc1	zahálka
-	-	kIx~	-
první	první	k4xOgFnSc1	první
Byronova	Byronův	k2eAgFnSc1d1	Byronova
básnická	básnický	k2eAgFnSc1d1	básnická
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ji	on	k3xPp3gFnSc4	on
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
ještě	ještě	k9	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
plnoletosti	plnoletost	k1gFnSc3	plnoletost
<g/>
.	.	kIx.	.
</s>
<s>
Sklidila	sklidit	k5eAaPmAgFnS	sklidit
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
lichotivou	lichotivý	k2eAgFnSc4d1	lichotivá
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Skotští	skotský	k2eAgMnPc1d1	skotský
bardi	bard	k1gMnPc1	bard
a	a	k8xC	a
angličtí	anglický	k2eAgMnPc1d1	anglický
recenzenti	recenzent	k1gMnPc1	recenzent
(	(	kIx(	(
<g/>
satira	satira	k1gFnSc1	satira
<g/>
)	)	kIx)	)
-	-	kIx~	-
toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
drsnou	drsný	k2eAgFnSc4d1	drsná
kritiku	kritika	k1gFnSc4	kritika
jeho	jeho	k3xOp3gFnSc2	jeho
prvotiny	prvotina	k1gFnSc2	prvotina
<g/>
.	.	kIx.	.
</s>
<s>
Napadá	napadat	k5eAaPmIp3nS	napadat
zde	zde	k6eAd1	zde
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
oné	onen	k3xDgFnSc2	onen
recenze	recenze	k1gFnSc2	recenze
<g/>
.	.	kIx.	.
</s>
<s>
Padla	padnout	k5eAaImAgFnS	padnout
zde	zde	k6eAd1	zde
jména	jméno	k1gNnSc2	jméno
i	i	k8xC	i
mnohých	mnohý	k2eAgMnPc2d1	mnohý
vážených	vážený	k2eAgMnPc2d1	vážený
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
se	se	k3xPyFc4	se
Byron	Byron	k1gMnSc1	Byron
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
<g/>
,	,	kIx,	,
omluvil	omluvit	k5eAaPmAgMnS	omluvit
se	se	k3xPyFc4	se
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
výpad	výpad	k1gInSc4	výpad
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
satirou	satira	k1gFnSc7	satira
se	se	k3xPyFc4	se
zviditelnil	zviditelnit	k5eAaPmAgInS	zviditelnit
<g/>
.	.	kIx.	.
</s>
<s>
Childe	Childe	k6eAd1	Childe
Haroldova	Haroldův	k2eAgFnSc1d1	Haroldova
pouť	pouť	k1gFnSc1	pouť
(	(	kIx(	(
<g/>
vydávána	vydáván	k2eAgNnPc1d1	vydáváno
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1812	[number]	k4	1812
<g/>
-	-	kIx~	-
<g/>
1818	[number]	k4	1818
<g/>
)	)	kIx)	)
-	-	kIx~	-
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
komponovaná	komponovaný	k2eAgFnSc1d1	komponovaná
<g/>
,	,	kIx,	,
lyricko-epická	lyrickopický	k2eAgFnSc1d1	lyricko-epická
skladba	skladba	k1gFnSc1	skladba
o	o	k7c6	o
čtyřech	čtyři	k4xCgInPc6	čtyři
zpěvech	zpěv	k1gInPc6	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
jeho	jeho	k3xOp3gNnSc4	jeho
putování	putování	k1gNnSc4	putování
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
přes	přes	k7c4	přes
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
Albánii	Albánie	k1gFnSc4	Albánie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
země	zem	k1gFnPc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
bývá	bývat	k5eAaImIp3nS	bývat
označován	označován	k2eAgInSc4d1	označován
zpěv	zpěv	k1gInSc4	zpěv
třetí	třetí	k4xOgNnPc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
(	(	kIx(	(
<g/>
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
těchto	tento	k3xDgInPc2	tento
zpěvů	zpěv	k1gInPc2	zpěv
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Percym	Percym	k1gInSc1	Percym
B.	B.	kA	B.
Shelleym	Shelleym	k1gInSc1	Shelleym
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
autobiografické	autobiografický	k2eAgFnSc2d1	autobiografická
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
zahořklý	zahořklý	k2eAgMnSc1d1	zahořklý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
mu	on	k3xPp3gMnSc3	on
ulehčuje	ulehčovat	k5eAaImIp3nS	ulehčovat
jeho	jeho	k3xOp3gInSc4	jeho
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
neustálé	neustálý	k2eAgNnSc4d1	neustálé
putování	putování	k1gNnSc4	putování
<g/>
.	.	kIx.	.
</s>
<s>
Romantik	romantik	k1gMnSc1	romantik
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
jistoty	jistota	k1gFnSc2	jistota
<g/>
,	,	kIx,	,
putoval	putovat	k5eAaImAgMnS	putovat
přírodou	příroda	k1gFnSc7	příroda
<g/>
;	;	kIx,	;
začíná	začínat	k5eAaImIp3nS	začínat
jako	jako	k9	jako
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
rytířské	rytířský	k2eAgInPc4d1	rytířský
romány	román	k1gInPc4	román
a	a	k8xC	a
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
jako	jako	k9	jako
básníkův	básníkův	k2eAgInSc1d1	básníkův
deník	deník	k1gInSc1	deník
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
měl	mít	k5eAaImAgMnS	mít
jmenovat	jmenovat	k5eAaBmF	jmenovat
Childe	Child	k1gInSc5	Child
Burun	Burun	k1gNnSc4	Burun
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
dílem	díl	k1gInSc7	díl
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
slávu	sláva	k1gFnSc4	sláva
v	v	k7c6	v
literárních	literární	k2eAgInPc6d1	literární
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Childe	Childe	k6eAd1	Childe
znamená	znamenat	k5eAaImIp3nS	znamenat
česky	česky	k6eAd1	česky
panic	panice	k1gFnPc2	panice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
středověký	středověký	k2eAgInSc4d1	středověký
titul	titul	k1gInSc4	titul
pro	pro	k7c4	pro
čekatele	čekatel	k1gMnPc4	čekatel
rytířství	rytířství	k1gNnSc2	rytířství
<g/>
.	.	kIx.	.
</s>
<s>
Korzár	korzár	k1gMnSc1	korzár
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
Vězeň	vězeň	k1gMnSc1	vězeň
chillonský	chillonský	k2eAgMnSc1d1	chillonský
-	-	kIx~	-
poéma	poéma	k1gFnSc1	poéma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yIgFnSc4	který
se	se	k3xPyFc4	se
Byron	Byron	k1gNnSc1	Byron
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navštívil	navštívit	k5eAaPmAgInS	navštívit
hrad	hrad	k1gInSc1	hrad
Chillon	Chillon	k1gInSc1	Chillon
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byli	být	k5eAaImAgMnP	být
vězněni	vězněn	k2eAgMnPc1d1	vězněn
političtí	politický	k2eAgMnPc1d1	politický
odpůrci	odpůrce	k1gMnPc1	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Velice	velice	k6eAd1	velice
působivá	působivý	k2eAgFnSc1d1	působivá
báseň	báseň	k1gFnSc1	báseň
o	o	k7c6	o
bratrech	bratr	k1gMnPc6	bratr
zmírajících	zmírající	k2eAgInPc2d1	zmírající
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
a	a	k8xC	a
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
nepoddajní	poddajný	k2eNgMnPc1d1	nepoddajný
a	a	k8xC	a
nekají	kát	k5eNaImIp3nP	kát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
si	se	k3xPyFc3	se
za	za	k7c7	za
svými	svůj	k3xOyFgInPc7	svůj
činy	čin	k1gInPc7	čin
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
všichni	všechen	k3xTgMnPc1	všechen
umírají	umírat	k5eAaImIp3nP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
poema	poema	k1gFnSc1	poema
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
K.	K.	kA	K.
H.	H.	kA	H.
Máchu	mách	k1gInSc6	mách
<g/>
.	.	kIx.	.
</s>
<s>
Lara	Lara	k1gFnSc1	Lara
(	(	kIx(	(
<g/>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
-	-	kIx~	-
lyricko-epická	lyrickopický	k2eAgFnSc1d1	lyricko-epická
poéma	poéma	k1gFnSc1	poéma
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
šlechtic	šlechtic	k1gMnSc1	šlechtic
Lara	Lara	k1gMnSc1	Lara
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
minulost	minulost	k1gFnSc1	minulost
je	být	k5eAaImIp3nS	být
zahalena	zahalit	k5eAaPmNgFnS	zahalit
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zámeckém	zámecký	k2eAgNnSc6d1	zámecké
plese	pleso	k1gNnSc6	pleso
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
hádce	hádka	k1gFnSc3	hádka
a	a	k8xC	a
vyzvání	vyzvánět	k5eAaImIp3nS	vyzvánět
na	na	k7c4	na
souboj	souboj	k1gInSc4	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Rozhoří	rozhořet	k5eAaPmIp3nS	rozhořet
se	se	k3xPyFc4	se
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
královstvími	království	k1gNnPc7	království
<g/>
.	.	kIx.	.
</s>
<s>
Lara	Lara	k6eAd1	Lara
je	být	k5eAaImIp3nS	být
nepřístupný	přístupný	k2eNgInSc1d1	nepřístupný
svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc1	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
např.	např.	kA	např.
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
utlačovaným	utlačovaný	k2eAgMnPc3d1	utlačovaný
lidem	člověk	k1gMnPc3	člověk
příbytek	příbytek	k1gInSc4	příbytek
<g/>
.	.	kIx.	.
</s>
<s>
Bojuje	bojovat	k5eAaImIp3nS	bojovat
se	se	k3xPyFc4	se
znepřáteleným	znepřátelený	k2eAgMnSc7d1	znepřátelený
šlechticem	šlechtic	k1gMnSc7	šlechtic
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
nevolníků	nevolník	k1gMnPc2	nevolník
<g/>
.	.	kIx.	.
</s>
<s>
Umírá	umírat	k5eAaImIp3nS	umírat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
mimo	mimo	k7c4	mimo
svou	svůj	k3xOyFgFnSc4	svůj
vlast	vlast	k1gFnSc4	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Džaur	džaur	k1gMnSc1	džaur
(	(	kIx(	(
<g/>
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
-	-	kIx~	-
posmrtná	posmrtný	k2eAgFnSc1d1	posmrtná
zpověď	zpověď	k1gFnSc1	zpověď
mnicha	mnich	k1gMnSc2	mnich
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
prožíval	prožívat	k5eAaImAgMnS	prožívat
milostná	milostný	k2eAgNnPc4d1	milostné
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
<g/>
,	,	kIx,	,
pomstil	pomstit	k5eAaImAgMnS	pomstit
smrt	smrt	k1gFnSc4	smrt
své	svůj	k3xOyFgFnSc2	svůj
dívky	dívka	k1gFnSc2	dívka
Laily	Laila	k1gFnSc2	Laila
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
utopil	utopit	k5eAaPmAgMnS	utopit
její	její	k3xOp3gMnSc1	její
pán	pán	k1gMnSc1	pán
(	(	kIx(	(
<g/>
byla	být	k5eAaImAgFnS	být
otrokyně	otrokyně	k1gFnSc1	otrokyně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
vešel	vejít	k5eAaPmAgInS	vejít
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Nevěsta	nevěsta	k1gFnSc1	nevěsta
z	z	k7c2	z
Abydu	Abyd	k1gInSc2	Abyd
-	-	kIx~	-
poema	poema	k1gFnSc1	poema
Obléhání	obléhání	k1gNnSc2	obléhání
Korintu	Korint	k1gInSc2	Korint
Beppo	Beppa	k1gFnSc5	Beppa
Ivan	Ivan	k1gMnSc1	Ivan
Mazepa	Mazep	k1gMnSc2	Mazep
Kain	Kain	k1gMnSc1	Kain
-	-	kIx~	-
na	na	k7c4	na
biblické	biblický	k2eAgNnSc4d1	biblické
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dialogu	dialog	k1gInSc2	dialog
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
je	být	k5eAaImIp3nS	být
Kain	Kain	k1gMnSc1	Kain
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
své	svůj	k3xOyFgFnPc4	svůj
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
téměř	téměř	k6eAd1	téměř
u	u	k7c2	u
nikoho	nikdo	k3yNnSc2	nikdo
nenalézá	nalézat	k5eNaImIp3nS	nalézat
pochopení	pochopení	k1gNnSc1	pochopení
<g/>
,	,	kIx,	,
krom	krom	k7c2	krom
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
a	a	k8xC	a
své	svůj	k3xOyFgFnSc2	svůj
ženy	žena	k1gFnSc2	žena
Ady	Ady	k1gFnSc2	Ady
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
mu	on	k3xPp3gMnSc3	on
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
mezi	mezi	k7c4	mezi
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
diskutují	diskutovat	k5eAaImIp3nP	diskutovat
spolu	spolu	k6eAd1	spolu
o	o	k7c6	o
různých	různý	k2eAgFnPc6d1	různá
otázkách	otázka	k1gFnPc6	otázka
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
ústředních	ústřední	k2eAgFnPc2d1	ústřední
myšlenek	myšlenka	k1gFnPc2	myšlenka
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Proč	proč	k6eAd1	proč
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
dobro	dobro	k1gNnSc4	dobro
<g/>
,	,	kIx,	,
stvořil	stvořit	k5eAaPmAgMnS	stvořit
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
člověka	člověk	k1gMnSc4	člověk
tomuto	tento	k3xDgNnSc3	tento
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Manfréd	Manfréd	k1gMnSc1	Manfréd
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
-	-	kIx~	-
báseň	báseň	k1gFnSc1	báseň
na	na	k7c4	na
faustovské	faustovský	k2eAgNnSc4d1	Faustovské
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
Manfréd	Manfréd	k1gMnSc1	Manfréd
žijící	žijící	k2eAgMnSc1d1	žijící
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
hradě	hrad	k1gInSc6	hrad
ve	v	k7c6	v
švýcarských	švýcarský	k2eAgFnPc6d1	švýcarská
Alpách	Alpy	k1gFnPc6	Alpy
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
využije	využít	k5eAaPmIp3nS	využít
svých	svůj	k3xOyFgFnPc2	svůj
tajemných	tajemný	k2eAgFnPc2d1	tajemná
duševních	duševní	k2eAgFnPc2d1	duševní
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
přivolá	přivolat	k5eAaPmIp3nS	přivolat
vesmírné	vesmírný	k2eAgMnPc4d1	vesmírný
duchy	duch	k1gMnPc4	duch
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mu	on	k3xPp3gMnSc3	on
nabídnou	nabídnout	k5eAaPmIp3nP	nabídnout
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
věk	věk	k1gInSc4	věk
a	a	k8xC	a
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Manfréd	Manfréd	k1gMnSc1	Manfréd
odmítá	odmítat	k5eAaImIp3nS	odmítat
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
duchy	duch	k1gMnPc4	duch
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
dali	dát	k5eAaPmAgMnP	dát
zapomnění	zapomnění	k1gNnSc4	zapomnění
<g/>
,	,	kIx,	,
takovou	takový	k3xDgFnSc4	takový
moc	moc	k6eAd1	moc
však	však	k9	však
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
vykouzlí	vykouzlit	k5eAaPmIp3nP	vykouzlit
překrásnou	překrásný	k2eAgFnSc4d1	překrásná
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Manfréd	Manfréd	k1gMnSc1	Manfréd
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
spatření	spatření	k1gNnSc6	spatření
omdlévá	omdlévat	k5eAaImIp3nS	omdlévat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nad	nad	k7c7	nad
ním	on	k3xPp3gInSc7	on
pronesena	pronesen	k2eAgFnSc1d1	pronesena
kletba	kletba	k1gFnSc1	kletba
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
hluboce	hluboko	k6eAd1	hluboko
prociťuje	prociťovat	k5eAaImIp3nS	prociťovat
úděl	úděl	k1gInSc4	úděl
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Manfréd	Manfréd	k1gMnSc1	Manfréd
je	být	k5eAaImIp3nS	být
titánský	titánský	k2eAgMnSc1d1	titánský
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
osudu	osud	k1gInSc3	osud
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
mu	on	k3xPp3gMnSc3	on
do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
dechu	dech	k1gInSc2	dech
vzdoruje	vzdorovat	k5eAaImIp3nS	vzdorovat
<g/>
.	.	kIx.	.
</s>
<s>
Snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
povznést	povznést	k5eAaPmF	povznést
nad	nad	k7c4	nad
své	svůj	k3xOyFgNnSc4	svůj
lidství	lidství	k1gNnSc4	lidství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
Byronovy	Byronův	k2eAgFnSc2d1	Byronova
poémy	poéma	k1gFnSc2	poéma
Manfréd	Manfréd	k1gMnSc1	Manfréd
napsal	napsat	k5eAaPmAgMnS	napsat
Petr	Petr	k1gMnSc1	Petr
Iljič	Iljič	k1gMnSc1	Iljič
Čajkovskij	Čajkovskij	k1gMnSc1	Čajkovskij
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
symfonii	symfonie	k1gFnSc4	symfonie
(	(	kIx(	(
<g/>
Symfonie	symfonie	k1gFnSc1	symfonie
Manfred	Manfred	k1gMnSc1	Manfred
h	h	k?	h
moll	moll	k1gNnPc2	moll
<g/>
,	,	kIx,	,
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
-	-	kIx~	-
veršovaný	veršovaný	k2eAgInSc1d1	veršovaný
satirický	satirický	k2eAgInSc1d1	satirický
román	román	k1gInSc1	román
(	(	kIx(	(
<g/>
epos	epos	k1gInSc1	epos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
provádí	provádět	k5eAaImIp3nS	provádět
hrdinu	hrdina	k1gMnSc4	hrdina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nedopsán	dopsán	k2eNgInSc1d1	dopsán
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měl	mít	k5eAaImAgInS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
12	[number]	k4	12
zpěvů	zpěv	k1gInPc2	zpěv
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Lord	lord	k1gMnSc1	lord
Byron	Byron	k1gMnSc1	Byron
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
povedlo	povést	k5eAaPmAgNnS	povést
dílo	dílo	k1gNnSc4	dílo
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Končí	končit	k5eAaImIp3nS	končit
jedenáctým	jedenáctý	k4xOgInSc7	jedenáctý
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
zpěvu	zpěv	k1gInSc2	zpěv
s	s	k7c7	s
láskyplnou	láskyplný	k2eAgFnSc7d1	láskyplná
ironií	ironie	k1gFnSc7	ironie
popsal	popsat	k5eAaPmAgMnS	popsat
svého	svůj	k3xOyFgMnSc4	svůj
přítele	přítel	k1gMnSc4	přítel
a	a	k8xC	a
básníka	básník	k1gMnSc4	básník
P.	P.	kA	P.
B.	B.	kA	B.
Shelleyho	Shelley	k1gMnSc2	Shelley
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgFnPc1d1	hebrejská
melodie	melodie	k1gFnPc1	melodie
Nebesa	nebesa	k1gNnPc4	nebesa
a	a	k8xC	a
země	zem	k1gFnPc4	zem
Marino	Marina	k1gFnSc5	Marina
Faliero	Faliera	k1gFnSc5	Faliera
Dva	dva	k4xCgMnPc1	dva
Foscariové	Foscariový	k2eAgFnPc1d1	Foscariový
Werner	Werner	k1gMnSc1	Werner
Sardanapal	Sardanapal	k1gMnSc1	Sardanapal
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Lorda	lord	k1gMnSc2	lord
Byrona	Byron	k1gMnSc2	Byron
se	se	k3xPyFc4	se
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
mnoho	mnoho	k4c1	mnoho
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
slovanských	slovanský	k2eAgFnPc6d1	Slovanská
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Sergejevič	Sergejevič	k1gMnSc1	Sergejevič
Puškin	Puškin	k1gMnSc1	Puškin
<g/>
,	,	kIx,	,
Michail	Michail	k1gMnSc1	Michail
Jurjevič	Jurjevič	k1gMnSc1	Jurjevič
Lermontov	Lermontov	k1gInSc1	Lermontov
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
<g/>
,	,	kIx,	,
z	z	k7c2	z
mimoslovanských	mimoslovanský	k2eAgInPc2d1	mimoslovanský
Heinrich	Heinrich	k1gMnSc1	Heinrich
Heine	Hein	k1gInSc5	Hein
<g/>
,	,	kIx,	,
Edgar	Edgar	k1gMnSc1	Edgar
Allan	Allan	k1gMnSc1	Allan
Poe	Poe	k1gMnSc1	Poe
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Hynek	Hynek	k1gMnSc1	Hynek
Mácha	Mácha	k1gMnSc1	Mácha
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
jeho	jeho	k3xOp3gFnSc4	jeho
poemou	poema	k1gFnSc7	poema
Vězeň	vězeň	k1gMnSc1	vězeň
chillonský	chillonský	k2eAgMnSc1d1	chillonský
a	a	k8xC	a
napsal	napsat	k5eAaPmAgInS	napsat
své	svůj	k3xOyFgNnSc4	svůj
nejslavnější	slavný	k2eAgNnSc4d3	nejslavnější
dílo	dílo	k1gNnSc4	dílo
Máj	Mája	k1gFnPc2	Mája
<g/>
.	.	kIx.	.
</s>
