<s>
Mezi	mezi	k7c4	mezi
jeho	on	k3xPp3gMnSc4	on
nejznámější	známý	k2eAgNnPc1d3	nejznámější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nP	patřit
Childe	Child	k1gInSc5	Child
Haroldova	Haroldův	k2eAgFnSc1d1	Haroldova
pouť	pouť	k1gFnSc1	pouť
(	(	kIx(	(
<g/>
popsáno	popsat	k5eAaPmNgNnS	popsat
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
a	a	k8xC	a
satirický	satirický	k2eAgInSc4d1	satirický
veršovaný	veršovaný	k2eAgInSc4d1	veršovaný
román	román	k1gInSc4	román
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zůstal	zůstat	k5eAaPmAgInS	zůstat
nedokončený	dokončený	k2eNgMnSc1d1	nedokončený
<g/>
.	.	kIx.	.
</s>
