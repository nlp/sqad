<s>
Vědeckofantastický	vědeckofantastický	k2eAgInSc4d1	vědeckofantastický
žánr	žánr	k1gInSc4	žánr
nebo	nebo	k8xC	nebo
též	též	k9	též
sci-fi	scii	k1gFnSc1	sci-fi
a	a	k8xC	a
SF	SF	kA	SF
(	(	kIx(	(
<g/>
zkratky	zkratka	k1gFnSc2	zkratka
anglického	anglický	k2eAgInSc2d1	anglický
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
umělecký	umělecký	k2eAgInSc4d1	umělecký
žánr	žánr	k1gInSc4	žánr
(	(	kIx(	(
<g/>
především	především	k9	především
literární	literární	k2eAgMnSc1d1	literární
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgInSc1d1	filmový
<g/>
,	,	kIx,	,
herní	herní	k2eAgInSc1d1	herní
či	či	k8xC	či
výtvarný	výtvarný	k2eAgInSc1d1	výtvarný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vymezený	vymezený	k2eAgInSc1d1	vymezený
výskytem	výskyt	k1gInSc7	výskyt
spekulativních	spekulativní	k2eAgFnPc2d1	spekulativní
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
anebo	anebo	k8xC	anebo
dosud	dosud	k6eAd1	dosud
neznámých	známý	k2eNgFnPc2d1	neznámá
forem	forma	k1gFnPc2	forma
života	život	k1gInSc2	život
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
sci-fi	scii	k1gFnSc2	sci-fi
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
budoucnosti	budoucnost	k1gFnSc2	budoucnost
či	či	k8xC	či
alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Sci-fi	scii	k1gFnSc1	sci-fi
je	být	k5eAaImIp3nS	být
žánr	žánr	k1gInSc4	žánr
blízký	blízký	k2eAgInSc4d1	blízký
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
místo	místo	k1gNnSc4	místo
technologií	technologie	k1gFnPc2	technologie
hlavní	hlavní	k2eAgFnSc7d1	hlavní
rekvizitou	rekvizita	k1gFnSc7	rekvizita
většinou	většinou	k6eAd1	většinou
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
fyzická	fyzický	k2eAgFnSc1d1	fyzická
síla	síla	k1gFnSc1	síla
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
příběhy	příběh	k1gInPc4	příběh
"	"	kIx"	"
<g/>
meče	meč	k1gInPc4	meč
a	a	k8xC	a
magie	magie	k1gFnPc4	magie
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
žánry	žánr	k1gInPc1	žánr
se	se	k3xPyFc4	se
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
prorůstají	prorůstat	k5eAaImIp3nP	prorůstat
<g/>
;	;	kIx,	;
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
takových	takový	k3xDgInPc2	takový
průniků	průnik	k1gInPc2	průnik
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
díla	dílo	k1gNnSc2	dílo
žánru	žánr	k1gInSc2	žánr
science	scienec	k1gInSc2	scienec
fantasy	fantas	k1gInPc1	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
žánru	žánr	k1gInSc2	žánr
bývají	bývat	k5eAaImIp3nP	bývat
považováni	považován	k2eAgMnPc1d1	považován
dva	dva	k4xCgMnPc1	dva
autoři	autor	k1gMnPc1	autor
–	–	k?	–
Jules	Jules	k1gMnSc1	Jules
Verne	Vern	k1gMnSc5	Vern
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
technickou	technický	k2eAgFnSc4d1	technická
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
,	,	kIx,	,
a	a	k8xC	a
Herbert	Herbert	k1gMnSc1	Herbert
George	Georg	k1gFnSc2	Georg
Wells	Wells	k1gInSc1	Wells
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
spíše	spíše	k9	spíše
vlivu	vliv	k1gInSc2	vliv
technologií	technologie	k1gFnPc2	technologie
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
H.	H.	kA	H.
G.	G.	kA	G.
Wells	Wells	k1gInSc4	Wells
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
nastolil	nastolit	k5eAaPmAgMnS	nastolit
mnoho	mnoho	k4c4	mnoho
témat	téma	k1gNnPc2	téma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
současná	současný	k2eAgFnSc1d1	současná
sci-fi	scii	k1gFnSc1	sci-fi
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
mimozemskými	mimozemský	k2eAgFnPc7d1	mimozemská
civilizacemi	civilizace	k1gFnPc7	civilizace
<g/>
,	,	kIx,	,
cesty	cesta	k1gFnSc2	cesta
časem	časem	k6eAd1	časem
<g/>
,	,	kIx,	,
biotechnologie	biotechnologie	k1gFnSc1	biotechnologie
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kořeny	kořen	k1gInPc4	kořen
fantaskního	fantaskní	k2eAgInSc2d1	fantaskní
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
sahají	sahat	k5eAaImIp3nP	sahat
k	k	k7c3	k
eposu	epos	k1gInSc3	epos
o	o	k7c6	o
Gilgamešovi	Gilgameš	k1gMnSc6	Gilgameš
a	a	k8xC	a
popisu	popis	k1gInSc3	popis
ideálního	ideální	k2eAgInSc2d1	ideální
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Platónově	Platónův	k2eAgFnSc6d1	Platónova
Ústavě	ústava	k1gFnSc6	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
českých	český	k2eAgMnPc2d1	český
fanoušků	fanoušek	k1gMnPc2	fanoušek
za	za	k7c4	za
fantaskní	fantaskní	k2eAgNnSc4d1	fantaskní
dílo	dílo	k1gNnSc4	dílo
považuje	považovat	k5eAaImIp3nS	považovat
Komenského	Komenského	k2eAgInSc1d1	Komenského
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
ráj	ráj	k1gInSc4	ráj
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
české	český	k2eAgFnSc2d1	Česká
sci-fi	scii	k1gFnSc2	sci-fi
položil	položit	k5eAaPmAgInS	položit
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Karel	Karel	k1gMnSc1	Karel
Pleskač	pleskač	k1gMnSc1	pleskač
v	v	k7c6	v
románu	román	k1gInSc6	román
Život	život	k1gInSc1	život
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
(	(	kIx(	(
<g/>
Bellmann	Bellmann	k1gInSc1	Bellmann
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgMnS	následovat
jej	on	k3xPp3gMnSc4	on
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Čech	Čech	k1gMnSc1	Čech
s	s	k7c7	s
dvojicí	dvojice	k1gFnSc7	dvojice
románů	román	k1gInPc2	román
Pravý	pravý	k2eAgInSc1d1	pravý
výlet	výlet	k1gInSc1	výlet
pana	pan	k1gMnSc2	pan
Broučka	Brouček	k1gMnSc2	Brouček
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
epochální	epochální	k2eAgInSc1d1	epochální
výlet	výlet	k1gInSc1	výlet
pana	pan	k1gMnSc2	pan
Broučka	Brouček	k1gMnSc2	Brouček
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
do	do	k7c2	do
XV	XV	kA	XV
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
výrazně	výrazně	k6eAd1	výrazně
technickým	technický	k2eAgMnSc7d1	technický
(	(	kIx(	(
<g/>
a	a	k8xC	a
taktéž	taktéž	k?	taktéž
spekulativním	spekulativní	k2eAgNnSc7d1	spekulativní
<g/>
)	)	kIx)	)
sci-fi	scii	k1gNnSc1	sci-fi
příběhem	příběh	k1gInSc7	příběh
je	být	k5eAaImIp3nS	být
anonymní	anonymní	k2eAgMnSc1d1	anonymní
<g/>
,	,	kIx,	,
neprávem	neprávo	k1gNnSc7	neprávo
zapomenutý	zapomenutý	k2eAgInSc4d1	zapomenutý
román	román	k1gInSc4	román
Na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poprvé	poprvé	k6eAd1	poprvé
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
brněnském	brněnský	k2eAgInSc6d1	brněnský
časopisu	časopis	k1gInSc6	časopis
Červánky	červánek	k1gInPc1	červánek
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
světové	světový	k2eAgFnSc2d1	světová
sci-fi	scii	k1gFnSc2	sci-fi
měla	mít	k5eAaImAgNnP	mít
díla	dílo	k1gNnPc1	dílo
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
R.	R.	kA	R.
<g/>
U.	U.	kA	U.
<g/>
R.	R.	kA	R.
<g/>
,	,	kIx,	,
Válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Mloky	mlok	k1gMnPc7	mlok
<g/>
,	,	kIx,	,
Krakatit	Krakatit	k1gInSc1	Krakatit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
skutečný	skutečný	k2eAgInSc4d1	skutečný
základ	základ	k1gInSc4	základ
sci-fi	scii	k1gNnPc2	sci-fi
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
až	až	k9	až
americký	americký	k2eAgInSc1d1	americký
časopis	časopis	k1gInSc1	časopis
Astounding	Astounding	k1gInSc1	Astounding
Stories	Stories	k1gInSc4	Stories
(	(	kIx(	(
<g/>
Ohromující	ohromující	k2eAgInPc4d1	ohromující
příběhy	příběh	k1gInPc4	příběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sehrál	sehrát	k5eAaPmAgInS	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
při	při	k7c6	při
rozvoji	rozvoj	k1gInSc6	rozvoj
sci-fi	scii	k1gNnPc2	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
převzal	převzít	k5eAaPmAgInS	převzít
jeho	jeho	k3xOp3gNnSc4	jeho
řízení	řízení	k1gNnSc4	řízení
John	John	k1gMnSc1	John
Wood	Wood	k1gMnSc1	Wood
Campbell	Campbell	k1gMnSc1	Campbell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jej	on	k3xPp3gMnSc4	on
přeměnil	přeměnit	k5eAaPmAgMnS	přeměnit
na	na	k7c4	na
Astounding	Astounding	k1gInSc4	Astounding
Science	Science	k1gFnSc1	Science
Fiction	Fiction	k1gInSc1	Fiction
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
sci-fi	scii	k1gFnSc4	sci-fi
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
<g/>
.	.	kIx.	.
</s>
<s>
Přeměnil	přeměnit	k5eAaPmAgInS	přeměnit
nejen	nejen	k6eAd1	nejen
ráz	ráz	k1gInSc4	ráz
časopisu	časopis	k1gInSc2	časopis
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
i	i	k9	i
vývoj	vývoj	k1gInSc1	vývoj
celého	celý	k2eAgNnSc2d1	celé
sci-fi	scii	k1gNnSc2	sci-fi
–	–	k?	–
lze	lze	k6eAd1	lze
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
tvrdit	tvrdit	k5eAaImF	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgMnS	dát
rámec	rámec	k1gInSc4	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Položil	položit	k5eAaPmAgMnS	položit
důraz	důraz	k1gInSc4	důraz
nejen	nejen	k6eAd1	nejen
na	na	k7c4	na
zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Cambell	Cambell	k1gMnSc1	Cambell
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
velké	velký	k2eAgFnSc6d1	velká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
lidstva	lidstvo	k1gNnSc2	lidstvo
jako	jako	k9	jako
takového	takový	k3xDgNnSc2	takový
<g/>
,	,	kIx,	,
tímto	tento	k3xDgInSc7	tento
svým	svůj	k3xOyFgInSc7	svůj
přístupem	přístup	k1gInSc7	přístup
ovlivnil	ovlivnit	k5eAaPmAgMnS	ovlivnit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
přišla	přijít	k5eAaPmAgFnS	přijít
BBC	BBC	kA	BBC
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
pořadem	pořad	k1gInSc7	pořad
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
Doctor	Doctor	k1gInSc4	Doctor
Who	Who	k1gMnPc2	Who
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejdéle	dlouho	k6eAd3	dlouho
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
sci-fi	scii	k1gNnSc7	sci-fi
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
spisovatelů	spisovatel	k1gMnPc2	spisovatel
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
<g/>
.	.	kIx.	.
</s>
<s>
Isaac	Isaac	k6eAd1	Isaac
Asimov	Asimov	k1gInSc1	Asimov
Ray	Ray	k1gFnSc2	Ray
Bradbury	Bradbura	k1gFnSc2	Bradbura
Arthur	Arthur	k1gMnSc1	Arthur
C.	C.	kA	C.
Clarke	Clarke	k1gFnSc1	Clarke
Philip	Philip	k1gMnSc1	Philip
K.	K.	kA	K.
Dick	Dick	k1gMnSc1	Dick
Robert	Robert	k1gMnSc1	Robert
A.	A.	kA	A.
Heinlein	Heinlein	k1gMnSc1	Heinlein
Frank	Frank	k1gMnSc1	Frank
Herbert	Herbert	k1gMnSc1	Herbert
Stanisław	Stanisław	k1gFnPc2	Stanisław
Lem	lem	k1gInSc1	lem
Ludvík	Ludvík	k1gMnSc1	Ludvík
Souček	Souček	k1gMnSc1	Souček
Arkadij	Arkadij	k1gMnSc1	Arkadij
a	a	k8xC	a
Boris	Boris	k1gMnSc1	Boris
Strugačtí	Strugacký	k1gMnPc1	Strugacký
David	David	k1gMnSc1	David
Brin	Brin	k1gMnSc1	Brin
Karel	Karel	k1gMnSc1	Karel
Čapek	Čapek	k1gMnSc1	Čapek
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
(	(	kIx(	(
<g/>
ilustrační	ilustrační	k2eAgMnSc1d1	ilustrační
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Čapek	Čapek	k1gMnSc1	Čapek
Teodor	Teodor	k1gMnSc1	Teodor
Rotrekl	Rotrekl	k1gMnSc1	Rotrekl
Alternativní	alternativní	k2eAgFnSc2d1	alternativní
historie	historie	k1gFnSc2	historie
Antiutopie	Antiutopie	k1gFnSc1	Antiutopie
Apokalyptická	apokalyptický	k2eAgFnSc1d1	apokalyptická
a	a	k8xC	a
postapokalyptická	postapokalyptický	k2eAgFnSc1d1	postapokalyptická
sci-fi	scii	k1gFnSc1	sci-fi
Dieselpunk	Dieselpunka	k1gFnPc2	Dieselpunka
Hard	Harda	k1gFnPc2	Harda
science	science	k1gFnSc2	science
fiction	fiction	k1gInSc1	fiction
Kyberpunk	Kyberpunk	k1gInSc1	Kyberpunk
Military	Militara	k1gFnSc2	Militara
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
Science	Science	k1gFnSc1	Science
fantasy	fantas	k1gInPc1	fantas
Soft	Soft	k?	Soft
science	scienec	k1gInSc2	scienec
fiction	fiction	k1gInSc1	fiction
Space	Space	k1gMnSc2	Space
opera	opera	k1gFnSc1	opera
Steampunk	Steampunk	k1gMnSc1	Steampunk
Technothriller	Technothriller	k1gMnSc1	Technothriller
Utopie	utopie	k1gFnSc1	utopie
</s>
