<p>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Brachylophus	Brachylophus	k1gInSc1	Brachylophus
vitiensis	vitiensis	k1gFnSc2	vitiensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
fidžijský	fidžijský	k2eAgInSc1d1	fidžijský
chocholatý	chocholatý	k2eAgInSc1d1	chocholatý
nebo	nebo	k8xC	nebo
fidžský	fidžský	k2eAgInSc1d1	fidžský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ještěr	ještěr	k1gMnSc1	ještěr
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
leguánovití	leguánovitý	k2eAgMnPc1d1	leguánovitý
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Brachylophus	Brachylophus	k1gInSc4	Brachylophus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
zástupci	zástupce	k1gMnPc1	zástupce
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
zeleno-modře	zelenoodro	k6eAd1	zeleno-modro
zbarveni	zbarvit	k5eAaPmNgMnP	zbarvit
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
je	být	k5eAaImIp3nS	být
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
je	být	k5eAaImIp3nS	být
endemitem	endemit	k1gInSc7	endemit
ostrova	ostrov	k1gInSc2	ostrov
Fidži	Fidž	k1gFnSc6	Fidž
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnPc4d1	vyskytující
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
ostrovech	ostrov	k1gInPc6	ostrov
jižního	jižní	k2eAgInSc2d1	jižní
Pacifiku	Pacifik	k1gInSc2	Pacifik
<g/>
;	;	kIx,	;
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Yadua	Yaduus	k1gMnSc2	Yaduus
Taba	Tabus	k1gMnSc2	Tabus
<g/>
,	,	kIx,	,
Monuriki	Monurik	k1gFnSc2	Monurik
a	a	k8xC	a
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Deviulau	Deviulaus	k1gInSc2	Deviulaus
a	a	k8xC	a
Waya	Way	k1gInSc2	Way
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgFnPc4d1	zvaná
Yasawa	Yasawum	k1gNnPc4	Yasawum
<g/>
.	.	kIx.	.
</s>
<s>
Zpozorován	zpozorovat	k5eAaPmNgInS	zpozorovat
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
rovněž	rovněž	k9	rovněž
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Monu	Monus	k1gInSc2	Monus
<g/>
,	,	kIx,	,
Qalito	Qalit	k2eAgNnSc1d1	Qalit
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
Malolo	Malola	k1gFnSc5	Malola
Levu	Lev	k1gMnSc3	Lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byl	být	k5eAaImAgInS	být
naposledy	naposledy	k6eAd1	naposledy
spatřen	spatřit	k5eAaPmNgInS	spatřit
také	také	k9	také
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Matacawa	Matacawum	k1gNnSc2	Matacawum
Levu	Levus	k1gInSc2	Levus
nebo	nebo	k8xC	nebo
Naviti	navit	k5eAaImF	navit
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
však	však	k9	však
již	již	k6eAd1	již
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyhynul	vyhynout	k5eAaPmAgMnS	vyhynout
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
zmíněných	zmíněný	k2eAgInPc6d1	zmíněný
ostrovech	ostrov	k1gInPc6	ostrov
evidován	evidován	k2eAgMnSc1d1	evidován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
a	a	k8xC	a
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
svého	svůj	k3xOyFgInSc2	svůj
rodu	rod	k1gInSc2	rod
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
skladbou	skladba	k1gFnSc7	skladba
užších	úzký	k2eAgInPc2d2	užší
svislých	svislý	k2eAgInPc2d1	svislý
pruhů	pruh	k1gInPc2	pruh
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
černě	černě	k6eAd1	černě
lemované	lemovaný	k2eAgFnSc6d1	lemovaná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
něco	něco	k3yInSc4	něco
patrnější	patrný	k2eAgMnSc1d2	patrnější
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
ostnatý	ostnatý	k2eAgInSc1d1	ostnatý
hřebínek	hřebínek	k1gInSc1	hřebínek
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
(	(	kIx(	(
<g/>
na	na	k7c6	na
páteři	páteř	k1gFnSc6	páteř
a	a	k8xC	a
hlavě	hlava	k1gFnSc6	hlava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzpřímený	vzpřímený	k2eAgInSc1d1	vzpřímený
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
černého	černý	k2eAgNnSc2d1	černé
zbarvení	zbarvení	k1gNnSc2	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
podstatně	podstatně	k6eAd1	podstatně
vyšších	vysoký	k2eAgInPc2d2	vyšší
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
až	až	k9	až
1,2	[number]	k4	1,2
metru	metr	k1gInSc2	metr
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
300	[number]	k4	300
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
<s>
Umí	umět	k5eAaImIp3nS	umět
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
změnit	změnit	k5eAaPmF	změnit
barvu	barva	k1gFnSc4	barva
svého	svůj	k3xOyFgNnSc2	svůj
těla	tělo	k1gNnSc2	tělo
<g/>
;	;	kIx,	;
od	od	k7c2	od
zelené	zelená	k1gFnSc2	zelená
přes	přes	k7c4	přes
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
až	až	k9	až
černou	černý	k2eAgFnSc7d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
činí	činit	k5eAaImIp3nS	činit
především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
námluv	námluva	k1gFnPc2	námluva
nebo	nebo	k8xC	nebo
při	při	k7c6	při
teritoriálním	teritoriální	k2eAgNnSc6d1	teritoriální
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
<g/>
Hnízdit	hnízdit	k5eAaImF	hnízdit
obvykle	obvykle	k6eAd1	obvykle
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
březnem	březen	k1gInSc7	březen
a	a	k8xC	a
dubnem	duben	k1gInSc7	duben
<g/>
,	,	kIx,	,
a	a	k8xC	a
námluvy	námluva	k1gFnPc1	námluva
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snese	snést	k5eAaPmIp3nS	snést
asi	asi	k9	asi
čtyři	čtyři	k4xCgNnPc1	čtyři
bílá	bílý	k2eAgNnPc1d1	bílé
vejce	vejce	k1gNnPc1	vejce
do	do	k7c2	do
vyhrabané	vyhrabaný	k2eAgFnSc2d1	vyhrabaná
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
následně	následně	k6eAd1	následně
zahrabe	zahrabat	k5eAaPmIp3nS	zahrabat
a	a	k8xC	a
snůšku	snůška	k1gFnSc4	snůška
bedlivě	bedlivě	k6eAd1	bedlivě
střeží	střežit	k5eAaImIp3nS	střežit
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
za	za	k7c4	za
neobvykle	obvykle	k6eNd1	obvykle
dlouho	dlouho	k6eAd1	dlouho
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
po	po	k7c6	po
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
osmi	osm	k4xCc6	osm
měsících	měsíc	k1gInPc6	měsíc
inkubace	inkubace	k1gFnSc2	inkubace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nory	nora	k1gFnSc2	nora
vylézají	vylézat	k5eAaImIp3nP	vylézat
příhodně	příhodně	k6eAd1	příhodně
v	v	k7c6	v
období	období	k1gNnSc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
a	a	k8xC	a
olizují	olizovat	k5eAaImIp3nP	olizovat
vláhu	vláha	k1gFnSc4	vláha
z	z	k7c2	z
mokrých	mokrý	k2eAgInPc2d1	mokrý
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
převážně	převážně	k6eAd1	převážně
býložravec	býložravec	k1gMnSc1	býložravec
<g/>
,	,	kIx,	,
živící	živící	k2eAgMnSc1d1	živící
se	se	k3xPyFc4	se
listy	list	k1gInPc1	list
a	a	k8xC	a
plody	plod	k1gInPc1	plod
stromů	strom	k1gInPc2	strom
nebo	nebo	k8xC	nebo
keřů	keř	k1gInPc2	keř
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
květy	květ	k1gInPc4	květ
ibišku	ibišek	k1gInSc2	ibišek
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejichž	jejichž	k3xOyRp3gInPc6	jejichž
stromech	strom	k1gInPc6	strom
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ohrožení	ohrožení	k1gNnSc1	ohrožení
===	===	k?	===
</s>
</p>
<p>
<s>
Úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
nejen	nejen	k6eAd1	nejen
těchto	tento	k3xDgMnPc2	tento
leguánů	leguán	k1gMnPc2	leguán
je	být	k5eAaImIp3nS	být
bezesporu	bezesporu	k9	bezesporu
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
soustavně	soustavně	k6eAd1	soustavně
ničí	ničit	k5eAaImIp3nS	ničit
jejich	jejich	k3xOp3gInSc1	jejich
biotop	biotop	k1gInSc1	biotop
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
deforestací	deforestace	k1gFnSc7	deforestace
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
nechtěnou	chtěný	k2eNgFnSc7d1	nechtěná
kolonizací	kolonizace	k1gFnSc7	kolonizace
invazivních	invazivní	k2eAgInPc2d1	invazivní
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
nepůvodní	původní	k2eNgInPc1d1	nepůvodní
vysazené	vysazený	k2eAgInPc1d1	vysazený
druhy	druh	k1gInPc1	druh
roslin	roslina	k1gFnPc2	roslina
<g/>
;	;	kIx,	;
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
divoká	divoký	k2eAgFnSc1d1	divoká
mimóza	mimóza	k1gFnSc1	mimóza
(	(	kIx(	(
<g/>
Leucaena	Leucaena	k1gFnSc1	Leucaena
leococephala	leococephat	k5eAaBmAgFnS	leococephat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
bílými	bílý	k2eAgInPc7d1	bílý
hlávkovými	hlávkový	k2eAgInPc7d1	hlávkový
květy	květ	k1gInPc7	květ
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
lusky	lusk	k1gInPc1	lusk
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
leguány	leguán	k1gMnPc4	leguán
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
.	.	kIx.	.
</s>
<s>
Problematická	problematický	k2eAgFnSc1d1	problematická
bývá	bývat	k5eAaImIp3nS	bývat
i	i	k9	i
predace	predace	k1gFnSc1	predace
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
divokých	divoký	k2eAgFnPc2d1	divoká
koz	koza	k1gFnPc2	koza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
už	už	k9	už
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hrozeb	hrozba	k1gFnPc2	hrozba
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
několik	několik	k4yIc1	několik
nepůvodních	původní	k2eNgMnPc2d1	nepůvodní
predátorů	predátor	k1gMnPc2	predátor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k9	třeba
kočky	kočka	k1gFnPc1	kočka
<g/>
,	,	kIx,	,
krysy	krysa	k1gFnPc1	krysa
nebo	nebo	k8xC	nebo
promyky	promyka	k1gFnPc1	promyka
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgFnPc4	ten
přeživší	přeživší	k2eAgFnPc4d1	přeživší
populaci	populace	k1gFnSc4	populace
leguánů	leguán	k1gMnPc2	leguán
ohrožují	ohrožovat	k5eAaImIp3nP	ohrožovat
asi	asi	k9	asi
nejvíce	nejvíce	k6eAd1	nejvíce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
a	a	k8xC	a
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
ochrany	ochrana	k1gFnSc2	ochrana
zdejší	zdejší	k2eAgFnSc2d1	zdejší
fauny	fauna	k1gFnSc2	fauna
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
Fidži	Fidž	k1gFnSc3	Fidž
přísně	přísně	k6eAd1	přísně
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
pohyb	pohyb	k1gInSc4	pohyb
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
neobydlených	obydlený	k2eNgInPc6d1	neobydlený
ostrůvcích	ostrůvek	k1gInPc6	ostrůvek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
na	na	k7c4	na
Yadua	Yadu	k2eAgMnSc4d1	Yadu
Taba	Tabus	k1gMnSc4	Tabus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
Fidži	Fidž	k1gFnSc3	Fidž
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
12	[number]	k4	12
až	až	k9	až
14	[number]	k4	14
tisíc	tisíc	k4xCgInSc4	tisíc
leguánů	leguán	k1gMnPc2	leguán
chocholatých	chocholatý	k2eAgMnPc2d1	chocholatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgInSc4d1	chocholatý
na	na	k7c4	na
arkive	arkiev	k1gFnPc4	arkiev
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
<p>
<s>
Leguán	leguán	k1gMnSc1	leguán
choholatý	choholatý	k2eAgMnSc1d1	choholatý
na	na	k7c4	na
iucnredlist	iucnredlist	k1gInSc4	iucnredlist
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
</p>
