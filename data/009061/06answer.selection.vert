<s>
Leguán	leguán	k1gMnSc1	leguán
chocholatý	chocholatý	k2eAgMnSc1d1	chocholatý
(	(	kIx(	(
<g/>
Brachylophus	Brachylophus	k1gInSc1	Brachylophus
vitiensis	vitiensis	k1gFnSc2	vitiensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
fidžijský	fidžijský	k2eAgInSc1d1	fidžijský
chocholatý	chocholatý	k2eAgInSc1d1	chocholatý
nebo	nebo	k8xC	nebo
fidžský	fidžský	k2eAgInSc1d1	fidžský
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ještěr	ještěr	k1gMnSc1	ještěr
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
leguánovití	leguánovitý	k2eAgMnPc1d1	leguánovitý
a	a	k8xC	a
rodu	rod	k1gInSc2	rod
Brachylophus	Brachylophus	k1gInSc4	Brachylophus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
zástupci	zástupce	k1gMnPc1	zástupce
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
zeleno-modře	zelenoodro	k6eAd1	zeleno-modro
zbarveni	zbarvit	k5eAaPmNgMnP	zbarvit
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
pobřežních	pobřežní	k2eAgInPc6d1	pobřežní
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
