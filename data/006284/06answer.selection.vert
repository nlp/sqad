<s>
Zbraní	zbraň	k1gFnSc7	zbraň
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
použitou	použitý	k2eAgFnSc4d1	použitá
ke	k	k7c3	k
spáchání	spáchání	k1gNnSc3	spáchání
atentátu	atentát	k1gInSc2	atentát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
puška	puška	k1gFnSc1	puška
(	(	kIx(	(
<g/>
karabina	karabina	k1gFnSc1	karabina
<g/>
)	)	kIx)	)
italské	italský	k2eAgFnSc2d1	italská
výroby	výroba	k1gFnSc2	výroba
–	–	k?	–
Carcano	Carcana	k1gFnSc5	Carcana
M	M	kA	M
<g/>
91	[number]	k4	91
<g/>
/	/	kIx~	/
<g/>
38	[number]	k4	38
ráže	ráže	k1gFnSc1	ráže
6,52	[number]	k4	6,52
x	x	k?	x
52	[number]	k4	52
mm	mm	kA	mm
<g/>
,	,	kIx,	,
se	s	k7c7	s
sériovým	sériový	k2eAgNnSc7d1	sériové
číslem	číslo	k1gNnSc7	číslo
C	C	kA	C
<g/>
2766	[number]	k4	2766
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
současnými	současný	k2eAgFnPc7d1	současná
puškami	puška	k1gFnPc7	puška
a	a	k8xC	a
karabinami	karabina	k1gFnPc7	karabina
nedá	dát	k5eNaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
jako	jako	k9	jako
o	o	k7c4	o
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
výkonné	výkonný	k2eAgInPc1d1	výkonný
či	či	k8xC	či
přesné	přesný	k2eAgInPc1d1	přesný
<g/>
.	.	kIx.	.
</s>
