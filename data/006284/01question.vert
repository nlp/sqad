<s>
Kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
puška	puška	k1gFnSc1	puška
Carcano	Carcana	k1gFnSc5	Carcana
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Kennedyho	Kennedy	k1gMnSc4	Kennedy
<g/>
?	?	kIx.	?
</s>
