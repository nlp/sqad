<p>
<s>
Johann	Johann	k1gMnSc1	Johann
Christoph	Christoph	k1gMnSc1	Christoph
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Schiller	Schiller	k1gMnSc1	Schiller
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1759	[number]	k4	1759
<g/>
,	,	kIx,	,
Marbach	Marbach	k1gInSc1	Marbach
am	am	k?	am
Neckar	Neckar	k1gInSc1	Neckar
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
Výmar	Výmar	k1gInSc1	Výmar
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
estetik	estetik	k1gMnSc1	estetik
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
vedoucí	vedoucí	k2eAgMnSc1d1	vedoucí
představitel	představitel	k1gMnSc1	představitel
německé	německý	k2eAgFnSc2d1	německá
klasiky	klasika	k1gFnSc2	klasika
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dramatické	dramatický	k2eAgNnSc1d1	dramatické
dílo	dílo	k1gNnSc1	dílo
patetickým	patetický	k2eAgInSc7d1	patetický
způsobem	způsob	k1gInSc7	způsob
hájí	hájit	k5eAaImIp3nP	hájit
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
práva	práv	k2eAgMnSc4d1	práv
člověka	člověk	k1gMnSc4	člověk
a	a	k8xC	a
lidské	lidský	k2eAgNnSc4d1	lidské
bratrství	bratrství	k1gNnSc4	bratrství
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Jeho	jeho	k3xOp3gInPc1	jeho
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
utvářely	utvářet	k5eAaImAgInP	utvářet
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
Rousseaua	Rousseau	k1gMnSc2	Rousseau
<g/>
,	,	kIx,	,
Lessinga	Lessing	k1gMnSc2	Lessing
a	a	k8xC	a
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
estetice	estetika	k1gFnSc6	estetika
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Kanta	Kant	k1gMnSc2	Kant
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnPc4	umění
považoval	považovat	k5eAaImAgInS	považovat
za	za	k7c4	za
prostředek	prostředek	k1gInSc4	prostředek
formování	formování	k1gNnSc2	formování
harmonické	harmonický	k2eAgFnSc2d1	harmonická
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svobodně	svobodně	k6eAd1	svobodně
tvoří	tvořit	k5eAaImIp3nS	tvořit
dobro	dobro	k1gNnSc4	dobro
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
pouze	pouze	k6eAd1	pouze
umění	umění	k1gNnSc1	umění
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
člověku	člověk	k1gMnSc3	člověk
získat	získat	k5eAaPmF	získat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1759	[number]	k4	1759
v	v	k7c6	v
Marbachu	Marbach	k1gInSc6	Marbach
ve	v	k7c6	v
Württembersku	Württembersko	k1gNnSc6	Württembersko
<g/>
.	.	kIx.	.
</s>
<s>
Schillerův	Schillerův	k2eAgMnSc1d1	Schillerův
otec	otec	k1gMnSc1	otec
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Johann	Johann	k1gMnSc1	Johann
Kaspar	Kaspar	k1gMnSc1	Kaspar
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
důstojníkem	důstojník	k1gMnSc7	důstojník
ve	v	k7c6	v
službě	služba	k1gFnSc6	služba
Karla	Karel	k1gMnSc2	Karel
Evžena	Evžen	k1gMnSc2	Evžen
<g/>
,	,	kIx,	,
místního	místní	k2eAgMnSc2d1	místní
vévody	vévoda	k1gMnSc2	vévoda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patřil	patřit	k5eAaImAgMnS	patřit
k	k	k7c3	k
nejhorším	zlý	k2eAgMnPc3d3	nejhorší
tyranům	tyran	k1gMnPc3	tyran
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
Schiller	Schiller	k1gMnSc1	Schiller
složil	složit	k5eAaPmAgMnS	složit
zemské	zemský	k2eAgFnSc2d1	zemská
zkoušky	zkouška	k1gFnSc2	zkouška
a	a	k8xC	a
otevřel	otevřít	k5eAaPmAgInS	otevřít
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
cestu	cesta	k1gFnSc4	cesta
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
chtěl	chtít	k5eAaImAgMnS	chtít
studovat	studovat	k5eAaImF	studovat
teologii	teologie	k1gFnSc4	teologie
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
na	na	k7c4	na
rozkaz	rozkaz	k1gInSc4	rozkaz
vévody	vévoda	k1gMnSc2	vévoda
nastoupit	nastoupit	k5eAaPmF	nastoupit
do	do	k7c2	do
Karlsschule	Karlsschule	k1gFnSc2	Karlsschule
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
celé	celý	k2eAgNnSc4d1	celé
své	svůj	k3xOyFgNnSc4	svůj
mládí	mládí	k1gNnSc4	mládí
–	–	k?	–
žil	žít	k5eAaImAgMnS	žít
zde	zde	k6eAd1	zde
od	od	k7c2	od
třinácti	třináct	k4xCc2	třináct
do	do	k7c2	do
jednadvaceti	jednadvacet	k4xCc2	jednadvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celou	celá	k1gFnSc4	celá
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
neměl	mít	k5eNaImAgMnS	mít
jedinou	jediný	k2eAgFnSc4d1	jediná
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgMnPc4	svůj
sourozence	sourozenec	k1gMnPc4	sourozenec
neviděl	vidět	k5eNaImAgMnS	vidět
ani	ani	k8xC	ani
jednou	jednou	k6eAd1	jednou
a	a	k8xC	a
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
při	při	k7c6	při
návštěvách	návštěva	k1gFnPc6	návštěva
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
ale	ale	k9	ale
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
i	i	k9	i
dozorčí	dozorčí	k1gMnPc1	dozorčí
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
pořádek	pořádek	k1gInSc1	pořádek
akademie	akademie	k1gFnSc2	akademie
prostě	prostě	k9	prostě
zastupoval	zastupovat	k5eAaImAgMnS	zastupovat
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
proto	proto	k8xC	proto
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
-	-	kIx~	-
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
hned	hned	k6eAd1	hned
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
prvním	první	k4xOgInSc6	první
dramatu	drama	k1gNnSc6	drama
Loupežníci	loupežník	k1gMnPc1	loupežník
(	(	kIx(	(
<g/>
první	první	k4xOgNnSc1	první
uvedení	uvedení	k1gNnSc1	uvedení
1782	[number]	k4	1782
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
slavným	slavný	k2eAgInSc7d1	slavný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uveřejnění	uveřejnění	k1gNnSc6	uveřejnění
hry	hra	k1gFnSc2	hra
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
stihl	stihnout	k5eAaPmAgInS	stihnout
zákaz	zákaz	k1gInSc1	zákaz
publikování	publikování	k1gNnSc2	publikování
a	a	k8xC	a
Schiller	Schiller	k1gMnSc1	Schiller
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Mannheimu	Mannheim	k1gInSc2	Mannheim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1784	[number]	k4	1784
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
měl	mít	k5eAaImAgMnS	mít
Schiller	Schiller	k1gMnSc1	Schiller
finanční	finanční	k2eAgFnSc4d1	finanční
nouzi	nouze	k1gFnSc4	nouze
a	a	k8xC	a
také	také	k9	také
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
nadějemi	naděje	k1gFnPc7	naděje
roku	rok	k1gInSc2	rok
1787	[number]	k4	1787
navštívil	navštívit	k5eAaPmAgInS	navštívit
Výmar	Výmar	k1gInSc1	Výmar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sice	sice	k8xC	sice
poznal	poznat	k5eAaPmAgMnS	poznat
Goetha	Goeth	k1gMnSc4	Goeth
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesblížili	sblížit	k5eNaPmAgMnP	sblížit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Schiller	Schiller	k1gMnSc1	Schiller
stal	stát	k5eAaPmAgMnS	stát
profesorem	profesor	k1gMnSc7	profesor
pro	pro	k7c4	pro
světové	světový	k2eAgFnPc4d1	světová
dějiny	dějiny	k1gFnPc4	dějiny
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
tuberkulózou	tuberkulóza	k1gFnSc7	tuberkulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
se	se	k3xPyFc4	se
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
s	s	k7c7	s
Charlotte	Charlott	k1gInSc5	Charlott
von	von	k1gInSc1	von
Lengefeld	Lengefeldo	k1gNnPc2	Lengefeldo
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
také	také	k9	také
roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
vzal	vzít	k5eAaPmAgMnS	vzít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
se	se	k3xPyFc4	se
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Schiller	Schiller	k1gMnSc1	Schiller
potkali	potkat	k5eAaPmAgMnP	potkat
v	v	k7c6	v
Jeně	Jena	k1gFnSc6	Jena
a	a	k8xC	a
hovořili	hovořit	k5eAaImAgMnP	hovořit
spolu	spolu	k6eAd1	spolu
po	po	k7c6	po
přednášce	přednáška	k1gFnSc6	přednáška
<g/>
,	,	kIx,	,
během	během	k7c2	během
rozhovoru	rozhovor	k1gInSc2	rozhovor
se	se	k3xPyFc4	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
přátelství	přátelství	k1gNnSc3	přátelství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pro	pro	k7c4	pro
oba	dva	k4xCgMnPc4	dva
znamenalo	znamenat	k5eAaImAgNnS	znamenat
velké	velký	k2eAgNnSc4d1	velké
duševní	duševní	k2eAgNnSc4d1	duševní
obohacení	obohacení	k1gNnSc4	obohacení
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Herderem	Herder	k1gInSc7	Herder
a	a	k8xC	a
Wielandem	Wieland	k1gInSc7	Wieland
tvořili	tvořit	k5eAaImAgMnP	tvořit
čtveřici	čtveřice	k1gFnSc6	čtveřice
tzv.	tzv.	kA	tzv.
výmarské	výmarský	k2eAgFnSc2d1	Výmarská
klasiky	klasika	k1gFnSc2	klasika
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1797	[number]	k4	1797
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgInS	nazývat
rokem	rok	k1gInSc7	rok
balad	balada	k1gFnPc2	balada
–	–	k?	–
Schiller	Schiller	k1gMnSc1	Schiller
i	i	k9	i
Goethe	Goethe	k1gInSc4	Goethe
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
mnoho	mnoho	k4c4	mnoho
balad	balada	k1gFnPc2	balada
každý	každý	k3xTgInSc1	každý
sám	sám	k3xTgInSc4	sám
i	i	k9	i
dohromady	dohromady	k6eAd1	dohromady
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
Schiller	Schiller	k1gMnSc1	Schiller
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
do	do	k7c2	do
Výmaru	Výmar	k1gInSc2	Výmar
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Schiller	Schiller	k1gMnSc1	Schiller
zemřel	zemřít	k5eAaPmAgMnS	zemřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Denní	denní	k2eAgInSc1d1	denní
program	program	k1gInSc1	program
v	v	k7c6	v
Karlsschule	Karlsschula	k1gFnSc6	Karlsschula
===	===	k?	===
</s>
</p>
<p>
<s>
Den	den	k1gInSc1	den
začínal	začínat	k5eAaImAgInS	začínat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
kasárnách	kasárny	k1gFnPc6	kasárny
–	–	k?	–
v	v	k7c4	v
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
v	v	k7c4	v
šest	šest	k4xCc4	šest
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
byl	být	k5eAaImAgInS	být
budíček	budíček	k1gInSc1	budíček
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
rychlé	rychlý	k2eAgNnSc4d1	rychlé
vstávání	vstávání	k1gNnSc4	vstávání
<g/>
,	,	kIx,	,
mytí	mytí	k1gNnSc4	mytí
<g/>
,	,	kIx,	,
oblékání	oblékání	k1gNnSc4	oblékání
a	a	k8xC	a
holení	holení	k1gNnSc4	holení
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
budíčku	budíček	k1gInSc6	budíček
následovala	následovat	k5eAaImAgFnS	následovat
ranní	ranní	k2eAgFnSc1d1	ranní
modlitba	modlitba	k1gFnSc1	modlitba
a	a	k8xC	a
snídaně	snídaně	k1gFnSc1	snídaně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
bílé	bílý	k2eAgFnSc2d1	bílá
polévky	polévka	k1gFnSc2	polévka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
od	od	k7c2	od
osmi	osm	k4xCc2	osm
<g/>
,	,	kIx,	,
probíhalo	probíhat	k5eAaImAgNnS	probíhat
do	do	k7c2	do
jedenácti	jedenáct	k4xCc2	jedenáct
vyučování	vyučování	k1gNnPc2	vyučování
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
hodina	hodina	k1gFnSc1	hodina
k	k	k7c3	k
údržbě	údržba	k1gFnSc3	údržba
uniforem	uniforma	k1gFnPc2	uniforma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
bezchybném	bezchybný	k2eAgInSc6d1	bezchybný
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
raport	raport	k1gInSc1	raport
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ujímal	ujímat	k5eAaImAgMnS	ujímat
sám	sám	k3xTgMnSc1	sám
vévoda	vévoda	k1gMnSc1	vévoda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
seskupení	seskupení	k1gNnSc6	seskupení
-	-	kIx~	-
tak	tak	k9	tak
žáci	žák	k1gMnPc1	žák
napochodovali	napochodovat	k5eAaBmAgMnP	napochodovat
do	do	k7c2	do
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
.	.	kIx.	.
</s>
<s>
Oběd	oběd	k1gInSc1	oběd
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
žák	žák	k1gMnSc1	žák
dostal	dostat	k5eAaPmAgMnS	dostat
denně	denně	k6eAd1	denně
půl	půl	k1xP	půl
libry	libra	k1gFnSc2	libra
masa	maso	k1gNnSc2	maso
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
musel	muset	k5eAaImAgMnS	muset
každý	každý	k3xTgMnSc1	každý
vzít	vzít	k5eAaPmF	vzít
tiše	tiš	k1gFnSc2	tiš
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pití	pití	k1gNnSc3	pití
bylo	být	k5eAaImAgNnS	být
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kyselé	kyselý	k2eAgNnSc4d1	kyselé
víno	víno	k1gNnSc4	víno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
následovala	následovat	k5eAaImAgFnS	následovat
procházka	procházka	k1gFnSc1	procházka
<g/>
,	,	kIx,	,
za	za	k7c2	za
špatného	špatný	k2eAgNnSc2d1	špatné
počasí	počasí	k1gNnSc2	počasí
cvičení	cvičení	k1gNnSc2	cvičení
uvnitř	uvnitř	k7c2	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
probíhalo	probíhat	k5eAaImAgNnS	probíhat
až	až	k9	až
do	do	k7c2	do
půl	půl	k1xP	půl
sedmé	sedmý	k4xOgFnSc2	sedmý
vyučování	vyučování	k1gNnPc2	vyučování
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
měli	mít	k5eAaImAgMnP	mít
žáci	žák	k1gMnPc1	žák
hodinu	hodina	k1gFnSc4	hodina
volno	volno	k6eAd1	volno
<g/>
.	.	kIx.	.
</s>
<s>
Večeře	večeře	k1gFnSc1	večeře
bývala	bývat	k5eAaImAgFnS	bývat
celkem	celkem	k6eAd1	celkem
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
,	,	kIx,	,
k	k	k7c3	k
pití	pití	k1gNnSc3	pití
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
jen	jen	k9	jen
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
devět	devět	k4xCc4	devět
hodin	hodina	k1gFnPc2	hodina
už	už	k6eAd1	už
všichni	všechen	k3xTgMnPc1	všechen
museli	muset	k5eAaImAgMnP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Schiller	Schiller	k1gMnSc1	Schiller
básníkem	básník	k1gMnSc7	básník
svobody	svoboda	k1gFnSc2	svoboda
===	===	k?	===
</s>
</p>
<p>
<s>
Schillerův	Schillerův	k2eAgInSc1d1	Schillerův
vývoj	vývoj	k1gInSc1	vývoj
byl	být	k5eAaImAgInS	být
poměry	poměra	k1gFnSc2	poměra
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
škole	škola	k1gFnSc6	škola
velmi	velmi	k6eAd1	velmi
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
<g/>
,	,	kIx,	,
vzpíral	vzpírat	k5eAaImAgMnS	vzpírat
se	se	k3xPyFc4	se
vojenskému	vojenský	k2eAgInSc3d1	vojenský
drilu	dril	k1gInSc3	dril
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
školní	školní	k2eAgFnPc1d1	školní
výsledky	výsledek	k1gInPc4	výsledek
byly	být	k5eAaImAgFnP	být
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
horší	zlý	k2eAgMnSc1d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
medicínu	medicína	k1gFnSc4	medicína
(	(	kIx(	(
<g/>
1776	[number]	k4	1776
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nabyl	nabýt	k5eAaPmAgMnS	nabýt
Schiller	Schiller	k1gMnSc1	Schiller
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
na	na	k7c4	na
sobě	se	k3xPyFc3	se
usilovně	usilovně	k6eAd1	usilovně
pracovat	pracovat	k5eAaImF	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Nadšeně	nadšeně	k6eAd1	nadšeně
poslouchal	poslouchat	k5eAaImAgInS	poslouchat
přednášky	přednáška	k1gFnSc2	přednáška
profesora	profesor	k1gMnSc2	profesor
Abela	Abel	k1gMnSc2	Abel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vyučoval	vyučovat	k5eAaImAgMnS	vyučovat
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
.	.	kIx.	.
</s>
<s>
Abel	Abel	k1gMnSc1	Abel
pobízel	pobízet	k5eAaImAgMnS	pobízet
své	svůj	k3xOyFgMnPc4	svůj
žáky	žák	k1gMnPc4	žák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
sami	sám	k3xTgMnPc1	sám
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vychovat	vychovat	k5eAaPmF	vychovat
osvícenské	osvícenský	k2eAgInPc4d1	osvícenský
a	a	k8xC	a
"	"	kIx"	"
<g/>
dobré	dobrý	k2eAgMnPc4d1	dobrý
<g/>
"	"	kIx"	"
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Popisoval	popisovat	k5eAaImAgMnS	popisovat
jim	on	k3xPp3gMnPc3	on
ideu	idea	k1gFnSc4	idea
génia	génius	k1gMnSc4	génius
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
poslouchá	poslouchat	k5eAaImIp3nS	poslouchat
jen	jen	k9	jen
své	svůj	k3xOyFgInPc4	svůj
city	cit	k1gInPc4	cit
a	a	k8xC	a
veškerá	veškerý	k3xTgNnPc1	veškerý
pravidla	pravidlo	k1gNnPc1	pravidlo
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
protiví	protivit	k5eAaImIp3nS	protivit
<g/>
.	.	kIx.	.
</s>
<s>
Šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
už	už	k6eAd1	už
léta	léto	k1gNnPc4	léto
psal	psát	k5eAaImAgMnS	psát
básně	báseň	k1gFnPc4	báseň
a	a	k8xC	a
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
býti	být	k5eAaImF	být
básníkem	básník	k1gMnSc7	básník
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
životní	životní	k2eAgNnSc4d1	životní
povolání	povolání	k1gNnSc4	povolání
<g/>
,	,	kIx,	,
cítil	cítit	k5eAaImAgMnS	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
Abelova	Abelův	k2eAgNnPc1d1	Abelův
slova	slovo	k1gNnPc1	slovo
jen	jen	k9	jen
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Zapálený	zapálený	k2eAgMnSc1d1	zapálený
básník	básník	k1gMnSc1	básník
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
tajný	tajný	k2eAgInSc4d1	tajný
spolek	spolek	k1gInSc4	spolek
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
Schillerových	Schillerových	k2eAgNnPc2d1	Schillerových
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
Óda	óda	k1gFnSc1	óda
na	na	k7c4	na
radost	radost	k1gFnSc4	radost
(	(	kIx(	(
<g/>
Ode	ode	k7c2	ode
an	an	k?	an
die	die	k?	die
Freude	Freud	k1gMnSc5	Freud
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zhudebněná	zhudebněný	k2eAgFnSc1d1	zhudebněná
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Ludwigem	Ludwig	k1gMnSc7	Ludwig
van	vana	k1gFnPc2	vana
Beethovenem	Beethoven	k1gMnSc7	Beethoven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
===	===	k?	===
</s>
</p>
<p>
<s>
Loupežníci	loupežník	k1gMnPc1	loupežník
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Räuber	Räuber	k1gMnSc1	Räuber
<g/>
,	,	kIx,	,
1781	[number]	k4	1781
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
K	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
Loupežníků	loupežník	k1gMnPc2	loupežník
Schillera	Schiller	k1gMnSc2	Schiller
podnítila	podnítit	k5eAaPmAgFnS	podnítit
povídka	povídka	k1gFnSc1	povídka
básníka	básník	k1gMnSc2	básník
Christiana	Christian	k1gMnSc2	Christian
Friedricha	Friedrich	k1gMnSc2	Friedrich
Daniela	Daniel	k1gMnSc2	Daniel
Schubarta	Schubart	k1gMnSc2	Schubart
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
scény	scéna	k1gFnPc1	scéna
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
patrně	patrně	k6eAd1	patrně
už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
<g/>
.	.	kIx.	.
</s>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
textu	text	k1gInSc6	text
bez	bez	k7c2	bez
ustání	ustání	k1gNnSc2	ustání
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
po	po	k7c6	po
nocích	noc	k1gFnPc6	noc
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgInPc2	jenž
riskoval	riskovat	k5eAaBmAgInS	riskovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
při	při	k7c6	při
takové	takový	k3xDgFnSc6	takový
zakázané	zakázaný	k2eAgFnSc6d1	zakázaná
činnosti	činnost	k1gFnSc6	činnost
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgInSc2	jeden
květnového	květnový	k2eAgInSc2d1	květnový
dne	den	k1gInSc2	den
roku	rok	k1gInSc2	rok
1778	[number]	k4	1778
se	se	k3xPyFc4	se
Schiller	Schiller	k1gMnSc1	Schiller
s	s	k7c7	s
několika	několik	k4yIc7	několik
kamarády	kamarád	k1gMnPc7	kamarád
oddělil	oddělit	k5eAaPmAgMnS	oddělit
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
ve	v	k7c6	v
volnů	voln	k1gInPc2	voln
chvíli	chvíle	k1gFnSc4	chvíle
jim	on	k3xPp3gMnPc3	on
část	část	k1gFnSc4	část
textu	text	k1gInSc2	text
přednesl	přednést	k5eAaPmAgInS	přednést
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
jeho	jeho	k3xOp3gMnPc2	jeho
současníků	současník	k1gMnPc2	současník
se	se	k3xPyFc4	se
připravované	připravovaný	k2eAgNnSc1d1	připravované
drama	drama	k1gNnSc1	drama
okamžitě	okamžitě	k6eAd1	okamžitě
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
vyšla	vyjít	k5eAaPmAgFnS	vyjít
tiskem	tisk	k1gInSc7	tisk
anonymně	anonymně	k6eAd1	anonymně
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc4	uvedení
textu	text	k1gInSc2	text
na	na	k7c6	na
jevišti	jeviště	k1gNnSc6	jeviště
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Mannheimu	Mannheim	k1gInSc6	Mannheim
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
ředitel	ředitel	k1gMnSc1	ředitel
však	však	k9	však
trval	trvat	k5eAaImAgMnS	trvat
na	na	k7c4	na
okleštění	okleštění	k1gNnSc4	okleštění
kontroverzního	kontroverzní	k2eAgInSc2d1	kontroverzní
textu	text	k1gInSc2	text
a	a	k8xC	a
zmírnění	zmírnění	k1gNnSc4	zmírnění
některých	některý	k3yIgFnPc2	některý
pasáží	pasáž	k1gFnPc2	pasáž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lehce	lehko	k6eAd1	lehko
pozměněné	pozměněný	k2eAgFnSc6d1	pozměněná
<g/>
,	,	kIx,	,
jen	jen	k9	jen
zčásti	zčásti	k6eAd1	zčásti
Schillerem	Schiller	k1gMnSc7	Schiller
odsouhlasené	odsouhlasený	k2eAgFnSc2d1	odsouhlasená
podobě	podoba	k1gFnSc6	podoba
si	se	k3xPyFc3	se
hra	hra	k1gFnSc1	hra
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1782	[number]	k4	1782
odbyla	odbýt	k5eAaPmAgFnS	odbýt
v	v	k7c6	v
Mannheimu	Mannheim	k1gInSc6	Mannheim
světovou	světový	k2eAgFnSc4d1	světová
premiéru	premiéra	k1gFnSc4	premiéra
a	a	k8xC	a
okamžitě	okamžitě	k6eAd1	okamžitě
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
strhla	strhnout	k5eAaPmAgFnS	strhnout
pozornost	pozornost	k1gFnSc1	pozornost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
hrdina	hrdina	k1gMnSc1	hrdina
Karl	Karl	k1gMnSc1	Karl
Moor	Moor	k1gMnSc1	Moor
<g/>
,	,	kIx,	,
prvorozený	prvorozený	k2eAgMnSc1d1	prvorozený
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
oblíbenec	oblíbenec	k1gMnSc1	oblíbenec
starého	starý	k2eAgMnSc2d1	starý
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Mooru	Moor	k1gInSc2	Moor
<g/>
,	,	kIx,	,
tráví	trávit	k5eAaImIp3nP	trávit
čas	čas	k1gInSc4	čas
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
Lipsku	Lipsko	k1gNnSc6	Lipsko
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc1d1	vlastní
rozmarnost	rozmarnost	k1gFnSc1	rozmarnost
a	a	k8xC	a
lehkomyslnost	lehkomyslnost	k1gFnSc1	lehkomyslnost
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
dostanou	dostat	k5eAaPmIp3nP	dostat
do	do	k7c2	do
finančních	finanční	k2eAgFnPc2d1	finanční
obtíží	obtíž	k1gFnPc2	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
odpuštění	odpuštění	k1gNnSc4	odpuštění
se	se	k3xPyFc4	se
Karl	Karl	k1gMnSc1	Karl
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
otce	otec	k1gMnSc4	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
závistivý	závistivý	k2eAgMnSc1d1	závistivý
bratr	bratr	k1gMnSc1	bratr
Franz	Franz	k1gMnSc1	Franz
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
Karla	Karel	k1gMnSc4	Karel
nenávidí	nenávidět	k5eAaImIp3nS	nenávidět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
zneužít	zneužít	k5eAaPmF	zneužít
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
zámeckým	zámecký	k2eAgMnSc7d1	zámecký
pánem	pán	k1gMnSc7	pán
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
Karlovu	Karlův	k2eAgFnSc4d1	Karlova
snoubenku	snoubenka	k1gFnSc4	snoubenka
Amálii	Amálie	k1gFnSc3	Amálie
přečte	přečíst	k5eAaPmIp3nS	přečíst
namísto	namísto	k7c2	namísto
Karlova	Karlův	k2eAgInSc2d1	Karlův
dopisu	dopis	k1gInSc2	dopis
s	s	k7c7	s
omluvou	omluva	k1gFnSc7	omluva
otci	otec	k1gMnSc6	otec
podvrženou	podvržený	k2eAgFnSc4d1	podvržená
zprávu	zpráva	k1gFnSc4	zpráva
od	od	k7c2	od
korespondenta	korespondent	k1gMnSc2	korespondent
z	z	k7c2	z
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Karla	Karel	k1gMnSc4	Karel
očerňuje	očerňovat	k5eAaImIp3nS	očerňovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Franzovo	Franzův	k2eAgNnSc4d1	Franzovo
naléhání	naléhání	k1gNnSc4	naléhání
zdrcený	zdrcený	k2eAgMnSc1d1	zdrcený
otec	otec	k1gMnSc1	otec
Karla	Karel	k1gMnSc2	Karel
vyžene	vyhnat	k5eAaPmIp3nS	vyhnat
a	a	k8xC	a
vydědí	vydědit	k5eAaPmIp3nS	vydědit
<g/>
.	.	kIx.	.
</s>
<s>
Karl	Karl	k1gMnSc1	Karl
založí	založit	k5eAaPmIp3nS	založit
loupežnickou	loupežnický	k2eAgFnSc4d1	loupežnická
bandu	banda	k1gFnSc4	banda
a	a	k8xC	a
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
jejím	její	k3xOp3gMnSc7	její
vůdcem	vůdce	k1gMnSc7	vůdce
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
druhy	druh	k1gInPc7	druh
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
znovu	znovu	k6eAd1	znovu
pokusit	pokusit	k5eAaPmF	pokusit
o	o	k7c6	o
usmíření	usmíření	k1gNnSc6	usmíření
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
<g/>
.	.	kIx.	.
</s>
<s>
Otce	Otka	k1gFnSc3	Otka
však	však	k9	však
nalézá	nalézat	k5eAaImIp3nS	nalézat
uvězněného	uvězněný	k2eAgMnSc4d1	uvězněný
ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jej	on	k3xPp3gMnSc4	on
zavřel	zavřít	k5eAaPmAgMnS	zavřít
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
moci	moc	k1gFnSc2	moc
Franz	Franza	k1gFnPc2	Franza
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
otec	otec	k1gMnSc1	otec
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Karl	Karl	k1gMnSc1	Karl
stal	stát	k5eAaPmAgMnS	stát
loupežníkem	loupežník	k1gMnSc7	loupežník
<g/>
,	,	kIx,	,
umírá	umírat	k5eAaImIp3nS	umírat
žalem	žal	k1gInSc7	žal
<g/>
.	.	kIx.	.
</s>
<s>
Franz	Franz	k1gMnSc1	Franz
dostane	dostat	k5eAaPmIp3nS	dostat
před	před	k7c7	před
loupežníky	loupežník	k1gMnPc7	loupežník
strach	strach	k1gInSc4	strach
a	a	k8xC	a
oběsí	oběsit	k5eAaPmIp3nP	oběsit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Loupežníci	loupežník	k1gMnPc1	loupežník
nakonec	nakonec	k6eAd1	nakonec
celý	celý	k2eAgInSc4d1	celý
zámek	zámek	k1gInSc4	zámek
podpálí	podpálit	k5eAaPmIp3nS	podpálit
<g/>
.	.	kIx.	.
</s>
<s>
Amalia	Amalia	k1gFnSc1	Amalia
Karla	Karel	k1gMnSc2	Karel
stále	stále	k6eAd1	stále
miluje	milovat	k5eAaImIp3nS	milovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
však	však	k9	však
coby	coby	k?	coby
loupežník	loupežník	k1gMnSc1	loupežník
vázán	vázat	k5eAaImNgMnS	vázat
přísahou	přísaha	k1gFnSc7	přísaha
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
se	se	k3xPyFc4	se
Karl	Karl	k1gMnSc1	Karl
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
snoubence	snoubenka	k1gFnSc3	snoubenka
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Amália	Amália	k1gFnSc1	Amália
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nechce	chtít	k5eNaImIp3nS	chtít
bez	bez	k7c2	bez
Karla	Karel	k1gMnSc2	Karel
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
ji	on	k3xPp3gFnSc4	on
poslechne	poslechnout	k5eAaPmIp3nS	poslechnout
<g/>
.	.	kIx.	.
</s>
<s>
Neboť	neboť	k8xC	neboť
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
<g/>
,	,	kIx,	,
co	co	k8xS	co
vše	všechen	k3xTgNnSc1	všechen
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
se	se	k3xPyFc4	se
Karl	Karlum	k1gNnPc2	Karlum
vydat	vydat	k5eAaPmF	vydat
řádnému	řádný	k2eAgInSc3d1	řádný
soudu	soud	k1gInSc3	soud
<g/>
.	.	kIx.	.
</s>
<s>
Potká	potkat	k5eAaPmIp3nS	potkat
chudého	chudý	k1gMnSc4	chudý
muže	muž	k1gMnSc4	muž
a	a	k8xC	a
řekne	říct	k5eAaPmIp3nS	říct
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
odvedl	odvést	k5eAaPmAgMnS	odvést
na	na	k7c4	na
policii	policie	k1gFnSc4	policie
<g/>
.	.	kIx.	.
</s>
<s>
Muž	muž	k1gMnSc1	muž
splní	splnit	k5eAaPmIp3nS	splnit
jeho	jeho	k3xOp3gNnSc4	jeho
přání	přání	k1gNnSc4	přání
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
vypsanou	vypsaný	k2eAgFnSc4d1	vypsaná
odměnu	odměna	k1gFnSc4	odměna
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
je	být	k5eAaImIp3nS	být
popraven	popraven	k2eAgMnSc1d1	popraven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fiesco	Fiesco	k1gMnSc1	Fiesco
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
janovské	janovský	k2eAgNnSc1d1	janovské
spiknutí	spiknutí	k1gNnSc1	spiknutí
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Verschwörung	Verschwörung	k1gMnSc1	Verschwörung
des	des	k1gNnSc2	des
Fiesco	Fiesco	k1gMnSc1	Fiesco
zu	zu	k?	zu
Genua	Genua	k1gMnSc1	Genua
<g/>
,	,	kIx,	,
1783	[number]	k4	1783
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
je	být	k5eAaImIp3nS	být
janovský	janovský	k2eAgMnSc1d1	janovský
dóže	dóže	k1gMnSc1	dóže
Andrea	Andrea	k1gFnSc1	Andrea
Dória	Dórius	k1gMnSc2	Dórius
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
spikne	spiknout	k5eAaPmIp3nS	spiknout
skupina	skupina	k1gFnSc1	skupina
republikánů	republikán	k1gMnPc2	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
náměty	námět	k1gInPc1	námět
jsou	být	k5eAaImIp3nP	být
obrazem	obraz	k1gInSc7	obraz
dobových	dobový	k2eAgInPc2d1	dobový
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
Fiesco	Fiesco	k1gMnSc1	Fiesco
se	se	k3xPyFc4	se
v	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
sám	sám	k3xTgMnSc1	sám
uchopí	uchopit	k5eAaPmIp3nS	uchopit
moci	moct	k5eAaImF	moct
<g/>
,	,	kIx,	,
stane	stanout	k5eAaPmIp3nS	stanout
se	se	k3xPyFc4	se
tyranem	tyran	k1gMnSc7	tyran
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
se	se	k3xPyFc4	se
smíří	smířit	k5eAaPmIp3nS	smířit
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
cyklus	cyklus	k1gInSc4	cyklus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úklady	úklad	k1gInPc1	úklad
a	a	k8xC	a
láska	láska	k1gFnSc1	láska
(	(	kIx(	(
<g/>
Kabale	kabala	k1gFnSc3	kabala
und	und	k?	und
Liebe	Lieb	k1gMnSc5	Lieb
<g/>
,	,	kIx,	,
1784	[number]	k4	1784
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
tragédii	tragédie	k1gFnSc6	tragédie
ztroskotá	ztroskotat	k5eAaPmIp3nS	ztroskotat
láska	láska	k1gFnSc1	láska
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
dívky	dívka	k1gFnSc2	dívka
a	a	k8xC	a
šlechtice	šlechtic	k1gMnSc2	šlechtic
na	na	k7c6	na
intrikách	intrika	k1gFnPc6	intrika
tyranského	tyranský	k2eAgInSc2d1	tyranský
a	a	k8xC	a
nemoralistického	moralistický	k2eNgInSc2d1	moralistický
knížecího	knížecí	k2eAgInSc2d1	knížecí
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Carlos	Carlos	k1gMnSc1	Carlos
(	(	kIx(	(
<g/>
1783	[number]	k4	1783
<g/>
–	–	k?	–
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
látku	látka	k1gFnSc4	látka
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
nizozemského	nizozemský	k2eAgInSc2d1	nizozemský
boje	boj	k1gInSc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
touhu	touha	k1gFnSc4	touha
po	po	k7c4	po
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
míru	míra	k1gFnSc4	míra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
klasicismu	klasicismus	k1gInSc2	klasicismus
===	===	k?	===
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
o	o	k7c6	o
naivním	naivní	k2eAgMnSc6d1	naivní
a	a	k8xC	a
sentimentálním	sentimentální	k2eAgNnSc6d1	sentimentální
básnictví	básnictví	k1gNnSc6	básnictví
(	(	kIx(	(
<g/>
Über	Über	k1gMnSc1	Über
naive	naiev	k1gFnSc2	naiev
und	und	k?	und
sentimentalische	sentimentalischat	k5eAaPmIp3nS	sentimentalischat
Dichtung	Dichtung	k1gInSc1	Dichtung
<g/>
,	,	kIx,	,
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
rozlišil	rozlišit	k5eAaPmAgMnS	rozlišit
dvojí	dvojí	k4xRgFnSc4	dvojí
rovinu	rovina	k1gFnSc4	rovina
básnictví	básnictví	k1gNnSc2	básnictví
a	a	k8xC	a
dramatu	drama	k1gNnSc2	drama
<g/>
:	:	kIx,	:
naivní	naivní	k2eAgFnPc1d1	naivní
a	a	k8xC	a
sentimentální	sentimentální	k2eAgFnPc1d1	sentimentální
</s>
</p>
<p>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
o	o	k7c6	o
Valdštejnovi	Valdštejn	k1gMnSc6	Valdštejn
(	(	kIx(	(
<g/>
Wallenstein-Trilogie	Wallenstein-Trilogie	k1gFnSc2	Wallenstein-Trilogie
<g/>
,	,	kIx,	,
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
táboře	tábor	k1gInSc6	tábor
vojska	vojsko	k1gNnSc2	vojsko
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
(	(	kIx(	(
<g/>
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
dějepise	dějepis	k1gInSc6	dějepis
je	být	k5eAaImIp3nS	být
nazýván	nazýván	k2eAgInSc1d1	nazýván
Wallenstein	Wallenstein	k1gInSc1	Wallenstein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vůbec	vůbec	k9	vůbec
nic	nic	k3yNnSc1	nic
neděje	dít	k5eNaImIp3nS	dít
–	–	k?	–
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
záběr	záběr	k1gInSc4	záběr
–	–	k?	–
vojáci	voják	k1gMnPc1	voják
pijí	pít	k5eAaImIp3nP	pít
<g/>
,	,	kIx,	,
hrají	hrát	k5eAaImIp3nP	hrát
<g/>
,	,	kIx,	,
vypravují	vypravovat	k5eAaImIp3nP	vypravovat
<g/>
,	,	kIx,	,
tlustý	tlustý	k2eAgMnSc1d1	tlustý
kapucín	kapucín	k1gMnSc1	kapucín
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
kázání	kázání	k1gNnSc4	kázání
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
ho	on	k3xPp3gMnSc4	on
vyženou	vyhnat	k5eAaPmIp3nP	vyhnat
–	–	k?	–
v	v	k7c6	v
běžném	běžný	k2eAgNnSc6d1	běžné
dramatu	drama	k1gNnSc6	drama
např.	např.	kA	např.
u	u	k7c2	u
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
–	–	k?	–
tady	tady	k6eAd1	tady
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
ústřední	ústřední	k2eAgNnSc4d1	ústřední
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
připravovat	připravovat	k5eAaImF	připravovat
další	další	k2eAgFnPc4d1	další
části	část	k1gFnPc4	část
–	–	k?	–
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
motivy	motiv	k1gInPc4	motiv
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Schiller	Schiller	k1gMnSc1	Schiller
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
části	část	k1gFnSc6	část
–	–	k?	–
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
přichází	přicházet	k5eAaImIp3nS	přicházet
císařský	císařský	k2eAgInSc4d1	císařský
rozkaz	rozkaz	k1gInSc4	rozkaz
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
má	mít	k5eAaImIp3nS	mít
moc	moc	k6eAd1	moc
nad	nad	k7c7	nad
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
odmítá	odmítat	k5eAaImIp3nS	odmítat
rozkazy	rozkaz	k1gInPc4	rozkaz
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
demisí	demise	k1gFnSc7	demise
<g/>
,	,	kIx,	,
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zachová	zachovat	k5eAaPmIp3nS	zachovat
věrnost	věrnost	k1gFnSc4	věrnost
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
tábora	tábor	k1gInSc2	tábor
začínají	začínat	k5eAaImIp3nP	začínat
vystupovat	vystupovat	k5eAaImF	vystupovat
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
dramatické	dramatický	k2eAgFnPc1d1	dramatická
postavy	postava	k1gFnPc1	postava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
.	.	kIx.	.
</s>
<s>
Řeší	řešit	k5eAaImIp3nP	řešit
zásadní	zásadní	k2eAgFnSc4d1	zásadní
mravní	mravní	k2eAgFnSc4d1	mravní
otázku	otázka	k1gFnSc4	otázka
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
zradit	zradit	k5eAaPmF	zradit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
odevzdat	odevzdat	k5eAaPmF	odevzdat
tyranovi	tyran	k1gMnSc3	tyran
<g/>
?	?	kIx.	?
</s>
<s>
Postava	postava	k1gFnSc1	postava
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
veliká	veliký	k2eAgFnSc1d1	veliká
Schillerova	Schillerův	k2eAgFnSc1d1	Schillerova
postava	postava	k1gFnSc1	postava
<g/>
,	,	kIx,	,
odcizení	odcizení	k1gNnSc1	odcizení
má	mít	k5eAaImIp3nS	mít
mravní	mravní	k2eAgFnSc4d1	mravní
povahu	povaha	k1gFnSc4	povaha
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
zobrazit	zobrazit	k5eAaPmF	zobrazit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zrádcem	zrádce	k1gMnSc7	zrádce
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
vůli	vůle	k1gFnSc3	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
trilogie	trilogie	k1gFnSc2	trilogie
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Valdštejnův	Valdštejnův	k2eAgInSc4d1	Valdštejnův
tábor	tábor	k1gInSc4	tábor
se	se	k3xPyFc4	se
hrála	hrát	k5eAaImAgFnS	hrát
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
postupně	postupně	k6eAd1	postupně
ve	v	k7c6	v
Výmarském	výmarský	k2eAgNnSc6d1	výmarské
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Stuartovna	Stuartovna	k1gFnSc1	Stuartovna
(	(	kIx(	(
<g/>
Maria	Maria	k1gFnSc1	Maria
Stuart	Stuarta	k1gFnPc2	Stuarta
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zápas	zápas	k1gInSc1	zápas
skotské	skotský	k2eAgFnSc2d1	skotská
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
s	s	k7c7	s
anglickou	anglický	k2eAgFnSc7d1	anglická
královnou	královna	k1gFnSc7	královna
Alžbětou	Alžběta	k1gFnSc7	Alžběta
I.	I.	kA	I.
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
zápasem	zápas	k1gInSc7	zápas
katolické	katolický	k2eAgFnSc2d1	katolická
a	a	k8xC	a
protestantské	protestantský	k2eAgFnSc2d1	protestantská
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Dramatičnost	dramatičnost	k1gFnSc1	dramatičnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Valdštějnovi	Valdštějn	k1gMnSc6	Valdštějn
situovaná	situovaný	k2eAgFnSc1d1	situovaná
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
osobě	osoba	k1gFnSc6	osoba
(	(	kIx(	(
<g/>
já	já	k3xPp1nSc1	já
proti	proti	k7c3	proti
sobě	sebe	k3xPyFc6	sebe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
osob	osoba	k1gFnPc2	osoba
<g/>
:	:	kIx,	:
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
Alžběty	Alžběta	k1gFnSc2	Alžběta
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Stuartovny	Stuartovna	k1gFnSc2	Stuartovna
byla	být	k5eAaImAgFnS	být
dlouho	dlouho	k6eAd1	dlouho
chápána	chápat	k5eAaImNgFnS	chápat
jako	jako	k9	jako
oběť	oběť	k1gFnSc1	oběť
<g/>
,	,	kIx,	,
Alžběta	Alžběta	k1gFnSc1	Alžběta
byla	být	k5eAaImAgFnS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
chápána	chápat	k5eAaImNgFnS	chápat
negativně	negativně	k6eAd1	negativně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Schillerově	Schillerův	k2eAgFnSc6d1	Schillerova
hře	hra	k1gFnSc6	hra
dojde	dojít	k5eAaPmIp3nS	dojít
poprvé	poprvé	k6eAd1	poprvé
k	k	k7c3	k
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
aspektů	aspekt	k1gInPc2	aspekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Panna	Panna	k1gFnSc1	Panna
Orleánská	orleánský	k2eAgFnSc1d1	Orleánská
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Jungfrau	Jungfrau	k1gFnSc2	Jungfrau
von	von	k1gInSc1	von
Orléans	Orléans	k1gInSc1	Orléans
<g/>
,	,	kIx,	,
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Postava	postava	k1gFnSc1	postava
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
velmi	velmi	k6eAd1	velmi
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
(	(	kIx(	(
<g/>
v	v	k7c6	v
Shakespearově	Shakespearův	k2eAgFnSc6d1	Shakespearova
hře	hra	k1gFnSc6	hra
Jindřich	Jindřich	k1gMnSc1	Jindřich
VI	VI	kA	VI
<g/>
.	.	kIx.	.
byla	být	k5eAaImAgFnS	být
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
jako	jako	k9	jako
děvka	děvka	k1gFnSc1	děvka
a	a	k8xC	a
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
pomocí	pomocí	k7c2	pomocí
satana	satan	k1gMnSc2	satan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
eposech	epos	k1gInPc6	epos
<g/>
,	,	kIx,	,
bývala	bývat	k5eAaImAgNnP	bývat
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
horším	zlý	k2eAgNnSc6d2	horší
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
než	než	k8xS	než
ji	on	k3xPp3gFnSc4	on
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Schillerova	Schillerův	k2eAgFnSc1d1	Schillerova
hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
rehabilitací	rehabilitace	k1gFnSc7	rehabilitace
Jany	Jana	k1gFnSc2	Jana
<g/>
,	,	kIx,	,
obecenstvo	obecenstvo	k1gNnSc1	obecenstvo
ale	ale	k8xC	ale
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
nemělo	mít	k5eNaImAgNnS	mít
rádo	rád	k2eAgNnSc1d1	rádo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Jany	Jana	k1gFnSc2	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
populární	populární	k2eAgFnSc1d1	populární
lidová	lidový	k2eAgFnSc1d1	lidová
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Jana	Jana	k1gFnSc1	Jana
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
postavou	postava	k1gFnSc7	postava
<g/>
,	,	kIx,	,
Schiller	Schiller	k1gMnSc1	Schiller
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgMnS	mít
nejradši	rád	k6eAd3	rád
<g/>
.	.	kIx.	.
</s>
<s>
Panna	Panna	k1gFnSc1	Panna
Orleánská	orleánský	k2eAgFnSc1d1	Orleánská
je	být	k5eAaImIp3nS	být
obrazem	obraz	k1gInSc7	obraz
poslušnosti	poslušnost	k1gFnSc2	poslušnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Messinská	messinský	k2eAgFnSc1d1	Messinská
nevěsta	nevěsta	k1gFnSc1	nevěsta
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Braut	Braut	k1gInSc4	Braut
von	von	k1gInSc1	von
Messina	Messina	k1gFnSc1	Messina
<g/>
,	,	kIx,	,
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Schiller	Schiller	k1gMnSc1	Schiller
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zrekonstruovat	zrekonstruovat	k5eAaPmF	zrekonstruovat
antickou	antický	k2eAgFnSc4d1	antická
tragédii	tragédie	k1gFnSc4	tragédie
i	i	k9	i
s	s	k7c7	s
chórem	chór	k1gInSc7	chór
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
zněl	znět	k5eAaImAgInS	znět
Bratři	bratr	k1gMnPc1	bratr
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Dramatici	dramatik	k1gMnPc1	dramatik
se	se	k3xPyFc4	se
často	často	k6eAd1	často
snažili	snažit	k5eAaImAgMnP	snažit
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
antice	antika	k1gFnSc3	antika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Tell	Tell	k1gMnSc1	Tell
(	(	kIx(	(
<g/>
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Tell	Tell	k1gMnSc1	Tell
<g/>
,	,	kIx,	,
1803	[number]	k4	1803
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Námět	námět	k1gInSc1	námět
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
hru	hra	k1gFnSc4	hra
Schiller	Schiller	k1gMnSc1	Schiller
získal	získat	k5eAaPmAgInS	získat
od	od	k7c2	od
Goethea	Goethe	k1gInSc2	Goethe
–	–	k?	–
Goethe	Goethe	k1gInSc1	Goethe
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
vyslechl	vyslechnout	k5eAaPmAgInS	vyslechnout
zde	zde	k6eAd1	zde
tuto	tento	k3xDgFnSc4	tento
pověst	pověst	k1gFnSc4	pověst
a	a	k8xC	a
vyprávěl	vyprávět	k5eAaImAgMnS	vyprávět
ji	on	k3xPp3gFnSc4	on
Schillerovi	Schiller	k1gMnSc6	Schiller
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Další	další	k2eAgNnPc1d1	další
díla	dílo	k1gNnPc1	dílo
====	====	k?	====
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
o	o	k7c6	o
estetické	estetický	k2eAgFnSc6d1	estetická
výchově	výchova	k1gFnSc6	výchova
(	(	kIx(	(
<g/>
Über	Über	k1gInSc1	Über
die	die	k?	die
ästhetische	ästhetischat	k5eAaPmIp3nS	ästhetischat
Erziehung	Erziehung	k1gInSc1	Erziehung
des	des	k1gNnSc2	des
Menschen	Menschna	k1gFnPc2	Menschna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
o	o	k7c6	o
kráse	krása	k1gFnSc6	krása
(	(	kIx(	(
<g/>
An	An	k1gMnSc1	An
Körner	Körner	k1gMnSc1	Körner
über	über	k1gMnSc1	über
die	die	k?	die
Schönheit	Schönheit	k1gMnSc1	Schönheit
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Polykratův	Polykratův	k2eAgInSc4d1	Polykratův
prsten	prsten	k1gInSc4	prsten
(	(	kIx(	(
<g/>
Der	drát	k5eAaImRp2nS	drát
Ring	ring	k1gInSc1	ring
des	des	k1gNnSc6	des
Polykrates	Polykrates	k1gInSc1	Polykrates
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Píseň	píseň	k1gFnSc1	píseň
o	o	k7c6	o
zvonu	zvon	k1gInSc6	zvon
(	(	kIx(	(
<g/>
Das	Das	k1gFnSc1	Das
Lied	Lied	k1gMnSc1	Lied
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Glocke	Glocke	k1gNnSc4	Glocke
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Goethem	Goeth	k1gInSc7	Goeth
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vydávání	vydávání	k1gNnSc6	vydávání
časopisu	časopis	k1gInSc2	časopis
Die	Die	k1gFnSc2	Die
Horen	horna	k1gFnPc2	horna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
na	na	k7c6	na
Musealmanachu	Musealmanach	k1gInSc6	Musealmanach
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
</s>
</p>
<p>
<s>
Medailon	medailon	k1gInSc1	medailon
Friedricha	Friedrich	k1gMnSc2	Friedrich
Schillera	Schiller	k1gMnSc2	Schiller
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgNnPc1d1	dostupné
díla	dílo	k1gNnPc1	dílo
F.	F.	kA	F.
Schillera	Schiller	k1gMnSc2	Schiller
v	v	k7c6	v
digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
NK	NK	kA	NK
ČR	ČR	kA	ČR
</s>
</p>
