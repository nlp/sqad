<s>
Tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
obvykle	obvykle	k6eAd1	obvykle
udělují	udělovat	k5eAaImIp3nP	udělovat
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
zkracován	zkracován	k2eAgMnSc1d1	zkracován
jako	jako	k8xC	jako
ThDr.	ThDr.	k1gMnSc1	ThDr.
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
