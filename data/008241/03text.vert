<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
theologiae	theologiae	k1gInSc1	theologiae
doctor	doctor	k1gInSc1	doctor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
akademický	akademický	k2eAgInSc4d1	akademický
titul	titul	k1gInSc4	titul
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
udělován	udělovat	k5eAaImNgInS	udělovat
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
(	(	kIx(	(
<g/>
bohosloví	bohosloví	k1gNnSc1	bohosloví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
obvykle	obvykle	k6eAd1	obvykle
udělují	udělovat	k5eAaImIp3nP	udělovat
teologické	teologický	k2eAgFnSc2d1	teologická
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
zkracován	zkracován	k2eAgMnSc1d1	zkracován
jako	jako	k8xC	jako
ThDr.	ThDr.	k1gMnSc1	ThDr.
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
obdržení	obdržení	k1gNnSc3	obdržení
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
získaný	získaný	k2eAgInSc4d1	získaný
titul	titul	k1gInSc4	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
příslušně	příslušně	k6eAd1	příslušně
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
obhajoba	obhajoba	k1gFnSc1	obhajoba
rigorózní	rigorózní	k2eAgFnSc2d1	rigorózní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
fakultativní	fakultativní	k2eAgFnSc4d1	fakultativní
zkoušku	zkouška	k1gFnSc4	zkouška
zpravidla	zpravidla	k6eAd1	zpravidla
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poplatky	poplatek	k1gInPc7	poplatek
–	–	k?	–
nikoli	nikoli	k9	nikoli
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc1	úroveň
dosažené	dosažený	k2eAgFnSc2d1	dosažená
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
(	(	kIx(	(
<g/>
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
,	,	kIx,	,
stejná	stejný	k2eAgNnPc1d1	stejné
jako	jako	k8xS	jako
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někdy	někdy	k6eAd1	někdy
též	též	k9	též
bývá	bývat	k5eAaImIp3nS	bývat
ThDr.	ThDr.	k1gMnSc1	ThDr.
označován	označovat	k5eAaImNgMnS	označovat
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jako	jako	k9	jako
ostatní	ostatní	k2eAgInPc4d1	ostatní
tituly	titul	k1gInPc4	titul
získané	získaný	k2eAgInPc4d1	získaný
tímto	tento	k3xDgNnSc7	tento
řízením	řízení	k1gNnSc7	řízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
tzv.	tzv.	kA	tzv.
malý	malý	k2eAgInSc4d1	malý
doktorát	doktorát	k1gInSc4	doktorát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1998-2016	[number]	k4	1998-2016
byl	být	k5eAaImAgInS	být
též	též	k9	též
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
udělován	udělován	k2eAgInSc4d1	udělován
i	i	k9	i
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ve	v	k7c6	v
zkratce	zkratka	k1gFnSc6	zkratka
(	(	kIx(	(
<g/>
za	za	k7c7	za
jménem	jméno	k1gNnSc7	jméno
<g/>
,	,	kIx,	,
odděleno	oddělit	k5eAaPmNgNnS	oddělit
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgInS	být
tzv.	tzv.	kA	tzv.
velký	velký	k2eAgInSc4d1	velký
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
dosahován	dosahovat	k5eAaImNgMnS	dosahovat
dalším	další	k1gNnSc7	další
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
letým	letý	k2eAgNnSc7d1	leté
studiem	studio	k1gNnSc7	studio
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
řádném	řádný	k2eAgNnSc6d1	řádné
ukončení	ukončení	k1gNnSc6	ukončení
magisterského	magisterský	k2eAgNnSc2d1	magisterské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
/	/	kIx~	/
<g/>
ThDr.	ThDr.	k1gMnSc1	ThDr.
<g/>
/	/	kIx~	/
<g/>
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rovněž	rovněž	k9	rovněž
možné	možný	k2eAgNnSc1d1	možné
následně	následně	k6eAd1	následně
studovat	studovat	k5eAaImF	studovat
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
<g />
.	.	kIx.	.
</s>
<s>
stupni	stupeň	k1gInSc3	stupeň
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
v	v	k7c6	v
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
udělován	udělován	k2eAgInSc4d1	udělován
standardní	standardní	k2eAgInSc4d1	standardní
vědecký	vědecký	k2eAgInSc4d1	vědecký
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
nyní	nyní	k6eAd1	nyní
uděluje	udělovat	k5eAaImIp3nS	udělovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
umělecké	umělecký	k2eAgFnSc2d1	umělecká
<g/>
)	)	kIx)	)
i	i	k9	i
náboženské	náboženský	k2eAgFnPc1d1	náboženská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
předpis	předpis	k1gInSc1	předpis
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
příslušný	příslušný	k2eAgInSc1d1	příslušný
rigorózní	rigorózní	k2eAgInSc1d1	rigorózní
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
nositel	nositel	k1gMnSc1	nositel
titulu	titul	k1gInSc2	titul
magistr	magistr	k1gMnSc1	magistr
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
případně	případně	k6eAd1	případně
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
předložená	předložený	k2eAgFnSc1d1	předložená
magisterská	magisterský	k2eAgFnSc1d1	magisterská
(	(	kIx(	(
<g/>
diplomová	diplomový	k2eAgFnSc1d1	Diplomová
<g/>
)	)	kIx)	)
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
či	či	k8xC	či
event.	event.	k?	event.
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
uznána	uznán	k2eAgFnSc1d1	uznána
i	i	k8xC	i
jako	jako	k9	jako
rigorózní	rigorózní	k2eAgFnPc4d1	rigorózní
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Rigorózum	rigorózum	k1gNnSc1	rigorózum
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
finančně	finančně	k6eAd1	finančně
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
–	–	k?	–
poplatky	poplatek	k1gInPc7	poplatek
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
spojené	spojený	k2eAgInPc1d1	spojený
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
příjmem	příjem	k1gInSc7	příjem
dané	daný	k2eAgFnSc2d1	daná
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
byl	být	k5eAaImAgMnS	být
ve	v	k7c4	v
své	svůj	k3xOyFgNnSc4	svůj
postgraduální	postgraduální	k2eAgNnSc4d1	postgraduální
variatně	variatně	k6eAd1	variatně
(	(	kIx(	(
<g/>
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
dříve	dříve	k6eAd2	dříve
udělován	udělovat	k5eAaImNgInS	udělovat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
obhajobě	obhajoba	k1gFnSc3	obhajoba
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
další	další	k2eAgInPc1d1	další
tři	tři	k4xCgInPc1	tři
roky	rok	k1gInPc1	rok
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teologický	teologický	k2eAgInSc1d1	teologický
titul	titul	k1gInSc1	titul
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
titulu	titul	k1gInSc2	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Mírný	mírný	k2eAgInSc1d1	mírný
zmatek	zmatek	k1gInSc1	zmatek
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
oba	dva	k4xCgInPc4	dva
akademické	akademický	k2eAgInPc4d1	akademický
tituly	titul	k1gInPc4	titul
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
stejně	stejně	k6eAd1	stejně
a	a	k8xC	a
odlišil	odlišit	k5eAaPmAgMnS	odlišit
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
zkratkou	zkratka	k1gFnSc7	zkratka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
zmatek	zmatek	k1gInSc1	zmatek
působí	působit	k5eAaImIp3nS	působit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
titul	titul	k1gInSc1	titul
ThDr.	ThDr.	k1gFnSc2	ThDr.
(	(	kIx(	(
<g/>
před	před	k7c7	před
jménem	jméno	k1gNnSc7	jméno
<g/>
)	)	kIx)	)
udělován	udělovat	k5eAaImNgInS	udělovat
teologickými	teologický	k2eAgFnPc7d1	teologická
fakultami	fakulta	k1gFnPc7	fakulta
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
absolvování	absolvování	k1gNnSc2	absolvování
doktorského	doktorský	k2eAgNnSc2d1	doktorské
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
obhajoby	obhajoba	k1gFnSc2	obhajoba
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
týž	týž	k3xTgInSc4	týž
titul	titul	k1gInSc4	titul
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
a	a	k8xC	a
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
označuje	označovat	k5eAaImIp3nS	označovat
různou	různý	k2eAgFnSc4d1	různá
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
ISCED	ISCED	kA	ISCED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgInSc4d1	český
byla	být	k5eAaImAgNnP	být
navíc	navíc	k6eAd1	navíc
dříve	dříve	k6eAd2	dříve
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
pravopisná	pravopisný	k2eAgFnSc1d1	pravopisná
podoba	podoba	k1gFnSc1	podoba
zkratky	zkratka	k1gFnSc2	zkratka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
jazykové	jazykový	k2eAgFnSc2d1	jazyková
poradny	poradna	k1gFnSc2	poradna
ÚJČ	ÚJČ	kA	ÚJČ
by	by	kYmCp3nS	by
české	český	k2eAgFnSc6d1	Česká
tradici	tradice	k1gFnSc6	tradice
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
spíše	spíše	k9	spíše
zkratka	zkratka	k1gFnSc1	zkratka
ThD	ThD	k1gFnSc1	ThD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
byl	být	k5eAaImAgMnS	být
dříve	dříve	k6eAd2	dříve
udílen	udílet	k5eAaImNgMnS	udílet
vedle	vedle	k7c2	vedle
dalších	další	k2eAgInPc2d1	další
doktorátů	doktorát	k1gInPc2	doktorát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
variantě	varianta	k1gFnSc6	varianta
ThDr.	ThDr.	k1gFnSc2	ThDr.
Doktorát	doktorát	k1gInSc4	doktorát
teologie	teologie	k1gFnSc2	teologie
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
též	též	k9	též
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
theol	theol	k1gInSc1	theol
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
dříve	dříve	k6eAd2	dříve
za	za	k7c2	za
Rakouska	Rakousko	k1gNnSc2	Rakousko
i	i	k9	i
za	za	k7c4	za
první	první	k4xOgFnPc4	první
československé	československý	k2eAgFnPc4d1	Československá
republiky	republika	k1gFnPc4	republika
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
čtyř	čtyři	k4xCgFnPc2	čtyři
rigorózních	rigorózní	k2eAgFnPc2d1	rigorózní
zkoušek	zkouška	k1gFnPc2	zkouška
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
po	po	k7c6	po
sepsání	sepsání	k1gNnSc6	sepsání
a	a	k8xC	a
obhájení	obhájení	k1gNnSc6	obhájení
disertační	disertační	k2eAgFnSc2d1	disertační
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
rigorózum	rigorózum	k1gNnSc1	rigorózum
bylo	být	k5eAaImAgNnS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
biblické	biblický	k2eAgNnSc4d1	biblické
studium	studium	k1gNnSc4	studium
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
na	na	k7c4	na
církevní	církevní	k2eAgFnPc4d1	církevní
dějiny	dějiny	k1gFnPc4	dějiny
a	a	k8xC	a
církevní	církevní	k2eAgNnSc4d1	církevní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc1	třetí
na	na	k7c4	na
dogmatickou	dogmatický	k2eAgFnSc4d1	dogmatická
a	a	k8xC	a
fundamentální	fundamentální	k2eAgFnSc4d1	fundamentální
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
čtvrté	čtvrtá	k1gFnPc4	čtvrtá
na	na	k7c6	na
teologii	teologie	k1gFnSc6	teologie
morální	morální	k2eAgFnSc6d1	morální
a	a	k8xC	a
pastorální	pastorální	k2eAgFnSc6d1	pastorální
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
rigorózní	rigorózní	k2eAgFnSc1d1	rigorózní
zkouška	zkouška	k1gFnSc1	zkouška
již	již	k9	již
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
komunisty	komunista	k1gMnSc2	komunista
byl	být	k5eAaImAgMnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přijat	přijat	k2eAgInSc1d1	přijat
Nejedlého	Nejedlého	k2eAgInSc1d1	Nejedlého
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
zrušil	zrušit	k5eAaPmAgInS	zrušit
tituly	titul	k1gInPc4	titul
a	a	k8xC	a
stavovská	stavovský	k2eAgNnPc4d1	Stavovské
označení	označení	k1gNnPc4	označení
pro	pro	k7c4	pro
nové	nový	k2eAgMnPc4d1	nový
absolventy	absolvent	k1gMnPc4	absolvent
a	a	k8xC	a
udíleny	udílen	k2eAgFnPc4d1	udílena
tak	tak	k8xS	tak
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
profesní	profesní	k2eAgNnSc4d1	profesní
označení	označení	k1gNnSc4	označení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
promovaný	promovaný	k2eAgMnSc1d1	promovaný
právník	právník	k1gMnSc1	právník
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
pak	pak	k9	pak
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
určitá	určitý	k2eAgFnSc1d1	určitá
reforma	reforma	k1gFnSc1	reforma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
trojstupňový	trojstupňový	k2eAgInSc4d1	trojstupňový
systém	systém	k1gInSc4	systém
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
novým	nový	k2eAgInSc7d1	nový
vysokoškolským	vysokoškolský	k2eAgInSc7d1	vysokoškolský
zákonem	zákon	k1gInSc7	zákon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byly	být	k5eAaImAgFnP	být
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc4d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
rigorózní	rigorózní	k2eAgInPc4d1	rigorózní
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
zrušeny	zrušen	k2eAgFnPc4d1	zrušena
–	–	k?	–
ThDr.	ThDr.	k1gFnPc4	ThDr.
tak	tak	k6eAd1	tak
udělován	udělován	k2eAgMnSc1d1	udělován
nebyl	být	k5eNaImAgInS	být
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
"	"	kIx"	"
<g/>
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
magisterský	magisterský	k2eAgInSc1d1	magisterský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
udělován	udělován	k2eAgInSc1d1	udělován
titul	titul	k1gInSc1	titul
magistra	magistra	k1gFnSc1	magistra
(	(	kIx(	(
<g/>
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
doktorátu	doktorát	k1gInSc2	doktorát
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
disertační	disertační	k2eAgInSc4d1	disertační
doktorát	doktorát	k1gInSc4	doktorát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Postgraduální	postgraduální	k2eAgNnSc1d1	postgraduální
studium	studium	k1gNnSc1	studium
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
8	[number]	k4	8
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
doctor	doctor	k1gInSc1	doctor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Boloňský	boloňský	k2eAgInSc1d1	boloňský
proces	proces	k1gInSc1	proces
následně	následně	k6eAd1	následně
sjednotil	sjednotit	k5eAaPmAgInS	sjednotit
evropské	evropský	k2eAgNnSc4d1	Evropské
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
se	s	k7c7	s
stavem	stav	k1gInSc7	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyly	být	k5eNaImAgInP	být
udělovány	udělován	k2eAgInPc1d1	udělován
tzv.	tzv.	kA	tzv.
(	(	kIx(	(
<g/>
fakultativní	fakultativní	k2eAgInPc4d1	fakultativní
<g/>
)	)	kIx)	)
malé	malý	k2eAgInPc4d1	malý
doktoráty	doktorát	k1gInPc4	doktorát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
přijetí	přijetí	k1gNnSc2	přijetí
nového	nový	k2eAgInSc2d1	nový
vysokoškolského	vysokoškolský	k2eAgInSc2d1	vysokoškolský
zákona	zákon	k1gInSc2	zákon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
titul	titul	k1gInSc1	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ThDr.	ThDr.	k1gFnSc1	ThDr.
<g/>
)	)	kIx)	)
znovu	znovu	k6eAd1	znovu
udělován	udělován	k2eAgInSc1d1	udělován
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
dodatečné	dodatečný	k2eAgFnSc6d1	dodatečná
a	a	k8xC	a
zpoplatněné	zpoplatněný	k2eAgFnSc6d1	zpoplatněná
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
zkoušce	zkouška	k1gFnSc6	zkouška
–	–	k?	–
jeho	jeho	k3xOp3gNnSc4	jeho
udělení	udělení	k1gNnSc4	udělení
tak	tak	k6eAd1	tak
nepředchází	předcházet	k5eNaImIp3nS	předcházet
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
formální	formální	k2eAgNnSc4d1	formální
studium	studium	k1gNnSc4	studium
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
jak	jak	k8xC	jak
ThDr.	ThDr.	k1gMnSc1	ThDr.
(	(	kIx(	(
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Mgr.	Mgr.	kA	Mgr.
(	(	kIx(	(
<g/>
magistr	magistr	k1gMnSc1	magistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgMnPc1	dva
označují	označovat	k5eAaImIp3nP	označovat
de	de	k?	de
facto	facto	k1gNnSc4	facto
stejnou	stejný	k2eAgFnSc4d1	stejná
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
(	(	kIx(	(
<g/>
magisterskou	magisterský	k2eAgFnSc4d1	magisterská
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
,	,	kIx,	,
7	[number]	k4	7
v	v	k7c6	v
ISCED	ISCED	kA	ISCED
<g/>
,	,	kIx,	,
master	master	k1gMnSc1	master
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
degree	degree	k1gFnSc7	degree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
bývá	bývat	k5eAaImIp3nS	bývat
předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
obdobně	obdobně	k6eAd1	obdobně
udělován	udělován	k2eAgInSc1d1	udělován
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
ThDr.	ThDr.	k1gFnSc2	ThDr.
–	–	k?	–
licenciát	licenciát	k1gInSc1	licenciát
teologie	teologie	k1gFnSc1	teologie
(	(	kIx(	(
<g/>
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
katolické	katolický	k2eAgFnSc2d1	katolická
teologie	teologie	k1gFnSc2	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Zaveden	zaveden	k2eAgInSc1d1	zaveden
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
též	též	k9	též
stejný	stejný	k2eAgInSc4d1	stejný
titul	titul	k1gInSc4	titul
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
zkratkou	zkratka	k1gFnSc7	zkratka
(	(	kIx(	(
<g/>
Th	Th	k1gFnSc1	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
disertační	disertační	k2eAgInSc1d1	disertační
doktorát	doktorát	k1gInSc1	doktorát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
absolventy	absolvent	k1gMnPc4	absolvent
doktorských	doktorský	k2eAgInPc2d1	doktorský
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
"	"	kIx"	"
<g/>
postgraduálního	postgraduální	k2eAgNnSc2d1	postgraduální
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
"	"	kIx"	"
<g/>
doktor	doktor	k1gMnSc1	doktor
teologie	teologie	k1gFnSc2	teologie
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stal	stát	k5eAaPmAgInS	stát
jediným	jediný	k2eAgInSc7d1	jediný
titulem	titul	k1gInSc7	titul
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
zkratky	zkratka	k1gFnPc4	zkratka
a	a	k8xC	a
značil	značit	k5eAaImAgMnS	značit
dvě	dva	k4xCgFnPc4	dva
různé	různý	k2eAgFnPc4d1	různá
kvalifikace	kvalifikace	k1gFnPc4	kvalifikace
(	(	kIx(	(
<g/>
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
novelizován	novelizován	k2eAgInSc1d1	novelizován
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
udílení	udílení	k1gNnSc1	udílení
postgraduálního	postgraduální	k2eAgInSc2d1	postgraduální
titulu	titul	k1gInSc2	titul
Th	Th	k1gFnSc2	Th
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
bylo	být	k5eAaImAgNnS	být
od	od	k7c2	od
září	září	k1gNnSc2	září
2016	[number]	k4	2016
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
absolventi	absolvent	k1gMnPc1	absolvent
doktorského	doktorský	k2eAgInSc2d1	doktorský
studijního	studijní	k2eAgInSc2d1	studijní
programu	program	k1gInSc2	program
získávají	získávat	k5eAaImIp3nP	získávat
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
titul	titul	k1gInSc1	titul
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
a	a	k8xC	a
doktorát	doktorát	k1gInSc4	doktorát
teologie	teologie	k1gFnSc2	teologie
tedy	tedy	k9	tedy
zůstal	zůstat	k5eAaPmAgInS	zůstat
jen	jen	k9	jen
v	v	k7c6	v
rigorózní	rigorózní	k2eAgFnSc6d1	rigorózní
formě	forma	k1gFnSc6	forma
ThDr.	ThDr.	k1gFnSc4	ThDr.
Dosavadní	dosavadní	k2eAgInPc4d1	dosavadní
velké	velký	k2eAgInPc4d1	velký
doktoráty	doktorát	k1gInPc4	doktorát
teologie	teologie	k1gFnSc2	teologie
nicméně	nicméně	k8xC	nicméně
zůstaly	zůstat	k5eAaPmAgInP	zůstat
zachovány	zachován	k2eAgInPc1d1	zachován
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
každý	každý	k3xTgMnSc1	každý
jeho	jeho	k3xOp3gMnSc1	jeho
nositel	nositel	k1gMnSc1	nositel
může	moct	k5eAaImIp3nS	moct
požádat	požádat	k5eAaPmF	požádat
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
nahrazení	nahrazení	k1gNnSc6	nahrazení
obecným	obecný	k2eAgInSc7d1	obecný
titulem	titul	k1gInSc7	titul
doktor	doktor	k1gMnSc1	doktor
(	(	kIx(	(
<g/>
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
též	též	k9	též
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
upravena	upravit	k5eAaPmNgFnS	upravit
možnost	možnost	k1gFnSc1	možnost
udělovat	udělovat	k5eAaImF	udělovat
ThLic	ThLic	k1gMnSc1	ThLic
<g/>
.	.	kIx.	.
i	i	k9	i
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
oblasti	oblast	k1gFnSc6	oblast
teologie	teologie	k1gFnSc2	teologie
než	než	k8xS	než
pouze	pouze	k6eAd1	pouze
katolické	katolický	k2eAgInPc1d1	katolický
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nyní	nyní	k6eAd1	nyní
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ThDr.	ThDr.	k1gFnSc1	ThDr.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
