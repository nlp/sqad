<s>
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
produkoval	produkovat	k5eAaImAgMnS	produkovat
Warren	Warrna	k1gFnPc2	Warrna
Beatty	Beatta	k1gFnSc2	Beatta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
i	i	k9	i
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
filmové	filmový	k2eAgFnSc2d1	filmová
historie	historie	k1gFnSc2	historie
kombinací	kombinace	k1gFnPc2	kombinace
násilí	násilí	k1gNnSc2	násilí
<g/>
,	,	kIx,	,
sexu	sex	k1gInSc2	sex
i	i	k8xC	i
humoru	humor	k1gInSc2	humor
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
téma	téma	k1gNnSc4	téma
odpadlého	odpadlý	k2eAgNnSc2d1	odpadlé
mládí	mládí	k1gNnSc2	mládí
bylo	být	k5eAaImAgNnS	být
přitažlivé	přitažlivý	k2eAgNnSc1d1	přitažlivé
pro	pro	k7c4	pro
mladé	mladý	k2eAgNnSc4d1	mladé
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
