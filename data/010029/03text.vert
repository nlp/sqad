<p>
<s>
Rmus	Rmus	k1gInSc1	Rmus
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
Marian	Marian	k1gMnSc1	Marian
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kosík	Kosík	k1gMnSc1	Kosík
OPraem	OPraem	k1gInSc1	OPraem
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
*	*	kIx~	*
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
moravský	moravský	k2eAgMnSc1d1	moravský
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
od	od	k7c2	od
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
opatem	opat	k1gMnSc7	opat
premonstrátského	premonstrátský	k2eAgInSc2d1	premonstrátský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Šaratic	šaratice	k1gFnPc2	šaratice
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
v	v	k7c6	v
kněžském	kněžský	k2eAgInSc6d1	kněžský
semináři	seminář	k1gInSc6	seminář
v	v	k7c6	v
Litoměřicích	Litoměřice	k1gInPc6	Litoměřice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kněze	kněz	k1gMnPc4	kněz
byl	být	k5eAaImAgMnS	být
vysvěcen	vysvěcen	k2eAgMnSc1d1	vysvěcen
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
duchovní	duchovní	k1gMnSc1	duchovní
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Žarošicích	Žarošik	k1gInPc6	Žarošik
u	u	k7c2	u
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
pak	pak	k6eAd1	pak
tak	tak	k9	tak
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996-2000	[number]	k4	1996-2000
v	v	k7c6	v
Brně-Židenicích	Brně-Židenice	k1gFnPc6	Brně-Židenice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
zemřel	zemřít	k5eAaPmAgMnS	zemřít
novoříšský	novoříšský	k2eAgMnSc1d1	novoříšský
opat	opat	k1gMnSc1	opat
Augustin	Augustin	k1gMnSc1	Augustin
Machalka	Machalka	k1gFnSc1	Machalka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
zvolen	zvolen	k2eAgMnSc1d1	zvolen
za	za	k7c2	za
jeho	jeho	k3xOp3gMnSc2	jeho
nástupce	nástupce	k1gMnSc2	nástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
opat	opat	k1gMnSc1	opat
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
o	o	k7c4	o
kompletní	kompletní	k2eAgFnSc4d1	kompletní
duchovní	duchovní	k2eAgFnSc4d1	duchovní
i	i	k8xC	i
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
obnovu	obnova	k1gFnSc4	obnova
této	tento	k3xDgFnSc2	tento
nejmenší	malý	k2eAgFnSc2d3	nejmenší
premonstrátské	premonstrátský	k2eAgFnSc2d1	premonstrátská
kanonie	kanonie	k1gFnSc2	kanonie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
iniciativy	iniciativa	k1gFnSc2	iniciativa
byl	být	k5eAaImAgInS	být
například	například	k6eAd1	například
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
opraven	opraven	k2eAgInSc1d1	opraven
klášterní	klášterní	k2eAgInSc1d1	klášterní
komplex	komplex	k1gInSc1	komplex
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Říši	říš	k1gFnSc6	říš
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
také	také	k9	také
klášterní	klášterní	k2eAgInSc4d1	klášterní
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc4	Pavel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opat	opat	k1gMnSc1	opat
Kosík	Kosík	k1gMnSc1	Kosík
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgMnSc7d1	častý
hostem	host	k1gMnSc7	host
na	na	k7c6	na
církevních	církevní	k2eAgFnPc6d1	církevní
slavnostech	slavnost	k1gFnPc6	slavnost
v	v	k7c6	v
bývalých	bývalý	k2eAgFnPc6d1	bývalá
premonstrátských	premonstrátský	k2eAgFnPc6d1	premonstrátská
farnostech	farnost	k1gFnPc6	farnost
jako	jako	k8xS	jako
Brno-Židenice	Brno-Židenice	k1gFnSc2	Brno-Židenice
nebo	nebo	k8xC	nebo
Křtiny	křtiny	k1gFnPc4	křtiny
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgNnPc6d1	další
mariánských	mariánský	k2eAgNnPc6d1	Mariánské
poutních	poutní	k2eAgNnPc6d1	poutní
místech	místo	k1gNnPc6	místo
zejména	zejména	k9	zejména
ve	v	k7c6	v
Sloupě	sloup	k1gInSc6	sloup
<g/>
,	,	kIx,	,
Žarošicích	Žarošice	k1gFnPc6	Žarošice
nebo	nebo	k8xC	nebo
Hlubokých	hluboký	k2eAgFnPc6d1	hluboká
Mašůvkách	Mašůvka	k1gFnPc6	Mašůvka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postoje	postoj	k1gInPc4	postoj
a	a	k8xC	a
stanoviska	stanovisko	k1gNnPc4	stanovisko
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
signoval	signovat	k5eAaBmAgInS	signovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
duchovními	duchovní	k2eAgInPc7d1	duchovní
petici	petice	k1gFnSc6	petice
Kněží	kněz	k1gMnPc1	kněz
podporují	podporovat	k5eAaImIp3nP	podporovat
rodiče	rodič	k1gMnSc4	rodič
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
přispěno	přispěn	k2eAgNnSc1d1	přispěno
k	k	k7c3	k
iniciativě	iniciativa	k1gFnSc3	iniciativa
katolických	katolický	k2eAgMnPc2d1	katolický
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
VORP	VORP	kA	VORP
proti	proti	k7c3	proti
formě	forma	k1gFnSc3	forma
a	a	k8xC	a
způsobu	způsob	k1gInSc3	způsob
zavedení	zavedení	k1gNnSc2	zavedení
sexuální	sexuální	k2eAgFnSc2d1	sexuální
výchovy	výchova	k1gFnSc2	výchova
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prosadila	prosadit	k5eAaPmAgFnS	prosadit
úřednická	úřednický	k2eAgFnSc1d1	úřednická
vláda	vláda	k1gFnSc1	vláda
rok	rok	k1gInSc4	rok
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
korunovace	korunovace	k1gFnSc1	korunovace
Staré	Staré	k2eAgFnSc2d1	Staré
Matky	matka	k1gFnSc2	matka
Boží	božit	k5eAaImIp3nS	božit
Žarošské	Žarošský	k2eAgNnSc1d1	Žarošský
<g/>
,	,	kIx,	,
Divotvůrkyně	divotvůrkyně	k1gFnSc1	divotvůrkyně
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Kartuziánské	kartuziánský	k2eAgNnSc1d1	Kartuziánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Poutě	pouť	k1gFnPc1	pouť
k	k	k7c3	k
Staré	Staré	k2eAgFnSc3d1	Staré
Matce	matka	k1gFnSc3	matka
Boží	božit	k5eAaImIp3nS	božit
Žarošské	Žarošský	k2eAgNnSc1d1	Žarošský
<g/>
,	,	kIx,	,
Divotvůrkyni	divotvůrkyně	k1gFnSc3	divotvůrkyně
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Petr	Petr	k1gMnSc1	Petr
Sypták	Sypták	k1gMnSc1	Sypták
<g/>
,	,	kIx,	,
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
století	století	k1gNnPc2	století
<g/>
:	:	kIx,	:
Zábrdovice	Zábrdovice	k1gFnPc1	Zábrdovice
<g/>
,	,	kIx,	,
Křtiny	křtiny	k1gFnPc1	křtiny
<g/>
,	,	kIx,	,
Nová	nový	k2eAgFnSc1d1	nová
Říše	říše	k1gFnSc1	říše
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Kartuziánské	kartuziánský	k2eAgNnSc1d1	Kartuziánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
kněžství	kněžství	k1gNnSc1	kněžství
<g/>
.	.	kIx.	.
</s>
<s>
Věstník	věstník	k1gInSc1	věstník
Historicko-vlastivědného	historickolastivědný	k2eAgInSc2d1	historicko-vlastivědný
kroužku	kroužek	k1gInSc2	kroužek
v	v	k7c6	v
Žarošicích	Žarošice	k1gFnPc6	Žarošice
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
č.	č.	k?	č.
18	[number]	k4	18
<g/>
,	,	kIx,	,
s.	s.	k?	s.
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgMnS	připravit
ke	k	k7c3	k
každému	každý	k3xTgNnSc3	každý
dobrému	dobrý	k2eAgNnSc3d1	dobré
dílu	dílo	k1gNnSc3	dílo
<g/>
:	:	kIx,	:
jaká	jaký	k3yIgFnSc1	jaký
je	být	k5eAaImIp3nS	být
premonstrátská	premonstrátský	k2eAgFnSc1d1	premonstrátská
spiritualita	spiritualita	k1gFnSc1	spiritualita
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Katolický	katolický	k2eAgInSc1d1	katolický
týdeník	týdeník	k1gInSc1	týdeník
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
č.	č.	k?	č.
7	[number]	k4	7
<g/>
,	,	kIx,	,
s.	s.	k?	s.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Náboženské	náboženský	k2eAgFnPc1d1	náboženská
pamětihodnosti	pamětihodnost	k1gFnPc1	pamětihodnost
kolem	kolem	k7c2	kolem
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
Žarošic	Žarošice	k1gFnPc2	Žarošice
<g/>
.	.	kIx.	.
</s>
<s>
Věstník	věstník	k1gInSc1	věstník
Historicko-vlastivědného	historickolastivědný	k2eAgInSc2d1	historicko-vlastivědný
kroužku	kroužek	k1gInSc2	kroužek
v	v	k7c6	v
Žarošicích	Žarošik	k1gInPc6	Žarošik
<g/>
,	,	kIx,	,
Žarošice	Žarošice	k1gInPc1	Žarošice
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
č.	č.	k?	č.
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOSÍK	Kosík	k1gMnSc1	Kosík
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Žarošice	Žarošice	k1gFnSc1	Žarošice
<g/>
.	.	kIx.	.
</s>
<s>
Malovaný	malovaný	k2eAgInSc1d1	malovaný
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
32	[number]	k4	32
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bibliografie	bibliografie	k1gFnSc2	bibliografie
==	==	k?	==
</s>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
opat	opat	k1gMnSc1	opat
v	v	k7c6	v
Nové	Nové	k2eAgFnSc6d1	Nové
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Katolický	katolický	k2eAgInSc1d1	katolický
týdeník	týdeník	k1gInSc1	týdeník
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
42	[number]	k4	42
<g/>
,	,	kIx,	,
s.	s.	k?	s.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Patnáctý	patnáctý	k4xOgMnSc1	patnáctý
novoříšský	novoříšský	k2eAgMnSc1d1	novoříšský
opat	opat	k1gMnSc1	opat
složil	složit	k5eAaPmAgMnS	složit
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
svůj	svůj	k3xOyFgInSc4	svůj
slib	slib	k1gInSc4	slib
<g/>
.	.	kIx.	.
</s>
<s>
Jihlavské	jihlavský	k2eAgInPc1d1	jihlavský
listy	list	k1gInPc1	list
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
,	,	kIx,	,
č.	č.	k?	č.
92	[number]	k4	92
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁK	Sedlák	k1gMnSc1	Sedlák
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
duchovní	duchovní	k2eAgFnSc2d1	duchovní
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
Šaraticích	šaratice	k1gFnPc6	šaratice
<g/>
.	.	kIx.	.
</s>
<s>
Cantus	Cantus	k1gInSc1	Cantus
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VALIHRACH	VALIHRACH	kA	VALIHRACH
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
fortnou	fortna	k1gFnSc7	fortna
novoříšského	novoříšský	k2eAgInSc2d1	novoříšský
kláštera	klášter	k1gInSc2	klášter
aneb	aneb	k?	aneb
den	den	k1gInSc1	den
s	s	k7c7	s
opatem	opat	k1gMnSc7	opat
Kosíkem	Kosík	k1gMnSc7	Kosík
<g/>
.	.	kIx.	.
</s>
<s>
Věstník	věstník	k1gInSc1	věstník
Historicko-vlastivědného	historickolastivědný	k2eAgInSc2d1	historicko-vlastivědný
kroužku	kroužek	k1gInSc2	kroužek
v	v	k7c6	v
Žarošicích	Žarošik	k1gInPc6	Žarošik
<g/>
,	,	kIx,	,
Žarošice	Žarošice	k1gInPc1	Žarošice
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
č.	č.	k?	č.
17	[number]	k4	17
<g/>
,	,	kIx,	,
s.	s.	k?	s.
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnPc4	poznámka
a	a	k8xC	a
reference	reference	k1gFnPc4	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Marian	Marian	k1gMnSc1	Marian
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kosík	Kosík	k1gMnSc1	Kosík
na	na	k7c6	na
Databázi	databáze	k1gFnSc6	databáze
knih	kniha	k1gFnPc2	kniha
</s>
</p>
<p>
<s>
Marian	Marian	k1gMnSc1	Marian
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kosík	Kosík	k1gMnSc1	Kosík
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Barvy	barva	k1gFnSc2	barva
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
