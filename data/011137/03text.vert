<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Babiš	Babiš	k1gMnSc1	Babiš
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
Andreje	Andrej	k1gMnSc2	Andrej
Babiše	Babiš	k1gMnSc2	Babiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1989	[number]	k4	1989
pracoval	pracovat	k5eAaImAgMnS	pracovat
Alexander	Alexandra	k1gFnPc2	Alexandra
Babiš	Babiš	k1gMnSc1	Babiš
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
Omnia	omnium	k1gNnSc2	omnium
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
dovozem	dovoz	k1gInSc7	dovoz
a	a	k8xC	a
vývozem	vývoz	k1gInSc7	vývoz
spotřební	spotřební	k2eAgFnSc2d1	spotřební
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
Státní	státní	k2eAgFnSc7d1	státní
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
jako	jako	k8xC	jako
spolupracovník	spolupracovník	k1gMnSc1	spolupracovník
<g/>
.	.	kIx.	.
</s>
<s>
StB	StB	k?	StB
Alexandera	Alexandera	k1gFnSc1	Alexandera
Babiše	Babiše	k1gFnSc1	Babiše
podle	podle	k7c2	podle
slovenského	slovenský	k2eAgInSc2d1	slovenský
časopisu	časopis	k1gInSc2	časopis
Týždeň	Týždeň	k1gFnSc1	Týždeň
zaevidovala	zaevidovat	k5eAaPmAgFnS	zaevidovat
jedenáct	jedenáct	k4xCc4	jedenáct
dní	den	k1gInPc2	den
před	před	k7c7	před
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
1989	[number]	k4	1989
jako	jako	k8xS	jako
agenta	agent	k1gMnSc4	agent
s	s	k7c7	s
krycím	krycí	k2eAgNnSc7d1	krycí
jménem	jméno	k1gNnSc7	jméno
Miki	Mik	k1gFnSc2	Mik
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Není	být	k5eNaImIp3nS	být
tajností	tajnost	k1gFnSc7	tajnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
o	o	k7c6	o
mně	já	k3xPp1nSc6	já
jsou	být	k5eAaImIp3nP	být
vedené	vedený	k2eAgInPc1d1	vedený
spisy	spis	k1gInPc1	spis
StB	StB	k1gFnPc2	StB
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
byli	být	k5eAaImAgMnP	být
monitorováni	monitorovat	k5eAaImNgMnP	monitorovat
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Babiš	Babiš	k1gMnSc1	Babiš
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
pracoval	pracovat	k5eAaImAgInS	pracovat
15	[number]	k4	15
let	léto	k1gNnPc2	léto
v	v	k7c6	v
bankovním	bankovní	k2eAgInSc6d1	bankovní
sektoru	sektor	k1gInSc6	sektor
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
finančním	finanční	k2eAgMnSc7d1	finanční
ředitelem	ředitel	k1gMnSc7	ředitel
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
<g/>
,	,	kIx,	,
slovenském	slovenský	k2eAgInSc6d1	slovenský
Agrofertu	Agrofert	k1gInSc6	Agrofert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obžaloba	obžaloba	k1gFnSc1	obžaloba
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
Alexander	Alexandra	k1gFnPc2	Alexandra
Babiš	Babiš	k1gMnSc1	Babiš
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
obžalován	obžalovat	k5eAaPmNgInS	obžalovat
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
společníkem	společník	k1gMnSc7	společník
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
CTHS	CTHS	kA	CTHS
byli	být	k5eAaImAgMnP	být
podezřelí	podezřelý	k2eAgMnPc1d1	podezřelý
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
energetické	energetický	k2eAgFnSc3d1	energetická
firmě	firma	k1gFnSc3	firma
ČEZ	ČEZ	kA	ČEZ
Slovensko	Slovensko	k1gNnSc4	Slovensko
podvodem	podvod	k1gInSc7	podvod
způsobili	způsobit	k5eAaPmAgMnP	způsobit
škodu	škoda	k1gFnSc4	škoda
195	[number]	k4	195
250	[number]	k4	250
eur	euro	k1gNnPc2	euro
(	(	kIx(	(
<g/>
5,3	[number]	k4	5,3
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
a	a	k8xC	a
hrozil	hrozit	k5eAaImAgMnS	hrozit
jim	on	k3xPp3gMnPc3	on
trest	trest	k1gInSc4	trest
odnětí	odnětí	k1gNnSc4	odnětí
svobody	svoboda	k1gFnSc2	svoboda
až	až	k9	až
na	na	k7c4	na
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
obžaloby	obžaloba	k1gFnSc2	obžaloba
z	z	k7c2	z
podvodu	podvod	k1gInSc2	podvod
pravomocně	pravomocně	k6eAd1	pravomocně
zprostil	zprostit	k5eAaPmAgInS	zprostit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poškozenou	poškozený	k2eAgFnSc4d1	poškozená
společnost	společnost	k1gFnSc4	společnost
ČEZ	ČEZ	kA	ČEZ
Slovensko	Slovensko	k1gNnSc1	Slovensko
odkázal	odkázat	k5eAaPmAgInS	odkázat
s	s	k7c7	s
nárokem	nárok	k1gInSc7	nárok
na	na	k7c4	na
náhradu	náhrada	k1gFnSc4	náhrada
škody	škoda	k1gFnSc2	škoda
na	na	k7c4	na
občanské	občanský	k2eAgNnSc4d1	občanské
soudní	soudní	k2eAgNnSc4d1	soudní
řízení	řízení	k1gNnSc4	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
Slovensko	Slovensko	k1gNnSc1	Slovensko
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
předala	předat	k5eAaPmAgFnS	předat
pohledávku	pohledávka	k1gFnSc4	pohledávka
vůči	vůči	k7c3	vůči
CTHS	CTHS	kA	CTHS
k	k	k7c3	k
exekuci	exekuce	k1gFnSc3	exekuce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
CTHS	CTHS	kA	CTHS
byla	být	k5eAaImAgFnS	být
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
škodu	škoda	k1gFnSc4	škoda
v	v	k7c6	v
občanském	občanský	k2eAgNnSc6d1	občanské
řízení	řízení	k1gNnSc6	řízení
nevymáhala	vymáhat	k5eNaImAgFnS	vymáhat
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
Slovensko	Slovensko	k1gNnSc1	Slovensko
je	být	k5eAaImIp3nS	být
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
společností	společnost	k1gFnSc7	společnost
českého	český	k2eAgInSc2d1	český
koncernu	koncern	k1gInSc2	koncern
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
působnosti	působnost	k1gFnSc2	působnost
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Alexander	Alexandra	k1gFnPc2	Alexandra
Babiš	Babiš	k1gMnSc1	Babiš
je	být	k5eAaImIp3nS	být
potřetí	potřetí	k4xO	potřetí
ženatý	ženatý	k2eAgInSc1d1	ženatý
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
