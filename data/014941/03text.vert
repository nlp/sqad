<s>
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc2d1
</s>
<s>
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Musca	Musc	k2eAgFnSc1d1
domestica	domestica	k1gFnSc1
<g/>
)	)	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
hmyz	hmyz	k1gInSc1
(	(	kIx(
<g/>
Insecta	Insecta	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
dvoukřídlí	dvoukřídlí	k1gMnPc1
(	(	kIx(
<g/>
Diptera	Dipter	k1gMnSc2
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
mouchovití	mouchovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Muscidae	Muscidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
moucha	moucha	k1gFnSc1
(	(	kIx(
<g/>
Musca	Musca	k1gFnSc1
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Musca	Musca	k1gMnSc1
domesticaLinnaeus	domesticaLinnaeus	k1gMnSc1
<g/>
,	,	kIx,
1758	#num#	k4
Poddruhy	poddruh	k1gInPc1
</s>
<s>
M.	M.	kA
d.	d.	k?
calleva	calleva	k1gFnSc1
(	(	kIx(
<g/>
Walker	Walker	k1gMnSc1
<g/>
,	,	kIx,
1849	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
M.	M.	kA
d.	d.	k?
domestica	domestica	k1gMnSc1
(	(	kIx(
<g/>
Linnaeus	Linnaeus	k1gMnSc1
<g/>
,	,	kIx,
1758	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
při	při	k7c6
páření	páření	k1gNnSc6
</s>
<s>
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
(	(	kIx(
<g/>
Musca	Musc	k2eAgFnSc1d1
domestica	domestica	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejčastější	častý	k2eAgInSc4d3
druh	druh	k1gInSc4
dvoukřídlých	dvoukřídlí	k1gMnPc2
vyskytující	vyskytující	k2eAgInPc4d1
se	se	k3xPyFc4
v	v	k7c6
domovech	domov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
škůdce	škůdce	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
může	moct	k5eAaImIp3nS
přenášet	přenášet	k5eAaImF
vážné	vážný	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hlavní	hlavní	k2eAgMnPc1d1
predátoři	predátor	k1gMnPc1
jsou	být	k5eAaImIp3nP
pavouci	pavouk	k1gMnPc1
<g/>
,	,	kIx,
ropuchy	ropucha	k1gFnPc1
a	a	k8xC
žáby	žába	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letu	let	k1gInSc6
dosahuje	dosahovat	k5eAaImIp3nS
rychlosti	rychlost	k1gFnPc4
kolem	kolem	k7c2
8	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzhled	vzhled	k1gInSc1
</s>
<s>
Má	mít	k5eAaImIp3nS
1	#num#	k4
pár	pár	k4xCyI
blanitých	blanitý	k2eAgNnPc2d1
křídel	křídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dospělí	dospělý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
mají	mít	k5eAaImIp3nP
délku	délka	k1gFnSc4
6	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInSc1
hrudník	hrudník	k1gInSc1
je	být	k5eAaImIp3nS
šedivý	šedivý	k2eAgInSc1d1
se	s	k7c7
čtyřmi	čtyři	k4xCgFnPc7
podélnými	podélný	k2eAgFnPc7d1
tmavými	tmavý	k2eAgFnPc7d1
čarami	čára	k1gFnPc7
na	na	k7c4
zádech	zádech	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spodní	spodní	k2eAgFnPc4d1
části	část	k1gFnPc4
břicha	břicho	k1gNnSc2
jsou	být	k5eAaImIp3nP
žluté	žlutý	k2eAgNnSc4d1
a	a	k8xC
celé	celý	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
mají	mít	k5eAaImIp3nP
pokryté	pokrytý	k2eAgInPc1d1
chlupy	chlup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnPc1
jsou	být	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
větší	veliký	k2eAgInSc1d2
než	než	k8xS
samci	samec	k1gMnPc1
a	a	k8xC
mají	mít	k5eAaImIp3nP
větší	veliký	k2eAgInSc4d2
prostor	prostor	k1gInSc4
mezi	mezi	k7c7
červenýma	červený	k2eAgNnPc7d1
složenýma	složený	k2eAgNnPc7d1
očima	oko	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc4
pár	pár	k1gInSc4
křídel	křídlo	k1gNnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgMnSc1
je	být	k5eAaImIp3nS
přeměněn	přeměnit	k5eAaPmNgMnS
v	v	k7c4
kyvadélko	kyvadélko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
ji	on	k3xPp3gFnSc4
velmi	velmi	k6eAd1
často	často	k6eAd1
považují	považovat	k5eAaImIp3nP
za	za	k7c4
škůdce	škůdce	k1gMnPc4
<g/>
,	,	kIx,
především	především	k9
v	v	k7c6
letních	letní	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedají	sedat	k5eAaImIp3nP
na	na	k7c4
jídlo	jídlo	k1gNnSc4
a	a	k8xC
svými	svůj	k3xOyFgInPc7
výkaly	výkal	k1gInPc7
mohou	moct	k5eAaImIp3nP
přenášet	přenášet	k5eAaImF
různé	různý	k2eAgFnPc4d1
nemoci	nemoc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1
</s>
<s>
Vajíčka	vajíčko	k1gNnSc2
klade	klást	k5eAaImIp3nS
především	především	k9
na	na	k7c4
hnojiště	hnojiště	k1gNnSc4
a	a	k8xC
na	na	k7c4
rozkládající	rozkládající	k2eAgFnPc4d1
se	se	k3xPyFc4
části	část	k1gFnSc2
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mívá	mívat	k5eAaImIp3nS
až	až	k9
pět	pět	k4xCc1
generací	generace	k1gFnPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Housefly	Housefly	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Musca	Musca	k1gMnSc1
domestica	domestica	k1gMnSc1
(	(	kIx(
<g/>
moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioLib	BioLib	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TISCALI	TISCALI	kA
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mouchy	moucha	k1gFnPc1
přenášejí	přenášet	k5eAaImIp3nP
víc	hodně	k6eAd2
chorob	choroba	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
dosud	dosud	k6eAd1
předpokládalo	předpokládat	k5eAaImAgNnS
-	-	kIx~
Tiscali	Tiscali	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tiscali	Tiscali	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CRHA	Crha	k1gMnSc1
<g/>
,	,	kIx,
Aleš	Aleš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moucha	Moucha	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Inspirace	inspirace	k1gFnSc1
pro	pro	k7c4
tryskáč	tryskáč	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
i	i	k9
pro	pro	k7c4
raketu	raketa	k1gFnSc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
moucha	moucha	k1gFnSc1
domácí	domácí	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Musca	Muscus	k1gMnSc2
domestica	domesticus	k1gMnSc2
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Moucha	moucha	k1gFnSc1
domácí	domácí	k2eAgFnSc1d1
na	na	k7c4
BioLib	BioLib	k1gInSc4
<g/>
.	.	kIx.
<g/>
czMoucha	czMoucha	k1gFnSc1
domácí	domácí	k2eAgFnSc2d1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Entomologie	entomologie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4183816-6	4183816-6	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85062571	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85062571	#num#	k4
</s>
