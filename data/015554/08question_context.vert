<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
(	(	kIx(
<g/>
tatarsky	tatarsky	k6eAd1
Altı	Altı	k1gInSc1
Urda	urda	k1gFnSc1
<g/>
,	,	kIx,
آ	آ	k?
ا	ا	k?
<g/>
,	,	kIx,
А	А	k?
У	У	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
říše	říše	k1gFnSc2
rozšířený	rozšířený	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
říši	říše	k1gFnSc4
nazývající	nazývající	k2eAgFnSc4d1
se	se	k3xPyFc4
Ulug	Uluga	k1gFnPc2
Ulus	Ulus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Veliký	veliký	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známé	známý	k2eAgNnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
Ulus	Ulus	k1gInSc1
Džuči	Džuč	k1gFnSc3
(	(	kIx(
<g/>
„	„	k?
<g/>
Džučiho	Džuči	k1gMnSc2
lid	lid	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
„	„	k?
<g/>
Džučiho	Džuči	k1gMnSc4
země	zem	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
З	З	k?
у	у	k?
<g/>
)	)	kIx)
či	či	k8xC
podle	podle	k7c2
podmaněných	podmaněný	k2eAgInPc2d1
Kypčaků	Kypčak	k1gInPc2
Kypčacký	Kypčacký	k2eAgInSc4d1
chanát	chanát	k1gInSc4
<g/>
.	.	kIx.
</s>