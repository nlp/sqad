<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
</s>
<s>
Ulug	Ulug	k1gMnSc1
UlusUlus	UlusUlus	k1gMnSc1
Džuči	Džuč	k1gFnSc3
</s>
<s>
↓	↓	k?
</s>
<s>
1260	#num#	k4
<g/>
–	–	k?
<g/>
1502	#num#	k4
</s>
<s>
↓	↓	k?
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
geografie	geografie	k1gFnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1300	#num#	k4
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1389	#num#	k4
</s>
<s>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Saraj-Bátú	Saraj-Bátú	k?
(	(	kIx(
<g/>
západní	západní	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
,	,	kIx,
později	pozdě	k6eAd2
celkově	celkově	k6eAd1
<g/>
)	)	kIx)
<g/>
Sig	Sig	k1gFnSc1
<g/>
'	'	kIx"
<g/>
naq	naq	k?
(	(	kIx(
<g/>
levé	levý	k2eAgNnSc4d1
křídlo	křídlo	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
rozloha	rozloha	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
zhruba	zhruba	k6eAd1
6	#num#	k4
000	#num#	k4
000	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
1310	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
jazyky	jazyk	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
střední	střední	k2eAgFnSc1d1
mongolština	mongolština	k1gFnSc1
<g/>
,	,	kIx,
turkické	turkický	k2eAgFnPc1d1
(	(	kIx(
<g/>
kypčacký	kypčacký	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
náboženství	náboženství	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
cca	cca	kA
1240	#num#	k4
do	do	k7c2
1313	#num#	k4
<g/>
:	:	kIx,
<g/>
tengristické	tengristický	k2eAgNnSc4d1
<g/>
,	,	kIx,
šamanistické	šamanistický	k2eAgNnSc4d1
<g/>
,	,	kIx,
<g/>
ortodoxní	ortodoxní	k2eAgNnSc4d1
křesťanství	křesťanství	k1gNnSc4
<g/>
,	,	kIx,
<g/>
tibetský	tibetský	k2eAgInSc4d1
buddhismusislámské	buddhismusislámský	k2eAgFnPc4d1
(	(	kIx(
<g/>
od	od	k7c2
1313	#num#	k4
do	do	k7c2
konce	konec	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
</s>
<s>
státní	státní	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
polo-volená	polo-volený	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
<g/>
,	,	kIx,
<g/>
později	pozdě	k6eAd2
dědičná	dědičný	k2eAgFnSc1d1
monarchie	monarchie	k1gFnSc1
(	(	kIx(
<g/>
chanát	chanát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
mateřskázemě	mateřskázemě	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
Mongolská	mongolský	k2eAgFnSc1d1
říše	říš	k1gFnSc2
(	(	kIx(
<g/>
do	do	k7c2
roku	rok	k1gInSc2
1313	#num#	k4
nebo	nebo	k8xC
1368	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
vznik	vznik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1260	#num#	k4
–	–	k?
křídlo	křídlo	k1gNnSc4
Mongolské	mongolský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
</s>
<s>
zánik	zánik	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
1502	#num#	k4
–	–	k?
rozpad	rozpad	k1gInSc1
na	na	k7c4
menší	malý	k2eAgInPc4d2
chanáty	chanát	k1gInPc4
</s>
<s>
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
a	a	k8xC
území	území	k1gNnSc4
</s>
<s>
předcházející	předcházející	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Kumánsko-kypčacký	Kumánsko-kypčacký	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
</s>
<s>
Volžské	volžský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
následující	následující	k2eAgInPc4d1
<g/>
:	:	kIx,
</s>
<s>
Uzbecký	uzbecký	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Kvasimský	Kvasimský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Gazarie	Gazarie	k1gFnSc1
<g/>
(	(	kIx(
<g/>
Janovská	janovský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Astrachaňský	astrachaňský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Kazašský	kazašský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Krymský	krymský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Sibiřský	sibiřský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Nogajská	Nogajský	k2eAgFnSc1d1
horda	horda	k1gFnSc1
</s>
<s>
Kazaňský	kazaňský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
horda	horda	k1gFnSc1
</s>
<s>
Moldavské	moldavský	k2eAgNnSc1d1
knížectví	knížectví	k1gNnSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
(	(	kIx(
<g/>
tatarsky	tatarsky	k6eAd1
Altı	Altı	k1gInSc1
Urda	urda	k1gFnSc1
<g/>
,	,	kIx,
آ	آ	k?
ا	ا	k?
<g/>
,	,	kIx,
А	А	k?
У	У	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
říše	říše	k1gFnSc2
rozšířený	rozšířený	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
říši	říše	k1gFnSc4
nazývající	nazývající	k2eAgFnSc4d1
se	se	k3xPyFc4
Ulug	Uluga	k1gFnPc2
Ulus	Ulus	k1gInSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
Veliký	veliký	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
známé	známý	k2eAgNnSc1d1
také	také	k9
jako	jako	k8xC,k8xS
Ulus	Ulus	k1gInSc1
Džuči	Džuč	k1gFnSc3
(	(	kIx(
<g/>
„	„	k?
<g/>
Džučiho	Džuči	k1gMnSc2
lid	lid	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
odtud	odtud	k6eAd1
„	„	k?
<g/>
Džučiho	Džuči	k1gMnSc4
země	zem	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
З	З	k?
у	у	k?
<g/>
)	)	kIx)
či	či	k8xC
podle	podle	k7c2
podmaněných	podmaněný	k2eAgInPc2d1
Kypčaků	Kypčak	k1gInPc2
Kypčacký	Kypčacký	k2eAgInSc4d1
chanát	chanát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
rozlehlý	rozlehlý	k2eAgInSc4d1
státní	státní	k2eAgInSc4d1
útvar	útvar	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1242	#num#	k4
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc4
mongolské	mongolský	k2eAgFnSc2d1
veleříše	veleříše	k1gFnSc2
na	na	k7c6
ohromném	ohromný	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
a	a	k8xC
Sibiře	Sibiř	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1260	#num#	k4
se	se	k3xPyFc4
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
stala	stát	k5eAaPmAgFnS
samostatným	samostatný	k2eAgInSc7d1
státním	státní	k2eAgInSc7d1
útvarem	útvar	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
na	na	k7c4
několik	několik	k4yIc4
menších	malý	k2eAgInPc2d2
chanátů	chanát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
(	(	kIx(
<g/>
mongolsky	mongolsky	k6eAd1
А	А	k?
О	О	k?
–	–	k?
Altan	Altan	k1gInSc1
Ord	orda	k1gFnPc2
<g/>
,	,	kIx,
kazašsky	kazašsky	k6eAd1
Altı	Altı	k1gInSc1
Orda	orda	k1gFnSc1
<g/>
,	,	kIx,
tatarsky	tatarsky	k6eAd1
Altı	Altı	k1gInSc1
Urda	urda	k1gFnSc1
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
З	З	k?
О	О	k?
–	–	k?
Zolotaja	Zolotaj	k2eAgFnSc1d1
Orda	orda	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
mnohem	mnohem	k6eAd1
později	pozdě	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k9
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
v	v	k7c6
ruských	ruský	k2eAgInPc6d1
pramenech	pramen	k1gInPc6
(	(	kIx(
<g/>
původně	původně	k6eAd1
pouze	pouze	k6eAd1
„	„	k?
<g/>
orda	orda	k1gFnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
a	a	k8xC
první	první	k4xOgInSc4
rozmach	rozmach	k1gInSc4
</s>
<s>
Bátú-chán	Bátú-chán	k2eAgInSc1d1
</s>
<s>
Říši	říše	k1gFnSc4
založil	založit	k5eAaPmAgMnS
po	po	k7c6
ukončení	ukončení	k1gNnSc6
tažení	tažení	k1gNnSc2
do	do	k7c2
Evropy	Evropa	k1gFnSc2
chán	chán	k1gMnSc1
Bátú	Bátú	k1gMnSc1
<g/>
,	,	kIx,
vnuk	vnuk	k1gMnSc1
Čingischánův	Čingischánův	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
dal	dát	k5eAaPmAgMnS
před	před	k7c7
pokračováním	pokračování	k1gNnSc7
výbojů	výboj	k1gInPc2
přednost	přednost	k1gFnSc4
uspořádání	uspořádání	k1gNnSc3
dosud	dosud	k6eAd1
dobytých	dobytý	k2eAgFnPc2d1
nesmírně	smírně	k6eNd1
rozlehlých	rozlehlý	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozkládala	rozkládat	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
ohromném	ohromný	k2eAgNnSc6d1
území	území	k1gNnSc6
od	od	k7c2
severního	severní	k2eAgNnSc2d1
Černomoří	Černomoří	k1gNnSc2
a	a	k8xC
úpatí	úpatí	k1gNnSc6
Kavkazu	Kavkaz	k1gInSc2
přes	přes	k7c4
Povolží	Povolží	k1gNnSc4
až	až	k9
na	na	k7c4
západní	západní	k2eAgFnSc4d1
Sibiř	Sibiř	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvořil	tvořit	k5eAaImAgInS
jeho	jeho	k3xOp3gFnSc4
severní	severní	k2eAgFnSc4d1
a	a	k8xC
východní	východní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
horní	horní	k2eAgInSc4d1
tok	tok	k1gInSc4
Obu	Ob	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gNnSc7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
byla	být	k5eAaImAgFnS
Saraj	Saraj	k1gFnSc1
Bátú	Bátú	k1gFnSc2
v	v	k7c6
deltě	delta	k1gFnSc6
Volhy	Volha	k1gFnSc2
<g/>
,	,	kIx,
blízko	blízko	k7c2
dnešní	dnešní	k2eAgFnSc2d1
Astrachaně	Astrachaň	k1gFnSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
Saraj	Saraj	k1gFnSc1
Berke	Berke	k1gFnSc1
(	(	kIx(
<g/>
město	město	k1gNnSc1
není	být	k5eNaImIp3nS
přesně	přesně	k6eAd1
lokalizováno	lokalizován	k2eAgNnSc1d1
<g/>
,	,	kIx,
soudí	soudit	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
existovalo	existovat	k5eAaImAgNnS
poblíž	poblíž	k7c2
dnešního	dnešní	k2eAgInSc2d1
Volgogradu	Volgograd	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
města	město	k1gNnSc2
stavěli	stavět	k5eAaImAgMnP
křesťanští	křesťanský	k2eAgMnPc1d1
zajatci	zajatec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolští	mongolský	k2eAgMnPc1d1
dobyvatelé	dobyvatel	k1gMnPc1
sice	sice	k8xC
žili	žít	k5eAaImAgMnP
ve	v	k7c6
stanech	stan	k1gInPc6
<g/>
,	,	kIx,
podporovali	podporovat	k5eAaImAgMnP
ale	ale	k9
usedlé	usedlý	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
říši	říš	k1gFnSc6
kvetla	kvést	k5eAaImAgNnP
bohatá	bohatý	k2eAgNnPc1d1
obchodní	obchodní	k2eAgNnPc1d1
města	město	k1gNnPc1
<g/>
:	:	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
roku	rok	k1gInSc2
1395	#num#	k4
zničil	zničit	k5eAaPmAgMnS
Timur	Timur	k1gMnSc1
Lenk	Lenk	k1gMnSc1
(	(	kIx(
<g/>
Tamerlán	Tamerlán	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
byla	být	k5eAaImAgFnS
zpočátku	zpočátku	k6eAd1
součástí	součást	k1gFnSc7
mongolské	mongolský	k2eAgFnSc2d1
veleříše	veleříše	k1gFnSc2
a	a	k8xC
podléhala	podléhat	k5eAaImAgFnS
vrchní	vrchní	k1gFnSc4
vládě	vláda	k1gFnSc3
velikého	veliký	k2eAgMnSc2d1
chána	chán	k1gMnSc2
ve	v	k7c6
vzdáleném	vzdálený	k2eAgInSc6d1
Karakorumu	Karakorum	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k6eAd1
Čingischánovi	Čingischánův	k2eAgMnPc1d1
vnuci	vnuk	k1gMnPc1
měli	mít	k5eAaImAgMnP
velké	velký	k2eAgInPc4d1
problémy	problém	k1gInPc4
s	s	k7c7
udržením	udržení	k1gNnSc7
jednoty	jednota	k1gFnSc2
tak	tak	k8xC,k8xS
rozsáhlého	rozsáhlý	k2eAgInSc2d1
celku	celek	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
zformoval	zformovat	k5eAaPmAgInS
rychlými	rychlý	k2eAgInPc7d1
výboji	výboj	k1gInPc7
<g/>
,	,	kIx,
ale	ale	k8xC
neupevnil	upevnit	k5eNaPmAgMnS
vnitřními	vnitřní	k2eAgFnPc7d1
ekonomickými	ekonomický	k2eAgFnPc7d1
vazbami	vazba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bojů	boj	k1gInPc2
o	o	k7c4
nástupnictví	nástupnictví	k1gNnSc4
po	po	k7c6
nástupu	nástup	k1gInSc6
velkého	velký	k2eAgMnSc2d1
chána	chán	k1gMnSc2
Kubilaje	Kubilaje	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1260	#num#	k4
se	se	k3xPyFc4
vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
mongolským	mongolský	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
a	a	k8xC
západním	západní	k2eAgInSc7d1
ulusem	ulus	k1gInSc7
Džuči	Džuč	k1gFnSc6
uvolnily	uvolnit	k5eAaPmAgFnP
a	a	k8xC
chán	chán	k1gMnSc1
Berke	Berk	k1gFnSc2
<g/>
,	,	kIx,
Bátúův	Bátúův	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
<g/>
,	,	kIx,
Zlatou	zlatý	k2eAgFnSc4d1
hordu	horda	k1gFnSc4
osamostatnil	osamostatnit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
mongolským	mongolský	k2eAgMnSc7d1
chánem	chán	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přijal	přijmout	k5eAaPmAgMnS
islám	islám	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
islamizoval	islamizovat	k5eAaBmAgMnS
tak	tak	k6eAd1
vládnoucí	vládnoucí	k2eAgFnSc4d1
dynastii	dynastie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
na	na	k7c6
Rusi	Rus	k1gFnSc6
</s>
<s>
Výběrčí	výběrčí	k1gFnSc1
daní	daň	k1gFnPc2
z	z	k7c2
hordy	horda	k1gFnSc2
ve	v	k7c6
městě	město	k1gNnSc6
na	na	k7c6
Rusi	Rus	k1gFnSc6
</s>
<s>
Ruská	ruský	k2eAgNnPc1d1
knížectví	knížectví	k1gNnPc1
formálně	formálně	k6eAd1
nebyla	být	k5eNaImAgFnS
součástí	součást	k1gFnSc7
území	území	k1gNnSc2
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
byla	být	k5eAaImAgFnS
Mongolům	Mongol	k1gMnPc3
podřízena	podřízen	k2eAgNnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
chtěla	chtít	k5eAaImAgFnS
rurikovská	rurikovský	k2eAgFnSc1d1
knížata	kníže	k1gMnPc1wR
vládnout	vládnout	k5eAaImF
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
je	být	k5eAaImIp3nS
v	v	k7c6
jejich	jejich	k3xOp3gInPc6
postech	post	k1gInPc6
potvrdit	potvrdit	k5eAaPmF
chán	chán	k1gMnSc1
zvláštní	zvláštní	k2eAgFnSc4d1
listinou	listina	k1gFnSc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
jarlykem	jarlyk	k1gInSc7
(	(	kIx(
<g/>
obdobou	obdoba	k1gFnSc7
evropského	evropský	k2eAgNnSc2d1
léna	léno	k1gNnSc2
<g/>
)	)	kIx)
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
uznala	uznat	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc4
svrchovanou	svrchovaný	k2eAgFnSc4d1
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
příslušnost	příslušnost	k1gFnSc1
k	k	k7c3
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
pravomoci	pravomoc	k1gFnSc6
velkého	velký	k2eAgMnSc2d1
chána	chán	k1gMnSc2
(	(	kIx(
<g/>
mongolsky	mongolsky	k6eAd1
čingischána	čingischán	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
Bátúovou	Bátúový	k2eAgFnSc7d1
povinností	povinnost	k1gFnSc7
bylo	být	k5eAaImAgNnS
posílat	posílat	k5eAaImF
ruská	ruský	k2eAgNnPc4d1
knížata	kníže	k1gNnPc4
za	za	k7c7
tímto	tento	k3xDgInSc7
účelem	účel	k1gInSc7
do	do	k7c2
Karakorumu	Karakorum	k1gInSc2
<g/>
,	,	kIx,
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
mongolské	mongolský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
to	ten	k3xDgNnSc1
však	však	k9
nedělal	dělat	k5eNaImAgMnS
a	a	k8xC
vyřizoval	vyřizovat	k5eAaImAgMnS
jejich	jejich	k3xOp3gFnPc4
záležitosti	záležitost	k1gFnPc4
osobně	osobně	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
táboře	tábor	k1gInSc6
a	a	k8xC
později	pozdě	k6eAd2
na	na	k7c6
svém	svůj	k3xOyFgInSc6
dvoře	dvůr	k1gInSc6
v	v	k7c6
Saráji	Saráj	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
„	„	k?
<g/>
hordě	horda	k1gFnSc6
<g/>
“	“	k?
byla	být	k5eAaImAgFnS
ruská	ruský	k2eAgFnSc1d1
knížata	kníže	k1gMnPc1wR
vystavována	vystavován	k2eAgNnPc4d1
ponižování	ponižování	k1gNnPc4
i	i	k8xC
osobnímu	osobní	k2eAgNnSc3d1
nebezpečí	nebezpečí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tataři	Tatar	k1gMnPc1
podporovali	podporovat	k5eAaImAgMnP
rozbroje	rozbroj	k1gInPc4
mezi	mezi	k7c7
znesvářenými	znesvářený	k2eAgMnPc7d1
Rurikovci	Rurikovec	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
brzy	brzy	k6eAd1
naučili	naučit	k5eAaPmAgMnP
proti	proti	k7c3
sobě	se	k3xPyFc3
v	v	k7c6
Saráji	Saráj	k1gInSc6
intrikovat	intrikovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	to	k9
na	na	k7c6
druhé	druhý	k4xOgFnSc6
straně	strana	k1gFnSc6
chánům	chán	k1gMnPc3
umožnilo	umožnit	k5eAaPmAgNnS
vykonávat	vykonávat	k5eAaImF
na	na	k7c6
Rusi	Rus	k1gFnSc6
úlohu	úloha	k1gFnSc4
jakéhosi	jakýsi	k3yIgMnSc2
arbitra	arbiter	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Koncem	koncem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
obyvatelstvo	obyvatelstvo	k1gNnSc4
ruských	ruský	k2eAgNnPc2d1
knížectví	knížectví	k1gNnPc2
podrobeno	podrobit	k5eAaPmNgNnS
tvrdým	tvrdý	k2eAgFnPc3d1
povinnostem	povinnost	k1gFnPc3
<g/>
,	,	kIx,
muselo	muset	k5eAaImAgNnS
poskytovat	poskytovat	k5eAaImF
Mongolům	Mongol	k1gMnPc3
rekruty	rekrut	k1gMnPc4
a	a	k8xC
platit	platit	k5eAaImF
daň	daň	k1gFnSc4
z	z	k7c2
hlavy	hlava	k1gFnSc2
–	–	k?
ruskými	ruský	k2eAgInPc7d1
prameny	pramen	k1gInPc7
nazývanou	nazývaný	k2eAgFnSc4d1
vychod	vychod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
tatarští	tatarský	k2eAgMnPc1d1
výběrčí	výběrčí	k1gMnPc1
postupovali	postupovat	k5eAaImAgMnP
velmi	velmi	k6eAd1
krutě	krutě	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
při	při	k7c6
vybírání	vybírání	k1gNnSc6
daní	daň	k1gFnPc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
častým	častý	k2eAgNnPc3d1
povstáním	povstání	k1gNnPc3
<g/>
,	,	kIx,
především	především	k6eAd1
ve	v	k7c6
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chování	chování	k1gNnSc1
knížat	kníže	k1gMnPc2wR
i	i	k8xC
prostých	prostý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
kontrolovali	kontrolovat	k5eAaImAgMnP
chánovi	chánův	k2eAgMnPc1d1
zmocněnci	zmocněnec	k1gMnPc1
<g/>
,	,	kIx,
baskakové	baskakový	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
hlavním	hlavní	k2eAgInSc7d1
ruským	ruský	k2eAgInSc7d1
problémem	problém	k1gInSc7
za	za	k7c2
tatarské	tatarský	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
ekonomické	ekonomický	k2eAgNnSc1d1
vykořisťování	vykořisťování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politická	politický	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
potvrzování	potvrzování	k1gNnSc2
knížat	kníže	k1gMnPc2wR
v	v	k7c6
jejich	jejich	k3xOp3gInPc6
postech	post	k1gInPc6
<g/>
,	,	kIx,
především	především	k6eAd1
velikého	veliký	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
vladimirského	vladimirský	k2eAgMnSc2d1
<g/>
,	,	kIx,
zůstala	zůstat	k5eAaPmAgFnS
zachována	zachovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nedotčeno	dotknout	k5eNaPmNgNnS
bylo	být	k5eAaImAgNnS
i	i	k9
pravoslavné	pravoslavný	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
obyvatel	obyvatel	k1gMnPc2
ruských	ruský	k2eAgNnPc2d1
knížectví	knížectví	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchovní	duchovní	k1gMnPc1
byli	být	k5eAaImAgMnP
naopak	naopak	k6eAd1
zvýhodněni	zvýhodnit	k5eAaPmNgMnP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
nemuseli	muset	k5eNaImAgMnP
například	například	k6eAd1
platit	platit	k5eAaImF
daně	daň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Mongolská	mongolský	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
a	a	k8xC
následná	následný	k2eAgFnSc1d1
nadvláda	nadvláda	k1gFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
Rusi	Rus	k1gFnSc6
dalekosáhlé	dalekosáhlý	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
v	v	k7c6
hospodářské	hospodářský	k2eAgFnSc6d1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
politické	politický	k2eAgFnSc6d1
a	a	k8xC
kulturní	kulturní	k2eAgFnSc6d1
sféře	sféra	k1gFnSc6
a	a	k8xC
ruské	ruský	k2eAgFnPc1d1
země	zem	k1gFnPc1
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
vypořádávaly	vypořádávat	k5eAaImAgFnP
po	po	k7c4
řadu	řada	k1gFnSc4
staletí	staletí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kulikovském	Kulikovský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kulikovském	Kulikovský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
Turkických	turkický	k2eAgInPc2d1
národů	národ	k1gInPc2
Historie	historie	k1gFnSc2
turkických	turkický	k2eAgInPc2d1
národůdo	národůdo	k1gNnSc4
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Turkucký	Turkucký	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
552	#num#	k4
<g/>
–	–	k?
<g/>
744	#num#	k4
</s>
<s>
Turkuti	Turkut	k1gMnPc1
</s>
<s>
Západní	západní	k2eAgMnPc1d1
Turkuti	Turkut	k1gMnPc1
</s>
<s>
Východní	východní	k2eAgFnPc1d1
Turkuti	Turkuť	k1gFnPc1
</s>
<s>
Modří	modrý	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Avarský	avarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
564	#num#	k4
<g/>
–	–	k?
<g/>
804	#num#	k4
</s>
<s>
Chazarský	chazarský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
618	#num#	k4
<g/>
–	–	k?
<g/>
1048	#num#	k4
</s>
<s>
Sı	Sı	k2eAgMnSc1d1
628	#num#	k4
<g/>
–	–	k?
<g/>
646	#num#	k4
</s>
<s>
Onogurie	Onogurie	k1gFnSc1
632	#num#	k4
<g/>
–	–	k?
<g/>
668	#num#	k4
</s>
<s>
Dunajské	dunajský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Volžské	volžský	k2eAgNnSc1d1
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Kangarská	Kangarský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
659	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
</s>
<s>
Türgešský	Türgešský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
699	#num#	k4
<g/>
–	–	k?
<g/>
766	#num#	k4
</s>
<s>
Ujgurský	Ujgurský	k2eAgInSc1d1
kaganát	kaganát	k1gInSc1
744	#num#	k4
<g/>
–	–	k?
<g/>
840	#num#	k4
</s>
<s>
Karlucký	Karlucký	k2eAgInSc1d1
jabguluk	jabguluk	k1gInSc1
756	#num#	k4
<g/>
–	–	k?
<g/>
940	#num#	k4
</s>
<s>
Karachánský	Karachánský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
840	#num#	k4
<g/>
–	–	k?
<g/>
1212	#num#	k4
</s>
<s>
Západní	západní	k2eAgMnPc1d1
Karachánové	Karachán	k1gMnPc1
</s>
<s>
Východní	východní	k2eAgFnSc1d1
Karachánové	Karachánové	k2eAgFnSc1d1
</s>
<s>
Kan-čouské	Kan-čouský	k2eAgNnSc1d1
království	království	k1gNnSc1
848-1036	848-1036	k4
</s>
<s>
Kao-čchangské	Kao-čchangský	k2eAgNnSc1d1
království	království	k1gNnSc1
856-1335	856-1335	k4
</s>
<s>
Pečeněžský	Pečeněžský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
860	#num#	k4
<g/>
–	–	k?
<g/>
1091	#num#	k4
</s>
<s>
Kimäcký	Kimäcký	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
743	#num#	k4
<g/>
–	–	k?
<g/>
1035	#num#	k4
</s>
<s>
Kumánie	Kumánie	k1gFnSc1
<g/>
1067	#num#	k4
<g/>
–	–	k?
<g/>
1239	#num#	k4
</s>
<s>
Oguzský	Oguzský	k2eAgInSc1d1
jabguluk	jabguluk	k1gInSc1
<g/>
750	#num#	k4
<g/>
–	–	k?
<g/>
1055	#num#	k4
</s>
<s>
Ghaznovská	Ghaznovský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
963	#num#	k4
<g/>
–	–	k?
<g/>
1186	#num#	k4
</s>
<s>
Seldžucká	Seldžucký	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1037	#num#	k4
<g/>
–	–	k?
<g/>
1194	#num#	k4
</s>
<s>
Chórezmská	Chórezmský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1077	#num#	k4
<g/>
–	–	k?
<g/>
1231	#num#	k4
</s>
<s>
Rúmský	Rúmský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
Seldžuků	Seldžuk	k1gInPc2
1092	#num#	k4
<g/>
–	–	k?
<g/>
1307	#num#	k4
</s>
<s>
Dillíský	dillíský	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
1206	#num#	k4
<g/>
–	–	k?
<g/>
1526	#num#	k4
</s>
<s>
Dynastie	dynastie	k1gFnSc1
otroků	otrok	k1gMnPc2
(	(	kIx(
<g/>
Dillí	Dillí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Chaldží	Chaldž	k1gFnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Tughlakovců	Tughlakovec	k1gMnPc2
</s>
<s>
Kypčacký	Kypčacký	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
1240	#num#	k4
<g/>
–	–	k?
<g/>
1502	#num#	k4
</s>
<s>
Mamlúcký	mamlúcký	k2eAgInSc1d1
sultanát	sultanát	k1gInSc1
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
1250	#num#	k4
<g/>
–	–	k?
<g/>
1517	#num#	k4
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Bahríovců	Bahríovec	k1gMnPc2
</s>
<s>
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
1299	#num#	k4
<g/>
–	–	k?
<g/>
1923	#num#	k4
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1
turkické	turkický	k2eAgFnPc1d1
dynastie	dynastie	k1gFnPc1
</s>
<s>
v	v	k7c6
Anatolii	Anatolie	k1gFnSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Artúkovců	Artúkovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Saltúkovců	Saltúkovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Ázerbájdžánu	Ázerbájdžán	k1gInSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Íldenizovců	Íldenizovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Egyptu	Egypt	k1gInSc3
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Tulúnovců	Tulúnovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Íchšídovců	Íchšídovec	k1gMnPc2
</s>
<s>
v	v	k7c6
Levantě	Levanta	k1gFnSc6
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Búríjovců	Búríjovec	k1gMnPc2
</s>
<s>
Dynastie	dynastie	k1gFnSc1
Zengíjovců	Zengíjovec	k1gMnPc2
</s>
<s>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
•	•	k?
editovat	editovat	k5eAaImF
</s>
<s>
Vlajka	vlajka	k1gFnSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
chána	chán	k1gMnSc2
Öz	Öz	k1gMnSc2
Bega	beg	k1gMnSc2
(	(	kIx(
<g/>
1313	#num#	k4
<g/>
–	–	k?
<g/>
1341	#num#	k4
<g/>
)	)	kIx)
zachycená	zachycený	k2eAgFnSc1d1
na	na	k7c6
obchodní	obchodní	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1339	#num#	k4
</s>
<s>
Bitva	bitva	k1gFnSc1
na	na	k7c6
Kulikovském	Kulikovský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
(	(	kIx(
<g/>
1380	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Do	do	k7c2
poloviny	polovina	k1gFnSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
nejrozsáhlejším	rozsáhlý	k2eAgNnPc3d3
a	a	k8xC
vojensky	vojensky	k6eAd1
nejsilnějším	silný	k2eAgInSc7d3
státem	stát	k1gInSc7
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jejího	její	k3xOp3gNnSc2
centra	centrum	k1gNnSc2
v	v	k7c6
Saráji	Saráj	k1gInSc6
plynuly	plynout	k5eAaImAgInP
nevýslovné	výslovný	k2eNgInPc1d1
příjmy	příjem	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
pocházely	pocházet	k5eAaImAgFnP
z	z	k7c2
daní	daň	k1gFnPc2
vybíraných	vybíraný	k2eAgFnPc2d1
v	v	k7c6
podmaněných	podmaněný	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
z	z	k7c2
kontroly	kontrola	k1gFnSc2
tranzitu	tranzit	k1gInSc2
probíhajícího	probíhající	k2eAgInSc2d1
po	po	k7c6
Volze	Volha	k1gFnSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
spojovala	spojovat	k5eAaImAgFnS
severomořsko-baltskou	severomořsko-baltský	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
s	s	k7c7
Orientem	Orient	k1gInSc7
<g/>
,	,	kIx,
z	z	k7c2
obchodních	obchodní	k2eAgFnPc2d1
transakcí	transakce	k1gFnPc2
Benátčanů	Benátčan	k1gMnPc2
a	a	k8xC
Janovanů	Janovan	k1gMnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
faktorie	faktorie	k1gFnPc1
byly	být	k5eAaImAgFnP
rozesety	rozesít	k5eAaPmNgFnP
po	po	k7c4
pobřeží	pobřeží	k1gNnSc4
Černého	Černého	k2eAgNnSc2d1
a	a	k8xC
Azovského	azovský	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodující	rozhodující	k2eAgInPc1d1
postavení	postavení	k1gNnSc4
v	v	k7c6
chanátu	chanát	k1gInSc6
zaujímala	zaujímat	k5eAaImAgFnS
od	od	k7c2
Bátúových	Bátúův	k2eAgFnPc2d1
dob	doba	k1gFnPc2
nomádská	nomádský	k2eAgFnSc1d1
mongolská	mongolský	k2eAgFnSc1d1
šlechta	šlechta	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
konzervativní	konzervativní	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
byl	být	k5eAaImAgInS
založen	založit	k5eAaPmNgInS
na	na	k7c6
úspěšné	úspěšný	k2eAgFnSc6d1
expanzi	expanze	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
té	ten	k3xDgFnSc6
bylo	být	k5eAaImAgNnS
ovšem	ovšem	k9
stále	stále	k6eAd1
méně	málo	k6eAd2
příležitostí	příležitost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
bylo	být	k5eAaImAgNnS
velmi	velmi	k6eAd1
nesnadné	snadný	k2eNgNnSc1d1
ovládat	ovládat	k5eAaImF
tak	tak	k9
obrovské	obrovský	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
rozdělovaly	rozdělovat	k5eAaImAgFnP
složité	složitý	k2eAgInPc4d1
politické	politický	k2eAgInPc4d1
<g/>
,	,	kIx,
etnické	etnický	k2eAgInPc4d1
i	i	k8xC
sociální	sociální	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1357	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zemřel	zemřít	k5eAaPmAgMnS
chán	chán	k1gMnSc1
Džani	Džan	k1gMnPc1
Beg	beg	k1gMnSc1
<g/>
,	,	kIx,
Uzbekův	Uzbekův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
začala	začít	k5eAaPmAgFnS
moc	moc	k1gFnSc1
chánů	chán	k1gMnPc2
výrazně	výrazně	k6eAd1
slábnout	slábnout	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1357	#num#	k4
až	až	k8xS
1381	#num#	k4
se	se	k3xPyFc4
jich	on	k3xPp3gFnPc2
na	na	k7c6
trůnu	trůn	k1gInSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
dvacet	dvacet	k4xCc1
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
téměř	téměř	k6eAd1
pravidlem	pravidlem	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
chanát	chanát	k1gInSc1
dělil	dělit	k5eAaImAgInS
mezi	mezi	k7c4
dva	dva	k4xCgMnPc4
vládce	vládce	k1gMnPc4
(	(	kIx(
<g/>
hranicí	hranice	k1gFnSc7
byla	být	k5eAaImAgFnS
Volha	Volha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Zlaté	zlatý	k2eAgFnSc6d1
hordě	horda	k1gFnSc6
se	se	k3xPyFc4
projevily	projevit	k5eAaPmAgFnP
decentralizační	decentralizační	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
<g/>
,	,	kIx,
osamostatnili	osamostatnit	k5eAaPmAgMnP
se	se	k3xPyFc4
Volžští	volžský	k2eAgMnPc1d1
Bulhaři	Bulhar	k1gMnPc1
a	a	k8xC
Chorézm	Chorézm	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
prudké	prudký	k2eAgFnSc2d1
litevské	litevský	k2eAgFnSc2d1
expanze	expanze	k1gFnSc2
byly	být	k5eAaImAgFnP
ztraceny	ztratit	k5eAaPmNgFnP
západoruské	západoruský	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
(	(	kIx(
<g/>
tatarské	tatarský	k2eAgNnSc1d1
panství	panství	k1gNnSc1
na	na	k7c6
tomto	tento	k3xDgNnSc6
teritoriu	teritorium	k1gNnSc6
ukončila	ukončit	k5eAaPmAgFnS
porážka	porážka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1363	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
ruským	ruský	k2eAgInSc7d1
severovýchodem	severovýchod	k1gInSc7
fakticky	fakticky	k6eAd1
převzal	převzít	k5eAaPmAgMnS
veliký	veliký	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
vladimirský	vladimirský	k2eAgMnSc1d1
Dmitrij	Dmitrij	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
mongolskou	mongolský	k2eAgFnSc7d1
vládnoucí	vládnoucí	k2eAgFnSc7d1
vrstvou	vrstva	k1gFnSc7
panovala	panovat	k5eAaImAgFnS
silná	silný	k2eAgFnSc1d1
řevnivost	řevnivost	k1gFnSc1
a	a	k8xC
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nedosáhli	dosáhnout	k5eNaPmAgMnP
postavení	postavení	k1gNnSc4
podle	podle	k7c2
svých	svůj	k3xOyFgFnPc2
představ	představa	k1gFnPc2
<g/>
,	,	kIx,
uspokojovali	uspokojovat	k5eAaImAgMnP
své	svůj	k3xOyFgFnPc4
ambice	ambice	k1gFnPc4
nájezdy	nájezd	k1gInPc7
do	do	k7c2
ruského	ruský	k2eAgNnSc2d1
pohraničí	pohraničí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proniknout	proniknout	k5eAaPmF
na	na	k7c4
severní	severní	k2eAgInSc4d1
břeh	břeh	k1gInSc4
Oky	oka	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
tvořila	tvořit	k5eAaImAgFnS
hranici	hranice	k1gFnSc4
moskevského	moskevský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
neodvažovali	odvažovat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
začala	začít	k5eAaPmAgNnP
ruská	ruský	k2eAgNnPc1d1
knížata	kníže	k1gNnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnPc1
postavení	postavení	k1gNnPc1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
upevňovalo	upevňovat	k5eAaImAgNnS
<g/>
,	,	kIx,
pomýšlet	pomýšlet	k5eAaImF
na	na	k7c4
ozbrojený	ozbrojený	k2eAgInSc4d1
odpor	odpor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
nabyly	nabýt	k5eAaPmAgFnP
pohraniční	pohraniční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
vážných	vážný	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivá	jednotlivý	k2eAgFnSc1d1
knížectví	knížectví	k1gNnSc2
se	se	k3xPyFc4
často	často	k6eAd1
dožadovala	dožadovat	k5eAaImAgFnS
pomoci	pomoct	k5eAaPmF
Moskvy	Moskva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1374	#num#	k4
vypuklo	vypuknout	k5eAaPmAgNnS
povstání	povstání	k1gNnSc1
proti	proti	k7c3
Tatarům	Tatar	k1gMnPc3
v	v	k7c6
Nižním	nižní	k2eAgInSc6d1
Novgorodu	Novgorod	k1gInSc6
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgMnSc6
přišli	přijít	k5eAaPmAgMnP
o	o	k7c4
život	život	k1gInSc4
členové	člen	k1gMnPc1
chánova	chánův	k2eAgNnSc2d1
poselstva	poselstvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1377	#num#	k4
si	se	k3xPyFc3
Dmitrij	Dmitrij	k1gMnSc1
dovolil	dovolit	k5eAaPmAgMnS
zpoplatnit	zpoplatnit	k5eAaPmF
místního	místní	k2eAgMnSc4d1
vládce	vládce	k1gMnSc4
v	v	k7c6
Kazani	Kazaň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
dosáhl	dosáhnout	k5eAaPmAgInS
prvního	první	k4xOgNnSc2
významnějšího	významný	k2eAgNnSc2d2
vítězství	vítězství	k1gNnSc2
nad	nad	k7c7
Tatary	tatar	k1gInPc7
na	na	k7c6
řece	řeka	k1gFnSc6
Voži	Vož	k1gFnSc2
(	(	kIx(
<g/>
1378	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
prokázal	prokázat	k5eAaPmAgMnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
Rusové	Rus	k1gMnPc1
mohou	moct	k5eAaImIp3nP
Tatary	Tatar	k1gMnPc4
porazit	porazit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
pochopil	pochopit	k5eAaPmAgMnS
také	také	k9
tatarský	tatarský	k2eAgMnSc1d1
emír	emír	k1gMnSc1
Mamaj	Mamaj	k1gMnSc1
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yQgMnSc3,k3yRgMnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
na	na	k7c4
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
ovládnout	ovládnout	k5eAaPmF
západní	západní	k2eAgFnSc4d1
část	část	k1gFnSc4
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodl	rozhodnout	k5eAaPmAgMnS
se	se	k3xPyFc4
uspořádat	uspořádat	k5eAaPmF
mohutné	mohutný	k2eAgNnSc1d1
tažení	tažení	k1gNnSc1
na	na	k7c4
Rus	Rus	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
zabránil	zabránit	k5eAaPmAgInS
její	její	k3xOp3gFnPc4
politické	politický	k2eAgFnPc4d1
emancipaci	emancipace	k1gFnSc4
a	a	k8xC
obnovil	obnovit	k5eAaPmAgInS
poplatnou	poplatný	k2eAgFnSc4d1
závislost	závislost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Početné	početný	k2eAgFnPc4d1
vojsko	vojsko	k1gNnSc1
sbíral	sbírat	k5eAaImAgMnS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
říši	říš	k1gFnSc6
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
spojencem	spojenec	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
rjazaňský	rjazaňský	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
Oleg	Oleg	k1gMnSc1
Ivanovič	Ivanovič	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
výrazně	výrazně	k6eAd1
upevnil	upevnit	k5eAaPmAgInS
knížecí	knížecí	k2eAgFnSc4d1
moc	moc	k1gFnSc4
v	v	k7c6
Rjazani	Rjazaň	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
mladý	mladý	k2eAgMnSc1d1
litevský	litevský	k2eAgMnSc1d1
velkokníže	velkokníže	k1gMnSc1
Jagello	Jagello	k1gNnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
chtěl	chtít	k5eAaImAgMnS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
dobyvačné	dobyvačný	k2eAgFnSc6d1
východní	východní	k2eAgFnSc6d1
politice	politika	k1gFnSc6
svého	svůj	k3xOyFgMnSc2
otce	otec	k1gMnSc2
Algirdase	Algirdasa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1380	#num#	k4
byli	být	k5eAaImAgMnP
Tataři	Tatar	k1gMnPc1
poraženi	poražen	k2eAgMnPc1d1
v	v	k7c6
první	první	k4xOgFnSc6
velké	velký	k2eAgFnSc6d1
otevřené	otevřený	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
s	s	k7c7
Rusy	Rus	k1gMnPc7
na	na	k7c6
Kulikovském	Kulikovský	k2eAgNnSc6d1
poli	pole	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mamaj	Mamaj	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pozoroval	pozorovat	k5eAaImAgMnS
celý	celý	k2eAgInSc4d1
boj	boj	k1gInSc4
z	z	k7c2
blízkého	blízký	k2eAgInSc2d1
pahorku	pahorek	k1gInSc2
<g/>
,	,	kIx,
uprchl	uprchnout	k5eAaPmAgMnS
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Kalky	kalk	k1gInPc1
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
bojiště	bojiště	k1gNnSc2
Bitvy	bitva	k1gFnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
Kalce	Kalce	k?
z	z	k7c2
roku	rok	k1gInSc2
1223	#num#	k4
<g/>
,	,	kIx,
ho	on	k3xPp3gInSc4
dostihl	dostihnout	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc1
soupeř	soupeř	k1gMnSc1
ve	v	k7c6
Zlaté	zlatý	k2eAgFnSc6d1
hordě	horda	k1gFnSc6
Tochtamiš	Tochtamiš	k1gInSc4
a	a	k8xC
uštědřil	uštědřit	k5eAaPmAgInS
zbytkům	zbytek	k1gInPc3
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vojska	vojsko	k1gNnSc2
další	další	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
se	se	k3xPyFc4
Mamaj	Mamaj	k1gMnSc1
uchýlil	uchýlit	k5eAaPmAgMnS
do	do	k7c2
Kaffy	Kaff	k1gInPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s>
Tochtamiš	Tochtamiš	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
novým	nový	k2eAgMnSc7d1
chánem	chán	k1gMnSc7
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
a	a	k8xC
počátkem	počátkem	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
dočasně	dočasně	k6eAd1
chanát	chanát	k1gInSc4
sjednotit	sjednotit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporu	podpor	k1gInSc2
mu	on	k3xPp3gMnSc3
poskytl	poskytnout	k5eAaPmAgMnS
tatarský	tatarský	k2eAgMnSc1d1
dobyvatel	dobyvatel	k1gMnSc1
Tímúr-Í-Lenk	Tímúr-Í-Lenk	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
nazývaný	nazývaný	k2eAgInSc1d1
Tamerlán	Tamerlán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
krátce	krátce	k6eAd1
předtím	předtím	k6eAd1
sjednotil	sjednotit	k5eAaPmAgInS
mongolské	mongolský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
(	(	kIx(
<g/>
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
jeho	jeho	k3xOp3gFnSc2
říše	říš	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Samarkand	Samarkand	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
mohutné	mohutný	k2eAgInPc4d1
výboje	výboj	k1gInPc4
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
od	od	k7c2
indického	indický	k2eAgNnSc2d1
Dillí	Dillí	k1gNnSc2
až	až	k9
po	po	k7c4
Moskvu	Moskva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
závislost	závislost	k1gFnSc1
ruských	ruský	k2eAgMnPc2d1
knížectví	knížectví	k1gNnSc2
byla	být	k5eAaImAgNnP
obnovena	obnovit	k5eAaPmNgNnP
pouze	pouze	k6eAd1
po	po	k7c6
stránce	stránka	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
resp.	resp.	kA
dynastická	dynastický	k2eAgFnSc1d1
relativní	relativní	k2eAgFnSc1d1
nezávislost	nezávislost	k1gFnSc1
zůstala	zůstat	k5eAaPmAgFnS
nezměněná	změněný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tochtamiš	Tochtamiš	k1gInSc1
podnikl	podniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1382	#num#	k4
další	další	k2eAgFnSc2d1
tažení	tažení	k1gNnSc4
na	na	k7c4
Rus	Rus	k1gFnSc4
<g/>
,	,	kIx,
během	během	k7c2
něhož	jenž	k3xRgMnSc2
Tataři	Tatar	k1gMnPc1
vydrancovali	vydrancovat	k5eAaPmAgMnP
a	a	k8xC
vypálili	vypálit	k5eAaPmAgMnP
Moskvu	Moskva	k1gFnSc4
a	a	k8xC
přinutili	přinutit	k5eAaPmAgMnP
Dmitrije	Dmitrije	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
znovu	znovu	k6eAd1
podrobil	podrobit	k5eAaPmAgMnS
a	a	k8xC
odvedl	odvést	k5eAaPmAgMnS
hordě	horda	k1gFnSc3
důchod	důchod	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
suma	suma	k1gFnSc1
se	se	k3xPyFc4
dokonce	dokonce	k9
zvýšila	zvýšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Oslabení	oslabení	k1gNnSc1
a	a	k8xC
rozpad	rozpad	k1gInSc1
</s>
<s>
Timurova	Timurův	k2eAgFnSc1d1
výprava	výprava	k1gFnSc1
proti	proti	k7c3
Zlaté	zlatý	k2eAgFnSc3d1
hordě	horda	k1gFnSc3
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
nastal	nastat	k5eAaPmAgInS
dramatický	dramatický	k2eAgInSc4d1
zvrat	zvrat	k1gInSc4
situace	situace	k1gFnSc2
ve	v	k7c6
Zlaté	zlatý	k2eAgFnSc6d1
hordě	horda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chán	chán	k1gMnSc1
Tochtamiš	Tochtamiš	k1gMnSc1
napadl	napadnout	k5eAaPmAgMnS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
středoasijské	středoasijský	k2eAgFnSc2d1
državy	država	k1gFnSc2
Tímúra	Tímúr	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
vyvolal	vyvolat	k5eAaPmAgInS
tak	tak	k6eAd1
mohutnou	mohutný	k2eAgFnSc4d1
odvetu	odveta	k1gFnSc4
dobyvatele	dobyvatel	k1gMnSc2
ze	z	k7c2
Samarkandu	Samarkand	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1391	#num#	k4
podnikl	podniknout	k5eAaPmAgMnS
Tímúr	Tímúr	k1gMnSc1
první	první	k4xOgFnSc2
velké	velká	k1gFnSc2
tažení	tažení	k1gNnSc2
proti	proti	k7c3
chanátu	chanát	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tochtamiš	Tochtamiš	k1gMnSc1
utrpěl	utrpět	k5eAaPmAgMnS
porážku	porážka	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
již	již	k9
rok	rok	k1gInSc4
nato	nato	k6eAd1
svoji	svůj	k3xOyFgFnSc4
vládu	vláda	k1gFnSc4
ve	v	k7c6
Zlaté	zlatý	k2eAgFnSc6d1
hordě	horda	k1gFnSc6
obnovil	obnovit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1395	#num#	k4
se	se	k3xPyFc4
však	však	k9
Tímúr	Tímúr	k1gMnSc1
obrátil	obrátit	k5eAaPmAgMnS
proti	proti	k7c3
Zlaté	zlatý	k2eAgFnSc3d1
hordě	horda	k1gFnSc3
podruhé	podruhé	k6eAd1
a	a	k8xC
rozdrtil	rozdrtit	k5eAaPmAgMnS
Tochtamišovo	Tochtamišův	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
na	na	k7c6
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Těreku	Těrek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
začali	začít	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc1
bojovníci	bojovník	k1gMnPc1
systematicky	systematicky	k6eAd1
drancovat	drancovat	k5eAaImF
území	území	k1gNnSc4
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
a	a	k8xC
ničit	ničit	k5eAaImF
zdejší	zdejší	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yQgNnPc1,k3yRgNnPc1
nebyla	být	k5eNaImAgNnP
opevněná	opevněný	k2eAgNnPc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
neodpovídalo	odpovídat	k5eNaImAgNnS
vojenské	vojenský	k2eAgFnSc3d1
cti	čest	k1gFnSc3
Tatarů	Tatar	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímúr	Tímúr	k1gInSc1
podlomil	podlomit	k5eAaPmAgInS
ekonomickou	ekonomický	k2eAgFnSc4d1
moc	moc	k1gFnSc4
chanátu	chanát	k1gInSc2
a	a	k8xC
navrátil	navrátit	k5eAaPmAgInS
ho	on	k3xPp3gMnSc4
ke	k	k7c3
kočovnému	kočovný	k2eAgNnSc3d1
hospodaření	hospodaření	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jaře	jaro	k1gNnSc6
1396	#num#	k4
se	se	k3xPyFc4
dobyvatelé	dobyvatel	k1gMnPc1
opět	opět	k6eAd1
vrátili	vrátit	k5eAaPmAgMnP
zpět	zpět	k6eAd1
na	na	k7c4
východ	východ	k1gInSc4
do	do	k7c2
Samarkandu	Samarkand	k1gInSc2
s	s	k7c7
ohromnou	ohromný	k2eAgFnSc7d1
kořistí	kořist	k1gFnSc7
a	a	k8xC
nespočetným	spočetný	k2eNgNnSc7d1
množstvím	množství	k1gNnSc7
otroků	otrok	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Část	část	k1gFnSc1
Tatarů	Tatar	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
prchala	prchat	k5eAaImAgFnS
před	před	k7c7
řáděním	řádění	k1gNnSc7
Tímúrových	Tímúrův	k2eAgNnPc2d1
vojsk	vojsko	k1gNnPc2
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
našla	najít	k5eAaPmAgFnS
útočiště	útočiště	k1gNnSc4
na	na	k7c6
Litvě	Litva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
také	také	k9
sám	sám	k3xTgMnSc1
Tochtamiš	Tochtamiš	k1gMnSc1
se	se	k3xPyFc4
svojí	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
a	a	k8xC
dvorem	dvůr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhnaného	vyhnaný	k2eAgMnSc2d1
tatarského	tatarský	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
připravoval	připravovat	k5eAaImAgMnS
odvetnou	odvetný	k2eAgFnSc4d1
akci	akce	k1gFnSc4
<g/>
,	,	kIx,
přijal	přijmout	k5eAaPmAgMnS
Vytautas	Vytautas	k1gMnSc1
(	(	kIx(
<g/>
Vitold	Vitold	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
uzavřel	uzavřít	k5eAaPmAgMnS
s	s	k7c7
ním	on	k3xPp3gNnSc7
smlouvu	smlouva	k1gFnSc4
o	o	k7c4
vzájemné	vzájemný	k2eAgFnPc4d1
pomoci	pomoc	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
nastolit	nastolit	k5eAaPmF
Tochtamiše	Tochtamiš	k1gMnPc4
znovu	znovu	k6eAd1
na	na	k7c4
trůn	trůn	k1gInSc4
v	v	k7c6
Saraji	Saraj	k1gInSc6
a	a	k8xC
Vitolda	Vitolda	k1gMnSc1
jako	jako	k8xS,k8xC
vládce	vládce	k1gMnSc1
Moskvy	Moskva	k1gFnSc2
a	a	k8xC
celé	celý	k2eAgFnSc2d1
ruské	ruský	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1399	#num#	k4
se	se	k3xPyFc4
Vitoldovi	Vitold	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
shromáždit	shromáždit	k5eAaPmF
obrovskou	obrovský	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sestávala	sestávat	k5eAaImAgFnS
z	z	k7c2
vojenského	vojenský	k2eAgInSc2d1
potenciálu	potenciál	k1gInSc2
celé	celý	k2eAgFnSc2d1
Litvy	Litva	k1gFnSc2
<g/>
,	,	kIx,
tatarských	tatarský	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
věrných	věrný	k2eAgMnPc2d1
Tochtamišovi	Tochtamišův	k2eAgMnPc1d1
a	a	k8xC
pomocných	pomocný	k2eAgInPc2d1
křižáckých	křižácký	k2eAgInPc2d1
oddílů	oddíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neobrátila	obrátit	k5eNaPmAgFnS
se	se	k3xPyFc4
však	však	k9
proti	proti	k7c3
Rusi	Rus	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
proti	proti	k7c3
Tímúrovým	Tímúrův	k2eAgMnPc3d1
Tatarům	Tatar	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kampaň	kampaň	k1gFnSc1
vedená	vedený	k2eAgFnSc1d1
proti	proti	k7c3
Tímúrovi	Tímúr	k1gMnSc3
nebyla	být	k5eNaImAgFnS
úspěšná	úspěšný	k2eAgFnSc1d1
a	a	k8xC
spojenci	spojenec	k1gMnPc1
utrpěli	utrpět	k5eAaPmAgMnP
roku	rok	k1gInSc2
1399	#num#	k4
těžkou	těžký	k2eAgFnSc4d1
porážku	porážka	k1gFnSc4
na	na	k7c6
řece	řeka	k1gFnSc6
Vorskle	Vorskl	k1gInSc5
<g/>
,	,	kIx,
přestože	přestože	k8xS
byli	být	k5eAaImAgMnP
mnohem	mnohem	k6eAd1
početnější	početní	k2eAgMnPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katastrofa	katastrofa	k1gFnSc1
na	na	k7c4
Vorskle	Vorskl	k1gInSc5
znamenala	znamenat	k5eAaImAgFnS
definitivní	definitivní	k2eAgInSc4d1
konec	konec	k1gInSc4
Tochtamišových	Tochtamišových	k2eAgFnSc7d1
ambicí	ambice	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Stání	stání	k1gNnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Ugře	Ugř	k1gInSc2
roku	rok	k1gInSc2
1480	#num#	k4
(	(	kIx(
<g/>
ilustrace	ilustrace	k1gFnPc1
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Kolem	kolem	k7c2
poloviny	polovina	k1gFnSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
začala	začít	k5eAaPmAgFnS
nenávratně	návratně	k6eNd1
rozpadat	rozpadat	k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
osamostatnění	osamostatnění	k1gNnSc6
Kazaňského	kazaňský	k2eAgInSc2d1
chanátu	chanát	k1gInSc2
(	(	kIx(
<g/>
1438	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
odtrhli	odtrhnout	k5eAaPmAgMnP
krymští	krymský	k2eAgMnPc1d1
Tataři	Tatar	k1gMnPc1
(	(	kIx(
<g/>
1441	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
Astrachaňský	astrachaňský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
(	(	kIx(
<g/>
1466	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
roku	rok	k1gInSc2
1490	#num#	k4
vznikl	vzniknout	k5eAaPmAgInS
ještě	ještě	k9
Sibiřský	sibiřský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okleštěná	okleštěný	k2eAgFnSc1d1
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
existovala	existovat	k5eAaImAgFnS
dále	daleko	k6eAd2
pod	pod	k7c7
názvem	název	k1gInSc7
Velká	velký	k2eAgFnSc1d1
horda	horda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tatarské	tatarský	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
na	na	k7c6
Rusi	Rus	k1gFnSc6
pominulo	pominout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástupnické	nástupnický	k2eAgInPc1d1
chanáty	chanát	k1gInPc1
si	se	k3xPyFc3
například	například	k6eAd1
činily	činit	k5eAaImAgFnP
nárok	nárok	k1gInSc4
vybírat	vybírat	k5eAaImF
jako	jako	k9
dědicové	dědic	k1gMnPc1
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
od	od	k7c2
Rusů	Rus	k1gMnPc2
vychod	vychoda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
však	však	k9
zdaleka	zdaleka	k6eAd1
nepanovala	panovat	k5eNaImAgFnS
jednota	jednota	k1gFnSc1
a	a	k8xC
toho	ten	k3xDgNnSc2
mohla	moct	k5eAaImAgFnS
ruská	ruský	k2eAgFnSc1d1
knížata	kníže	k1gNnPc4
využít	využít	k5eAaPmF
ve	v	k7c4
svůj	svůj	k3xOyFgInSc4
prospěch	prospěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1480	#num#	k4
se	se	k3xPyFc4
vojska	vojsko	k1gNnSc2
Hordy	horda	k1gFnSc2
chystala	chystat	k5eAaImAgFnS
ke	k	k7c3
střetu	střet	k1gInSc3
s	s	k7c7
ruským	ruský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
z	z	k7c2
pozičního	poziční	k2eAgInSc2d1
„	„	k?
<g/>
stání	stání	k1gNnSc1
na	na	k7c6
řece	řeka	k1gFnSc6
Ugře	Ugř	k1gMnSc2
<g/>
“	“	k?
nakonec	nakonec	k9
chán	chán	k1gMnSc1
Achmat	Achmat	k1gInSc1
bez	bez	k7c2
boje	boj	k1gInSc2
ustoupil	ustoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
znamenalo	znamenat	k5eAaImAgNnS
konečnou	konečný	k2eAgFnSc4d1
ztrátu	ztráta	k1gFnSc4
vlády	vláda	k1gFnSc2
nad	nad	k7c7
ruskými	ruský	k2eAgNnPc7d1
knížectvími	knížectví	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
horda	horda	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
brzy	brzy	k6eAd1
rozpadla	rozpadnout	k5eAaPmAgFnS
<g/>
,	,	kIx,
poslední	poslední	k2eAgMnSc1d1
chán	chán	k1gMnSc1
Saih	Saih	k1gMnSc1
Ahmed	Ahmed	k1gMnSc1
byl	být	k5eAaImAgMnS
roku	rok	k1gInSc2
1502	#num#	k4
zajat	zajat	k2eAgMnSc1d1
a	a	k8xC
na	na	k7c6
Litvě	Litva	k1gFnSc6
popraven	popraven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Význam	význam	k1gInSc1
Zlaté	zlatý	k2eAgFnSc2d1
Hordy	horda	k1gFnSc2
</s>
<s>
Staleté	staletý	k2eAgNnSc1d1
panování	panování	k1gNnSc1
Zlaté	zlatý	k2eAgFnSc2d1
Hordy	horda	k1gFnSc2
silně	silně	k6eAd1
poznamenalo	poznamenat	k5eAaPmAgNnS
politický	politický	k2eAgInSc4d1
<g/>
,	,	kIx,
kulturní	kulturní	k2eAgInSc4d1
a	a	k8xC
především	především	k6eAd1
hospodářský	hospodářský	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
ruských	ruský	k2eAgNnPc2d1
knížectví	knížectví	k1gNnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
přerušilo	přerušit	k5eAaPmAgNnS
předchozí	předchozí	k2eAgInSc4d1
nadějný	nadějný	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
a	a	k8xC
prohloubilo	prohloubit	k5eAaPmAgNnS
izolaci	izolace	k1gFnSc4
pravoslavné	pravoslavný	k2eAgFnSc2d1
země	zem	k1gFnSc2
od	od	k7c2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dolním	dolní	k2eAgNnSc6d1
Povolží	Povolží	k1gNnSc6
a	a	k8xC
na	na	k7c6
Krymu	Krym	k1gInSc6
sice	sice	k8xC
kvetla	kvést	k5eAaImAgFnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bohatá	bohatý	k2eAgNnPc1d1
města	město	k1gNnPc1
s	s	k7c7
rozvinutým	rozvinutý	k2eAgInSc7d1
dálkovým	dálkový	k2eAgInSc7d1
obchodem	obchod	k1gInSc7
<g/>
,	,	kIx,
vedeným	vedený	k2eAgInSc7d1
jak	jak	k8xS,k8xC
po	po	k7c6
moři	moře	k1gNnSc6
do	do	k7c2
Káhiry	Káhira	k1gFnSc2
<g/>
,	,	kIx,
Janova	Janov	k1gInSc2
a	a	k8xC
Benátek	Benátky	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
po	po	k7c6
souši	souš	k1gFnSc6
přes	přes	k7c4
Kyjev	Kyjev	k1gInSc4
do	do	k7c2
Polska	Polsko	k1gNnSc2
<g/>
,	,	kIx,
Litvy	Litva	k1gFnSc2
<g/>
,	,	kIx,
Uher	Uhry	k1gFnPc2
a	a	k8xC
Německa	Německo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruská	ruský	k2eAgFnSc1d1
knížectví	knížectví	k1gNnPc1
však	však	k9
byla	být	k5eAaImAgNnP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
izolována	izolován	k2eAgFnSc1d1
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc1
obyvatelstvo	obyvatelstvo	k1gNnSc1
sužováno	sužován	k2eAgNnSc1d1
placením	placení	k1gNnSc7
vysoké	vysoký	k2eAgFnSc2d1
daně	daň	k1gFnSc2
z	z	k7c2
hlavy	hlava	k1gFnSc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
vychodu	vychoda	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
země	zem	k1gFnPc1
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
později	pozdě	k6eAd2
<g/>
,	,	kIx,
když	když	k8xS
začala	začít	k5eAaPmAgFnS
mít	mít	k5eAaImF
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
vlastní	vlastnit	k5eAaImIp3nS
politické	politický	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
ničena	ničit	k5eAaImNgFnS
tvrdými	tvrdý	k2eAgInPc7d1
loupeživými	loupeživý	k2eAgInPc7d1
vpády	vpád	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taktika	taktika	k1gFnSc1
chánů	chán	k1gMnPc2
vůči	vůči	k7c3
Rusku	Rusko	k1gNnSc3
a	a	k8xC
Litvě	Litva	k1gFnSc3
nakonec	nakonec	k6eAd1
posílila	posílit	k5eAaPmAgFnS
význam	význam	k1gInSc4
moskevského	moskevský	k2eAgNnSc2d1
knížectví	knížectví	k1gNnSc2
<g/>
,	,	kIx,
jádra	jádro	k1gNnSc2
budoucího	budoucí	k2eAgNnSc2d1
velkého	velký	k2eAgNnSc2d1
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chánové	chán	k1gMnPc1
Zlaté	zlatý	k2eAgFnSc2d1
Hordy	horda	k1gFnSc2
sice	sice	k8xC
podporovali	podporovat	k5eAaImAgMnP
své	svůj	k3xOyFgMnPc4
vazaly	vazal	k1gMnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gInPc7
moskevského	moskevský	k2eAgMnSc2d1
knížete	kníže	k1gMnSc2
Ivana	Ivan	k1gMnSc4
Kalitu	Kalita	k1gMnSc4
(	(	kIx(
<g/>
†	†	k?
1341	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
pád	pád	k1gInSc1
Hordy	horda	k1gFnSc2
vedl	vést	k5eAaImAgInS
k	k	k7c3
posílení	posílení	k1gNnSc3
moci	moc	k1gFnSc2
moskevských	moskevský	k2eAgMnPc2d1
knížat	kníže	k1gMnPc2wR
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
pak	pak	k6eAd1
Rusko	Rusko	k1gNnSc1
v	v	k7c6
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sjednotila	sjednotit	k5eAaPmAgFnS
a	a	k8xC
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
naopak	naopak	k6eAd1
dobyla	dobýt	k5eAaPmAgNnP
dolní	dolní	k2eAgNnPc1d1
Povolží	Povolží	k1gNnPc1
a	a	k8xC
pokračovala	pokračovat	k5eAaImAgFnS
v	v	k7c6
expanzi	expanze	k1gFnSc6
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Válečná	válečný	k2eAgFnSc1d1
taktika	taktika	k1gFnSc1
</s>
<s>
Ve	v	k7c6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
měla	mít	k5eAaImAgFnS
lehce	lehko	k6eAd1
vyzbrojená	vyzbrojený	k2eAgFnSc1d1
<g/>
,	,	kIx,
dobře	dobře	k6eAd1
vycvičená	vycvičený	k2eAgFnSc1d1
a	a	k8xC
velmi	velmi	k6eAd1
disciplinovaná	disciplinovaný	k2eAgFnSc1d1
mongolská	mongolský	k2eAgFnSc1d1
jízda	jízda	k1gFnSc1
<g/>
,	,	kIx,
ozbrojená	ozbrojený	k2eAgFnSc1d1
luky	luk	k1gInPc4
a	a	k8xC
šípy	šíp	k1gInPc4
<g/>
,	,	kIx,
velkou	velký	k2eAgFnSc4d1
převahu	převaha	k1gFnSc4
jak	jak	k8xC,k8xS
nad	nad	k7c4
oddíly	oddíl	k1gInPc4
ruských	ruský	k2eAgMnPc2d1
knížat	kníže	k1gMnPc2wR
<g/>
,	,	kIx,
tak	tak	k6eAd1
nad	nad	k7c7
středoevropskými	středoevropský	k2eAgMnPc7d1
rytíři	rytíř	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
válečnou	válečný	k2eAgFnSc4d1
disciplinu	disciplina	k1gFnSc4
neznali	neznat	k5eAaImAgMnP,k5eNaImAgMnP
a	a	k8xC
byli	být	k5eAaImAgMnP
zvyklí	zvyklý	k2eAgMnPc1d1
bojovat	bojovat	k5eAaImF
jednotlivě	jednotlivě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mongolové	Mongol	k1gMnPc1
za	za	k7c7
sebou	se	k3xPyFc7
zanechávali	zanechávat	k5eAaImAgMnP
naprostou	naprostý	k2eAgFnSc4d1
zkázu	zkáza	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
domnívali	domnívat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
tak	tak	k6eAd1
odradí	odradit	k5eAaPmIp3nP
obyvatelstvo	obyvatelstvo	k1gNnSc4
napadených	napadený	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
od	od	k7c2
odporu	odpor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
nesnášeli	snášet	k5eNaImAgMnP
a	a	k8xC
tvrdě	tvrdě	k6eAd1
jej	on	k3xPp3gMnSc4
trestali	trestat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
vážné	vážný	k2eAgFnSc2d1
krize	krize	k1gFnSc2
v	v	k7c6
důsledku	důsledek	k1gInSc6
krvavých	krvavý	k2eAgInPc2d1
vnitropolitických	vnitropolitický	k2eAgInPc2d1
bojů	boj	k1gInPc2
<g/>
,	,	kIx,
odvážili	odvážit	k5eAaPmAgMnP
se	se	k3xPyFc4
podmanění	podmaněný	k2eAgMnPc1d1
Rusové	Rus	k1gMnPc1
znovu	znovu	k6eAd1
k	k	k7c3
vojenskému	vojenský	k2eAgInSc3d1
odporu	odpor	k1gInSc3
a	a	k8xC
dosáhli	dosáhnout	k5eAaPmAgMnP
prvních	první	k4xOgInPc2
vítězství	vítězství	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
CAVENDISH	CAVENDISH	kA
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peoples	Peoples	k1gInSc1
of	of	k?
Western	Western	kA
Asia	Asia	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Marshall	Marshall	k1gInSc1
Cavendish	Cavendisha	k1gFnPc2
Corporation	Corporation	k1gInSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7614	#num#	k4
<g/>
-	-	kIx~
<g/>
7677	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7614	#num#	k4
<g/>
-	-	kIx~
<g/>
7677	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOSWORTH	BOSWORTH	kA
<g/>
,	,	kIx,
Clifford	Clifford	k1gMnSc1
Edmund	Edmund	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historic	Historic	k1gMnSc1
Cities	Cities	k1gMnSc1
of	of	k?
the	the	k?
Islamic	Islamic	k1gMnSc1
World	World	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leiden	Leidna	k1gFnPc2
<g/>
:	:	kIx,
Brill	Brill	k1gInSc1
NV	NV	kA
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
280	#num#	k4
s.	s.	k?
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
90	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
15388	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BORRERO	BORRERO	kA
<g/>
,	,	kIx,
Mauricio	Mauricio	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Russia	Russium	k1gNnPc1
<g/>
:	:	kIx,
A	a	k9
Reference	reference	k1gFnPc1
Guide	Guid	k1gInSc5
from	fro	k1gNnSc7
the	the	k?
Renaissance	Renaissance	k1gFnSc2
to	ten	k3xDgNnSc1
the	the	k?
Present	Present	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Facts	Facts	k1gInSc1
On	on	k3xPp3gInSc1
File	File	k1gInSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
162	#num#	k4
s.	s.	k?
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8160	#num#	k4
<g/>
-	-	kIx~
<g/>
4454	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DRŠKA	drška	k1gFnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
;	;	kIx,
PICKOVÁ	Picková	k1gFnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
středověké	středověký	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Aleš	Aleš	k1gMnSc1
Skřivan	Skřivan	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
364	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86493	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ílchanát	Ílchanát	k1gInSc1
</s>
<s>
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lehnice	Lehnice	k1gFnSc2
</s>
<s>
Insignie	insignie	k1gFnPc1
prezidenta	prezident	k1gMnSc2
Turecka	Turecko	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Zlatá	zlatý	k2eAgFnSc1d1
horda	horda	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Turko-mongolské	Turko-mongolský	k2eAgInPc1d1
národy	národ	k1gInPc1
Kurzíva	kurzíva	k1gFnSc1
se	s	k7c7
znakem	znak	k1gInSc7
†	†	k?
ukazují	ukazovat	k5eAaImIp3nP
vymřelé	vymřelý	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
Turko-mongolové	Turko-mongolový	k2eAgFnPc1d1
</s>
<s>
Avaři	Avar	k1gMnPc1
(	(	kIx(
<g/>
též	též	k9
Turkoavaři	Turkoavař	k1gMnPc1
či	či	k8xC
Evroasijští	evroasijský	k2eAgMnPc1d1
Avaři	Avar	k1gMnPc1
<g/>
)	)	kIx)
†	†	k?
•	•	k?
Hunové	Hun	k1gMnPc1
†	†	k?
•	•	k?
Kerejté	Kerejtý	k2eAgFnSc2d1
•	•	k?
Merkité	Merkitý	k2eAgNnSc1d1
†	†	k?
•	•	k?
Najmani	Najman	k1gMnPc1
•	•	k?
30	#num#	k4
Oguzové-Tataři	Oguzové-Tatař	k1gMnPc1
†	†	k?
•	•	k?
Sienpiové	Sienpiový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Siungnuové	Siungnuová	k1gFnSc2
(	(	kIx(
<g/>
též	též	k9
Pei-ti	Pei-ti	k1gNnSc2
<g/>
)	)	kIx)
†	†	k?
•	•	k?
Tabgačové	Tabgačová	k1gFnSc2
†	†	k?
•	•	k?
Žuanžuané	Žuanžuaná	k1gFnSc2
†	†	k?
Zaniklé	zaniklý	k2eAgInPc4d1
státní	státní	k2eAgInPc4d1
útvary	útvar	k1gInPc4
spravované	spravovaný	k2eAgFnSc2d1
Turko-mongoly	Turko-mongola	k1gFnSc2
</s>
<s>
Avarská	avarský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
•	•	k?
Čagatajský	Čagatajský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
•	•	k?
Hunská	hunský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
•	•	k?
Chanát	chanát	k1gInSc4
Zlaté	zlatý	k2eAgFnSc2d1
hordy	horda	k1gFnSc2
•	•	k?
Ílchanát	Ílchanát	k1gInSc1
•	•	k?
Kazaňský	kazaňský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
•	•	k?
Krymský	krymský	k2eAgInSc1d1
chanát	chanát	k1gInSc1
•	•	k?
Mughalská	Mughalský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
(	(	kIx(
<g/>
Bábur	Bábur	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Nogajská	Nogajský	k2eAgFnSc1d1
horda	horda	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc7d1
Wej	Wej	k1gFnSc7
•	•	k?
Sienpiská	Sienpiský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
•	•	k?
Siungnuská	Siungnuský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
•	•	k?
Tímúrovská	Tímúrovský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
</s>
<s>
Turkické	turkický	k2eAgInPc1d1
národy	národ	k1gInPc1
Kurzíva	kurzíva	k1gFnSc1
s	s	k7c7
†	†	k?
ukazují	ukazovat	k5eAaImIp3nP
vymřelé	vymřelý	k2eAgFnPc1d1
skupiny	skupina	k1gFnPc1
–	–	k?
znak	znak	k1gInSc1
†	†	k?
ukazuje	ukazovat	k5eAaImIp3nS
vymírající	vymírající	k2eAgFnPc4d1
skupiny	skupina	k1gFnPc4
Historické	historický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
národů	národ	k1gInPc2
<g/>
,	,	kIx,
kmenů	kmen	k1gInPc2
a	a	k8xC
klanů	klan	k1gInPc2
</s>
<s>
Turkuti	Turkut	k1gMnPc1
†	†	k?
•	•	k?
Jenisejští	jenisejský	k2eAgMnPc1d1
Kyrgyzové	Kyrgyz	k1gMnPc1
†	†	k?
•	•	k?
Kanglyové	Kanglyová	k1gFnSc2
†	†	k?
•	•	k?
Karlukové	Karlukový	k2eAgFnSc2d1
•	•	k?
Kimakové	Kimakový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Kumáni	Kumán	k1gMnPc1
(	(	kIx(
<g/>
Polovci	Polovec	k1gMnPc1
<g/>
)	)	kIx)
•	•	k?
Kypčaci	Kypčace	k1gFnSc6
•	•	k?
Kı	Kı	k1gFnSc2
•	•	k?
Ogurští	Ogurský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
–	–	k?
Avaři	Avar	k1gMnPc1
†	†	k?
•	•	k?
Hunové	Hun	k1gMnPc1
†	†	k?
•	•	k?
Chazaři	Chazar	k1gMnPc1
†	†	k?
•	•	k?
Prabulhaři	Prabulhař	k1gMnSc3
†	†	k?
•	•	k?
Oguzští	Oguzský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Türgešové	Türgešové	k2eAgMnSc4d1
†	†	k?
•	•	k?
Siungnuové	Siungnuová	k1gFnSc2
†	†	k?
•	•	k?
Šatoové	Šatoová	k1gFnSc2
†	†	k?
•	•	k?
Tinglingové	Tinglingový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Wusunové	Wusunový	k2eAgFnSc2d1
†	†	k?
Současné	současný	k2eAgInPc4d1
národy	národ	k1gInPc4
a	a	k8xC
národnosti	národnost	k1gFnPc4
</s>
<s>
Altajové	Altajový	k2eAgFnPc4d1
•	•	k?
Azerové	Azerová	k1gFnPc4
•	•	k?
Balkaři	Balkař	k1gMnSc3
•	•	k?
Baškirové	Baškir	k1gMnPc1
•	•	k?
Chakasové	Chakasové	k2eAgInPc2d1
<g/>
•	•	k?
Chaladžové	Chaladžový	k2eAgFnSc2d1
•	•	k?
Chorásánští	chorásánský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Čulymci	Čulymec	k1gMnSc3
•	•	k?
Čuvaši	Čuvaš	k1gMnSc3
•	•	k?
Dolgani	Dolgaň	k1gFnSc6
†	†	k?
•	•	k?
Gagauzové	Gagauzová	k1gFnSc2
•	•	k?
Iráčtí	irácký	k2eAgMnPc1d1
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Jakuti	Jakut	k1gMnPc1
•	•	k?
Karačajové	Karačajový	k2eAgNnSc1d1
†	†	k?
•	•	k?
Karaité	Karaitý	k2eAgNnSc1d1
†	†	k?
•	•	k?
Karakalpaci	Karakalpace	k1gFnSc4
•	•	k?
Karamanliové	Karamanliový	k2eAgFnSc2d1
•	•	k?
Karapapachové	Karapapachová	k1gFnSc2
†	†	k?
•	•	k?
Kaškajové	Kaškajový	k2eAgFnSc2d1
•	•	k?
Kazaši	Kazach	k1gMnPc1
•	•	k?
Krymčáci	Krymčák	k1gMnPc1
†	†	k?
•	•	k?
Krymští	krymský	k2eAgMnPc1d1
Tataři	Tatar	k1gMnPc1
•	•	k?
Kumandynci	Kumandynec	k1gMnSc3
†	†	k?
•	•	k?
Kumykové	Kumyková	k1gFnSc2
•	•	k?
Kyrgyzové	Kyrgyzová	k1gFnSc2
•	•	k?
Meschetští	Meschetský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
•	•	k?
Najmani	Najman	k1gMnPc1
•	•	k?
Nogajové	Nogajový	k2eAgFnSc2d1
•	•	k?
Salarové	Salarová	k1gFnSc2
•	•	k?
Syrští	syrský	k2eAgMnPc1d1
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Šorci	šorc	k1gInSc6
•	•	k?
Tataři	Tatar	k1gMnPc1
•	•	k?
Telengiti	Telengit	k2eAgMnPc1d1
•	•	k?
Teleuti	Teleuť	k1gFnPc4
•	•	k?
Tofalaři	Tofalař	k1gMnSc3
†	†	k?
•	•	k?
Turci	Turek	k1gMnPc1
•	•	k?
Turkmeni	Turkmen	k1gMnPc1
•	•	k?
Tuvinci	Tuvinec	k1gInSc3
•	•	k?
Ujgurové	Ujgurový	k2eAgFnSc2d1
•	•	k?
Urumové	Urumová	k1gFnSc2
•	•	k?
Uzbeci	Uzbek	k1gMnPc1
•	•	k?
Jugurové	Jugurový	k2eAgNnSc1d1
†	†	k?
Západoevropští	západoevropský	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Turci	Turek	k1gMnPc1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
Balkánští	balkánský	k2eAgMnPc1d1
a	a	k8xC
ostatní	ostatní	k2eAgMnPc1d1
Turci	Turek	k1gMnPc1
</s>
<s>
Turci	Turek	k1gMnPc1
v	v	k7c6
Bulharsku	Bulharsko	k1gNnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Egyptu	Egypt	k1gInSc3
•	•	k?
Turečtí	turecký	k2eAgMnPc1d1
Kyprioti	Kypriot	k1gMnPc1
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c4
Kosovu	Kosův	k2eAgFnSc4d1
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Makedonii	Makedonie	k1gFnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Rumunsku	Rumunsko	k1gNnSc6
•	•	k?
Turci	Turek	k1gMnPc1
v	v	k7c6
Západní	západní	k2eAgFnSc6d1
Thrákii	Thrákie	k1gFnSc6
•	•	k?
Muslimové	muslim	k1gMnPc1
v	v	k7c6
Řecku	Řecko	k1gNnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
211216	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4093721-5	4093721-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2003032748	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
146778791	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2003032748	#num#	k4
</s>
