<p>
<s>
Sv.	sv.	kA	sv.
Viktor	Viktor	k1gMnSc1	Viktor
I.	I.	kA	I.
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Victor	Victor	k1gMnSc1	Victor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
186	[number]	k4	186
<g/>
/	/	kIx~	/
<g/>
189	[number]	k4	189
–	–	k?	–
197	[number]	k4	197
<g/>
/	/	kIx~	/
<g/>
201	[number]	k4	201
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
papežem	papež	k1gMnSc7	papež
afrického	africký	k2eAgInSc2d1	africký
původu	původ	k1gInSc2	původ
a	a	k8xC	a
podle	podle	k7c2	podle
všech	všecek	k3xTgFnPc2	všecek
známek	známka	k1gFnPc2	známka
energický	energický	k2eAgMnSc1d1	energický
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
plně	plně	k6eAd1	plně
uplatňoval	uplatňovat	k5eAaImAgMnS	uplatňovat
autoritu	autorita	k1gFnSc4	autorita
hlavy	hlava	k1gFnSc2	hlava
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
římským	římský	k2eAgMnSc7d1	římský
biskupem	biskup	k1gMnSc7	biskup
v	v	k7c6	v
době	doba	k1gFnSc6	doba
relativního	relativní	k2eAgInSc2d1	relativní
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nebyli	být	k5eNaImAgMnP	být
křesťané	křesťan	k1gMnPc1	křesťan
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
příliš	příliš	k6eAd1	příliš
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
soustředit	soustředit	k5eAaPmF	soustředit
na	na	k7c4	na
problémy	problém	k1gInPc4	problém
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
otázkám	otázka	k1gFnPc3	otázka
liturgie	liturgie	k1gFnSc2	liturgie
a	a	k8xC	a
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
stíhat	stíhat	k5eAaImF	stíhat
heretiky	heretik	k1gMnPc4	heretik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znovu	znovu	k6eAd1	znovu
otevřel	otevřít	k5eAaPmAgInS	otevřít
starý	starý	k2eAgInSc1d1	starý
spor	spor	k1gInSc1	spor
mezi	mezi	k7c7	mezi
východní	východní	k2eAgFnSc7d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
církví	církev	k1gFnSc7	církev
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
termínu	termín	k1gInSc2	termín
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
papež	papež	k1gMnSc1	papež
Pius	Pius	k1gMnSc1	Pius
I.	I.	kA	I.
stanovil	stanovit	k5eAaPmAgMnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
svátkem	svátek	k1gInSc7	svátek
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
je	být	k5eAaImIp3nS	být
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
dnem	den	k1gInSc7	den
svátku	svátek	k1gInSc2	svátek
určil	určit	k5eAaPmAgInS	určit
neděli	neděle	k1gFnSc4	neděle
(	(	kIx(	(
<g/>
Hod	hod	k1gInSc4	hod
Boží	božit	k5eAaImIp3nS	božit
Velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
církev	církev	k1gFnSc1	církev
však	však	k9	však
slavila	slavit	k5eAaImAgFnS	slavit
jako	jako	k8xS	jako
vrcholný	vrcholný	k2eAgInSc1d1	vrcholný
den	den	k1gInSc1	den
Velikonoc	Velikonoce	k1gFnPc2	Velikonoce
den	den	k1gInSc4	den
ukřižování	ukřižování	k1gNnPc2	ukřižování
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
a	a	k8xC	a
tento	tento	k3xDgInSc4	tento
svátek	svátek	k1gInSc4	svátek
kladla	klást	k5eAaImAgFnS	klást
na	na	k7c4	na
čtrnáctý	čtrnáctý	k4xOgInSc4	čtrnáctý
den	den	k1gInSc4	den
měsíce	měsíc	k1gInSc2	měsíc
Nisan	nisan	k1gInSc1	nisan
podle	podle	k7c2	podle
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
den	den	k1gInSc4	den
v	v	k7c6	v
týdnu	týden	k1gInSc6	týden
svátek	svátek	k1gInSc1	svátek
připadl	připadnout	k5eAaPmAgInS	připadnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
jednak	jednak	k8xC	jednak
koincidoval	koincidovat	k5eAaImAgMnS	koincidovat
se	s	k7c7	s
židovským	židovský	k2eAgInSc7d1	židovský
svátkem	svátek	k1gInSc7	svátek
Pesach	pesach	k1gInSc1	pesach
a	a	k8xC	a
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
přinášelo	přinášet	k5eAaImAgNnS	přinášet
i	i	k9	i
potíže	potíž	k1gFnPc1	potíž
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
věřících	věřící	k1gMnPc2	věřící
na	na	k7c6	na
bohoslužbách	bohoslužba	k1gFnPc6	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
I.	I.	kA	I.
hodlal	hodlat	k5eAaImAgMnS	hodlat
tento	tento	k3xDgInSc4	tento
spor	spor	k1gInSc4	spor
řešit	řešit	k5eAaImF	řešit
dokonce	dokonce	k9	dokonce
vyobcováním	vyobcování	k1gNnSc7	vyobcování
odpůrců	odpůrce	k1gMnPc2	odpůrce
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
krajnímu	krajní	k2eAgInSc3d1	krajní
řešení	řešení	k1gNnSc4	řešení
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
svatý	svatý	k2eAgInSc4d1	svatý
Ireneus	Ireneus	k1gInSc4	Ireneus
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
přizpůsobily	přizpůsobit	k5eAaPmAgFnP	přizpůsobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viktor	Viktor	k1gMnSc1	Viktor
I.	I.	kA	I.
také	také	k6eAd1	také
zavedl	zavést	k5eAaPmAgMnS	zavést
latinské	latinský	k2eAgFnPc4d1	Latinská
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
mše	mše	k1gFnPc1	mše
slouženy	sloužen	k2eAgFnPc1d1	sloužena
v	v	k7c6	v
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pastýřské	pastýřský	k2eAgInPc4d1	pastýřský
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
teologické	teologický	k2eAgInPc4d1	teologický
spisy	spis	k1gInPc4	spis
a	a	k8xC	a
korespondenci	korespondence	k1gFnSc4	korespondence
psal	psát	k5eAaImAgMnS	psát
papež	papež	k1gMnSc1	papež
latinsky	latinsky	k6eAd1	latinsky
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
latina	latina	k1gFnSc1	latina
byla	být	k5eAaImAgFnS	být
užívána	užívat	k5eAaImNgFnS	užívat
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pocházel	pocházet	k5eAaImAgMnS	pocházet
<g/>
.	.	kIx.	.
</s>
<s>
Univerzálním	univerzální	k2eAgInSc7d1	univerzální
jazykem	jazyk	k1gInSc7	jazyk
křesťanů	křesťan	k1gMnPc2	křesťan
se	se	k3xPyFc4	se
však	však	k9	však
latina	latina	k1gFnSc1	latina
stala	stát	k5eAaPmAgFnS	stát
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Viktoru	Viktor	k1gMnSc3	Viktor
I.	I.	kA	I.
se	se	k3xPyFc4	se
také	také	k9	také
připisuje	připisovat	k5eAaImIp3nS	připisovat
stanovení	stanovení	k1gNnSc4	stanovení
základních	základní	k2eAgFnPc2d1	základní
zásad	zásada	k1gFnPc2	zásada
církevního	církevní	k2eAgNnSc2d1	církevní
soudnictví	soudnictví	k1gNnSc2	soudnictví
a	a	k8xC	a
zákaz	zákaz	k1gInSc1	zákaz
používání	používání	k1gNnSc2	používání
skleněných	skleněný	k2eAgFnPc2d1	skleněná
a	a	k8xC	a
dřevěných	dřevěný	k2eAgFnPc2d1	dřevěná
nádob	nádoba	k1gFnPc2	nádoba
při	při	k7c6	při
bohoslužebných	bohoslužebný	k2eAgInPc6d1	bohoslužebný
obřadech	obřad	k1gInPc6	obřad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
doba	doba	k1gFnSc1	doba
a	a	k8xC	a
způsob	způsob	k1gInSc1	způsob
jeho	on	k3xPp3gNnSc2	on
umučení	umučení	k1gNnSc2	umučení
i	i	k9	i
místo	místo	k7c2	místo
jeho	jeho	k3xOp3gInSc2	jeho
původního	původní	k2eAgInSc2d1	původní
hrobu	hrob	k1gInSc2	hrob
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
římské	římský	k2eAgFnPc4d1	římská
katakomby	katakomby	k1gFnPc4	katakomby
<g/>
)	)	kIx)	)
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgInSc1d1	barokní
nápis	nápis	k1gInSc1	nápis
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
rakvi	rakev	k1gFnSc6	rakev
v	v	k7c6	v
poutním	poutní	k2eAgInSc6d1	poutní
chrámu	chrám	k1gInSc6	chrám
na	na	k7c6	na
olomouckém	olomoucký	k2eAgInSc6d1	olomoucký
Svatém	svatý	k2eAgInSc6d1	svatý
Kopečku	kopeček	k1gInSc6	kopeček
jej	on	k3xPp3gMnSc4	on
označuje	označovat	k5eAaImIp3nS	označovat
za	za	k7c2	za
prvomučedníka	prvomučedník	k1gMnSc2	prvomučedník
římského	římský	k2eAgMnSc2d1	římský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
náhrobní	náhrobní	k2eAgInSc1d1	náhrobní
kámen	kámen	k1gInSc1	kámen
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
schodišti	schodiště	k1gNnSc6	schodiště
(	(	kIx(	(
<g/>
vlevo	vlevo	k6eAd1	vlevo
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
při	při	k7c6	při
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
patra	patro	k1gNnSc2	patro
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
čitelným	čitelný	k2eAgInSc7d1	čitelný
latinským	latinský	k2eAgInSc7d1	latinský
nápisem	nápis	k1gInSc7	nápis
<g/>
)	)	kIx)	)
musea	museum	k1gNnPc4	museum
v	v	k7c6	v
africkém	africký	k2eAgInSc6d1	africký
Tunisu	Tunis	k1gInSc6	Tunis
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Tuniska	Tunisko	k1gNnSc2	Tunisko
<g/>
,	,	kIx,	,
ostatky	ostatek	k1gInPc1	ostatek
spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
prosklené	prosklený	k2eAgFnSc6d1	prosklená
rakvi	rakev	k1gFnSc6	rakev
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
pilíři	pilíř	k1gInSc6	pilíř
klenby	klenba	k1gFnSc2	klenba
kostela	kostel	k1gInSc2	kostel
Navštívení	navštívení	k1gNnSc2	navštívení
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Svatém	svatý	k2eAgMnSc6d1	svatý
Kopečku	Kopeček	k1gMnSc6	Kopeček
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
ostatky	ostatek	k1gInPc7	ostatek
sv.	sv.	kA	sv.
Viktora	Viktora	k1gMnSc1	Viktora
součástí	součást	k1gFnSc7	součást
monumentálního	monumentální	k2eAgInSc2d1	monumentální
sousedního	sousední	k2eAgInSc2d1	sousední
barokního	barokní	k2eAgInSc2d1	barokní
jižního	jižní	k2eAgInSc2d1	jižní
oltáře	oltář	k1gInSc2	oltář
<g/>
,	,	kIx,	,
na	na	k7c6	na
pilíři	pilíř	k1gInSc6	pilíř
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
umístěn	umístěn	k2eAgInSc1d1	umístěn
relikviář	relikviář	k1gInSc1	relikviář
s	s	k7c7	s
ostatky	ostatek	k1gInPc7	ostatek
-	-	kIx~	-
krví-	krví-	k?	krví-
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
20	[number]	k4	20
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
jeho	jeho	k3xOp3gFnSc2	jeho
návštěvy	návštěva	k1gFnSc2	návštěva
této	tento	k3xDgFnSc2	tento
baziliky	bazilika	k1gFnSc2	bazilika
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
mládeží	mládež	k1gFnSc7	mládež
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památku	památka	k1gFnSc4	památka
sv.	sv.	kA	sv.
Viktora	Viktor	k1gMnSc2	Viktor
I.	I.	kA	I.
si	se	k3xPyFc3	se
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
připomíná	připomínat	k5eAaImIp3nS	připomínat
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
i	i	k9	i
součástí	součást	k1gFnPc2	součást
českého	český	k2eAgInSc2d1	český
národního	národní	k2eAgInSc2d1	národní
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Viktor	Viktor	k1gMnSc1	Viktor
I.	I.	kA	I.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
The	The	k?	The
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
-	-	kIx~	-
www.newadvent.org/cathen	www.newadvent.org/cathen	k1gInSc1	www.newadvent.org/cathen
</s>
</p>
<p>
<s>
Documenta	Documenta	k1gFnSc1	Documenta
Catholica	Catholic	k1gInSc2	Catholic
Omnia	omnium	k1gNnSc2	omnium
latinsky	latinsky	k6eAd1	latinsky
</s>
</p>
