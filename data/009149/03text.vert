<p>
<s>
Dok	dok	k1gInSc1	dok
(	(	kIx(	(
<g/>
z	z	k7c2	z
nizozemštiny	nizozemština	k1gFnSc2	nizozemština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zařízení	zařízení	k1gNnSc1	zařízení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stavbu	stavba	k1gFnSc4	stavba
a	a	k8xC	a
opravy	oprava	k1gFnPc4	oprava
větších	veliký	k2eAgFnPc2d2	veliký
a	a	k8xC	a
velkých	velký	k2eAgFnPc2d1	velká
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
doky	dok	k1gInPc1	dok
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
oddělená	oddělený	k2eAgFnSc1d1	oddělená
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
uzavíratelná	uzavíratelný	k2eAgNnPc4d1	uzavíratelné
kotviště	kotviště	k1gNnPc4	kotviště
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
usnadňují	usnadňovat	k5eAaImIp3nP	usnadňovat
nakládání	nakládání	k1gNnSc4	nakládání
a	a	k8xC	a
vykládání	vykládání	k1gNnSc4	vykládání
například	například	k6eAd1	například
při	při	k7c6	při
proměnlivé	proměnlivý	k2eAgFnSc6d1	proměnlivá
hladině	hladina	k1gFnSc6	hladina
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přílivu	příliv	k1gInSc2	příliv
a	a	k8xC	a
odlivu	odliv	k1gInSc2	odliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Suchý	suchý	k2eAgInSc1d1	suchý
dok	dok	k1gInSc1	dok
==	==	k?	==
</s>
</p>
<p>
<s>
Suchý	suchý	k2eAgInSc1d1	suchý
dok	dok	k1gInSc1	dok
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
nebo	nebo	k8xC	nebo
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
uzavřít	uzavřít	k5eAaPmF	uzavřít
vraty	vrat	k1gInPc4	vrat
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
plavební	plavební	k2eAgFnSc4d1	plavební
komoru	komora	k1gFnSc4	komora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vpluje	vplout	k5eAaPmIp3nS	vplout
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
vrata	vrata	k1gNnPc1	vrata
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
doku	dok	k1gInSc2	dok
vyčerpá	vyčerpat	k5eAaPmIp3nS	vyčerpat
<g/>
,	,	kIx,	,
dosedne	dosednout	k5eAaPmIp3nS	dosednout
loď	loď	k1gFnSc1	loď
na	na	k7c4	na
připravené	připravený	k2eAgFnPc4d1	připravená
podpěry	podpěra	k1gFnPc4	podpěra
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
opravovat	opravovat	k5eAaImF	opravovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velkých	velký	k2eAgInPc2d1	velký
doků	dok	k1gInPc2	dok
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
vrat	vrata	k1gNnPc2	vrata
užívají	užívat	k5eAaImIp3nP	užívat
speciální	speciální	k2eAgNnPc1d1	speciální
plavidla	plavidlo	k1gNnPc1	plavidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
přesně	přesně	k6eAd1	přesně
dosednou	dosednout	k5eAaPmIp3nP	dosednout
na	na	k7c4	na
vpust	vpust	k1gFnSc4	vpust
doku	dok	k1gInSc2	dok
a	a	k8xC	a
tak	tak	k6eAd1	tak
jej	on	k3xPp3gMnSc4	on
uzavřou	uzavřít	k5eAaPmIp3nP	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc1d3	veliký
současné	současný	k2eAgInPc1d1	současný
suché	suchý	k2eAgInPc1d1	suchý
doky	dok	k1gInPc1	dok
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
až	až	k9	až
660	[number]	k4	660
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Plovoucí	plovoucí	k2eAgInSc1d1	plovoucí
dok	dok	k1gInSc1	dok
==	==	k?	==
</s>
</p>
<p>
<s>
Plovoucí	plovoucí	k2eAgInSc1d1	plovoucí
dok	dok	k1gInSc1	dok
je	být	k5eAaImIp3nS	být
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ocelové	ocelový	k2eAgNnSc1d1	ocelové
plavidlo	plavidlo	k1gNnSc1	plavidlo
s	s	k7c7	s
trupem	trup	k1gInSc7	trup
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
žlabu	žlab	k1gInSc2	žlab
a	a	k8xC	a
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
vodními	vodní	k2eAgFnPc7d1	vodní
komorami	komora	k1gFnPc7	komora
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
komory	komora	k1gFnSc2	komora
naplní	naplnit	k5eAaPmIp3nS	naplnit
<g/>
,	,	kIx,	,
plavidlo	plavidlo	k1gNnSc1	plavidlo
klesne	klesnout	k5eAaPmIp3nS	klesnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
loď	loď	k1gFnSc1	loď
může	moct	k5eAaImIp3nS	moct
vplout	vplout	k5eAaPmF	vplout
<g/>
.	.	kIx.	.
</s>
<s>
Vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
komor	komora	k1gFnPc2	komora
se	se	k3xPyFc4	se
dok	dok	k1gInSc1	dok
vynoří	vynořit	k5eAaPmIp3nS	vynořit
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
loď	loď	k1gFnSc1	loď
dosedne	dosednout	k5eAaPmIp3nS	dosednout
na	na	k7c4	na
podpěry	podpěra	k1gFnPc4	podpěra
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
dně	dno	k1gNnSc6	dno
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
ovšem	ovšem	k9	ovšem
pečlivě	pečlivě	k6eAd1	pečlivě
vyvážena	vyvážit	k5eAaPmNgFnS	vyvážit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
podepřena	podepřen	k2eAgFnSc1d1	podepřena
i	i	k9	i
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
suché	suchý	k2eAgInPc1d1	suchý
doky	dok	k1gInPc1	dok
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
třetího	třetí	k4xOgNnSc2	třetí
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
asi	asi	k9	asi
120	[number]	k4	120
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Suezu	Suez	k1gInSc2	Suez
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Lóthal	Lóthal	k1gMnSc1	Lóthal
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Indu	Indus	k1gInSc2	Indus
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
plovoucího	plovoucí	k2eAgInSc2d1	plovoucí
doku	dok	k1gInSc2	dok
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1691	[number]	k4	1691
dal	dát	k5eAaPmAgMnS	dát
dánský	dánský	k2eAgMnSc1d1	dánský
admirál	admirál	k1gMnSc1	admirál
Henrik	Henrik	k1gMnSc1	Henrik
Span	Span	k1gMnSc1	Span
postavit	postavit	k5eAaPmF	postavit
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
dok	dok	k1gInSc4	dok
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
podoba	podoba	k1gFnSc1	podoba
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
na	na	k7c6	na
dobové	dobový	k2eAgFnSc6d1	dobová
medaili	medaile	k1gFnSc6	medaile
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
moderní	moderní	k2eAgInSc4d1	moderní
plovoucí	plovoucí	k2eAgInSc4d1	plovoucí
dok	dok	k1gInSc4	dok
postavila	postavit	k5eAaPmAgFnS	postavit
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
firma	firma	k1gFnSc1	firma
Gilbert	gilbert	k1gInSc1	gilbert
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
naučný	naučný	k2eAgInSc1d1	naučný
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Dok	dok	k1gInSc1	dok
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
7	[number]	k4	7
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
767	[number]	k4	767
</s>
</p>
<p>
<s>
Ottův	Ottův	k2eAgInSc4d1	Ottův
slovník	slovník	k1gInSc4	slovník
naučný	naučný	k2eAgInSc4d1	naučný
nové	nový	k2eAgNnSc4d1	nové
doby	doba	k1gFnPc4	doba
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Dok	dok	k1gInSc1	dok
<g/>
.	.	kIx.	.
</s>
<s>
Sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
193	[number]	k4	193
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Přístav	přístav	k1gInSc1	přístav
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dok	dok	k1gInSc1	dok
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Encyclopedia	Encyclopedium	k1gNnPc1	Encyclopedium
Brittanica	Brittanic	k1gInSc2	Brittanic
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
Dry	Dry	k1gFnSc2	Dry
dock	docka	k1gFnPc2	docka
</s>
</p>
