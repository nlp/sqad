<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
následujícím	následující	k2eAgNnSc6d1	následující
po	po	k7c6	po
restauraci	restaurace	k1gFnSc6	restaurace
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
se	se	k3xPyFc4	se
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
často	často	k6eAd1	často
hrál	hrát	k5eAaImAgMnS	hrát
se	s	k7c7	s
šťastným	šťastný	k2eAgInSc7d1	šťastný
koncem	konec	k1gInSc7	konec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
publiku	publikum	k1gNnSc6	publikum
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
temný	temný	k2eAgInSc1d1	temný
a	a	k8xC	a
depresivní	depresivní	k2eAgInSc1d1	depresivní
tón	tón	k1gInSc1	tón
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
