<p>
<s>
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
King	King	k1gMnSc1	King
Lear	Lear	k1gMnSc1	Lear
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tragédie	tragédie	k1gFnSc1	tragédie
anglického	anglický	k2eAgMnSc2d1	anglický
dramatika	dramatik	k1gMnSc2	dramatik
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
napsaná	napsaný	k2eAgFnSc1d1	napsaná
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1603	[number]	k4	1603
<g/>
–	–	k?	–
<g/>
1606	[number]	k4	1606
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příběh	příběh	k1gInSc4	příběh
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
propadne	propadnout	k5eAaPmIp3nS	propadnout
šílenství	šílenství	k1gNnSc4	šílenství
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
pošetile	pošetile	k6eAd1	pošetile
rozdělí	rozdělit	k5eAaPmIp3nS	rozdělit
dědictví	dědictví	k1gNnSc1	dědictví
mezi	mezi	k7c7	mezi
dvě	dva	k4xCgFnPc1	dva
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
tří	tři	k4xCgFnPc2	tři
dcer	dcera	k1gFnPc2	dcera
na	na	k7c6	na
základě	základ	k1gInSc6	základ
jejich	jejich	k3xOp3gFnPc2	jejich
lichotek	lichotka	k1gFnPc2	lichotka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
tragické	tragický	k2eAgInPc4d1	tragický
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hra	hra	k1gFnSc1	hra
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
legendě	legenda	k1gFnSc6	legenda
o	o	k7c6	o
keltském	keltský	k2eAgMnSc6d1	keltský
králi	král	k1gMnSc6	král
Leirovi	Leir	k1gMnSc6	Leir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
následujícím	následující	k2eAgNnSc6d1	následující
po	po	k7c6	po
restauraci	restaurace	k1gFnSc6	restaurace
Stuartovců	Stuartovec	k1gMnPc2	Stuartovec
se	se	k3xPyFc4	se
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
často	často	k6eAd1	často
hrál	hrát	k5eAaImAgMnS	hrát
se	s	k7c7	s
šťastným	šťastný	k2eAgInSc7d1	šťastný
koncem	konec	k1gInSc7	konec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
publiku	publikum	k1gNnSc6	publikum
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgInS	líbit
temný	temný	k2eAgInSc1d1	temný
a	a	k8xC	a
depresivní	depresivní	k2eAgInSc1d1	depresivní
tón	tón	k1gInSc1	tón
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Shakespearova	Shakespearův	k2eAgFnSc1d1	Shakespearova
původní	původní	k2eAgFnSc1d1	původní
verze	verze	k1gFnSc1	verze
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
vrcholů	vrchol	k1gInPc2	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
George	Georg	k1gMnSc2	Georg
Bernard	Bernard	k1gMnSc1	Bernard
Shaw	Shaw	k1gMnSc1	Shaw
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Nikdo	nikdo	k3yNnSc1	nikdo
nikdy	nikdy	k6eAd1	nikdy
nenapíše	napsat	k5eNaPmIp3nS	napsat
lepší	dobrý	k2eAgFnSc4d2	lepší
tragédii	tragédie	k1gFnSc4	tragédie
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
Lear	Lear	k1gInSc1	Lear
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	dít	k5eAaBmRp2nS	dít
hry	hra	k1gFnSc2	hra
==	==	k?	==
</s>
</p>
<p>
<s>
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
keltský	keltský	k2eAgMnSc1d1	keltský
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
velice	velice	k6eAd1	velice
oblíbený	oblíbený	k2eAgMnSc1d1	oblíbený
a	a	k8xC	a
milovaný	milovaný	k2eAgMnSc1d1	milovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stáří	stáří	k1gNnSc1	stáří
ho	on	k3xPp3gNnSc4	on
přimělo	přimět	k5eAaPmAgNnS	přimět
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nebál	bát	k5eNaImAgMnS	bát
se	se	k3xPyFc4	se
opustit	opustit	k5eAaPmF	opustit
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
měl	mít	k5eAaImAgMnS	mít
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
mohl	moct	k5eAaImAgInS	moct
své	svůj	k3xOyFgFnPc4	svůj
vladařské	vladařský	k2eAgFnPc4d1	vladařská
povinnosti	povinnost	k1gFnPc4	povinnost
předat	předat	k5eAaPmF	předat
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
miloval	milovat	k5eAaImAgInS	milovat
svoji	svůj	k3xOyFgFnSc4	svůj
nejmladší	mladý	k2eAgFnSc4d3	nejmladší
dceru	dcera	k1gFnSc4	dcera
Cordelii	Cordelie	k1gFnSc4	Cordelie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
chtěl	chtít	k5eAaImAgInS	chtít
být	být	k5eAaImF	být
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakými	jaký	k3yQgNnPc7	jaký
slovy	slovo	k1gNnPc7	slovo
dcery	dcera	k1gFnSc2	dcera
vyjádří	vyjádřit	k5eAaPmIp3nS	vyjádřit
svoji	svůj	k3xOyFgFnSc4	svůj
lásku	láska	k1gFnSc4	láska
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
tolik	tolik	k6eAd1	tolik
jim	on	k3xPp3gMnPc3	on
dá	dát	k5eAaPmIp3nS	dát
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgInS	být
mile	mile	k6eAd1	mile
překvapen	překvapit	k5eAaPmNgInS	překvapit
neupřímnými	upřímný	k2eNgFnPc7d1	neupřímná
řečmi	řeč	k1gFnPc7	řeč
dvou	dva	k4xCgFnPc6	dva
starších	starý	k2eAgFnPc2d2	starší
dcer	dcera	k1gFnPc2	dcera
<g/>
,	,	kIx,	,
Goneril	Gonerila	k1gFnPc2	Gonerila
a	a	k8xC	a
Regan	Regana	k1gFnPc2	Regana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nemile	mile	k6eNd1	mile
překvapen	překvapen	k2eAgMnSc1d1	překvapen
slovy	slovo	k1gNnPc7	slovo
Cordelie	Cordelie	k1gFnSc2	Cordelie
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neuměla	umět	k5eNaImAgFnS	umět
slovy	slovo	k1gNnPc7	slovo
popsat	popsat	k5eAaPmF	popsat
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
cítí	cítit	k5eAaImIp3nP	cítit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
velice	velice	k6eAd1	velice
zarmoucen	zarmoutit	k5eAaPmNgMnS	zarmoutit
<g/>
.	.	kIx.	.
</s>
<s>
Lear	Lear	k1gMnSc1	Lear
ji	on	k3xPp3gFnSc4	on
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cordelie	Cordelie	k1gFnSc1	Cordelie
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
sestry	sestra	k1gFnPc1	sestra
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
otce	otec	k1gMnSc4	otec
milé	milá	k1gFnSc2	milá
jen	jen	k9	jen
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
blaho	blaho	k1gNnSc4	blaho
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
k	k	k7c3	k
otci	otec	k1gMnSc3	otec
chovaly	chovat	k5eAaImAgInP	chovat
velice	velice	k6eAd1	velice
hrubě	hrubě	k6eAd1	hrubě
a	a	k8xC	a
chladně	chladně	k6eAd1	chladně
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
ho	on	k3xPp3gMnSc4	on
dohnaly	dohnat	k5eAaPmAgFnP	dohnat
k	k	k7c3	k
šílenství	šílenství	k1gNnSc3	šílenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
Lear	Lear	k1gMnSc1	Lear
Cordelii	Cordelie	k1gFnSc4	Cordelie
<g/>
,	,	kIx,	,
křivdil	křivdit	k5eAaImAgMnS	křivdit
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Gloster	Gloster	k1gInSc4	Gloster
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Edgarovi	Edgar	k1gMnSc3	Edgar
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Edmund	Edmund	k1gMnSc1	Edmund
chtěl	chtít	k5eAaImAgMnS	chtít
získat	získat	k5eAaPmF	získat
celé	celý	k2eAgNnSc4d1	celé
dědictví	dědictví	k1gNnSc4	dědictví
a	a	k8xC	a
donutil	donutit	k5eAaPmAgInS	donutit
otce	otec	k1gMnSc4	otec
staršího	starší	k1gMnSc4	starší
bratra	bratr	k1gMnSc4	bratr
Edgara	Edgar	k1gMnSc4	Edgar
nenávidět	nenávidět	k5eAaImF	nenávidět
<g/>
.	.	kIx.	.
</s>
<s>
Edgar	Edgar	k1gMnSc1	Edgar
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
převléci	převléct	k5eAaPmF	převléct
za	za	k7c4	za
chudého	chudý	k1gMnSc4	chudý
blázna	blázen	k1gMnSc4	blázen
a	a	k8xC	a
schovávat	schovávat	k5eAaImF	schovávat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lear	Lear	k1gMnSc1	Lear
bloudil	bloudit	k5eAaImAgMnS	bloudit
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
šaškem	šašek	k1gMnSc7	šašek
a	a	k8xC	a
Kentem	Kent	k1gMnSc7	Kent
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Edgarem	Edgar	k1gMnSc7	Edgar
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
tím	ten	k3xDgInSc7	ten
soupeřily	soupeřit	k5eAaImAgInP	soupeřit
jeho	jeho	k3xOp3gFnPc4	jeho
dcery	dcera	k1gFnPc4	dcera
Goneril	Gonerila	k1gFnPc2	Gonerila
a	a	k8xC	a
Regan	Regana	k1gFnPc2	Regana
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
otci	otec	k1gMnSc6	otec
se	se	k3xPyFc4	se
donesly	donést	k5eAaPmAgFnP	donést
i	i	k9	i
ke	k	k7c3	k
Cordelii	Cordelie	k1gFnSc3	Cordelie
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jet	jet	k5eAaImF	jet
otce	otec	k1gMnSc4	otec
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Vstup	vstup	k1gInSc1	vstup
Francouzů	Francouz	k1gMnPc2	Francouz
na	na	k7c4	na
britské	britský	k2eAgNnSc4d1	Britské
území	území	k1gNnSc4	území
však	však	k8xC	však
sestry	sestra	k1gFnPc4	sestra
velice	velice	k6eAd1	velice
rozčílil	rozčílit	k5eAaPmAgMnS	rozčílit
a	a	k8xC	a
chystaly	chystat	k5eAaImAgFnP	chystat
se	se	k3xPyFc4	se
na	na	k7c4	na
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
Britové	Brit	k1gMnPc1	Brit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
Cordelie	Cordelie	k1gFnSc1	Cordelie
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
setkala	setkat	k5eAaPmAgFnS	setkat
<g/>
.	.	kIx.	.
</s>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
velel	velet	k5eAaImAgMnS	velet
části	část	k1gFnPc4	část
vítězného	vítězný	k2eAgNnSc2d1	vítězné
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
Cordelii	Cordelie	k1gFnSc4	Cordelie
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Cordelie	Cordelie	k1gFnSc1	Cordelie
byla	být	k5eAaImAgFnS	být
oběšena	oběsit	k5eAaPmNgFnS	oběsit
a	a	k8xC	a
Lear	Lear	k1gInSc1	Lear
umřel	umřít	k5eAaPmAgInS	umřít
ze	z	k7c2	z
zármutku	zármutek	k1gInSc2	zármutek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
i	i	k9	i
Edgar	Edgar	k1gMnSc1	Edgar
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
mu	on	k3xPp3gMnSc3	on
dokázal	dokázat	k5eAaPmAgMnS	dokázat
odpustit	odpustit	k5eAaPmF	odpustit
<g/>
.	.	kIx.	.
</s>
<s>
Usmířený	usmířený	k2eAgMnSc1d1	usmířený
otec	otec	k1gMnSc1	otec
nakonec	nakonec	k6eAd1	nakonec
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
náručí	náručí	k1gNnSc4	náručí
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Goneril	Goneril	k1gInSc1	Goneril
a	a	k8xC	a
Regan	Regan	k1gInSc1	Regan
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
zabily	zabít	k5eAaPmAgFnP	zabít
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
Edgar	Edgar	k1gMnSc1	Edgar
toto	tento	k3xDgNnSc4	tento
všechno	všechen	k3xTgNnSc4	všechen
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
2	[number]	k4	2
<g/>
/	/	kIx~	/
M-	M-	k1gMnPc2	M-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
960	[number]	k4	960
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
459	[number]	k4	459
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JIŘÍK	Jiřík	k1gMnSc1	Jiřík
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgInPc1d1	stručný
výklady	výklad	k1gInPc1	výklad
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
dramat	drama	k1gNnPc2	drama
Shakespearových	Shakespearových	k2eAgNnPc2d1	Shakespearových
<g/>
.	.	kIx.	.
</s>
<s>
Vyškov	Vyškov	k1gInSc1	Vyškov
<g/>
:	:	kIx,	:
Hanácké	hanácký	k2eAgNnSc1d1	Hanácké
knihkupectví	knihkupectví	k1gNnSc1	knihkupectví
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Pátka	Pátek	k1gMnSc2	Pátek
<g/>
,	,	kIx,	,
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
<g/>
,	,	kIx,	,
s.	s.	k?	s.
69	[number]	k4	69
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SHAKESPEARE	Shakespeare	k1gMnSc1	Shakespeare
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
:	:	kIx,	:
tragedie	tragedie	k1gFnSc1	tragedie
v	v	k7c6	v
pěti	pět	k4xCc6	pět
jednáních	jednání	k1gNnPc6	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Ladislav	Ladislava	k1gFnPc2	Ladislava
Čelakovský	Čelakovský	k2eAgMnSc1d1	Čelakovský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Museum	museum	k1gNnSc1	museum
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
,	,	kIx,	,
1856	[number]	k4	1856
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
69	[number]	k4	69
<g/>
-	-	kIx~	-
<g/>
77	[number]	k4	77
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
knižní	knižní	k2eAgNnSc1d1	knižní
vydání	vydání	k1gNnSc1	vydání
Shakespearovy	Shakespearův	k2eAgFnSc2d1	Shakespearova
známé	známý	k2eAgFnSc2d1	známá
alžbětinské	alžbětinský	k2eAgFnSc2d1	Alžbětinská
tragedie	tragedie	k1gFnSc2	tragedie
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Ladislava	Ladislav	k1gMnSc2	Ladislav
Čelakovského	Čelakovský	k2eAgMnSc2d1	Čelakovský
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Král	Král	k1gMnSc1	Král
Lear	Leara	k1gFnPc2	Leara
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
King	King	k1gMnSc1	King
Lear	Lear	k1gMnSc1	Lear
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Král	Král	k1gMnSc1	Král
Lear	Lear	k1gMnSc1	Lear
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
</s>
</p>
