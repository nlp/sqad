<s>
Stratus	stratus	k1gInSc1	stratus
(	(	kIx(	(
<g/>
St	St	kA	St
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
sloha	sloha	k1gFnSc1	sloha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
oblak	oblak	k1gInSc1	oblak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
neliší	lišit	k5eNaImIp3nP	lišit
od	od	k7c2	od
mlhy	mlha	k1gFnSc2	mlha
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nejnižších	nízký	k2eAgFnPc6d3	nejnižší
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
jen	jen	k9	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
mlhy	mlha	k1gFnSc2	mlha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
spodní	spodní	k2eAgFnSc1d1	spodní
hranice	hranice	k1gFnSc1	hranice
se	se	k3xPyFc4	se
zvedá	zvedat	k5eAaImIp3nS	zvedat
od	od	k7c2	od
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
větší	veliký	k2eAgFnSc2d2	veliký
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
<g/>
-li	i	k?	-li
inverzní	inverzní	k2eAgNnSc4d1	inverzní
počasí	počasí	k1gNnSc4	počasí
po	po	k7c4	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
stratus	stratus	k1gInSc1	stratus
jako	jako	k8xC	jako
jednolitá	jednolitý	k2eAgFnSc1d1	jednolitá
mlhavá	mlhavý	k2eAgFnSc1d1	mlhavá
šedá	šedý	k2eAgFnSc1d1	šedá
vrstva	vrstva	k1gFnSc1	vrstva
zatahovat	zatahovat	k5eAaImF	zatahovat
oblohu	obloha	k1gFnSc4	obloha
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
psychiku	psychika	k1gFnSc4	psychika
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
noční	noční	k2eAgFnSc1d1	noční
inverze	inverze	k1gFnSc1	inverze
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
spojená	spojený	k2eAgFnSc1d1	spojená
oblačnost	oblačnost	k1gFnSc1	oblačnost
stratus	stratus	k1gInSc1	stratus
v	v	k7c6	v
dopoledních	dopolední	k2eAgFnPc6d1	dopolední
hodinách	hodina	k1gFnPc6	hodina
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
přecházet	přecházet	k5eAaImF	přecházet
tyto	tento	k3xDgInPc4	tento
oblaky	oblak	k1gInPc4	oblak
do	do	k7c2	do
tvarů	tvar	k1gInPc2	tvar
stratocumulus	stratocumulus	k1gInSc1	stratocumulus
až	až	k8xS	až
cumulus	cumulus	k1gInSc1	cumulus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
často	často	k6eAd1	často
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
ze	z	k7c2	z
stratu	stratus	k1gInSc2	stratus
mrholení	mrholení	k1gNnSc1	mrholení
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stratus	stratus	k1gInSc1	stratus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
