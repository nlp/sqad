<s>
Velština	velština	k1gFnSc1	velština
(	(	kIx(	(
<g/>
velšsky	velšsky	k6eAd1	velšsky
Cymraeg	Cymraeg	k1gMnSc1	Cymraeg
nebo	nebo	k8xC	nebo
y	y	k?	y
Gymraeg	Gymraeg	k1gInSc1	Gymraeg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
keltský	keltský	k2eAgInSc4d1	keltský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
hovoří	hovořit	k5eAaImIp3nP	hovořit
Velšané	Velšan	k1gMnPc1	Velšan
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
<g/>
,	,	kIx,	,
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Walesem	Wales	k1gInSc7	Wales
a	a	k8xC	a
v	v	k7c6	v
enklávách	enkláva	k1gFnPc6	enkláva
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Velština	velština	k1gFnSc1	velština
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
keltským	keltský	k2eAgInSc7d1	keltský
jazykem	jazyk	k1gInSc7	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
význam	význam	k1gInSc1	význam
velštiny	velština	k1gFnSc2	velština
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
upadal	upadat	k5eAaPmAgInS	upadat
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
obrození	obrození	k1gNnSc3	obrození
<g/>
,	,	kIx,	,
když	když	k8xS	když
Britský	britský	k2eAgInSc1d1	britský
parlament	parlament	k1gInSc1	parlament
postavil	postavit	k5eAaPmAgInS	postavit
velštinu	velština	k1gFnSc4	velština
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
úroveň	úroveň	k1gFnSc4	úroveň
jako	jako	k8xC	jako
angličtinu	angličtina	k1gFnSc4	angličtina
pro	pro	k7c4	pro
používání	používání	k1gNnSc4	používání
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
veřejném	veřejný	k2eAgInSc6d1	veřejný
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
průzkumu	průzkum	k1gInSc2	průzkum
o	o	k7c4	o
používání	používání	k1gNnSc4	používání
velšského	velšský	k2eAgInSc2d1	velšský
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Walesu	Wales	k1gInSc6	Wales
611	[number]	k4	611
000	[number]	k4	000
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
21,7	[number]	k4	21,7
procent	procento	k1gNnPc2	procento
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Velština	velština	k1gFnSc1	velština
používá	používat	k5eAaImIp3nS	používat
latinskou	latinský	k2eAgFnSc4d1	Latinská
abecedu	abeceda	k1gFnSc4	abeceda
<g/>
+	+	kIx~	+
<g/>
následující	následující	k2eAgNnPc4d1	následující
písmena	písmeno	k1gNnPc4	písmeno
s	s	k7c7	s
diakritikou	diakritika	k1gFnSc7	diakritika
<g/>
:	:	kIx,	:
Á	á	k0	á
<g/>
,	,	kIx,	,
À	À	k?	À
<g/>
,	,	kIx,	,
Â	Â	kA	Â
<g/>
,	,	kIx,	,
Ä	Ä	kA	Ä
<g/>
,	,	kIx,	,
É	É	kA	É
<g/>
,	,	kIx,	,
È	È	k?	È
<g/>
,	,	kIx,	,
Ê	Ê	k?	Ê
<g/>
,	,	kIx,	,
Ë	Ë	kA	Ë
<g/>
,	,	kIx,	,
Ì	Ì	k?	Ì
<g/>
,	,	kIx,	,
Î	Î	kA	Î
<g/>
,	,	kIx,	,
Ï	Ï	k?	Ï
<g/>
,	,	kIx,	,
Ó	ó	k0	ó
<g/>
,	,	kIx,	,
Ò	Ò	k?	Ò
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Ô	Ô	kA	Ô
<g/>
,	,	kIx,	,
Ö	Ö	kA	Ö
<g/>
,	,	kIx,	,
Ù	Ù	k?	Ù
<g/>
,	,	kIx,	,
Û	Û	k?	Û
<g/>
,	,	kIx,	,
Ẁ	Ẁ	k?	Ẁ
<g/>
,	,	kIx,	,
Ŵ	Ŵ	k?	Ŵ
<g/>
,	,	kIx,	,
Ẅ	Ẅ	k?	Ẅ
<g/>
,	,	kIx,	,
Ý	Ý	kA	Ý
<g/>
,	,	kIx,	,
Ỳ	Ỳ	k?	Ỳ
<g/>
,	,	kIx,	,
Ŷ	Ŷ	k?	Ŷ
<g/>
,	,	kIx,	,
Ÿ	Ÿ	k?	Ÿ
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
velština	velština	k1gFnSc1	velština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
velština	velština	k1gFnSc1	velština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
<g />
.	.	kIx.	.
</s>
<s>
Velština	velština	k1gFnSc1	velština
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
jazyk	jazyk	k1gInSc1	jazyk
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
renesanci	renesance	k1gFnSc3	renesance
-	-	kIx~	-
článek	článek	k1gInSc1	článek
(	(	kIx(	(
<g/>
CS	CS	kA	CS
<g/>
)	)	kIx)	)
Jazykolam	jazykolam	k1gInSc1	jazykolam
zvaný	zvaný	k2eAgInSc1d1	zvaný
waleština	waleština	k1gFnSc1	waleština
-	-	kIx~	-
článek	článek	k1gInSc1	článek
(	(	kIx(	(
<g/>
CS	CS	kA	CS
<g/>
)	)	kIx)	)
Y	Y	kA	Y
Wladfa	Wladf	k1gMnSc2	Wladf
-	-	kIx~	-
The	The	k1gMnSc1	The
Welsh	Welsh	k1gMnSc1	Welsh
in	in	k?	in
Patagonia	Patagonium	k1gNnSc2	Patagonium
/	/	kIx~	/
O	o	k7c6	o
velštině	velština	k1gFnSc6	velština
v	v	k7c6	v
Patagonii	Patagonie	k1gFnSc6	Patagonie
(	(	kIx(	(
<g/>
EN	EN	kA	EN
<g/>
)	)	kIx)	)
Welsh	Welsh	k1gMnSc1	Welsh
Language	language	k1gFnSc2	language
Act	Act	k1gFnSc1	Act
1993	[number]	k4	1993
(	(	kIx(	(
<g/>
plné	plný	k2eAgNnSc1d1	plné
znění	znění	k1gNnSc1	znění
<g/>
,	,	kIx,	,
EN	EN	kA	EN
<g/>
)	)	kIx)	)
Radio	radio	k1gNnSc1	radio
Cymru	Cymr	k1gInSc2	Cymr
<g/>
,	,	kIx,	,
velšské	velšský	k2eAgNnSc4d1	Velšské
vysílání	vysílání	k1gNnSc4	vysílání
BBC	BBC	kA	BBC
</s>
