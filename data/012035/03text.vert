<p>
<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
umělecky	umělecky	k6eAd1	umělecky
bohatě	bohatě	k6eAd1	bohatě
zdobená	zdobený	k2eAgFnSc1d1	zdobená
<g/>
,	,	kIx,	,
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
šlechtickým	šlechtický	k2eAgInPc3d1	šlechtický
rodům	rod	k1gInPc3	rod
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
členům	člen	k1gMnPc3	člen
velkoburžoazie	velkoburžoazie	k1gFnSc2	velkoburžoazie
jako	jako	k8xS	jako
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
a	a	k8xC	a
pohodlné	pohodlný	k2eAgNnSc1d1	pohodlné
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
zámků	zámek	k1gInPc2	zámek
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
základů	základ	k1gInPc2	základ
hradní	hradní	k2eAgFnSc2d1	hradní
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
klášterní	klášterní	k2eAgFnSc1d1	klášterní
koncepce	koncepce	k1gFnSc1	koncepce
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
však	však	k9	však
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinou	většina	k1gFnSc7	většina
postrádá	postrádat	k5eAaImIp3nS	postrádat
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
článek	článek	k1gInSc1	článek
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
zámcích	zámek	k1gInPc6	zámek
coby	coby	k?	coby
obytných	obytný	k2eAgFnPc6d1	obytná
budovách	budova	k1gFnPc6	budova
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
architektonickém	architektonický	k2eAgInSc6d1	architektonický
a	a	k8xC	a
historicko-uměleckém	historickomělecký	k2eAgNnSc6d1	historicko-umělecký
na	na	k7c6	na
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
přehled	přehled	k1gInSc1	přehled
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
staveb	stavba	k1gFnPc2	stavba
<g/>
:	:	kIx,	:
stavby	stavba	k1gFnPc1	stavba
obehnané	obehnaný	k2eAgFnPc1d1	obehnaná
příkopem	příkop	k1gInSc7	příkop
nebo	nebo	k8xC	nebo
postavené	postavený	k2eAgFnPc4d1	postavená
na	na	k7c6	na
řekách	řeka	k1gFnPc6	řeka
či	či	k8xC	či
jezerech	jezero	k1gNnPc6	jezero
bývají	bývat	k5eAaImIp3nP	bývat
označovány	označován	k2eAgFnPc1d1	označována
jako	jako	k8xC	jako
vodní	vodní	k2eAgInSc1d1	vodní
zámek	zámek	k1gInSc1	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Lovecký	lovecký	k2eAgInSc1d1	lovecký
zámek	zámek	k1gInSc1	zámek
(	(	kIx(	(
<g/>
častěji	často	k6eAd2	často
"	"	kIx"	"
<g/>
zámeček	zámeček	k1gInSc4	zámeček
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k9	jako
přijímací	přijímací	k2eAgNnSc4d1	přijímací
místo	místo	k1gNnSc4	místo
dvorní	dvorní	k2eAgFnSc2d1	dvorní
společnosti	společnost	k1gFnSc2	společnost
při	při	k7c6	při
pořádání	pořádání	k1gNnSc6	pořádání
honů	hon	k1gInPc2	hon
na	na	k7c4	na
zvěř	zvěř	k1gFnSc4	zvěř
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
letohrádek	letohrádek	k1gInSc1	letohrádek
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
může	moct	k5eAaImIp3nS	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
rezidence	rezidence	k1gFnSc1	rezidence
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
převážně	převážně	k6eAd1	převážně
sloužila	sloužit	k5eAaImAgFnS	sloužit
jen	jen	k9	jen
k	k	k7c3	k
potěšení	potěšení	k1gNnSc3	potěšení
a	a	k8xC	a
odpočinku	odpočinek	k1gInSc3	odpočinek
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
pro	pro	k7c4	pro
státní	státní	k2eAgFnPc4d1	státní
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Letohrádek	letohrádek	k1gInSc4	letohrádek
královny	královna	k1gFnSc2	královna
Anny	Anna	k1gFnSc2	Anna
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
označujeme	označovat	k5eAaImIp1nP	označovat
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
osídlena	osídlit	k5eAaPmNgFnS	osídlit
jen	jen	k9	jen
sezónně	sezónně	k6eAd1	sezónně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Trojský	trojský	k2eAgInSc4d1	trojský
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Praze-Troji	Praze-Troj	k1gInSc6	Praze-Troj
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k6eAd1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
označení	označení	k1gNnPc2	označení
je	být	k5eAaImIp3nS	být
však	však	k9	však
víceméně	víceméně	k9	víceméně
orientační	orientační	k2eAgFnPc1d1	orientační
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
mísit	mísit	k5eAaImF	mísit
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
zámků	zámek	k1gInPc2	zámek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnSc2	staletí
rozšiřováno	rozšiřován	k2eAgNnSc1d1	rozšiřováno
a	a	k8xC	a
přestavováno	přestavován	k2eAgNnSc1d1	přestavováno
<g/>
.	.	kIx.	.
</s>
<s>
Palácem	palác	k1gInSc7	palác
všeobecně	všeobecně	k6eAd1	všeobecně
nazýváme	nazývat	k5eAaImIp1nP	nazývat
obzvláště	obzvláště	k6eAd1	obzvláště
reprezentativně	reprezentativně	k6eAd1	reprezentativně
vyvedené	vyvedený	k2eAgNnSc4d1	vyvedené
obytné	obytný	k2eAgNnSc4d1	obytné
a	a	k8xC	a
vládní	vládní	k2eAgNnSc4d1	vládní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
setkáme	setkat	k5eAaPmIp1nP	setkat
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
orientálními	orientální	k2eAgFnPc7d1	orientální
nebo	nebo	k8xC	nebo
antickými	antický	k2eAgFnPc7d1	antická
zámeckými	zámecký	k2eAgFnPc7d1	zámecká
stavbami	stavba	k1gFnPc7	stavba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přejat	přejmout	k5eAaPmNgMnS	přejmout
z	z	k7c2	z
italského	italský	k2eAgInSc2d1	italský
Palazzo	Palazza	k1gFnSc5	Palazza
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
jej	on	k3xPp3gNnSc4	on
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
obměnách	obměna	k1gFnPc6	obměna
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
:	:	kIx,	:
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
reprezentativní	reprezentativní	k2eAgFnPc1d1	reprezentativní
stavby	stavba	k1gFnPc1	stavba
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
Palace	Palace	k1gFnPc1	Palace
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
slovo	slovo	k1gNnSc1	slovo
Palais	Palais	k1gFnSc2	Palais
označuje	označovat	k5eAaImIp3nS	označovat
reprezentativní	reprezentativní	k2eAgFnSc4d1	reprezentativní
městskou	městský	k2eAgFnSc4d1	městská
stavbu	stavba	k1gFnSc4	stavba
(	(	kIx(	(
<g/>
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
Château	château	k1gNnPc3	château
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
však	však	k9	však
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
přesná	přesný	k2eAgFnSc1d1	přesná
definice	definice	k1gFnSc1	definice
obou	dva	k4xCgInPc2	dva
pojmů	pojem	k1gInPc2	pojem
(	(	kIx(	(
<g/>
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c6	v
českém	český	k2eAgInSc6d1	český
jazyce	jazyk	k1gInSc6	jazyk
používány	používat	k5eAaImNgFnP	používat
záměnně	záměnně	k6eAd1	záměnně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
středověku	středověk	k1gInSc2	středověk
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
novověku	novověk	k1gInSc2	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Zásluhou	zásluhou	k7c2	zásluhou
vynálezu	vynález	k1gInSc2	vynález
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
ztrácely	ztrácet	k5eAaImAgInP	ztrácet
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
původní	původní	k2eAgInPc4d1	původní
opevněné	opevněný	k2eAgInPc4d1	opevněný
<g/>
,	,	kIx,	,
nepohodlné	pohodlný	k2eNgInPc4d1	nepohodlný
hrady	hrad	k1gInPc4	hrad
stísněné	stísněný	k2eAgInPc4d1	stísněný
v	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
zdech	zeď	k1gFnPc6	zeď
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
ochrany	ochrana	k1gFnSc2	ochrana
a	a	k8xC	a
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
a	a	k8xC	a
zdokonalováním	zdokonalování	k1gNnSc7	zdokonalování
střelných	střelný	k2eAgFnPc2d1	střelná
zbraní	zbraň	k1gFnPc2	zbraň
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
i	i	k9	i
vojenská	vojenský	k2eAgFnSc1d1	vojenská
taktika	taktika	k1gFnSc1	taktika
a	a	k8xC	a
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
hraničních	hraniční	k2eAgInPc2d1	hraniční
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
rodových	rodový	k2eAgFnPc2d1	rodová
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
též	též	k9	též
skrze	skrze	k?	skrze
šikovnou	šikovný	k2eAgFnSc4d1	šikovná
sňatkovou	sňatkový	k2eAgFnSc4d1	sňatková
politiku	politika	k1gFnSc4	politika
<g/>
)	)	kIx)	)
z	z	k7c2	z
maličkých	maličká	k1gFnPc2	maličká
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
knížectví	knížectví	k1gNnSc2	knížectví
a	a	k8xC	a
vévodství	vévodství	k1gNnSc2	vévodství
či	či	k8xC	či
hrabství	hrabství	k1gNnSc2	hrabství
pomalu	pomalu	k6eAd1	pomalu
vyrůstaly	vyrůstat	k5eAaImAgInP	vyrůstat
větší	veliký	k2eAgInPc1d2	veliký
celistvé	celistvý	k2eAgInPc1d1	celistvý
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
hrady	hrad	k1gInPc1	hrad
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
zranitelnosti	zranitelnost	k1gFnSc2	zranitelnost
proti	proti	k7c3	proti
stále	stále	k6eAd1	stále
účinnějším	účinný	k2eAgNnPc3d2	účinnější
dělům	dělo	k1gNnPc3	dělo
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
sotva	sotva	k8xS	sotva
ještě	ještě	k6eAd1	ještě
nějakou	nějaký	k3yIgFnSc4	nějaký
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ten	ten	k3xDgInSc4	ten
dopad	dopad	k1gInSc4	dopad
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
stále	stále	k6eAd1	stále
větším	veliký	k2eAgNnSc6d2	veliký
území	území	k1gNnSc6	území
a	a	k8xC	a
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
nastolován	nastolován	k2eAgInSc4d1	nastolován
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k9	již
nic	nic	k3yNnSc1	nic
nestálo	stát	k5eNaImAgNnS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
přání	přání	k1gNnPc2	přání
i	i	k8xC	i
nutnosti	nutnost	k1gFnSc3	nutnost
pohodlnějších	pohodlný	k2eAgFnPc2d2	pohodlnější
a	a	k8xC	a
reprezentativnějších	reprezentativní	k2eAgFnPc2d2	reprezentativnější
obytných	obytný	k2eAgFnPc2d1	obytná
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jak	jak	k8xS	jak
zámky	zámek	k1gInPc1	zámek
vznikaly	vznikat	k5eAaImAgInP	vznikat
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
shrnout	shrnout	k5eAaPmF	shrnout
jejich	jejich	k3xOp3gInSc4	jejich
uměleckohistorický	uměleckohistorický	k2eAgInSc4d1	uměleckohistorický
vývoj	vývoj	k1gInSc4	vývoj
do	do	k7c2	do
několika	několik	k4yIc2	několik
větších	veliký	k2eAgFnPc2d2	veliký
vývojových	vývojový	k2eAgFnPc2d1	vývojová
epoch	epocha	k1gFnPc2	epocha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Rozvoj	rozvoj	k1gInSc1	rozvoj
zámecké	zámecký	k2eAgFnSc2d1	zámecká
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Románský	románský	k2eAgInSc1d1	románský
sloh	sloh	k1gInSc1	sloh
a	a	k8xC	a
gotika	gotika	k1gFnSc1	gotika
===	===	k?	===
</s>
</p>
<p>
<s>
Za	za	k7c2	za
časů	čas	k1gInPc2	čas
středověku	středověk	k1gInSc2	středověk
byly	být	k5eAaImAgFnP	být
čistě	čistě	k6eAd1	čistě
zámecké	zámecký	k2eAgFnPc1d1	zámecká
stavby	stavba	k1gFnPc1	stavba
řídké	řídký	k2eAgFnPc1d1	řídká
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgInSc1d3	nejsilnější
důraz	důraz	k1gInSc1	důraz
byl	být	k5eAaImAgInS	být
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
a	a	k8xC	a
pro	pro	k7c4	pro
dosud	dosud	k6eAd1	dosud
ranou	rána	k1gFnSc7	rána
šlechty	šlechta	k1gFnSc2	šlechta
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
hrady	hrad	k1gInPc1	hrad
coby	coby	k?	coby
sídla	sídlo	k1gNnSc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
chránily	chránit	k5eAaImAgFnP	chránit
před	před	k7c7	před
nepřátelskými	přátelský	k2eNgMnPc7d1	nepřátelský
sousedy	soused	k1gMnPc7	soused
a	a	k8xC	a
leckde	leckde	k6eAd1	leckde
vytvářely	vytvářet	k5eAaImAgInP	vytvářet
střed	střed	k1gInSc4	střed
budoucích	budoucí	k2eAgInPc2d1	budoucí
zámků	zámek	k1gInPc2	zámek
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zámožní	zámožný	k2eAgMnPc1d1	zámožný
hradní	hradní	k2eAgMnPc1d1	hradní
páni	pan	k1gMnPc1	pan
si	se	k3xPyFc3	se
zpočátku	zpočátku	k6eAd1	zpočátku
své	svůj	k3xOyFgFnSc2	svůj
pevnosti	pevnost	k1gFnSc2	pevnost
nechali	nechat	k5eAaPmAgMnP	nechat
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přejatého	přejatý	k2eAgInSc2d1	přejatý
románského	románský	k2eAgInSc2d1	románský
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
gotického	gotický	k2eAgInSc2d1	gotický
slohu	sloh	k1gInSc2	sloh
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Působivými	působivý	k2eAgMnPc7d1	působivý
svědky	svědek	k1gMnPc7	svědek
těchto	tento	k3xDgInPc2	tento
hrado-zámků	hradoámek	k1gInPc2	hrado-zámek
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
věhlasné	věhlasný	k2eAgInPc1d1	věhlasný
francouzské	francouzský	k2eAgInPc1d1	francouzský
zámky	zámek	k1gInPc1	zámek
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Loiře	Loira	k1gFnSc6	Loira
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
sice	sice	k8xC	sice
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
měnily	měnit	k5eAaImAgFnP	měnit
svou	svůj	k3xOyFgFnSc4	svůj
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gInSc4	jejich
původní	původní	k2eAgInSc4d1	původní
vzhled	vzhled	k1gInSc4	vzhled
byl	být	k5eAaImAgInS	být
přenesen	přenést	k5eAaPmNgInS	přenést
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
manuskript	manuskript	k1gInSc4	manuskript
Trè	Trè	k1gMnSc2	Trè
Riches	Richesa	k1gFnPc2	Richesa
Heures	Heures	k1gMnSc1	Heures
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Berry	Berra	k1gFnSc2	Berra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Svaté	svatý	k2eAgFnPc4d1	svatá
říše	říš	k1gFnPc4	říš
římské	římský	k2eAgFnPc4d1	římská
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
palácové	palácový	k2eAgInPc1d1	palácový
hrady	hrad	k1gInPc1	hrad
zámeckého	zámecký	k2eAgInSc2d1	zámecký
střihu	střih	k1gInSc2	střih
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
zpočátku	zpočátku	k6eAd1	zpočátku
cestujícím	cestující	k2eAgMnPc3d1	cestující
příslušníkům	příslušník	k1gMnPc3	příslušník
dvora	dvůr	k1gInSc2	dvůr
a	a	k8xC	a
císaři	císař	k1gMnPc1	císař
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Působivým	působivý	k2eAgInSc7d1	působivý
příkladem	příklad	k1gInSc7	příklad
přechodu	přechod	k1gInSc2	přechod
od	od	k7c2	od
opevněného	opevněný	k2eAgInSc2d1	opevněný
hradu	hrad	k1gInSc2	hrad
k	k	k7c3	k
zámkovému	zámkový	k2eAgInSc3d1	zámkový
komplexu	komplex	k1gInSc3	komplex
je	být	k5eAaImIp3nS	být
Albrechtsburg	Albrechtsburg	k1gInSc1	Albrechtsburg
v	v	k7c6	v
Míšni	Míšeň	k1gFnSc6	Míšeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pak	pak	k6eAd1	pak
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
a	a	k8xC	a
Florencii	Florencie	k1gFnSc6	Florencie
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInPc1	první
městské	městský	k2eAgInPc1d1	městský
paláce	palác	k1gInPc1	palác
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
třeba	třeba	k9	třeba
Ca	ca	kA	ca
<g/>
'	'	kIx"	'
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Oro	Oro	k1gMnSc1	Oro
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
ještě	ještě	k6eAd1	ještě
sice	sice	k8xC	sice
nebyly	být	k5eNaImAgFnP	být
skutečnými	skutečný	k2eAgInPc7d1	skutečný
"	"	kIx"	"
<g/>
zámky	zámek	k1gInPc7	zámek
<g/>
"	"	kIx"	"
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
a	a	k8xC	a
často	často	k6eAd1	často
patřily	patřit	k5eAaImAgFnP	patřit
nikoli	nikoli	k9	nikoli
šlechtickým	šlechtický	k2eAgInPc3d1	šlechtický
rodům	rod	k1gInPc3	rod
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
jenom	jenom	k9	jenom
<g/>
"	"	kIx"	"
bohatým	bohatý	k2eAgMnPc3d1	bohatý
kupcům	kupec	k1gMnPc3	kupec
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
rozmach	rozmach	k1gInSc1	rozmach
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
a	a	k8xC	a
spojení	spojení	k1gNnSc1	spojení
pohodlí	pohodlí	k1gNnSc2	pohodlí
bydlení	bydlení	k1gNnSc2	bydlení
a	a	k8xC	a
reprezentativnosti	reprezentativnost	k1gFnSc2	reprezentativnost
brzo	brzo	k6eAd1	brzo
začaly	začít	k5eAaPmAgFnP	začít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
vzor	vzor	k1gInSc4	vzor
světských	světský	k2eAgFnPc2d1	světská
staveb	stavba	k1gFnPc2	stavba
ostatních	ostatní	k2eAgFnPc2d1	ostatní
epoch	epocha	k1gFnPc2	epocha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Renesance	renesance	k1gFnSc2	renesance
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
renesanční	renesanční	k2eAgFnSc6d1	renesanční
Itálii	Itálie	k1gFnSc6	Itálie
vyrůstaly	vyrůstat	k5eAaImAgInP	vyrůstat
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vedle	vedle	k7c2	vedle
městských	městský	k2eAgFnPc2d1	městská
palazzi	palazze	k1gFnSc6	palazze
první	první	k4xOgInSc4	první
samostatně	samostatně	k6eAd1	samostatně
stojící	stojící	k2eAgFnPc4d1	stojící
villy	villa	k1gFnPc4	villa
již	již	k6eAd1	již
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
antiky	antika	k1gFnSc2	antika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
La	la	k1gNnSc1	la
Rotonda	Rotond	k1gMnSc2	Rotond
poblíž	poblíž	k7c2	poblíž
Vicenzy	Vicenza	k1gFnSc2	Vicenza
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
domy	dům	k1gInPc1	dům
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgNnSc1d1	postavené
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
městské	městský	k2eAgMnPc4d1	městský
aristokraty	aristokrat	k1gMnPc4	aristokrat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
prvními	první	k4xOgMnPc7	první
stavbami	stavba	k1gFnPc7	stavba
evropského	evropský	k2eAgInSc2d1	evropský
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejichž	jejichž	k3xOyRp3gFnSc6	jejichž
stavbě	stavba	k1gFnSc6	stavba
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
stálo	stát	k5eAaImAgNnS	stát
přání	přání	k1gNnSc1	přání
po	po	k7c4	po
pohodlí	pohodlí	k1gNnSc4	pohodlí
nebo	nebo	k8xC	nebo
reprezentativnost	reprezentativnost	k1gFnSc1	reprezentativnost
a	a	k8xC	a
stavební	stavební	k2eAgInPc1d1	stavební
plány	plán	k1gInPc1	plán
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
začlenění	začlenění	k1gNnSc4	začlenění
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
přírody	příroda	k1gFnSc2	příroda
nebo	nebo	k8xC	nebo
vzhledu	vzhled	k1gInSc2	vzhled
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Renesanční	renesanční	k2eAgInSc1d1	renesanční
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
orientoval	orientovat	k5eAaBmAgInS	orientovat
na	na	k7c4	na
architekturu	architektura	k1gFnSc4	architektura
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
brzo	brzo	k6eAd1	brzo
našel	najít	k5eAaPmAgMnS	najít
velkou	velký	k2eAgFnSc4d1	velká
odezvu	odezva	k1gFnSc4	odezva
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Imitovaly	imitovat	k5eAaBmAgFnP	imitovat
se	se	k3xPyFc4	se
proporce	proporce	k1gFnPc1	proporce
a	a	k8xC	a
stavební	stavební	k2eAgInPc1d1	stavební
detaily	detail	k1gInPc1	detail
starých	starý	k2eAgInPc2d1	starý
chrámů	chrám	k1gInPc2	chrám
a	a	k8xC	a
svatyní	svatyně	k1gFnPc2	svatyně
nebo	nebo	k8xC	nebo
samotného	samotný	k2eAgNnSc2d1	samotné
kolosea	koloseum	k1gNnSc2	koloseum
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
zdobeny	zdobit	k5eAaImNgFnP	zdobit
klasickým	klasický	k2eAgNnSc7d1	klasické
sloupořadím	sloupořadí	k1gNnSc7	sloupořadí
v	v	k7c6	v
řecko-římském	řecko-římský	k2eAgInSc6d1	řecko-římský
stylu	styl	k1gInSc6	styl
a	a	k8xC	a
velkolepými	velkolepý	k2eAgInPc7d1	velkolepý
štíty	štít	k1gInPc7	štít
<g/>
,	,	kIx,	,
městské	městský	k2eAgInPc1d1	městský
paláce	palác	k1gInPc1	palác
dostaly	dostat	k5eAaPmAgFnP	dostat
souměrné	souměrný	k2eAgFnPc1d1	souměrná
Fasády	fasáda	k1gFnPc1	fasáda
s	s	k7c7	s
velkorysými	velkorysý	k2eAgFnPc7d1	velkorysá
řadami	řada	k1gFnPc7	řada
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
vyzdobenými	vyzdobený	k2eAgInPc7d1	vyzdobený
vchodovými	vchodový	k2eAgInPc7d1	vchodový
portály	portál	k1gInPc7	portál
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
prostě	prostě	k6eAd1	prostě
přestavovány	přestavován	k2eAgFnPc4d1	přestavována
pozůstalé	pozůstalý	k2eAgFnPc4d1	pozůstalá
hradní	hradní	k2eAgFnPc4d1	hradní
koncepce	koncepce	k1gFnPc4	koncepce
nebo	nebo	k8xC	nebo
rozšiřovány	rozšiřován	k2eAgFnPc4d1	rozšiřována
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nového	nový	k2eAgInSc2d1	nový
slohu	sloh	k1gInSc2	sloh
(	(	kIx(	(
<g/>
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zhusta	zhusta	k6eAd1	zhusta
interpretován	interpretovat	k5eAaBmNgInS	interpretovat
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
sotva	sotva	k6eAd1	sotva
našel	najít	k5eAaPmAgMnS	najít
nějaký	nějaký	k3yIgMnSc1	nějaký
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nP	by
podobné	podobný	k2eAgFnPc4d1	podobná
stavby	stavba	k1gFnPc4	stavba
viděl	vidět	k5eAaImAgMnS	vidět
na	na	k7c4	na
vlastní	vlastní	k2eAgNnPc4d1	vlastní
oči	oko	k1gNnPc4	oko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
posloužit	posloužit	k5eAaPmF	posloužit
německý	německý	k2eAgInSc1d1	německý
Heidelberský	heidelberský	k2eAgInSc1d1	heidelberský
zámek	zámek	k1gInSc1	zámek
nebo	nebo	k8xC	nebo
francouzský	francouzský	k2eAgInSc1d1	francouzský
zámek	zámek	k1gInSc1	zámek
Amboise	Amboise	k1gFnSc2	Amboise
<g/>
.	.	kIx.	.
</s>
<s>
Zámky	zámek	k1gInPc1	zámek
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgFnP	být
nejprve	nejprve	k6eAd1	nejprve
ještě	ještě	k6eAd1	ještě
velice	velice	k6eAd1	velice
nepravidelně	pravidelně	k6eNd1	pravidelně
odstupňované	odstupňovaný	k2eAgInPc1d1	odstupňovaný
a	a	k8xC	a
zřídkakdy	zřídkakdy	k6eAd1	zřídkakdy
se	se	k3xPyFc4	se
postupovalo	postupovat	k5eAaImAgNnS	postupovat
podle	podle	k7c2	podle
nějakého	nějaký	k3yIgInSc2	nějaký
jednotného	jednotný	k2eAgInSc2d1	jednotný
stavebního	stavební	k2eAgInSc2d1	stavební
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
se	se	k3xPyFc4	se
přistavovaly	přistavovat	k5eAaImAgFnP	přistavovat
nebo	nebo	k8xC	nebo
přestavovaly	přestavovat	k5eAaImAgFnP	přestavovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
budovy	budova	k1gFnPc4	budova
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc4	jejich
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
hlavní	hlavní	k2eAgInSc1d1	hlavní
účel	účel	k1gInSc1	účel
byl	být	k5eAaImAgInS	být
splněn	splnit	k5eAaPmNgInS	splnit
<g/>
:	:	kIx,	:
vymanit	vymanit	k5eAaPmF	vymanit
se	se	k3xPyFc4	se
z	z	k7c2	z
tísnivých	tísnivý	k2eAgFnPc2d1	tísnivá
prostor	prostora	k1gFnPc2	prostora
starých	starý	k2eAgNnPc2d1	staré
hradních	hradní	k2eAgNnPc2d1	hradní
opevnění	opevnění	k1gNnPc2	opevnění
a	a	k8xC	a
vystavět	vystavět	k5eAaPmF	vystavět
nové	nový	k2eAgFnPc4d1	nová
samostatné	samostatný	k2eAgFnPc4d1	samostatná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
nové	nový	k2eAgInPc4d1	nový
zámky	zámek	k1gInPc4	zámek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
třeba	třeba	k6eAd1	třeba
zámek	zámek	k1gInSc1	zámek
Chambord	Chamborda	k1gFnPc2	Chamborda
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
nebo	nebo	k8xC	nebo
Escorial	Escorial	k1gInSc4	Escorial
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
kostelem	kostel	k1gInSc7	kostel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Baroko	baroko	k1gNnSc1	baroko
===	===	k?	===
</s>
</p>
<p>
<s>
Příchod	příchod	k1gInSc1	příchod
baroka	baroko	k1gNnSc2	baroko
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
projevovat	projevovat	k5eAaImF	projevovat
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
kvetoucího	kvetoucí	k2eAgInSc2d1	kvetoucí
absolutismu	absolutismus	k1gInSc2	absolutismus
<g/>
.	.	kIx.	.
</s>
<s>
Knížata	kníže	k1gMnPc1wR	kníže
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
shromažďovala	shromažďovat	k5eAaImAgFnS	shromažďovat
moc	moc	k6eAd1	moc
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
také	také	k6eAd1	také
chtěli	chtít	k5eAaImAgMnP	chtít
dát	dát	k5eAaPmF	dát
patřičně	patřičně	k6eAd1	patřičně
najevo	najevo	k6eAd1	najevo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
honosných	honosný	k2eAgNnPc2d1	honosné
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Symetrie	symetrie	k1gFnPc4	symetrie
patří	patřit	k5eAaImIp3nP	patřit
kněžstvu	kněžstvo	k1gNnSc3	kněžstvo
a	a	k8xC	a
souměrné	souměrný	k2eAgInPc4d1	souměrný
zámky	zámek	k1gInPc4	zámek
vyhasínající	vyhasínající	k2eAgFnSc3d1	vyhasínající
renesanci	renesance	k1gFnSc3	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
celá	celý	k2eAgNnPc4d1	celé
města	město	k1gNnPc4	město
nyní	nyní	k6eAd1	nyní
vypadaly	vypadat	k5eAaImAgInP	vypadat
zámky	zámek	k1gInPc1	zámek
vystavěné	vystavěný	k2eAgInPc1d1	vystavěný
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nespokojily	spokojit	k5eNaPmAgFnP	spokojit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
úlohou	úloha	k1gFnSc7	úloha
středobodu	středobod	k1gInSc2	středobod
celého	celý	k2eAgNnSc2d1	celé
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
také	také	k9	také
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
ve	v	k7c6	v
Versailles	Versailles	k1gFnSc6	Versailles
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
jako	jako	k9	jako
předloha	předloha	k1gFnSc1	předloha
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgInPc4d1	mnohý
působivé	působivý	k2eAgInPc4d1	působivý
příklady	příklad	k1gInPc4	příklad
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
paláci	palác	k1gInSc6	palác
Caserta	Caserta	k1gFnSc1	Caserta
<g/>
,	,	kIx,	,
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
zámku	zámek	k1gInSc6	zámek
Rastatt	Rastatt	k1gInSc1	Rastatt
nebo	nebo	k8xC	nebo
ruském	ruský	k2eAgInSc6d1	ruský
Petrodvorci	Petrodvorec	k1gInSc6	Petrodvorec
v	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavby	stavba	k1gFnPc1	stavba
se	se	k3xPyFc4	se
zdobily	zdobit	k5eAaImAgFnP	zdobit
mimořádným	mimořádný	k2eAgNnSc7d1	mimořádné
množstvím	množství	k1gNnSc7	množství
sloupů	sloup	k1gInPc2	sloup
<g/>
,	,	kIx,	,
pilířů	pilíř	k1gInPc2	pilíř
a	a	k8xC	a
soch	socha	k1gFnPc2	socha
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
baroko	baroko	k1gNnSc1	baroko
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Francii	Francie	k1gFnSc3	Francie
bylo	být	k5eAaImAgNnS	být
sice	sice	k8xC	sice
praktikováno	praktikovat	k5eAaImNgNnS	praktikovat
dosti	dosti	k6eAd1	dosti
přísně	přísně	k6eAd1	přísně
a	a	k8xC	a
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
se	se	k3xPyFc4	se
věcná	věcný	k2eAgFnSc1d1	věcná
<g/>
,	,	kIx,	,
klasicistní	klasicistní	k2eAgFnSc1d1	klasicistní
dynamika	dynamika	k1gFnSc1	dynamika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zámecká	zámecký	k2eAgFnSc1d1	zámecká
architektura	architektura	k1gFnSc1	architektura
v	v	k7c6	v
ostatní	ostatní	k2eAgFnSc6d1	ostatní
Evropě	Evropa	k1gFnSc6	Evropa
dosti	dosti	k6eAd1	dosti
bouřlivá	bouřlivý	k2eAgFnSc1d1	bouřlivá
a	a	k8xC	a
z	z	k7c2	z
mnohé	mnohý	k2eAgFnSc2d1	mnohá
fasády	fasáda	k1gFnSc2	fasáda
často	často	k6eAd1	často
prýští	prýštit	k5eAaImIp3nS	prýštit
nápady	nápad	k1gInPc4	nápad
a	a	k8xC	a
bohatým	bohatý	k2eAgInSc7d1	bohatý
detailem	detail	k1gInSc7	detail
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
budovy	budova	k1gFnSc2	budova
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
pokračování	pokračování	k1gNnPc4	pokračování
reprezentačních	reprezentační	k2eAgFnPc2d1	reprezentační
místností	místnost	k1gFnPc2	místnost
a	a	k8xC	a
slavnostních	slavnostní	k2eAgInPc2d1	slavnostní
sálů	sál	k1gInPc2	sál
<g/>
,	,	kIx,	,
rytmicky	rytmicky	k6eAd1	rytmicky
se	se	k3xPyFc4	se
stupňujících	stupňující	k2eAgFnPc2d1	stupňující
vedlejších	vedlejší	k2eAgFnPc2d1	vedlejší
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
postranních	postranní	k2eAgNnPc2d1	postranní
křídel	křídlo	k1gNnPc2	křídlo
k	k	k7c3	k
velkému	velký	k2eAgInSc3d1	velký
Corps	corps	k1gInSc4	corps
de	de	k?	de
logis	logis	k1gInSc1	logis
uprostřed	uprostřed	k7c2	uprostřed
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
zpravidla	zpravidla	k6eAd1	zpravidla
předcházelo	předcházet	k5eAaImAgNnS	předcházet
veliké	veliký	k2eAgNnSc4d1	veliké
čestný	čestný	k2eAgInSc4d1	čestný
dvůr	dvůr	k1gInSc4	dvůr
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
cour	cour	k?	cour
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
honneur	honneur	k1gMnSc1	honneur
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
půdorys	půdorys	k1gInSc1	půdorys
stavby	stavba	k1gFnSc2	stavba
byl	být	k5eAaImAgInS	být
ornamentem	ornament	k1gInSc7	ornament
a	a	k8xC	a
gigantické	gigantický	k2eAgInPc1d1	gigantický
zámecké	zámecký	k2eAgInPc1d1	zámecký
parky	park	k1gInPc1	park
prodlužovaly	prodlužovat	k5eAaImAgInP	prodlužovat
architekturu	architektura	k1gFnSc4	architektura
ven	ven	k6eAd1	ven
do	do	k7c2	do
krajiny	krajina	k1gFnSc2	krajina
<g/>
.	.	kIx.	.
</s>
<s>
Epocha	epocha	k1gFnSc1	epocha
skončila	skončit	k5eAaPmAgFnS	skončit
rokokem	rokoko	k1gNnSc7	rokoko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
posledním	poslední	k2eAgInSc7d1	poslední
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
hravým	hravý	k2eAgNnSc7d1	hravé
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
barokního	barokní	k2eAgNnSc2d1	barokní
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klasicismus	klasicismus	k1gInSc4	klasicismus
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
osvícenství	osvícenství	k1gNnSc2	osvícenství
se	se	k3xPyFc4	se
pozměnil	pozměnit	k5eAaPmAgInS	pozměnit
názor	názor	k1gInSc1	názor
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
temperamentní	temperamentní	k2eAgInSc1d1	temperamentní
<g/>
,	,	kIx,	,
živý	živý	k2eAgInSc1d1	živý
barokní	barokní	k2eAgInSc1d1	barokní
styl	styl	k1gInSc1	styl
byl	být	k5eAaImAgInS	být
nadále	nadále	k6eAd1	nadále
vnímán	vnímat	k5eAaImNgInS	vnímat
jako	jako	k8xS	jako
příliš	příliš	k6eAd1	příliš
nabubřelý	nabubřelý	k2eAgInSc1d1	nabubřelý
a	a	k8xC	a
přehnaný	přehnaný	k2eAgInSc1d1	přehnaný
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
již	již	k6eAd1	již
před	před	k7c7	před
dvěma	dva	k4xCgNnPc7	dva
stoletími	století	k1gNnPc7	století
se	se	k3xPyFc4	se
architekti	architekt	k1gMnPc1	architekt
začali	začít	k5eAaPmAgMnP	začít
znovu	znovu	k6eAd1	znovu
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
antiku	antika	k1gFnSc4	antika
a	a	k8xC	a
objevením	objevení	k1gNnSc7	objevení
Pompejí	Pompeje	k1gFnPc2	Pompeje
se	se	k3xPyFc4	se
antické	antický	k2eAgInPc1d1	antický
vlivy	vliv	k1gInPc1	vliv
opět	opět	k6eAd1	opět
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasicismus	klasicismus	k1gInSc1	klasicismus
dal	dát	k5eAaPmAgInS	dát
vzniknout	vzniknout	k5eAaPmF	vzniknout
novým	nový	k2eAgFnPc3d1	nová
stavbám	stavba	k1gFnPc3	stavba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
klidnější	klidný	k2eAgFnPc1d2	klidnější
a	a	k8xC	a
jasnější	jasný	k2eAgFnPc1d2	jasnější
linie	linie	k1gFnPc1	linie
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
např.	např.	kA	např.
zámek	zámek	k1gInSc1	zámek
Neuhardenberg	Neuhardenberg	k1gInSc1	Neuhardenberg
nebo	nebo	k8xC	nebo
Kurfiřtský	kurfiřtský	k2eAgInSc1d1	kurfiřtský
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Koblenci	Koblence	k1gFnSc6	Koblence
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgInPc1d1	Staré
barokní	barokní	k2eAgInPc1d1	barokní
zámky	zámek	k1gInPc1	zámek
byly	být	k5eAaImAgInP	být
přezdobeny	přezdobit	k5eAaPmNgInP	přezdobit
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
rokajové	rokajový	k2eAgFnPc4d1	rokajová
dekorace	dekorace	k1gFnPc4	dekorace
již	již	k6eAd1	již
platily	platit	k5eAaImAgInP	platit
za	za	k7c4	za
staromódní	staromódní	k2eAgNnSc4d1	staromódní
a	a	k8xC	a
překonané	překonaný	k2eAgNnSc4d1	překonané
a	a	k8xC	a
okázalá	okázalý	k2eAgFnSc1d1	okázalá
výzdoba	výzdoba	k1gFnSc1	výzdoba
byla	být	k5eAaImAgFnS	být
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc4	příklad
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
rezidenčního	rezidenční	k2eAgInSc2d1	rezidenční
zámku	zámek	k1gInSc2	zámek
Ludwigsburg	Ludwigsburg	k1gInSc4	Ludwigsburg
nebo	nebo	k8xC	nebo
zámku	zámek	k1gInSc3	zámek
Sondershausen	Sondershausna	k1gFnPc2	Sondershausna
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Fasády	fasáda	k1gFnSc2	fasáda
zámků	zámek	k1gInPc2	zámek
byly	být	k5eAaImAgFnP	být
vyzdobeny	vyzdobit	k5eAaPmNgFnP	vyzdobit
mohutnými	mohutný	k2eAgFnPc7d1	mohutná
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
chrámovými	chrámový	k2eAgFnPc7d1	chrámová
lomenicemi	lomenice	k1gFnPc7	lomenice
a	a	k8xC	a
od	od	k7c2	od
systému	systém	k1gInSc2	systém
stupňovitých	stupňovitý	k2eAgFnPc2d1	stupňovitá
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
pavilonů	pavilon	k1gInPc2	pavilon
se	se	k3xPyFc4	se
již	již	k6eAd1	již
často	často	k6eAd1	často
upouštělo	upouštět	k5eAaImAgNnS	upouštět
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
bujností	bujnost	k1gFnSc7	bujnost
baroka	baroko	k1gNnSc2	baroko
a	a	k8xC	a
rokoka	rokoko	k1gNnSc2	rokoko
zanikaly	zanikat	k5eAaImAgFnP	zanikat
také	také	k9	také
symetrické	symetrický	k2eAgFnPc1d1	symetrická
<g/>
,	,	kIx,	,
esteticky	esteticky	k6eAd1	esteticky
propracované	propracovaný	k2eAgFnSc2d1	propracovaná
zahrady	zahrada	k1gFnSc2	zahrada
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
ustupovaly	ustupovat	k5eAaImAgFnP	ustupovat
přirozeně	přirozeně	k6eAd1	přirozeně
působícím	působící	k2eAgInPc3d1	působící
parkům	park	k1gInPc3	park
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
příklad	příklad	k1gInSc1	příklad
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaImF	jmenovat
zámek	zámek	k1gInSc4	zámek
Wilhelmshöhe	Wilhelmshöh	k1gFnSc2	Wilhelmshöh
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
však	však	k9	však
také	také	k6eAd1	také
mísily	mísit	k5eAaImAgInP	mísit
zahradnické	zahradnický	k2eAgInPc1d1	zahradnický
styly	styl	k1gInPc1	styl
<g/>
,	,	kIx,	,
jako	jako	k9	jako
např.	např.	kA	např.
u	u	k7c2	u
schwetzingerského	schwetzingerský	k2eAgInSc2d1	schwetzingerský
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Historismus	historismus	k1gInSc4	historismus
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vznikal	vznikat	k5eAaImAgInS	vznikat
sloh	sloh	k1gInSc1	sloh
historismus	historismus	k1gInSc1	historismus
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
svých	svůj	k3xOyFgFnPc6	svůj
podobách	podoba	k1gFnPc6	podoba
a	a	k8xC	a
projevech	projev	k1gInPc6	projev
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
někdejších	někdejší	k2eAgInPc2d1	někdejší
slohů	sloh	k1gInPc2	sloh
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
vznikala	vznikat	k5eAaImAgNnP	vznikat
díla	dílo	k1gNnPc1	dílo
od	od	k7c2	od
novorománského	novorománský	k2eAgInSc2d1	novorománský
<g/>
,	,	kIx,	,
novogotického	novogotický	k2eAgInSc2d1	novogotický
a	a	k8xC	a
novorenesančního	novorenesanční	k2eAgInSc2d1	novorenesanční
až	až	k9	až
po	po	k7c4	po
stavby	stavba	k1gFnPc4	stavba
v	v	k7c6	v
novobarokním	novobarokní	k2eAgInSc6d1	novobarokní
slohu	sloh	k1gInSc6	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
<g/>
-li	i	k?	-li
dřívější	dřívější	k2eAgInPc4d1	dřívější
slohy	sloh	k1gInPc4	sloh
vždy	vždy	k6eAd1	vždy
utvářeny	utvářen	k2eAgInPc4d1	utvářen
místními	místní	k2eAgInPc7d1	místní
vlivy	vliv	k1gInPc7	vliv
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
odteď	odtedit	k5eAaPmRp2nS	odtedit
se	se	k3xPyFc4	se
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
interevropské	interevropský	k2eAgFnSc3d1	interevropský
umělecké	umělecký	k2eAgFnSc3d1	umělecká
chápaní	chápaný	k2eAgMnPc1d1	chápaný
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
volně	volně	k6eAd1	volně
užíváno	užívat	k5eAaImNgNnS	užívat
vzorů	vzor	k1gInPc2	vzor
cizích	cizí	k2eAgFnPc2d1	cizí
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
dávných	dávný	k2eAgFnPc2d1	dávná
epoch	epocha	k1gFnPc2	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
dokonce	dokonce	k9	dokonce
exotické	exotický	k2eAgInPc4d1	exotický
stavební	stavební	k2eAgInPc4d1	stavební
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Royal	Royal	k1gInSc1	Royal
Pavilion	Pavilion	k1gInSc1	Pavilion
v	v	k7c6	v
Brightonu	Brighton	k1gInSc6	Brighton
nebo	nebo	k8xC	nebo
Palácio	Palácio	k1gMnSc1	Palácio
da	da	k?	da
Pena	Pena	k1gMnSc1	Pena
v	v	k7c6	v
Sintře	Sintra	k1gFnSc6	Sintra
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
evropské	evropský	k2eAgFnPc1d1	Evropská
stavební	stavební	k2eAgFnPc1d1	stavební
slohy	sloha	k1gFnPc1	sloha
byly	být	k5eAaImAgFnP	být
nově	nově	k6eAd1	nově
vykládány	vykládán	k2eAgFnPc1d1	vykládána
a	a	k8xC	a
imitovány	imitován	k2eAgFnPc1d1	imitována
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
dokonce	dokonce	k9	dokonce
rozmanitými	rozmanitý	k2eAgInPc7d1	rozmanitý
způsoby	způsob	k1gInPc7	způsob
společně	společně	k6eAd1	společně
slučovány	slučovat	k5eAaImNgFnP	slučovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
se	se	k3xPyFc4	se
stavělo	stavět	k5eAaImAgNnS	stavět
v	v	k7c6	v
romantickém	romantický	k2eAgInSc6d1	romantický
charakteru	charakter	k1gInSc6	charakter
starých	starý	k2eAgFnPc2d1	stará
hradních	hradní	k2eAgFnPc2d1	hradní
zřícenin	zřícenina	k1gFnPc2	zřícenina
a	a	k8xC	a
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
přetvářeny	přetvářet	k5eAaImNgFnP	přetvářet
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
Stolzenfels	Stolzenfelsa	k1gFnPc2	Stolzenfelsa
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
se	se	k3xPyFc4	se
stavitelé	stavitel	k1gMnPc1	stavitel
navracejí	navracet	k5eAaBmIp3nP	navracet
k	k	k7c3	k
barokní	barokní	k2eAgFnSc3d1	barokní
renesanční	renesanční	k2eAgFnSc3d1	renesanční
formě	forma	k1gFnSc3	forma
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Zvěřínský	Zvěřínský	k2eAgInSc1d1	Zvěřínský
zámek	zámek	k1gInSc1	zámek
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
historicky	historicky	k6eAd1	historicky
vyhlížející	vyhlížející	k2eAgFnPc1d1	vyhlížející
novostavby	novostavba	k1gFnPc1	novostavba
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
stavby	stavba	k1gFnPc1	stavba
této	tento	k3xDgFnSc2	tento
epochy	epocha	k1gFnSc2	epocha
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bavorského	bavorský	k2eAgMnSc4d1	bavorský
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vybudováním	vybudování	k1gNnSc7	vybudování
hradu	hrad	k1gInSc2	hrad
Neuschwanstein	Neuschwanstein	k1gInSc4	Neuschwanstein
znovu	znovu	k6eAd1	znovu
proměnil	proměnit	k5eAaPmAgMnS	proměnit
v	v	k7c4	v
rytířský	rytířský	k2eAgInSc4d1	rytířský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc4	zámek
Herrenchiemsee	Herrenchiemse	k1gFnSc2	Herrenchiemse
v	v	k7c4	v
absolutistický	absolutistický	k2eAgInSc4d1	absolutistický
palác	palác	k1gInSc4	palác
a	a	k8xC	a
zámek	zámek	k1gInSc4	zámek
Linderhof	Linderhof	k1gInSc1	Linderhof
v	v	k7c4	v
letohrádek	letohrádek	k1gInSc4	letohrádek
francouzského	francouzský	k2eAgInSc2d1	francouzský
střihu	střih	k1gInSc2	střih
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
doba	doba	k1gFnSc1	doba
převelikých	převeliký	k2eAgInPc2d1	převeliký
zámků	zámek	k1gInPc2	zámek
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
zemí	zem	k1gFnPc2	zem
již	již	k6eAd1	již
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
zenitem	zenit	k1gInSc7	zenit
<g/>
,	,	kIx,	,
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
sílící	sílící	k2eAgFnSc2d1	sílící
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
vrstvy	vrstva	k1gFnSc2	vrstva
pozbývala	pozbývat	k5eAaImAgFnS	pozbývat
šlechta	šlechta	k1gFnSc1	šlechta
pomalu	pomalu	k6eAd1	pomalu
svou	svůj	k3xOyFgFnSc4	svůj
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
vliv	vliv	k1gInSc4	vliv
a	a	k8xC	a
velkolepé	velkolepý	k2eAgInPc1d1	velkolepý
stavební	stavební	k2eAgInPc1d1	stavební
projekty	projekt	k1gInPc1	projekt
byly	být	k5eAaImAgInP	být
čím	co	k3yRnSc7	co
dál	daleko	k6eAd2	daleko
řidší	řídký	k2eAgFnPc1d2	řidší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zámky	zámek	k1gInPc1	zámek
dnes	dnes	k6eAd1	dnes
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
pádem	pád	k1gInSc7	pád
hlavních	hlavní	k2eAgFnPc2d1	hlavní
evropských	evropský	k2eAgFnPc2d1	Evropská
monarchií	monarchie	k1gFnPc2	monarchie
byl	být	k5eAaImAgMnS	být
definitivně	definitivně	k6eAd1	definitivně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
období	období	k1gNnSc4	období
zámeckých	zámecký	k2eAgFnPc2d1	zámecká
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jen	jen	k9	jen
málo	málo	k4c1	málo
zámků	zámek	k1gInPc2	zámek
patří	patřit	k5eAaImIp3nS	patřit
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
svým	svůj	k3xOyFgMnPc3	svůj
původním	původní	k2eAgMnPc3d1	původní
majitelům	majitel	k1gMnPc3	majitel
a	a	k8xC	a
málokdy	málokdy	k6eAd1	málokdy
jsou	být	k5eAaImIp3nP	být
obydleny	obydlet	k5eAaPmNgFnP	obydlet
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnohé	mnohý	k2eAgFnPc4d1	mnohá
budovy	budova	k1gFnPc4	budova
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
nalezeno	nalezen	k2eAgNnSc1d1	Nalezeno
nové	nový	k2eAgNnSc1d1	nové
využití	využití	k1gNnSc1	využití
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
münsterském	münsterský	k2eAgInSc6d1	münsterský
zámku	zámek	k1gInSc6	zámek
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Leineschlossu	Leineschloss	k1gInSc6	Leineschloss
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Hannoveru	Hannover	k1gInSc6	Hannover
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
měly	mít	k5eAaImAgFnP	mít
to	ten	k3xDgNnSc4	ten
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
že	že	k8xS	že
přečkaly	přečkat	k5eAaPmAgFnP	přečkat
revoluce	revoluce	k1gFnPc1	revoluce
<g/>
,	,	kIx,	,
požáry	požár	k1gInPc1	požár
nebo	nebo	k8xC	nebo
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nP	sloužit
dnes	dnes	k6eAd1	dnes
zámky	zámek	k1gInPc1	zámek
hlavně	hlavně	k9	hlavně
jako	jako	k9	jako
muzea	muzeum	k1gNnPc1	muzeum
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
i	i	k9	i
jako	jako	k9	jako
kulturní	kulturní	k2eAgNnPc1d1	kulturní
střediska	středisko	k1gNnPc1	středisko
<g/>
,	,	kIx,	,
v	v	k7c6	v
hojné	hojný	k2eAgFnSc6d1	hojná
míře	míra	k1gFnSc6	míra
pod	pod	k7c7	pod
ochranou	ochrana	k1gFnSc7	ochrana
památkových	památkový	k2eAgInPc2d1	památkový
úřadů	úřad	k1gInPc2	úřad
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
nákladná	nákladný	k2eAgFnSc1d1	nákladná
záležitost	záležitost	k1gFnSc1	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
také	také	k9	také
hodnotnými	hodnotný	k2eAgMnPc7d1	hodnotný
svědky	svědek	k1gMnPc7	svědek
zašlých	zašlý	k2eAgFnPc2d1	zašlá
epoch	epocha	k1gFnPc2	epocha
<g/>
,	,	kIx,	,
též	též	k9	též
jsou	být	k5eAaImIp3nP	být
středem	střed	k1gInSc7	střed
zájmu	zájem	k1gInSc2	zájem
mnoha	mnoho	k4c2	mnoho
návštěvníků	návštěvník	k1gMnPc2	návštěvník
a	a	k8xC	a
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
i	i	k9	i
důležitým	důležitý	k2eAgInSc7d1	důležitý
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
subjektem	subjekt	k1gInSc7	subjekt
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
umělecko-historické	uměleckoistorický	k2eAgInPc1d1	umělecko-historický
památníky	památník	k1gInPc1	památník
jsou	být	k5eAaImIp3nP	být
naprosto	naprosto	k6eAd1	naprosto
nepostradatelné	postradatelný	k2eNgFnPc1d1	nepostradatelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zámek	zámek	k1gInSc1	zámek
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
městský	městský	k2eAgInSc1d1	městský
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
nebo	nebo	k8xC	nebo
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
někdy	někdy	k6eAd1	někdy
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
nebo	nebo	k8xC	nebo
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
objekt	objekt	k1gInSc4	objekt
znovu	znovu	k6eAd1	znovu
vystavět	vystavět	k5eAaPmF	vystavět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
stranu	strana	k1gFnSc4	strana
byly	být	k5eAaImAgInP	být
mnohé	mnohý	k2eAgInPc1d1	mnohý
zámky	zámek	k1gInPc1	zámek
středem	středem	k7c2	středem
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
později	pozdě	k6eAd2	pozdě
připadly	připadnout	k5eAaPmAgFnP	připadnout
městům	město	k1gNnPc3	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
patřily	patřit	k5eAaImAgFnP	patřit
z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
neoddělitelně	oddělitelně	k6eNd1	oddělitelně
k	k	k7c3	k
tamní	tamní	k2eAgFnSc3d1	tamní
oblasti	oblast	k1gFnSc3	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
dříve	dříve	k6eAd2	dříve
dělala	dělat	k5eAaImAgNnP	dělat
též	též	k9	též
politická	politický	k2eAgNnPc1d1	politické
rozhodnutí	rozhodnutí	k1gNnPc1	rozhodnutí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ovlivňovala	ovlivňovat	k5eAaImAgFnS	ovlivňovat
danou	daný	k2eAgFnSc4d1	daná
část	část	k1gFnSc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
též	též	k9	též
stoupencům	stoupenec	k1gMnPc3	stoupenec
nové	nový	k2eAgFnSc2d1	nová
výstavby	výstavba	k1gFnSc2	výstavba
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
již	již	k6eAd1	již
pozbylo	pozbýt	k5eAaPmAgNnS	pozbýt
svou	svůj	k3xOyFgFnSc4	svůj
historickou	historický	k2eAgFnSc4d1	historická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
nové	nový	k2eAgFnPc4d1	nová
výstavby	výstavba	k1gFnPc4	výstavba
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
rekonstrukcemi	rekonstrukce	k1gFnPc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
zde	zde	k6eAd1	zde
také	také	k9	také
panují	panovat	k5eAaImIp3nP	panovat
oprávněné	oprávněný	k2eAgFnPc1d1	oprávněná
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
takovou	takový	k3xDgFnSc4	takový
stavbu	stavba	k1gFnSc4	stavba
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
sotva	sotva	k6eAd1	sotva
našlo	najít	k5eAaPmAgNnS	najít
nějaké	nějaký	k3yIgNnSc4	nějaký
vlastní	vlastní	k2eAgNnSc4d1	vlastní
smysluplné	smysluplný	k2eAgNnSc4d1	smysluplné
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
DAVID	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
SOUKUP	Soukup	k1gMnSc1	Soukup
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Euromedia	Euromedium	k1gNnPc1	Euromedium
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
k.	k.	k?	k.
s.	s.	k?	s.
-	-	kIx~	-
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
424	[number]	k4	424
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
4226	[number]	k4	4226
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LUKÁŠOVÁ	Lukášová	k1gFnSc1	Lukášová
<g/>
,	,	kIx,	,
Eva	Eva	k1gFnSc1	Eva
<g/>
.	.	kIx.	.
</s>
<s>
Zámecké	zámecký	k2eAgInPc1d1	zámecký
interiéry	interiér	k1gInPc1	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Pohledy	pohled	k1gInPc4	pohled
do	do	k7c2	do
aristokratických	aristokratický	k2eAgNnPc2d1	aristokratické
sídel	sídlo	k1gNnPc2	sídlo
od	od	k7c2	od
časů	čas	k1gInPc2	čas
renesance	renesance	k1gFnSc2	renesance
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
352	[number]	k4	352
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7422	[number]	k4	7422
<g/>
-	-	kIx~	-
<g/>
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
</s>
</p>
<p>
<s>
Kaštel	kaštel	k1gInSc1	kaštel
</s>
</p>
<p>
<s>
Letohrádek	letohrádek	k1gInSc1	letohrádek
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zámek	zámek	k1gInSc1	zámek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
zámek	zámek	k1gInSc4	zámek
ve	v	k7c4	v
WikislovníkuMarie	WikislovníkuMarie	k1gFnPc4	WikislovníkuMarie
Pospíšilová	Pospíšilová	k1gFnSc1	Pospíšilová
<g/>
:	:	kIx,	:
Romantické	romantický	k2eAgInPc1d1	romantický
zámecké	zámecký	k2eAgInPc1d1	zámecký
interiéry	interiér	k1gInPc1	interiér
<g/>
,	,	kIx,	,
KSSPPOP	KSSPPOP	kA	KSSPPOP
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
