<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
umělecky	umělecky	k6eAd1	umělecky
bohatě	bohatě	k6eAd1	bohatě
zdobená	zdobený	k2eAgFnSc1d1	zdobená
<g/>
,	,	kIx,	,
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
šlechticům	šlechtic	k1gMnPc3	šlechtic
jako	jako	k8xC	jako
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
?	?	kIx.	?
</s>
