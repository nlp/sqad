<s>
Zámek	zámek	k1gInSc1	zámek
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
<g/>
,	,	kIx,	,
umělecky	umělecky	k6eAd1	umělecky
bohatě	bohatě	k6eAd1	bohatě
zdobená	zdobený	k2eAgFnSc1d1	zdobená
<g/>
,	,	kIx,	,
velkolepá	velkolepý	k2eAgFnSc1d1	velkolepá
budova	budova	k1gFnSc1	budova
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
šlechtickým	šlechtický	k2eAgInPc3d1	šlechtický
rodům	rod	k1gInPc3	rod
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
členům	člen	k1gMnPc3	člen
velkoburžoazie	velkoburžoazie	k1gFnSc2	velkoburžoazie
jako	jako	k8xS	jako
reprezentativní	reprezentativní	k2eAgNnSc1d1	reprezentativní
a	a	k8xC	a
pohodlné	pohodlný	k2eAgNnSc1d1	pohodlné
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
.	.	kIx.	.
</s>
