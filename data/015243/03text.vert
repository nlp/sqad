<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Pratt	Pratta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
CanadaZákladní	CanadaZákladný	k2eAgMnPc1d1
údaje	údaj	k1gInPc4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Datum	datum	k1gNnSc1
založení	založení	k1gNnSc2
</s>
<s>
1928	#num#	k4
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Longueuil	Longueuil	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
Adresa	adresa	k1gFnSc1
sídla	sídlo	k1gNnSc2
</s>
<s>
Longueuil	Longueuil	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
Charakteristika	charakteristika	k1gFnSc1
firmy	firma	k1gFnSc2
Oblast	oblast	k1gFnSc1
činnosti	činnost	k1gFnSc2
</s>
<s>
letecký	letecký	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
Produkty	produkt	k1gInPc1
</s>
<s>
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
Mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
United	United	k1gInSc1
Technologies	Technologiesa	k1gFnPc2
Corporation	Corporation	k1gInSc1
Majitel	majitel	k1gMnSc1
</s>
<s>
United	United	k1gMnSc1
Technologies	Technologies	k1gMnSc1
Corporation	Corporation	k1gInSc4
Identifikátory	identifikátor	k1gInPc4
Oficiální	oficiální	k2eAgInPc4d1
web	web	k1gInSc4
</s>
<s>
http://www.pwc.ca/	http://www.pwc.ca/	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitney	k1gInPc1
Canada	Canada	k1gFnSc1
(	(	kIx(
<g/>
zkratkou	zkratka	k1gFnSc7
PWC	PWC	kA
nebo	nebo	k8xC
P	P	kA
<g/>
&	&	k?
<g/>
WC	WC	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kanadský	kanadský	k2eAgMnSc1d1
výrobce	výrobce	k1gMnSc1
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
quebeckém	quebecký	k2eAgInSc6d1
Longueuil	Longueuil	k1gInSc1
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
Montréalu	Montréal	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
jeho	jeho	k3xOp3gMnSc1
americký	americký	k2eAgMnSc1d1
akcionář	akcionář	k1gMnSc1
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
je	být	k5eAaImIp3nS
divizí	divize	k1gFnSc7
společnosti	společnost	k1gFnSc2
United	United	k1gInSc1
Technologies	Technologies	k1gMnSc1
Corporation	Corporation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
koncernu	koncern	k1gInSc2
United	United	k1gMnSc1
Technologies	Technologies	k1gMnSc1
je	být	k5eAaImIp3nS
firma	firma	k1gFnSc1
Pratt	Prattum	k1gNnPc2
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
pověřená	pověřený	k2eAgFnSc1d1
vývojem	vývoj	k1gInSc7
a	a	k8xC
výrobou	výroba	k1gFnSc7
menších	malý	k2eAgInPc2d2
leteckých	letecký	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
se	se	k3xPyFc4
věnuje	věnovat	k5eAaImIp3nS,k5eAaPmIp3nS
motorům	motor	k1gInPc3
o	o	k7c6
větších	veliký	k2eAgInPc6d2
výkonech	výkon	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
Pratt	Prattum	k1gNnPc2
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
oblastech	oblast	k1gFnPc6
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
výroby	výroba	k1gFnSc2
i	i	k8xC
prodeje	prodej	k1gInSc2
výrobků	výrobek	k1gInPc2
na	na	k7c6
firmě	firma	k1gFnSc6
Pratt	Prattum	k1gNnPc2
&	&	k?
Whitney	Whitnea	k1gMnSc2
zcela	zcela	k6eAd1
nezávislá	závislý	k2eNgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
také	také	k9
samostatný	samostatný	k2eAgInSc1d1
výzkumný	výzkumný	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
P	P	kA
<g/>
&	&	k?
<g/>
WC	WC	kA
investovala	investovat	k5eAaBmAgFnS
do	do	k7c2
výzkumu	výzkum	k1gInSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
400	#num#	k4
milionů	milion	k4xCgInPc2
dolarů	dolar	k1gInPc2
a	a	k8xC
zaměstnávala	zaměstnávat	k5eAaImAgFnS
633	#num#	k4
inženýrů	inženýr	k1gMnPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
zařadila	zařadit	k5eAaPmAgFnS
na	na	k7c4
čtvrté	čtvrtý	k4xOgNnSc4
místo	místo	k1gNnSc4
mezi	mezi	k7c7
quebeckými	quebecký	k2eAgMnPc7d1
zaměstnavateli	zaměstnavatel	k1gMnPc7
této	tento	k3xDgFnSc6
profese	profese	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
měla	mít	k5eAaImAgFnS
firma	firma	k1gFnSc1
celkem	celkem	k6eAd1
9	#num#	k4
200	#num#	k4
zaměstnanců	zaměstnanec	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
5	#num#	k4
000	#num#	k4
v	v	k7c6
provincii	provincie	k1gFnSc6
Québec	Québec	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Společnost	společnost	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
jako	jako	k8xS,k8xC
servisní	servisní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
pro	pro	k7c4
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
značky	značka	k1gFnSc2
Pratt	Pratta	k1gFnPc2
&	&	k?
Whitney	Whitney	k1gInPc1
pod	pod	k7c7
názvem	název	k1gInSc7
Canadian	Canadian	k1gMnSc1
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Aircraft	Aircrafta	k1gFnPc2
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
Ltd	ltd	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
začala	začít	k5eAaPmAgFnS
montovat	montovat	k5eAaImF
motory	motor	k1gInPc4
řady	řada	k1gFnSc2
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Wasp	Wasp	k1gMnSc1
z	z	k7c2
dílů	díl	k1gInPc2
vyráběných	vyráběný	k2eAgInPc2d1
v	v	k7c6
USA	USA	kA
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
k	k	k7c3
ní	on	k3xPp3gFnSc6
byla	být	k5eAaImAgFnS
jejich	jejich	k3xOp3gMnPc3
výroba	výroba	k1gFnSc1
zcela	zcela	k6eAd1
převedena	převést	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
mateřská	mateřský	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
mohla	moct	k5eAaImAgFnS
soustředit	soustředit	k5eAaPmF
na	na	k7c4
vývoj	vývoj	k1gInSc4
leteckých	letecký	k2eAgInPc2d1
turbínových	turbínový	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
začala	začít	k5eAaPmAgFnS
firma	firma	k1gFnSc1
P	P	kA
<g/>
&	&	k?
<g/>
WC	WC	kA
vyvíjet	vyvíjet	k5eAaImF
vlastní	vlastní	k2eAgInPc4d1
turbínové	turbínový	k2eAgInPc4d1
letecké	letecký	k2eAgInPc4d1
motory	motor	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
jako	jako	k8xS,k8xC
první	první	k4xOgMnSc1
dosáhl	dosáhnout	k5eAaPmAgInS
úspěchu	úspěch	k1gInSc3
turbovrtulový	turbovrtulový	k2eAgInSc1d1
typ	typ	k1gInSc1
PT6	PT6	k1gFnSc2
vzniklý	vzniklý	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c7
lety	let	k1gInPc7
1962	#num#	k4
až	až	k9
1975	#num#	k4
společnost	společnost	k1gFnSc1
nesla	nést	k5eAaImAgFnS
název	název	k1gInSc4
United	United	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
of	of	k?
Canada	Canada	k1gFnSc1
(	(	kIx(
<g/>
UAC	UAC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1974	#num#	k4
<g/>
–	–	k?
<g/>
1975	#num#	k4
v	v	k7c6
továrně	továrna	k1gFnSc6
v	v	k7c4
Longueuil	Longueuil	k1gInSc4
proběhla	proběhnout	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejnásilnějších	násilný	k2eAgFnPc2d3
stávek	stávka	k1gFnPc2
v	v	k7c6
historii	historie	k1gFnSc6
Quebeku	Quebek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2017	#num#	k4
závod	závod	k1gInSc1
opustil	opustit	k5eAaPmAgInS
stotisící	stotisící	k2eAgInSc1d1
vyrobený	vyrobený	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
motor	motor	k1gInSc1
a	a	k8xC
výrobky	výrobek	k1gInPc1
firmy	firma	k1gFnSc2
slouží	sloužit	k5eAaImIp3nP
12	#num#	k4
300	#num#	k4
zákazníkům	zákazník	k1gMnPc3
ve	v	k7c4
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
zemích	zem	k1gFnPc6
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Produkty	produkt	k1gInPc1
</s>
<s>
Proudové	proudový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
Motor	motor	k1gInSc1
P	P	kA
<g/>
&	&	k?
<g/>
WC	WC	kA
JT15D	JT15D	k1gFnSc2
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gMnSc2
JT12	JT12	k1gMnSc2
(	(	kIx(
<g/>
vývoj	vývoj	k1gInSc1
dokončen	dokončit	k5eAaPmNgInS
u	u	k7c2
mateřské	mateřský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
JT15D	JT15D	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW300	PW300	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW500	PW500	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW600	PW600	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW800	PW800	k1gFnSc1
</s>
<s>
Turbovrtulové	turbovrtulový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
Řez	řez	k1gInSc1
motorem	motor	k1gInSc7
PT6	PT6	k1gFnSc2
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PT6	PT6	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW100	PW100	k1gFnSc1
</s>
<s>
Turbohřídelové	Turbohřídelový	k2eAgInPc1d1
motory	motor	k1gInPc1
</s>
<s>
P	P	kA
<g/>
&	&	k?
<g/>
WC	WC	kA
PW	PW	kA
<g/>
206	#num#	k4
<g/>
,	,	kIx,
jeden	jeden	k4xCgInSc1
z	z	k7c2
motorů	motor	k1gInPc2
rodiny	rodina	k1gFnSc2
PW	PW	kA
<g/>
200	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
vrtulníku	vrtulník	k1gInSc6
Eurocopter	Eurocoptra	k1gFnPc2
EC	EC	kA
135	#num#	k4
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PT	PT	kA
<g/>
6	#num#	k4
<g/>
B	B	kA
<g/>
/	/	kIx~
<g/>
C	C	kA
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PT6T	PT6T	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW100	PW100	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW200	PW200	k1gFnSc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW210	PW210	k1gFnSc1
</s>
<s>
Pomocné	pomocný	k2eAgInPc1d1
zdroje	zdroj	k1gInPc1
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
PW900	PW900	k1gFnSc1
–	–	k?
rodina	rodina	k1gFnSc1
auxiliary	auxiliara	k1gFnSc2
power	powra	k1gFnPc2
unit	unita	k1gFnPc2
<g/>
,	,	kIx,
užívaných	užívaný	k2eAgFnPc2d1
například	například	k6eAd1
u	u	k7c2
letounů	letoun	k1gInPc2
Boeing	boeing	k1gInSc1
747	#num#	k4
(	(	kIx(
<g/>
PW	PW	kA
<g/>
901	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Airbus	airbus	k1gInSc1
A380	A380	k1gFnSc2
(	(	kIx(
<g/>
PW	PW	kA
<g/>
980	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
MORAZAIN	MORAZAIN	kA
<g/>
,	,	kIx,
Jeanne	Jeann	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
in	in	k?
Top	topit	k5eAaImRp2nS
45	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plan	plan	k1gInSc1
-	-	kIx~
La	la	k1gNnSc1
revue	revue	k1gFnSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Ordre	ordre	k1gInSc1
des	des	k1gNnSc1
Ingénieurs	Ingénieurs	k1gInSc1
du	du	k?
Québec	Québec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
<g/>
/	/	kIx~
<g/>
únor	únor	k1gInSc1
2008	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
32	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
536	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Quelques	Quelques	k1gInSc1
faits	faits	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitney	k1gInPc1
Canada	Canada	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
LEYES	LEYES	kA
II	II	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
A.	A.	kA
<g/>
;	;	kIx,
FLEMING	FLEMING	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
A.	A.	kA
The	The	k1gFnSc2
History	Histor	k1gInPc4
of	of	k?
North	North	k1gInSc1
American	American	k1gMnSc1
Small	Small	k1gMnSc1
Gas	Gas	k1gMnSc1
Turbine	Turbin	k1gInSc5
Aircraft	Aircraftum	k1gNnPc2
Engines	Enginesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
:	:	kIx,
Smithsonian	Smithsonian	k1gInSc1
Institution	Institution	k1gInSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
56347	#num#	k4
<g/>
-	-	kIx~
<g/>
332	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
433	#num#	k4
<g/>
–	–	k?
<g/>
434	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PT6	PT6	k1gFnSc1
engine	enginout	k5eAaPmIp3nS
-	-	kIx~
The	The	k1gFnSc1
Legend	legenda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.pt6nation.com	www.pt6nation.com	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Déclenchement	Déclenchement	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
une	une	k?
grè	grè	k5eAaPmIp3nS
à	à	k?
la	la	k1gNnSc1
United	United	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Grè	Grè	k1gFnPc1
à	à	k?
la	la	k1gNnSc1
United	United	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
La	la	k1gNnSc7
grè	grè	k1gFnSc2
de	de	k?
la	la	k1gNnSc1
United	United	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
<g/>
↑	↑	k?
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitney	k1gInPc1
Canada	Canada	k1gFnSc1
Produces	Produces	k1gInSc1
100,000	100,000	k4
<g/>
th	th	k?
Engine	Engin	k1gInSc5
<g/>
:	:	kIx,
Demonstrates	Demonstrates	k1gMnSc1
Continued	Continued	k1gMnSc1
Focus	Focus	k1gMnSc1
on	on	k3xPp3gMnSc1
Driving	Driving	k1gInSc4
Innovation	Innovation	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pratt	Pratt	k1gMnSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
<g/>
,	,	kIx,
2017-05-02	2017-05-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
Kanady	Kanada	k1gFnSc2
</s>
<s>
Pratt	Pratt	k1gInSc1
&	&	k?
Whitney	Whitnea	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pratt	Pratta	k1gFnPc2
&	&	k?
Whitney	Whitnea	k1gFnSc2
Canada	Canada	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Cel	celet	k5eAaImRp2nS
aérospatial	aérospatial	k1gInSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
|	|	kIx~
Kanada	Kanada	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8062	#num#	k4
8789	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
97069839	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
122177714	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
97069839	#num#	k4
</s>
