<s>
Sirius	Sirius	k1gInSc1	Sirius
nebo	nebo	k8xC	nebo
též	též	k9	též
Psí	psí	k2eAgFnSc1d1	psí
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Aschere	Ascher	k1gInSc5	Ascher
nebo	nebo	k8xC	nebo
Canicula	Canicula	k1gFnSc1	Canicula
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvojhvězdu	dvojhvězda	k1gFnSc4	dvojhvězda
-	-	kIx~	-
Sirius	Sirius	k1gInSc4	Sirius
A	a	k9	a
a	a	k8xC	a
Sirius	Sirius	k1gMnSc1	Sirius
B	B	kA	B
<g/>
)	)	kIx)	)
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Velkého	velký	k2eAgMnSc4d1	velký
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
pes	pes	k1gMnSc1	pes
představoval	představovat	k5eAaImAgMnS	představovat
původně	původně	k6eAd1	původně
egyptského	egyptský	k2eAgMnSc4d1	egyptský
boha	bůh	k1gMnSc4	bůh
Anupa	Anup	k1gMnSc4	Anup
se	s	k7c7	s
šakalí	šakalí	k2eAgFnSc7d1	šakalí
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
hvězda	hvězda	k1gFnSc1	hvězda
zimního	zimní	k2eAgInSc2d1	zimní
šestiúhelníku	šestiúhelník	k1gInSc2	šestiúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Sirius	Sirius	k1gMnSc1	Sirius
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
seírios	seíriosa	k1gFnPc2	seíriosa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
blikotající	blikotající	k2eAgInSc1d1	blikotající
<g/>
,	,	kIx,	,
jiskřící	jiskřící	k2eAgInSc1d1	jiskřící
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
dostala	dostat	k5eAaPmAgFnS	dostat
hvězda	hvězda	k1gFnSc1	hvězda
od	od	k7c2	od
Řeků	Řek	k1gMnPc2	Řek
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
blikotavou	blikotavý	k2eAgFnSc4d1	blikotavá
záři	záře	k1gFnSc4	záře
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Sirius	Sirius	k1gInSc1	Sirius
nachází	nacházet	k5eAaImIp3nS	nacházet
blízko	blízko	k7c2	blízko
obzoru	obzor	k1gInSc2	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Neklid	neklid	k1gInSc1	neklid
vzduchu	vzduch	k1gInSc2	vzduch
proto	proto	k8xC	proto
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jeho	jeho	k3xOp3gFnSc4	jeho
scintilaci	scintilace	k1gFnSc4	scintilace
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
za	za	k7c2	za
mrazivých	mrazivý	k2eAgFnPc2d1	mrazivá
nocí	noc	k1gFnPc2	noc
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gMnSc1	Sirius
je	být	k5eAaImIp3nS	být
pozorovatelný	pozorovatelný	k2eAgMnSc1d1	pozorovatelný
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
obydleného	obydlený	k2eAgNnSc2d1	obydlené
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
-	-	kIx~	-
<g/>
m.	m.	k?	m.
Jeho	jeho	k3xOp3gFnSc4	jeho
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
8,6	[number]	k4	8,6
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
složkách	složka	k1gFnPc6	složka
trojhvězdy	trojhvězda	k1gFnSc2	trojhvězda
Alfa	alfa	k1gNnSc2	alfa
Centauri	Centaur	k1gFnSc2	Centaur
třetí	třetí	k4xOgFnSc1	třetí
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
hvězda	hvězda	k1gFnSc1	hvězda
viditelná	viditelný	k2eAgFnSc1d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
viditelná	viditelný	k2eAgFnSc1d1	viditelná
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
1,3	[number]	k4	1,3
vteřin	vteřina	k1gFnPc2	vteřina
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
patří	patřit	k5eAaImIp3nS	patřit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
k	k	k7c3	k
proudu	proud	k1gInSc3	proud
hvězd	hvězda	k1gFnPc2	hvězda
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
<g/>
.	.	kIx.	.
</s>
<s>
Pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
jako	jako	k8xS	jako
ony	onen	k3xDgMnPc4	onen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
jejich	jejich	k3xOp3gInSc4	jejich
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
o	o	k7c6	o
spektrální	spektrální	k2eAgFnSc6d1	spektrální
třídě	třída	k1gFnSc6	třída
A0	A0	k1gFnSc6	A0
nebo	nebo	k8xC	nebo
A1	A1	k1gFnSc6	A1
a	a	k8xC	a
třídě	třída	k1gFnSc6	třída
svítivosti	svítivost	k1gFnSc2	svítivost
Vm	Vm	k1gFnSc2	Vm
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
2,4	[number]	k4	2,4
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
hmotnost	hmotnost	k1gFnSc1	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
průměr	průměr	k1gInSc1	průměr
je	být	k5eAaImIp3nS	být
1,8	[number]	k4	1,8
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
svítivost	svítivost	k1gFnSc1	svítivost
je	být	k5eAaImIp3nS	být
26	[number]	k4	26
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
000	[number]	k4	000
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
příčina	příčina	k1gFnSc1	příčina
jeho	jeho	k3xOp3gFnSc2	jeho
velké	velký	k2eAgFnSc2d1	velká
jasnosti	jasnost	k1gFnSc2	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
přímočarého	přímočarý	k2eAgInSc2d1	přímočarý
pohybu	pohyb	k1gInSc2	pohyb
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc1	jeho
průvodce	průvodce	k1gMnSc1	průvodce
objeven	objevit	k5eAaPmNgMnS	objevit
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgInSc3d1	velký
rozdílu	rozdíl	k1gInSc3	rozdíl
ve	v	k7c6	v
svítivosti	svítivost	k1gFnSc6	svítivost
a	a	k8xC	a
malé	malý	k2eAgFnSc3d1	malá
úhlové	úhlový	k2eAgFnSc3d1	úhlová
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
maximálně	maximálně	k6eAd1	maximálně
11,4	[number]	k4	11,4
vteřin	vteřina	k1gFnPc2	vteřina
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1862	[number]	k4	1862
Alvenem	Alven	k1gMnSc7	Alven
Grahemem	Grahem	k1gInSc7	Grahem
Clarkem	Clarko	k1gNnSc7	Clarko
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
své	svůj	k3xOyFgFnSc3	svůj
jasnější	jasný	k2eAgFnSc3d2	jasnější
hvězdě	hvězda	k1gFnSc3	hvězda
Siriu	Sirium	k1gNnSc3	Sirium
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
průvodce	průvodce	k1gMnSc1	průvodce
Sirius	Sirius	k1gMnSc1	Sirius
B	B	kA	B
jen	jen	k6eAd1	jen
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
8,5	[number]	k4	8,5
<g/>
m	m	kA	m
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bílým	bílý	k2eAgMnSc7d1	bílý
trpaslíkem	trpaslík	k1gMnSc7	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
až	až	k9	až
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ačkoli	ačkoli	k8xS	ačkoli
Sirius	Sirius	k1gInSc1	Sirius
B	B	kA	B
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
těžký	těžký	k2eAgMnSc1d1	těžký
jako	jako	k8xS	jako
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
25	[number]	k4	25
000	[number]	k4	000
K	K	kA	K
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
velké	velký	k2eAgFnSc6d1	velká
hmotnosti	hmotnost	k1gFnSc6	hmotnost
malé	malý	k2eAgInPc4d1	malý
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
B	B	kA	B
má	mít	k5eAaImIp3nS	mít
0,94	[number]	k4	0,94
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
<g/>
,	,	kIx,	,
a	a	k8xC	a
obíhá	obíhat	k5eAaImIp3nS	obíhat
Sirius	Sirius	k1gInSc4	Sirius
A	a	k9	a
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
necelých	celý	k2eNgNnPc2d1	necelé
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
poloosa	poloosa	k1gFnSc1	poloosa
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
činí	činit	k5eAaImIp3nS	činit
4	[number]	k4	4
miliardy	miliarda	k4xCgFnSc2	miliarda
km	km	kA	km
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
excentricita	excentricita	k1gFnSc1	excentricita
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
0,58	[number]	k4	0,58
velice	velice	k6eAd1	velice
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sirius	Sirius	k1gMnSc1	Sirius
má	mít	k5eAaImIp3nS	mít
další	další	k2eAgMnSc1d1	další
průvodce	průvodce	k1gMnSc1	průvodce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyli	být	k5eNaImAgMnP	být
dosud	dosud	k6eAd1	dosud
žádní	žádný	k3yNgMnPc1	žádný
objeveni	objeven	k2eAgMnPc1d1	objeven
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
zmíněn	zmínit	k5eAaPmNgInS	zmínit
ve	v	k7c6	v
vědeckofantastické	vědeckofantastický	k2eAgFnSc6d1	vědeckofantastická
povídce	povídka	k1gFnSc6	povídka
Riziko	riziko	k1gNnSc4	riziko
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Isaaca	Isaacus	k1gMnSc2	Isaacus
Asimova	Asimův	k2eAgMnSc2d1	Asimův
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
také	také	k9	také
zmíněn	zmínit	k5eAaPmNgInS	zmínit
dosti	dosti	k6eAd1	dosti
podrobně	podrobně	k6eAd1	podrobně
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Toma	Tom	k1gMnSc2	Tom
Robbinse	Robbins	k1gMnSc2	Robbins
V	v	k7c6	v
žabím	žabí	k2eAgNnSc6d1	žabí
pyžamu	pyžamo	k1gNnSc6	pyžamo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Psí	psí	k2eAgFnSc1d1	psí
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
Sirius	Sirius	k1gInSc4	Sirius
zmíněn	zmíněn	k2eAgInSc4d1	zmíněn
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Forresta	Forrest	k1gMnSc2	Forrest
Cartera	Carter	k1gMnSc2	Carter
Škola	škola	k1gFnSc1	škola
malého	malý	k2eAgInSc2d1	malý
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hvězdy	hvězda	k1gFnSc2	hvězda
Sirius	Sirius	k1gInSc1	Sirius
je	být	k5eAaImIp3nS	být
pojmenován	pojmenován	k2eAgMnSc1d1	pojmenován
kmotr	kmotr	k1gMnSc1	kmotr
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
159	[number]	k4	159
<g/>
.	.	kIx.	.
čísle	číslo	k1gNnSc6	číslo
časopisu	časopis	k1gInSc2	časopis
Čtyřlístek	čtyřlístek	k1gInSc4	čtyřlístek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Psí	psí	k2eAgFnSc1d1	psí
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
psí	psí	k2eAgMnPc1d1	psí
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
ze	z	k7c2	z
Siria	Sirium	k1gNnSc2	Sirium
V	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Děti	dítě	k1gFnPc1	dítě
ze	z	k7c2	z
Síria	Sírius	k1gInSc2	Sírius
objevuje	objevovat	k5eAaImIp3nS	objevovat
dvojice	dvojice	k1gFnSc1	dvojice
dětí	dítě	k1gFnPc2	dítě
mimozemskou	mimozemský	k2eAgFnSc4d1	mimozemská
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
Zem	zem	k1gFnSc4	zem
dostala	dostat	k5eAaPmAgFnS	dostat
ze	z	k7c2	z
Siria	Sirium	k1gNnSc2	Sirium
</s>
