<s>
Sirius	Sirius	k1gInSc1	Sirius
nebo	nebo	k8xC	nebo
též	též	k9	též
Psí	psí	k2eAgFnSc1d1	psí
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
Aschere	Ascher	k1gInSc5	Ascher
nebo	nebo	k8xC	nebo
Canicula	Canicula	k1gFnSc1	Canicula
je	být	k5eAaImIp3nS	být
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvojhvězdu	dvojhvězda	k1gFnSc4	dvojhvězda
-	-	kIx~	-
Sirius	Sirius	k1gInSc4	Sirius
A	a	k9	a
a	a	k8xC	a
Sirius	Sirius	k1gMnSc1	Sirius
B	B	kA	B
<g/>
)	)	kIx)	)
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
a	a	k8xC	a
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Velkého	velký	k2eAgMnSc4d1	velký
psa	pes	k1gMnSc4	pes
<g/>
.	.	kIx.	.
</s>
