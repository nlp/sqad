<p>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgInSc2d1	francouzský
premiè	premiè	k?	premiè
–	–	k?	–
první	první	k4xOgFnSc1	první
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
"	"	kIx"	"
<g/>
první	první	k4xOgNnPc4	první
provedení	provedení	k1gNnPc4	provedení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc2d1	televizní
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
symfonií	symfonie	k1gFnPc2	symfonie
<g/>
,	,	kIx,	,
baletů	balet	k1gInPc2	balet
<g/>
,	,	kIx,	,
muzikálů	muzikál	k1gInPc2	muzikál
atd.	atd.	kA	atd.
Premiéry	premiéra	k1gFnSc2	premiéra
divadelních	divadelní	k2eAgMnPc2d1	divadelní
<g/>
,	,	kIx,	,
filmových	filmový	k2eAgInPc2d1	filmový
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgNnPc2d1	hudební
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
kulturních	kulturní	k2eAgNnPc2d1	kulturní
představení	představení	k1gNnPc2	představení
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaImF	stát
významnými	významný	k2eAgFnPc7d1	významná
a	a	k8xC	a
extravagantními	extravagantní	k2eAgFnPc7d1	extravagantní
událostmi	událost	k1gFnPc7	událost
<g/>
,	,	kIx,	,
přitahujícími	přitahující	k2eAgFnPc7d1	přitahující
značné	značný	k2eAgInPc4d1	značný
množství	množství	k1gNnSc4	množství
prominentů	prominent	k1gMnPc2	prominent
a	a	k8xC	a
pozornost	pozornost	k1gFnSc4	pozornost
masmédií	masmédium	k1gNnPc2	masmédium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
typické	typický	k2eAgFnPc4d1	typická
součásti	součást	k1gFnPc4	součást
premiér	premiéra	k1gFnPc2	premiéra
patří	patřit	k5eAaImIp3nS	patřit
promenáda	promenáda	k1gFnSc1	promenáda
na	na	k7c6	na
červeném	červený	k2eAgInSc6d1	červený
koberci	koberec	k1gInSc6	koberec
<g/>
.	.	kIx.	.
</s>
<s>
Filmových	filmový	k2eAgFnPc2d1	filmová
premiér	premiéra	k1gFnPc2	premiéra
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
účastní	účastnit	k5eAaImIp3nP	účastnit
hlavní	hlavní	k2eAgMnPc1d1	hlavní
herci	herec	k1gMnPc1	herec
či	či	k8xC	či
jiní	jiný	k2eAgMnPc1d1	jiný
významní	významný	k2eAgMnPc1d1	významný
členové	člen	k1gMnPc1	člen
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Světovou	světový	k2eAgFnSc7d1	světová
premiérou	premiéra	k1gFnSc7	premiéra
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
premiéra	premiéra	k1gFnSc1	premiéra
nějakého	nějaký	k3yIgNnSc2	nějaký
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
provedeních	provedení	k1gNnPc6	provedení
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
dočká	dočkat	k5eAaPmIp3nS	dočkat
dalších	další	k2eAgFnPc2d1	další
premiér	premiéra	k1gFnPc2	premiéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Premiere	Premier	k1gInSc5	Premier
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
premiéra	premiér	k1gMnSc2	premiér
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
