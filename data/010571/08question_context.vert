<s>
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1889	[number]	k4	1889
Braunau	Brauna	k2eAgFnSc4d1	Brauna
am	am	k?	am
Inn	Inn	k1gFnSc4	Inn
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
německý	německý	k2eAgMnSc1d1	německý
nacistický	nacistický	k2eAgMnSc1d1	nacistický
politik	politik	k1gMnSc1	politik
rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
diktátor	diktátor	k1gMnSc1	diktátor
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
takzvaný	takzvaný	k2eAgMnSc1d1	takzvaný
Vůdce	vůdce	k1gMnSc1	vůdce
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Führer	Führer	k1gMnSc1	Führer
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
odpovědný	odpovědný	k2eAgMnSc1d1	odpovědný
za	za	k7c4	za
zločiny	zločin	k1gInPc4	zločin
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
za	za	k7c4	za
vyvražďování	vyvražďování	k1gNnPc4	vyvražďování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
Romů	Rom	k1gMnPc2	Rom
a	a	k8xC	a
postižených	postižený	k1gMnPc2	postižený
<g/>
.	.	kIx.	.
</s>
<s>
Agresivní	agresivní	k2eAgFnSc7d1	agresivní
politikou	politika	k1gFnSc7	politika
zprvu	zprvu	k6eAd1	zprvu
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
územní	územní	k2eAgInPc4d1	územní
zisky	zisk	k1gInPc4	zisk
a	a	k8xC	a
ústupky	ústupek	k1gInPc4	ústupek
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
však	však	k9	však
napadením	napadení	k1gNnSc7	napadení
Polska	Polsko	k1gNnSc2	Polsko
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Německo	Německo	k1gNnSc1	Německo
nakonec	nakonec	k6eAd1	nakonec
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>

