#! /usr/bin/python3
# coding: utf-8
import os
import re
import sys
import pickle


def create_symlink(path_list, category, target_dir):
    for path in path_list:
        q_id = path.strip().split('/')[-1]
        if not os.path.exists('{0}/{1}/{2}'.format(target_dir, category, q_id)):
            sys.stderr.write('Creating dir {0}/{1}/{2}\n'.format(target_dir, category, q_id))
            os.makedirs('{0}/{1}/{2}'.format(target_dir, category, q_id))

        for _, _, files in os.walk(path):
            for file_name in files:
                if re.match('.*?(\.vert|\.txt)', file_name):
                    sys.stderr.write('Creating symlink {}/{}/{}/{}\n'.format(target_dir, category, q_id, file_name))
                    os.symlink('{0}/{1}'.format(path, file_name),
                               '{0}/{1}/{2}/{3}'.format(target_dir, category, q_id, file_name))


def extract_type(path):
    q_type, a_type = None, None
    with open("{0}/05metadata.txt".format(path), 'r') as metadata_f:
        for line in metadata_f:
            if re.match('<q_type>.*?</q_type>', line):
                q_type = re.match('<q_type>(.*?)</q_type>', line).group(1)
            if re.match('<a_type>.*?</a_type>', line):
                a_type = re.match('<a_type>(.*?)</a_type>', line).group(1)

    return q_type, a_type


def get_source(source_dir):
    records_list = []

    for root, dirs, _ in os.walk(source_dir):
        for dir_name in dirs:
            if re.match('\d{4}', dir_name):
                records_list.append(os.path.join(root, dir_name))

    return records_list


def make_distribution(dir_list, target_dir, distribution):
    qa_dict = {}
    pickle_parts = {'train': [], 'test': [], 'eval': []}

    if not os.path.exists('{0}/train'.format(target_dir)):
        sys.stderr.write('Creating dir {0}\n'.format('{0}/train'.format(target_dir)))
        os.makedirs('{0}/train'.format(target_dir))
    if not os.path.exists('{0}/test'.format(target_dir)):
        sys.stderr.write('Creating dir {0}\n'.format('{0}/test'.format(target_dir)))
        os.makedirs('{0}/test'.format(target_dir))
    if not os.path.exists('{0}/eval'.format(target_dir)):
        sys.stderr.write('Creating dir {0}\n'.format('{0}/eval'.format(target_dir)))
        os.makedirs('{0}/eval'.format(target_dir))

    for path in dir_list:
        qa_type = extract_type(path)
        try:
            qa_dict[qa_type].append(path)
        except KeyError:
            qa_dict[qa_type] = [path]

    for q_type, record_list in qa_dict.items():
        # distribution
        sys.stderr.write(f'splitting {q_type}\n')
        train_proportion = len(record_list) * int(distribution.strip().split('/')[0]) // 100
        test_proportion = len(record_list) * int(distribution.strip().split('/')[1]) // 100

        train_part = record_list[:train_proportion]
        test_part = record_list[train_proportion:train_proportion+test_proportion]
        eval_part = record_list[train_proportion+test_proportion:]

        pickle_parts['train'] += [x.split('/')[-1] for x in train_part]
        pickle_parts['test'] += [x.split('/')[-1] for x in test_part]
        pickle_parts['eval'] += [x.split('/')[-1] for x in eval_part]

        create_symlink(train_part, 'train', target_dir)
        create_symlink(test_part, 'test', target_dir)
        create_symlink(eval_part, 'eval', target_dir)

    with open(f'{target_dir}/parts.dump', 'wb') as fd:
        pickle.dump(pickle_parts, fd)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Split database to evenly distributed parts.')
    parser.add_argument('-s', '--source_dir', type=str,
                        required=True, help='Source dir with data.')
    parser.add_argument('-t', '--target_dir', type=str,
                        required=True, help='Target dir for splitted database')
    parser.add_argument('-d', '--distribution', type=str,
                        required=True, help='Percentage distribution of data between train/test/eval. '
                                            'Example: 50/40/10')
    args = parser.parse_args()

    dir_list = get_source(args.source_dir)
    make_distribution(dir_list, args.target_dir, args.distribution)


if __name__ == "__main__":
    main()

