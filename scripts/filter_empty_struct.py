#! /usr/bin/python3
# coding: utf-8
import sys
import re


def get_struct(data, struct_id):
    struct = []
    struct_start = False
    struct_end = False
    length = 0

    for line in data:
        line = line.strip()
        if re.match("^<{}>$".format(struct_id), line) or \
                re.match("^<{} .*?>$".format(struct_id), line):
            struct_start = True

        if re.match("^</{}>$".format(struct_id), line):
            struct_end = True
        if struct_start:
            if not (re.match("^<.*>$", line) or
                    re.match("^</.*>$", line)):
                length += 1
            struct.append(line)
        if struct_start and struct_end:
            yield struct, length
            length = 0
            struct = []
            struct_start = False
            struct_end = False


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Filter out empty structures')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-s', '--struct', type=str,
                        required=True,
                        help='Structure to be be filtered')

    args = parser.parse_args()

    for struct, length in get_struct(args.input, args.struct):
        if length > 0:
            args.output.write('{}\n'.format('\n'.join(struct)))


if __name__ == "__main__":
    main()
