#!/usr/bin/python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import re

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Unify yes/no answers.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    args = parser.parse_args()

    answer = ''
    for line in args.input:
        line = line.strip()
        if not re.match('^<', line):
            if len(line.split('\t')) > 1:
                if 'ano' in line.split('\t')[1]:
                    answer = 'yes'
                    break
                elif 'ne' in line.split('\t')[1]:
                    answer = 'no'
                    break
            else:
                if 'yes' in line:
                    answer = 'yes'
                    break
                elif 'no' in line:
                    answer = 'no'
                    break

    if answer:
        args.output.write(f'{answer}\n')
    else:
        sys.stderr.write('ERROR answer')

if __name__ == '__main__':
    main()
