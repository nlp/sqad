#!/bin/bash
if [ "$1" = '-v' ]; then
    uninorm -v
    unitok -v
    tag_sentences -v
    rpm -q majka majka-data majka_pipe 1>&2
    exit 0
fi

set -eo pipefail

THISDIR="/opt/majka_pipe"
UNINORM="uninorm --quotes --dashes"
UNITOK="unitok --trim 200 $THISDIR/unitok_czech_v2.py"
VERTCHAIN="vertchain doc 104858"
TAGGER="$THISDIR/desamb.utf8.majka.sh"
GENDERDIR="$THISDIR/czech_gender_lemma"
ADD_GENDER_LEMMA="$GENDERDIR/add_gender_lemma.py $GENDERDIR/cztenten12_clean.gender_list"
POSTPROCESS="$THISDIR/postprocess_v2.py"

$UNINORM | $VERTCHAIN "$TAGGER" | $ADD_GENDER_LEMMA | $POSTPROCESS