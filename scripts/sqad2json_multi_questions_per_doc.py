#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import re
import os
import sys
import json
import pickle
from vert2plain_modul import vert2plain
from urllib.parse import unquote
"""
example: a['data'][0]['paragraphs'][0]
{version: 0,
 data: [{title: 'Beyoncé'
        paragraphs: [{'qas': [{'question': 'When did Beyonce start becoming popular?',
                               'id': '56be85543aeaaa14008c9063',
                               'answers': [{'text': 'in the late 1990s', 'answer_start': 269}],
                               'is_impossible': False
                               }
                              ]
                      'context': 'Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/ bee-YON-say) ...'
                     }
                    ]
       ]
}
"""

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Create SQuAD json format from SQAD db')
    parser.add_argument('-o', '--output', type=str,
                        required=True,
                        help='Output json')
    parser.add_argument('-d', '--sqad_data_dir', type=str,
                        required=True,
                        help='SQAD data dir')
    parser.add_argument('-v', '--version', type=str,
                        required=True,
                        help='SQAD version')
    args = parser.parse_args()

    processed_docs = set()
    resutl_json = {'version': args.version,
                   'data': []}
    num = 0
    for root, dirs, files in os.walk(args.sqad_data_dir):
        for file_name in files:
            if file_name == '01question.vert':
                num += 1
                rid = root.strip().split('/')[-1]
                sys.stdout.write(f'Processing {rid} ({num})\n')
                with open(os.path.join(root, '04url.txt'), 'r') as f:
                    url = unquote(f.readline().strip())

                with open(os.path.join(root, '01question.vert'), 'r') as f:
                    question = vert2plain(f.readlines(), True, 0)

                with open(os.path.join(root, '03text.vert'), 'r') as f:
                    text_content = vert2plain(f.readlines(), True, 0)

                with open(os.path.join(root, '06answer.selection.vert'), 'r') as f:
                    answer_sentence = vert2plain(f.readlines(), True, 0)
                    try:
                        as_start = re.search(re.escape(answer_sentence), text_content).start()
                    except:
                        sys.stderr.write(f'ErrAS\t{rid}\n')
                        sys.stderr.write(f'{as_start}\n')
                        sys.stderr.write(f'{answer_sentence}\n')
                        sys.stderr.write(f'{text_content}\n')
                        sys.stderr.write(f'=================================\n')
                        continue

                if os.path.exists(os.path.join(root, '09answer_extraction.vert')):
                    with open(os.path.join(root, '09answer_extraction.vert'), 'r') as f:
                        answer_extraction = vert2plain(f.readlines(), True, 0)
                        try:
                            ae_start = re.search(re.escape(answer_extraction.lower()), text_content[as_start:].lower()).start()
                        except:
                            sys.stderr.write(f'ErrAE\t{rid}\n')
                            sys.stderr.write(f'{as_start}\n')
                            sys.stderr.write(f'{ae_start}\n')
                            sys.stderr.write(f'{answer_extraction}\n')
                            sys.stderr.write(f'{answer_sentence}\n')
                            sys.stderr.write(f'=================================\n')
                            continue
                else:
                    answer_extraction = answer_sentence
                    ae_start = re.search(re.escape(answer_extraction.lower()), text_content[as_start:].lower()).start()

                if url in processed_docs:
                    for doc in resutl_json['data']:
                        if url == doc['title']:
                            doc['paragraphs'][0]['qas'].append({'question': question,
                                                                'id': rid,
                                                                'answers': {'text': answer_extraction, 'answer_start': as_start + ae_start},
                                                                'is_impossible': False})
                else:
                    processed_docs.add(url)
                    resutl_json['data'].append({'title': url,
                                                'paragraphs': [{'qas': [{'question': question,
                                                                         'id': rid,
                                                                         'answers': {'text': answer_extraction, 'answer_start': as_start + ae_start},
                                                                         'is_impossible': False}],
                                                                'context': text_content
                                                                }
                                                              ]
                                               })
    with open(args.output, 'w') as out:
        json.dump(resutl_json, out)


if __name__ == '__main__':
    main()
