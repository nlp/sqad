#! /usr/bin/python3
# coding: utf-8
import sys
import os
import re
from urllib.parse import unquote

def get_text_words(file_data):
    text_words = []

    for line in file_data:
        if not re.match('^<.*?>$', line.strip()):
            spl = line.strip().split('\t')
            if len(spl) >= 3:
                # ordering must be word, lemma, tag in vertical
                word = spl[0]
                text_words.append(word.lower())

    return ' '.join(text_words)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Remove duplicate questions from database.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-d', '--dir', type=str,
                        required=True,
                        help='Directory with data')

    args = parser.parse_args()
    question_dict = {}

    for root, dirs, files in os.walk(args.dir):
        for file_name in files:
            if file_name == "01question.vert":
                with open(os.path.join(root, '04url.txt')) as uf:
                    url = unquote(uf.readline().strip())
                with open(os.path.join(root, file_name)) as f:
                    question_f = f.readlines()
                    try:
                        question_dict[(get_text_words(question_f), url)].append(root)
                    except KeyError:
                        question_dict[(get_text_words(question_f), url)] = [root]
    for key, value in question_dict.items():
        if len(value) > 1:
            print(f'{key}: {value}')


if __name__ == "__main__":
    main()
