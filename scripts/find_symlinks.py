#! /usr/bin/python3
# coding: utf-8
import sys
import os
import errno
import re
from urllib.parse import unquote
from pprint import pprint

dir_path = os.path.dirname(os.path.realpath(__file__))

def force_symlink(real_file, symlink_file):
    try:
        os.symlink(real_file, symlink_file)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(symlink_file)
            os.symlink(real_file, symlink_file)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Identify records that should have symlinks')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-s', '--start', type=int,
                        required=True,
                        help='Starting record id')
    parser.add_argument('-e', '--end', type=int,
                        required=True,
                        help='Ending record id')
    args = parser.parse_args()

    url2records = {}

    for number in range(args.start, args.end + 1):
        r_id = '{:06d}'.format(number)
        with open('{}/../data/{}/04url.txt'.format(dir_path, r_id), 'r') as urlf:
            url = [x.strip() for x in urlf.readlines()]

        if len(url) > 1 and url[1]:
            print([unquote(x) for x in url])
            sys.stderr.write('File not correct {}\n'.format(r_id))
            sys.exit()
        else:
            try:
                url2records[unquote(url[0])].append(r_id)
            except KeyError:
                url2records[unquote(url[0])] = [r_id]

    symlinks = []
    for url, record_ids in url2records.items():
        source_text_file = ''
        for rid in record_ids:
            if not os.path.islink('{}/../data/{}/03text.vert'.format(dir_path, rid)):
                # print('{}/../data/{}/03text.vert'.format(dir_path, rid))
                source_text_file = '{}'.format(rid)
                break

        symlinks.append((url, source_text_file, record_ids))

    # pprint(symlinks)
    for _, source_txt, same_records in symlinks:
        for sr in same_records:
            if sr != source_txt:
                # if not os.path.islink('{}/../data/{}/03text.vert'.format(dir_path, sr)):
                    # print(f'symlink {sr} -> {source_txt}')
                force_symlink('../{}/03text.vert'.format(source_txt), '{}/../data/{}/03text.vert'.format(dir_path, sr))


if __name__ == "__main__":
    main()
