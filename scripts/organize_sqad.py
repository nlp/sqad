#! /usr/bin/python3
# coding: utf-8
import sys
import os
import errno
import shutil


dir_path = os.path.dirname(os.path.realpath(__file__))

def force_symlink(real_file, symlink_file):
    try:
        os.symlink(real_file, symlink_file)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(symlink_file)
            os.symlink(real_file, symlink_file)


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Remove hols in SQAD and repair structure with symlinks '
                                                 'propper allign')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument('-s', '--start_id', type=int,
                        required=False, default=1,
                        help='Start ID')
    parser.add_argument('-e', '--end_id', type=int,
                        required=False, default=16099,
                        help='End ID')

    args = parser.parse_args()
    moved = {}

    for dir_id in range(args.start_id, args.end_id):
        if os.path.isdir('{}/../data/{:06d}'.format(dir_path, dir_id)):
            continue
        else:
            for next_pssible in range(dir_id, args.end_id):
                if os.path.isdir('{}/../data/{:06d}'.format(dir_path, next_pssible)):
                    shutil.move('{}/../data/{:06d}'.format(dir_path, next_pssible), '{}/../data/{:06d}'.format(dir_path, dir_id))
                    print('Moving: {} -> {}'.format('{}/../data/{:06d}'.format(dir_path, next_pssible),
                                                    '{}/../data/{:06d}'.format(dir_path, dir_id)))
                    try:
                        moved[next_pssible]
                        print('Two same keys')
                        sys.exit()
                    except KeyError:
                        moved[next_pssible] = dir_id
                    break

    for root, dirs, files in os.walk("{}/../data/".format(dir_path)):
        for file_name in files:
            if file_name == '03text.vert':
                if os.path.islink(os.path.join(root, file_name)):
                    try:
                        simlink_target = int(os.readlink(os.path.join(root, file_name)).strip().split('/')[-2])
                        moved_to = moved[simlink_target]
                        print(f'relinking {os.path.join(root, file_name)} -> {moved_to}')
                        force_symlink('../{:06d}/03text.vert'.format(moved_to), os.path.join(root, file_name))
                    except KeyError:
                        pass

if __name__ == "__main__":
    main()
