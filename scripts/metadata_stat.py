#! /usr/bin/python3
# coding: utf-8
import sys
import os
import re
from urllib.parse import unquote


def initialize_matrix(question_types, answer_types):
    matrix = {}

    for q_type in question_types:
        row = {}
        for a_type in answer_types:
            row.update({a_type: 0})
        matrix.update({q_type: row})
    return matrix


def count_usage(table, questions):
    for key, value in questions.items():
        q_type = key[0]
        a_type = key[1]
        table[q_type][a_type] += value


def pretty_print(table, question_types, answer_types):
    print("{:<24}".format(""), "".join("{:<15}".format(a_type) for a_type in answer_types))
    for q_type in question_types:
        print("{:<15}".format(q_type), end='')
        for a_type in answer_types:
            print("{:>15}".format(table[q_type][a_type]), end='')
        print()


def get_struct(data, struct_id):
    struct = []
    struct_start = False

    for line in data:
        if re.match("^<{}>$".format(struct_id), line.strip()) or \
                re.match("^<{} .*?>$".format(struct_id), line.strip()):
            struct_start = True
        if struct_start:
            struct.append(line.strip())
        if re.match("^</{}>$".format(struct_id), line.strip()):
            yield struct
            struct = []
            struct_start = False


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Statistics on SQAD database metadata')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-d', '--dir', type=str,
                        required=True,
                        help='Directory with data')

    args = parser.parse_args()

    q_type_dict = {}
    a_type_dict = {}
    q_a_type_dict = {}
    multi_doc_qa = 0
    single_doc_qa = 0
    total = 0
    urls_dict = {}
    struct_re = re.compile('^<[^>]*>$')
    all_doc_sent_lengths = [] # on each index is list of lengths of one document sentences
    all_q_sent_lengths = []

    for root, dirs, files in os.walk(args.dir):
        for file_name in files:
            if file_name == '05metadata.txt':
                total += 1
                with open(os.path.join(root, file_name), 'r') as f:
                    q_type = None
                    a_type = None

                    for line in f:
                        if re.match('<q_type>.*?</q_type>', line):
                            q_type = re.match('<q_type>(.*?)</q_type>', line).group(1)
                            try:
                                q_type_dict[q_type] += 1
                            except KeyError:
                                q_type_dict[q_type] = 1

                        elif re.match('<a_type>.*?</a_type>', line):
                            a_type = re.match('<a_type>(.*?)</a_type>', line).group(1)
                            try:
                                a_type_dict[a_type] += 1
                            except KeyError:
                                a_type_dict[a_type] = 1

                    try:
                        q_a_type_dict[(q_type, a_type)] += 1
                    except KeyError:
                        q_a_type_dict[(q_type, a_type)] = 1
            elif file_name == '01question.vert':
                with open(os.path.join(root, file_name), 'r') as f:
                    q_senteces = []
                    for sentence in get_struct(f, 's'):
                        sent_length = 0
                        for token in sentence:
                            if not struct_re.match(token):
                                sent_length += 1
                        q_senteces.append(sent_length)
                all_q_sent_lengths.append(q_senteces)

            elif file_name == '03text.vert':
                if os.path.islink(os.path.join(root, file_name)):
                    multi_doc_qa +=1
                else:
                    single_doc_qa += 1
                    # doc sente length
                    with open(os.path.join(root, file_name), 'r') as f:
                        doc_senteces = []
                        for sentence in get_struct(f, 's'):
                            sent_length = 0
                            for token in sentence:
                                if not struct_re.match(token):
                                    sent_length += 1
                            doc_senteces.append(sent_length)
                    all_doc_sent_lengths.append(doc_senteces)

            elif file_name == '04url.txt':
                with open(os.path.join(root, file_name), 'r') as f:
                    url = unquote(f.read().strip())
                    if urls_dict.get(url):
                        urls_dict[url] += 1
                    else:
                        urls_dict[url] = 1

    print('\n=========================================================\n')
    print('Total: {}'.format(total))

    print('\n=========================================================\n')
    print('Q-Type statistics:')
    for key, value in q_type_dict.items():
        print('{:<15}{:>10}{:>10.2f}%'.format(key, value, float(value * 100) / total))

    print('\n=========================================================\n')
    print('A-Type statistics:')
    for key, value in a_type_dict.items():
        print('{:<15}{:>10}{:>10.2f}%'.format(key, value, float(value * 100) / total))

    print('\n=========================================================\n')
    print('Matrix:')
    matrix = initialize_matrix(q_type_dict.keys(), a_type_dict.keys())
    count_usage(matrix, q_a_type_dict)
    pretty_print(matrix, q_type_dict.keys(), a_type_dict.keys())

    print('\n=========================================================\n')
    print('Number of wikipedia articles used in SQAD datbase: {}\n'.format(single_doc_qa))
    print('Number of docuemnts that sharing document: {}\n'.format(multi_doc_qa))
    question_per_doc = {}
    for _, value in urls_dict.items():
        if question_per_doc.get(value):
            question_per_doc[value] += 1
        else:
            question_per_doc[value] = 1
    print('\n=========================================================\n')
    print(f'num_of_questions\tnum_of_docuemnts')
    for num_of_questions, num_of_docuemnts in question_per_doc.items():
        print(f'{num_of_questions}\t{num_of_docuemnts}')

    print('\n=========================================================\n')
    print('Max sentece length: {}'.format(max([max(x) for x in all_doc_sent_lengths])))
    print('Min sentece length: {}'.format(min([min(x) for x in all_doc_sent_lengths])))
    print('Average sentece length: {}'.format(sum([sum(x)/len(x) for x in all_doc_sent_lengths]) / len(all_doc_sent_lengths)))

    print('\n=========================================================\n')
    print('Max question sentece length: {}'.format(max([max(x) for x in all_q_sent_lengths])))
    print('Min question sentece length: {}'.format(min([min(x) for x in all_q_sent_lengths])))
    print('Average question sentece length: {}'.format(sum([sum(x)/len(x) for x in all_q_sent_lengths]) / len(all_doc_sent_lengths)))
    print('\n=========================================================\n')

    print('Max document length: {}'.format(max([len(x) for x in all_doc_sent_lengths])))
    print('Min document length: {}'.format(min([len(x) for x in all_doc_sent_lengths])))
    print('Average docuemnt length: {}'.format(sum([len(x) for x in all_doc_sent_lengths]) / len([len(x) for x in all_doc_sent_lengths])))

if __name__ == "__main__":
    main()

