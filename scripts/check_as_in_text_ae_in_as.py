#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import os
import re

def get_text_words(file_data):
    text_words = []

    for line in file_data:
        if not re.match('^<.*?>$', line.strip()):
            spl = line.strip().split('\t')
            if len(spl) >= 3:
                # ordering must be word, lemma, tag in vertical
                word = spl[0]
                text_words.append(word.lower())

    return text_words


def contains(small, big):
    for i in range(len(big)-len(small)+1):
        for j in range(len(small)):
            if big[i+j] != small[j]:
                break
        else:
            return i, i+len(small)
    return False


def main():
    import argparse
    parser = argparse.ArgumentParser(description='TODO')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-d', '--dir', type=str,
                        required=True,
                        help='Directory with data')

    args = parser.parse_args()
    question_dict = {}

    for root, dirs, files in os.walk(args.dir):
        for file_name in files:
            if file_name == "03text.vert":
                sys.stderr.write(f'{root}\n')
                with open(os.path.join(root, '05metadata.txt')) as f:
                    metadata = f.read()
                with open(os.path.join(root, '06answer.selection.vert')) as f:
                    as_f = f.readlines()
                with open(os.path.join(root, '03text.vert')) as f:
                    text_f = f.readlines()

                if 'YES_NO' not in metadata:
                    with open(os.path.join(root, '09answer_extraction.vert')) as f:
                        ae_f = f.readlines()

                    ans_sel = get_text_words(as_f)
                    ans_ext = get_text_words(ae_f)
                    text = get_text_words(text_f)


                    if not contains(ans_sel, text):
                        print(f'Error AS: {root}')
                    if not contains(ans_ext, ans_sel):
                        print(f'Error AE: {root}')
                else:
                    ans_sel = get_text_words(as_f)
                    text = get_text_words(text_f)

                    if not contains(ans_sel, text):
                        print(f'Error AS: {root}')


if __name__ == "__main__":
    main()
