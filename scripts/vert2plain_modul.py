#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import re
import sys

SGML_TAG = r"""(?:<!-- .*? -->|<[!?/]?(?!\d)\w[-\.:\w]*(?:[^>'"]*(?:'[^']*'|"[^"]*"))*\s*/?\s*>)"""
SGML_TAG_RE = re.compile(SGML_TAG, re.UNICODE | re.VERBOSE | re.DOTALL)
SENT_TAG_RE = re.compile(r'^</?s[ >]')
GLUE_TAG = '<g/>'

    
def vert2plain(input_stream, no_sentences, max_line_width):
    output = ''
    glue = False
    line_width = 0
    for line in input_stream:
        line = line.strip()
        if SGML_TAG_RE.match(line):
            if no_sentences and SENT_TAG_RE.match(line):
                continue
            if line == GLUE_TAG:
                glue = True
                continue
            if line_width > 0:
                output += '\n'
                line_width = 0
            output += line + '\n'
        else:
            token = line.split('\t')[0]
            if line_width > 0 and not glue:
                if max_line_width and line_width + len(token) + 1 > max_line_width:
                    output += '\n'
                    line_width = 0
                else:
                    output += ' '
                    line_width+= 1
            output += token
            line_width+= len(token)
        glue = False
    return output


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Converts a (tagged) vertical to untagged plain text file. SGML tags are preserved.')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')
    parser.add_argument("-l", "--max-line-width", 
                        help="maximum line width (0 = no limit)", 
                        type=int, default=80, required=False)
    parser.add_argument("-s", "--no-sentences", 
                        help="strip sentence (<s>) tags", type=bool,
                        action="store_true", default=False, required=False)
    args = parser.parse_args()
    
    args.output.write(vert2plain(args.input, args.no_sentences, args.max_line_width))


if __name__ == '__main__':
    main()