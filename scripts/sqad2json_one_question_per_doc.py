#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import re
import os
import sys
import json
import pickle
import datetime
from vert2plain_modul import vert2plain
from urllib.parse import unquote
"""
example: a['data'][0]['paragraphs'][0]
{version: 0,
 data: [{title: 'Beyoncé'
        paragraphs: [{'qas': [{'question': 'When did Beyonce start becoming popular?',
                               'id': '56be85543aeaaa14008c9063',
                               'answers': [{'text': 'in the late 1990s', 'answer_start': 269}],
                               'is_impossible': False
                               }
                              ]
                      'context': 'Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/ bee-YON-say) ...'
                     }
                    ]
       ]
}
"""

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Create SQuAD json format from SQAD db')
    parser.add_argument('-o', '--output_dir', type=str,
                        required=True,
                        help='Output json')
    parser.add_argument('-d', '--sqad_data_dir', type=str,
                        required=True,
                        help='SQAD data dir')
    parser.add_argument('-v', '--version', type=str,
                        required=True,
                        help='SQAD version')
    parser.add_argument('-p', '--parts', type=str,
                        required=True,
                        help='Train, test, eval splitting')
    parser.add_argument('-m', '--merge_train_eval', action='store_true',
                        required=False, default=False,
                        help='Merge train and eval part to train')
    args = parser.parse_args()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%I-%S")
    train_no, test_no, eval_no = 0, 0, 0

    with open(args.parts, 'rb') as parts_f:
        parts = pickle.load(parts_f)

    train_json = {'version': args.version,
                  'data': []}
    test_json = {'version': args.version,
                 'data': []}
    eval_json = {'version': args.version,
                 'data': []}
    result_json = None

    num = 0
    for root, dirs, files in os.walk(args.sqad_data_dir):
        for file_name in files:
            if file_name == '01question.vert':
                num += 1
                rid = root.strip().split('/')[-1]
                sys.stdout.write(f'{rid} ({num})\r')

                if rid in parts['train']:
                    result_json = train_json
                    train_no += 1
                elif rid in parts['test']:
                    result_json = test_json
                    test_no += 1
                elif rid in parts['eval']:
                    if args.merge_train_eval:
                        result_json = train_json
                        train_no += 1
                    else:
                        result_json = eval_json
                        eval_no += 1
                else:
                    sys.stderr.write(f'ErrPart {rid} not bellonging to any part')
                    sys.exit()

                with open(os.path.join(root, '04url.txt'), 'r') as f:
                    url = unquote(f.readline().strip())

                with open(os.path.join(root, '01question.vert'), 'r') as f:
                    question = vert2plain(f.readlines(), True, 0)

                with open(os.path.join(root, '03text.vert'), 'r') as f:
                    text_content = vert2plain(f.readlines(), True, 0)

                with open(os.path.join(root, '06answer.selection.vert'), 'r') as f:
                    answer_sentence = vert2plain(f.readlines(), True, 0)
                    try:
                        as_start = re.search(re.escape(answer_sentence), text_content).start()
                    except:
                        sys.stderr.write(f'ErrAS\t{rid}\n')
                        sys.stderr.write(f'{as_start}\n')
                        sys.stderr.write(f'{answer_sentence}\n')
                        sys.stderr.write(f'{text_content}\n')
                        sys.stderr.write(f'=================================\n')
                        continue

                if os.path.exists(os.path.join(root, '09answer_extraction.vert')):
                    with open(os.path.join(root, '09answer_extraction.vert'), 'r') as f:
                        answer_extraction = vert2plain(f.readlines(), True, 0)
                        try:
                            ae_start = re.search(re.escape(answer_extraction.lower()), text_content[as_start:].lower()).start()
                        except:
                            sys.stderr.write(f'ErrAE\t{rid}\n')
                            sys.stderr.write(f'{as_start}\n')
                            sys.stderr.write(f'{ae_start}\n')
                            sys.stderr.write(f'{answer_extraction}\n')
                            sys.stderr.write(f'{answer_sentence}\n')
                            sys.stderr.write(f'=================================\n')
                            continue
                else:
                    answer_extraction = answer_sentence
                    ae_start = re.search(re.escape(answer_extraction.lower()), text_content[as_start:].lower()).start()

                result_json['data'].append({'title': url,
                                            'paragraphs': [{'qas': [{'question': question,
                                                                     'id': rid,
                                                                     'answers': {'text': answer_extraction, 'answer_start': as_start + ae_start},
                                                                     'is_impossible': False}],
                                                            'context': text_content
                                                            }
                                                          ]
                                           })

    with open(f'{args.output_dir}/{timestamp}_train.json', 'w') as t_out:
        json.dump(train_json, t_out)
    with open(f'{args.output_dir}/{timestamp}_test.json', 'w') as s_out:
        json.dump(test_json, s_out)
    if not args.merge_train_eval:
        with open(f'{args.output_dir}/{timestamp}_eval.json', 'w') as e_out:
            json.dump(eval_json, e_out)

    sys.stdout.write(f'train: {train_no}, test: {test_no}, eval: {eval_no}\n')

if __name__ == '__main__':
    main()
