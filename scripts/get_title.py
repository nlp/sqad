#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
from urllib.parse import unquote
import argparse


parser = argparse.ArgumentParser(description='Get text title form URL')
parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                    required=False, default=sys.stdin,
                    help='Input')
parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                    required=False, default=sys.stdout,
                    help='Output')

args = parser.parse_args()

un_url = unquote(args.input.read().strip())
title = ' '.join(un_url.split('/')[-1].split('_'))

args.output.write(title)
