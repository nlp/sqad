#!/usr/bin/env python3
# coding: utf-8
# Author: Marek Medved, marek.medved@sketchengine.eu, Lexical Computing CZ
import sys
import os
import os.path


def main():
    import argparse
    parser = argparse.ArgumentParser(description='SQAD database stats')
    parser.add_argument('-i', '--input', type=argparse.FileType('r'),
                        required=False, default=sys.stdin,
                        help='Input')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        required=False, default=sys.stdout,
                        help='Output')

    parser.add_argument('-d', '--dir', type=str,
                        required=True,
                        help='Directory with SQAD records')
    args = parser.parse_args()

    for root, dirs, files in os.walk(args.dir):
        for file_name in files:
            if file_name == '03text.vert':
                if not os.path.islink(file_name):
                    with open(os.path.join(root, file_name)) as text_f:
                        for line in text_f:


if __name__ == "__main__":
    main()

